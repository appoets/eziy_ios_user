//
//  RelativeDetailViewController.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 21/02/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import ObjectMapper
import GooglePlaces
//
//enum ProfileViewOption{
//    case personal = "Personal"
//    case medical = "Medical"
//    case lifestyle = "Lifestyle"
//}

@available(iOS 13.0, *)
class RelativeDetailViewController: UIViewController {
    
    @IBOutlet weak var submitBttn: UIButton!
    @IBOutlet weak var personalBtn : UIButton!
    @IBOutlet weak var medicalBtn : UIButton!
    @IBOutlet weak var lifestyleBtn : UIButton!
    
    @IBOutlet weak var alcoholYesBtn : UIButton!
    @IBOutlet weak var smokeYesBtn : UIButton!
//    @IBOutlet weak var smokeImg : UIImageView!
//    @IBOutlet weak var alcohoImg : UIImageView!

    @IBOutlet weak var personalView : UIView!
    @IBOutlet weak var medicalView : UIView!
    @IBOutlet weak var lifestyleView : UIView!
    
    @IBOutlet weak var segmentProfile : UISegmentedControl!

    @IBOutlet weak var profileImage : UIImageView!
    @IBOutlet weak var nameTxt : UITextField!
    @IBOutlet weak var contantNumTxt : UITextField!
    @IBOutlet weak var emailidTxt : UITextField!
    @IBOutlet weak var genderTxt : UITextField!
    @IBOutlet weak var dobTXT : UITextField!
    @IBOutlet weak var bloodgroupTxt : UITextField!
    @IBOutlet weak var maritalStateTxt : UITextField!
    @IBOutlet weak var heightTxt : UITextField!
    @IBOutlet weak var weightTxt : UITextField!
    @IBOutlet weak var emergencyTxt : UITextField!
    @IBOutlet weak var locationTxt : UITextField!
    
    
    @IBOutlet weak var allergiesTxt : UITextField!
    @IBOutlet weak var currentMedicnTxt : UITextField!
    @IBOutlet weak var pastMedicnTxt : UITextField!
    @IBOutlet weak var chronicTxt : UITextField!
    @IBOutlet weak var injuriesTxt : UITextField!
    @IBOutlet weak var surgeriesTxt : UITextField!
    
    @IBOutlet weak var alchoholLbl : UILabel!
    @IBOutlet weak var smokingLbl : UILabel!
    @IBOutlet weak var backView : UIView!

    
//    @IBOutlet weak var smokinghabitTxt : HoshiTextField!
//    @IBOutlet weak var alcoholTxt : HoshiTextField!
    @IBOutlet weak var activityTxt : UITextField!
    @IBOutlet weak var foodPreferenceTxt : UITextField!
    @IBOutlet weak var occupationTxt : UITextField!
    @IBOutlet weak var lastNameTxt: UITextField!
    
//    var value  : [ProfileViewOption]!
    var isSmoking : Bool = false
    var isAlchol : Bool = false
    var martialStatusArray = ["Single","Married","Others"]
    var genderArray = ["Male","Female","Other"]
    var dlat : Double?
    var dlon : Double?
    var dAddress = String()
    var updateProfile = UIImage()
    var googlePlacesHelper : GooglePlacesHelper?
    var allergies = [String]()
    var isNewRelative : Bool = true
    var userRelatives: RelativeManagementListEntity?
//    var googlePlace = googlePlacesHelper()
    private lazy var loader : UIView = {
        return createActivityIndicator(UIScreen.main.focusedView ?? self.view)
    }()
    
    let checkedImg = UIImage(systemName: "checkmark.circle.fill") //UIImage(named: "")
    let uncheckedImg = UIImage(systemName: "circle") //UIImage(named: "circle")

    override func viewDidLoad() {
        super.viewDidLoad()
        self.googlePlacesHelper = GooglePlacesHelper()
        self.initialLoad()
    }
    
    func initialLoad()  {
        self.backView.addTap {
            self.navigationController?.popViewController(animated: true)
        }
        if !isNewRelative{
            self.submitBttn.isHidden = true
        }else{
            self.submitBttn.isHidden = false
        }
        self.setupNavigation()
        self.setupAction()
        self.localize()
        if !isNewRelative{
        self.setValues()
        }
        submitBttn.layer.cornerRadius = submitBttn.frame.height/2
        IQKeyboardManager.shared.enable = true
        self.dobTXT.delegate = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(updateImage))
        self.profileImage.isUserInteractionEnabled = true
        self.profileImage.addGestureRecognizer(tap)
        self.allergiesTxt.delegate = self
        self.maritalStateTxt.delegate = self
        self.genderTxt.delegate = self
        self.locationTxt.delegate = self
        
        self.getTextfield(view: self.view).forEach { (text) in Common.setFontWithType(to: text, size: 14, type: .regular)}
//            Common.setFontWithType(to: (text.placeholder)!, size: 16, type: .meduim)
            
        [self.smokingLbl,self.alchoholLbl].forEach { (text) in Common.setFontWithType(to: text!, size: 16, type: .regular)}
       
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.profileImage.makeRoundedCorner()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.shared.enable = true
    }
    
    private func setValues(){
        //         let profile : ProfileModel = profileDetali
        self.nameTxt.text = "\(self.userRelatives?.name ?? "")"
        //               self.lastNameTxt.text = "\(self.userRelatives.last_name ?? "")"
        self.emailidTxt.text = "\(self.userRelatives?.email ?? "")"
        self.contantNumTxt.text = "\(self.userRelatives?.contact_number ?? "")"
        self.profileImage.setURLImage(self.userRelatives?.picture ?? "")
        self.genderTxt.text = "\(self.userRelatives?.gender ?? "")"
        self.bloodgroupTxt.text = "\(self.userRelatives?.blood_group ?? "")"
        self.dobTXT.text = "\(self.userRelatives?.dob ?? "")"
        //               self.locationTxt.text = "\(self.userRelatives?.address ?? "")"
        self.maritalStateTxt.text = self.userRelatives?.martial_status ?? ""
        self.weightTxt.text = self.userRelatives?.weight ?? ""
        self.heightTxt.text = self.userRelatives?.height ?? ""
        self.emergencyTxt.text = self.userRelatives?.emergency_contact ?? ""
        self.allergiesTxt.text = self.userRelatives?.allergies ?? ""
        self.currentMedicnTxt.text = self.userRelatives?.current_medications ?? ""
        self.pastMedicnTxt.text = self.userRelatives?.past_medications ?? ""
        self.chronicTxt.text = self.userRelatives?.chronic_disease ?? ""
        self.injuriesTxt.text = self.userRelatives?.injury ?? ""
        self.surgeriesTxt.text = self.userRelatives?.surgery ?? ""
        self.isSmoking = self.userRelatives?.smoking_habits == "YES" ? true : false
        self.isAlchol = self.userRelatives?.alcohol_consumption == "YES" ?  true : false
        self.activityTxt.text = self.userRelatives?.activity_level ?? ""
        self.foodPreferenceTxt.text = self.userRelatives?.food_preference ?? ""
        self.occupationTxt.text = self.userRelatives?.occupation ?? ""
        self.alcoholYesBtn.setImage(!isAlchol ? uncheckedImg : checkedImg, for: .normal)
        self.smokeYesBtn.setImage(!isSmoking ? uncheckedImg : checkedImg, for: .normal)
    }
       
      @objc private func updateValues(){
        guard  let name = self.nameTxt.text , !name.isEmpty  else {
            showToast(msg: Constants.string.enterName.localize())
            return
        }
    
//    guard  let lastName = self.lastNameTxt.text ,!lastName.isEmpty else {
//        showToast(msg: ErrorMessage.list.enterLastName.localize())
//        return
//    }

        guard  let phoneNumber = self.contantNumTxt.text,!phoneNumber.isEmpty else {
            showToast(msg: ErrorMessage.list.enterPhoneNumber.localize())
            return
        }
        guard  let email = self.emailidTxt.text,!email.isEmpty else {
            showToast(msg: ErrorMessage.list.enterEmail.localize())
            return
        }
        guard let gender = self.genderTxt.text ,!gender.isEmpty else {
            showToast(msg: ErrorMessage.list.enterGender.localize())
            return
        }
        guard let dob = self.dobTXT.text,!dob.isEmpty else {
            showToast(msg: ErrorMessage.list.enterDOB.localize())
            return
        }
        
        guard let bloodGroup = self.bloodgroupTxt.text,!bloodGroup.isEmpty else {
            showToast(msg: ErrorMessage.list.enterBloodGroup.localize())
            return
        }
        
        guard let marital_status = self.maritalStateTxt.text, !marital_status.isEmpty else {
            showToast(msg: ErrorMessage.list.enterMartialStatus.localize())
            return
        }
        
        guard let height = self.heightTxt.text, !height.isEmpty else {
            showToast(msg: ErrorMessage.list.enterHeight.localize())
            return
        }
        
        guard  let weight = self.weightTxt.text, !weight.isEmpty else {
            showToast(msg: ErrorMessage.list.enterWeight.localize())
            return
        }
        
        guard  let emergency_contact = self.emergencyTxt.text, !emergency_contact.isEmpty else {
            showToast(msg: ErrorMessage.list.enterEmergencyContact.localize())
            return
        }
        
        guard  let location = self.locationTxt.text else { //, !location.isEmpty
            showToast(msg: ErrorMessage.list.enterLocation.localize())
            return
        }
        guard  let allergies = self.allergiesTxt.text, !allergies.isEmpty else {
            showToast(msg: ErrorMessage.list.enterAllergies.localize())
            return
        }
           var imageData = [String:Data]()
          let imageValue = self.profileImage.image?.pngData() ?? Data()
           imageData.updateValue(imageValue, forKey: "picture")
        
//        let date = dateConvertor(dob, _input: .DMY, _output: .YMD)
        var updateDate = String()
          let dateFormatte = DateFormatter()
        dateFormatte.dateFormat = "dd-MM-yyyy"
        let date = dateFormatte.date(from: dob)
        dateFormatte.dateFormat = "MM/dd/yyyy"
        updateDate = dateFormatte.string(from: date ?? Date())
        
        
           
           var params = [String:Any]()
           params.updateValue(name, forKey: "name")
//           params.updateValue(lastName, forKey: "last_name")
           params.updateValue(phoneNumber, forKey: "contact_number")
           params.updateValue(email, forKey: "email")
           params.updateValue(gender, forKey: "gender")
           params.updateValue(updateDate, forKey: "dob")
           params.updateValue(bloodGroup, forKey: "blood_group")
           params.updateValue(marital_status, forKey: "martial_status")
           params.updateValue(height, forKey: "height")
           params.updateValue(weight, forKey: "weight")
           params.updateValue(emergency_contact, forKey: "emergency_contact")
           params.updateValue(location, forKey: "location")
           params.updateValue(allergies, forKey: "allergies")
           params.updateValue(currentMedicnTxt.text ?? "", forKey: "current_medications")
           params.updateValue(pastMedicnTxt.text ?? "", forKey: "past_medications")
           params.updateValue(chronicTxt.text ?? "", forKey: "chronic_diseases")
           params.updateValue(injuriesTxt.text ?? "", forKey: "injuries")
           params.updateValue(surgeriesTxt.text ?? "", forKey: "surgeries")
           params.updateValue(isSmoking ? "YES" : "NO", forKey: "smoking")
           params.updateValue(isAlchol ? "YES" : "NO", forKey: "alcohol")
           params.updateValue(activityTxt.text ?? "", forKey: "activity")
           params.updateValue(foodPreferenceTxt.text ?? "", forKey: "food")
           params.updateValue(occupationTxt.text ?? "", forKey: "occupation")
           params.updateValue(UserDefaultConfig.PatientID, forKey: "patient_id")
           print("Profile Update",params)
          if isNewRelative{
              self.doctorPresenter?.PostRelativeManagement(PostRelativeManagement: params, imageData: imageData)
              self.loader.isHidden = false
//        self.presenter?.HITAPI(api: Base.createRelative.rawValue, params: params, methodType: .POST, modelClass: CreateRelativeResponse.self, token: true)
//        self.presenter?.IMAGEPOST(api: Base.createRelative.rawValue, params: params, methodType: .POST, imgData: imageData, imgName: "profile_pic", modelClass: CreateRelativeResponse.self, token: true)
       }else{
        //   self.doctorPresenter?.PostRelativeManagement(PostRelativeManagement: params, imageData: imageData)
           self.loader.isHidden = false
//       let url = "\(Base.createRelative.rawValue)/\(self.userRelatives.id ?? 0)"
//           self.presenter?.IMAGEPOST(api: url, params: params, methodType: .POST, imgData: imageData, imgName: "profile_pic", modelClass: CreateRelativeResponse.self, token: true)
       }
          self.loader.isHidden = false
//
       }
    
    private func localize(){
        self.nameTxt.placeholder = Constants.string.enterName.localize()
        self.emailidTxt.placeholder = Constants.string.enterEmail.localize()
        self.contantNumTxt.placeholder = Constants.string.enterPhone.localize()
        self.genderTxt.placeholder = Constants.string.gender.localize()
        self.bloodgroupTxt.placeholder = Constants.string.bloodGroup.localize()
        self.dobTXT.placeholder = Constants.string.dob.localize()
        self.locationTxt.placeholder = Constants.string.location.localize()
        self.maritalStateTxt.placeholder = Constants.string.martialStatus.localize()
        self.weightTxt.placeholder = Constants.string.weight.localize()
        self.heightTxt.placeholder = Constants.string.height.localize()
        self.emergencyTxt.placeholder = Constants.string.emergencyContact.localize()
        self.allergiesTxt.placeholder = Constants.string.allergies.localize()
        self.currentMedicnTxt.placeholder = Constants.string.currentMedication.localize()
        self.pastMedicnTxt.placeholder = Constants.string.pastMedication.localize()
        self.chronicTxt.placeholder = Constants.string.chronicDiseases.localize()
        self.injuriesTxt.placeholder = Constants.string.injuries.localize()
        self.surgeriesTxt.placeholder = Constants.string.surgeries.localize()
        self.isSmoking = false
        self.isAlchol = false
        self.activityTxt.placeholder = Constants.string.activity.localize()
        self.foodPreferenceTxt.placeholder = Constants.string.foodPreference.localize()
        self.occupationTxt.placeholder = Constants.string.occupation.localize()
        self.alcoholYesBtn.setImage(!isAlchol ? uncheckedImg : checkedImg, for: .normal)
        self.smokeYesBtn.setImage(!isSmoking ? uncheckedImg : checkedImg, for: .normal)
    }
       
   }

    



@available(iOS 13.0, *)
extension RelativeDetailViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
          if textField == dobTXT{
              PickerManager.shared.showDatePicker(selectedDate: textField.text, completionHandler: { date in
                  textField.text = date
                  
              })
              return false
              
          }else if textField == maritalStateTxt {
              
            
              PickerManager.shared.showPicker(pickerData: martialStatusArray, selectedData: textField.text, completionHandler: { selectedData in
                  textField.text = selectedData
                
//                PickerManager = nil
              })
              return false
          }else if textField == self.locationTxt {
            self.googlePlacesHelper?.getGoogleAutoComplete(completion: { (place) in
                  self.dAddress = place.formattedAddress ?? ""
                textField.text   = place.formattedAddress ?? ""
                  self.dlat = place.coordinate.latitude
                  self.dlon = place.coordinate.longitude
              })
           
              return false
          }else if textField == self.allergiesTxt {
//            self.present(id: "AllergyViewController", animation: true)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AllergyViewController") as! AllergyViewController
            vc.modalPresentationStyle = .overFullScreen
            vc.onClickDone = { content in
                self.allergies = content
                print(self.allergies)
                textField.text = self.allergies.joined(separator: ",")
                vc.dismiss(animated: true, completion: nil)
                
                
            }
            vc.onClickCancel = { content in
                textField.text = content
                vc.dismiss(animated: true, completion: nil)
                
            }
            self.present(vc, animated: true, completion: nil)
            
            return false
          }else if textField == self.genderTxt {
            PickerManager.shared.showPicker(pickerData: self.genderArray, selectedData: textField.text, completionHandler: { selectedData in
                textField.text = selectedData
                
                
            })
            return false
          }
          return true
      }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       // Try to find next responder
       if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
          nextField.becomeFirstResponder()
       } else {
          // Not found, so remove keyboard.
          textField.resignFirstResponder()
       }
       // Do not add a line break
       return false
    }
}


// MARK:- Basha

@available(iOS 13.0, *)
extension RelativeDetailViewController{
    func setupNavigation(){
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = Constants.string.yourProfile.localize()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: Constants.string.Done.localize(), style: .plain, target: self, action: #selector(doneAction(sender:)))
      }
    
    @IBAction private func doneAction (sender : UIBarButtonItem){
        
        ///Functionality
        self.updateValues()
    }
    
    @IBAction func submitAction(_ sender: Any) {
        self.updateValues()
    }
    
    func setupAction(){
        
        self.segmentAction(sender: self.segmentProfile)
        self.segmentProfile.setTitle(Constants.string.personal.localize().uppercased(), forSegmentAt: 0)
        self.segmentProfile.setTitle(Constants.string.medical.localize().uppercased(), forSegmentAt: 1)
        self.segmentProfile.setTitle(Constants.string.lifestyle.localize().uppercased(), forSegmentAt: 2)
//        self.smokeYesBtn.tag = 101
        [self.alcoholYesBtn,self.smokeYesBtn].forEach { (btn) in

            if #available(iOS 14.0, *) {
                btn?.menu = UIMenu(title: btn?.tag == 101 ? Constants.string.alcholHabit.localize() : Constants.string.smokingHabit.localize(), options: .displayInline, children: [

                    UIAction(title: Constants.string.Yes.localize(), image: checkedImg, handler: { [self] (action) in

                        btn?.setImage(checkedImg, for: .normal)
                        if btn?.tag == 101 {
                            isAlchol = true
                        }else{
                           
                            isSmoking = true
                        }


                    }),

                    UIAction(title: Constants.string.No.localize(), image: uncheckedImg, handler: { [self] (action) in

                        btn?.setImage(uncheckedImg, for: .normal)
                        if btn?.tag == 101 {
                            isAlchol = false
                        }else{
                            isSmoking = false
                            
                        }

                    })
                ])
                
                btn?.showsMenuAsPrimaryAction = true
                
            } else {
                
                btn?.addTap {
                    showAlert(message: btn?.tag == 101 ? Constants.string.alcholHabit.localize() : Constants.string.smokingHabit.localize(), btnHandler: { [self] (value) in
                  
                        if value == 101 {
                            btn?.setImage(value == 1 ? checkedImg : uncheckedImg, for: .normal)
                            self.isSmoking = value == 1 ? true : false
                        }else{
                            btn?.setImage(value == 1 ? checkedImg : uncheckedImg, for: .normal)
                            self.isAlchol = value == 1 ? true : false
                        }
                        
                    }, fromView: self)
                    
                }

            }

                
        }

    }
    
    
    @objc private func updateImage(){
        self.showImage { (image) in
            self.updateProfile = image!
            self.profileImage.image = self.updateProfile
        }
    }
    
    
    
    @IBAction private func segmentAction (sender : UISegmentedControl){
        
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        sender.setTitleTextAttributes(titleTextAttributes, for: .highlighted)
        sender.setTitleTextAttributes(titleTextAttributes, for: .selected)
        
        switch sender.selectedSegmentIndex {
        case 0:
            self.segmentProfile.selectedSegmentTintColor = .AppBlueColor
            self.personalView.isHidden = false
            [ self.lifestyleView,self.medicalView].forEach { (view) in view?.isHidden = true}
            break

        case 1:
            self.segmentProfile.selectedSegmentTintColor = .AppBlueColor
            self.medicalView.isHidden = false
            [ self.lifestyleView,self.personalView].forEach { (view) in view?.isHidden = true}
            break

        case 2:
            self.segmentProfile.selectedSegmentTintColor = .AppBlueColor
            self.lifestyleView.isHidden = false
            [ self.medicalView,self.personalView].forEach { (view) in view?.isHidden = true}
        break
            
        default:
            break
        }
    }
    
    


}
@available(iOS 13.0, *)
extension RelativeDetailViewController : DoctorPresenterToDoctorViewProtocol{
    func PostRelativeManagement(PostRelativeManagement: PostRelativeManagementEntity) {
        self.loader.isHidden = true
        
        if PostRelativeManagement.title == "OK" {
            showToast(msg: "Upload Successfully")
            self.navigationController?.popViewController(animated: true)
        }
    }

    
    func showError(error: CustomError) {
        self.loader.isHidden = true
        showToast(msg: error.localizedDescription)
    }
    
    
}
//extension RelativeDetailViewController : PresenterOutputProtocol {
//    func showSuccess(api: String, dataArray: [Mappable]?, dataDict: Mappable?, modelClass: Any) {
//        switch String(describing: modelClass) {
//        case model.type.CreateRelativeResponse:
//                    let data = dataDict as? CreateRelativeResponse
//                    print(data!)
//                    self.loader.isHideInMainThread(true)
//                showToast(msg:Constants.string.profileUpdated.localize())
//
//                if isNewRelative{
//                    self.navigationController?.popViewController(animated: true)
//                }
//                break
//
//            default: break
//
//        }
//    }
//
//    func showError(error: CustomError) {
//        showToast(msg: error.localizedDescription)
//        self.loader.isHideInMainThread(true)
//    }
//
//
//}




@available(iOS 13.0, *)
extension RelativeDetailViewController{
    
    func getTextfield(view: UIView) -> [UITextField] {
    var results = [UITextField]()
    for subview in view.subviews as [UIView] {
        if let textField = subview as? UITextField {
            results += [textField]
        } else {
            results += getTextfield(view: subview)
        }
    }
    return results
}
}

