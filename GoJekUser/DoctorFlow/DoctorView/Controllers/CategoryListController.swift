//
//  CategoryListController.swift
//  Project
//
//  Created by Chan Basha on 23/04/20.
//  Copyright © 2020 css. All rights reserved.
//

import UIKit
import ObjectMapper
import IQKeyboardManagerSwift

class CategoryListController: UIViewController {
    
    @IBOutlet weak var categoryListCV: UICollectionView!
    @IBOutlet weak var doctorSearchBar: UISearchBar!
    @IBOutlet weak var categoriesTitleLabel: UILabel!
    @IBOutlet weak var backbtn: UIView!
    //      var category : [Category] = [Category]()
    var subcategory : [ResponseData] = [] {
        didSet {
            categoryListCV.reloadInMainThread()
        }
    }
   
    private lazy var loader  : UIView = {
        return createActivityIndicator(self.view)
    }()
    
     
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared.enable = true
        self.doctorSearchBar.delegate = self
        registerCell()
        setNav()
        if let layout = categoryListCV?.collectionViewLayout as? UICollectionViewFlowLayout{
        layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
            layout.sectionInset = UIEdgeInsets(top: 0, left: 7, bottom: 0, right: 0)
        let size = CGSize(width:categoryListCV.bounds.width/2, height: 175)
        layout.itemSize = size
        }
        self.doctorSearchBar.change(textFont: .systemFont(ofSize: 17))
//        getCatagoryList()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.categoriesTitleLabel.text = Constants.string.categories.localize()
        doctorPresenter?.getfinddoctor(id: (SendRequestInput.shared.mainServiceId ?? 0))
        
    }
    
    
    
    func setNav() {
        
//           self.navigationController?.isNavigationBarHidden = false
//        self.navigationItem.title = Constants.string.findDoctors.localize()
//           Common.setFont(to: self.navigationItem.title!, isTitle: true, size: 18)
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
//           self.navigationController?.navigationBar.isTranslucent = false
//           self.navigationController?.navigationBar.barTintColor = UIColor(named: "AppBlueColor")
        self.backbtn.addTap {
            self.popOrDismiss(animation: true)
        }
        self.doctorSearchBar.placeholder = Constants.string.searchDoctorPlaceHolder.localize()
       }
 
    private func registerCell(){
       
        
        categoryListCV.register(UINib(nibName: XIB.Names.CategoryCell, bundle: .main), forCellWithReuseIdentifier: XIB.Names.CategoryCell)
        
    
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        
        return .lightContent
    }

}

extension CategoryListController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
          return self.subcategory.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        

        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: XIB.Names.CategoryCell, for: indexPath) as! CategoryCell
        cell.labelCategoryName.text = subcategory[indexPath.row].service_subcategory_name
//        let url = URL(string:self.subcategory[indexPath.row].picture ?? "")
//                let data = try? Data(contentsOf: url!)
//        cell.categoryImg.image = UIImage(data: data!)
//        
//        let imageurl = self.subcategory[indexPath.row].picture ?? ""
//        print(imageurl)
//        cell.categoryImg.setURLImage(imageurl)
        
        if let url = URL( string:self.subcategory[indexPath.row].picture ?? "")
        {
            DispatchQueue.global().async {
              if let data = try? Data( contentsOf:url)
              {
                DispatchQueue.main.async {
                  cell.categoryImg.image = UIImage( data:data)
                }
              }
           }
        }

        return cell
        
    }
    
  
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
        let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
        let width:CGFloat = (categoryListCV.frame.size.width - space) / 2.0
        let height:CGFloat = (categoryListCV.frame.size.width - space) / 2.3
        return CGSize(width: width, height: height)
        
       }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = DoctorsListController.initVC(storyBoardName: .doctor, vc: DoctorsListController.self, viewConrollerID: "DoctorsListController")
        vc.catagoryID = self.subcategory[indexPath.row].id ?? 0
        self.push(from: self, ToViewContorller: vc)
    }


    
}


//Api calls
extension CategoryListController : DoctorPresenterToDoctorViewProtocol{
    func showSuccess(api: String, dataArray: [Mappable]?, dataDict: Mappable?, modelClass: Any) {
        switch String(describing: modelClass) {
            case model.type.CategoryList:
                self.loader.isHideInMainThread(true)
                let data = dataDict as? CategoryList
//                self.subcategory = data?.category ?? [Category]()
                self.categoryListCV.reloadData()
                break
           
            default: break
            
        }
    }
    
    func showError(error: CustomError) {
        
    }
   

    func getfinddoctor(SubCategoryEntity: SubCategoryEntity) {
        
        self.subcategory = SubCategoryEntity.responseData ?? []
    }
    
//    func getCatagoryList(){
//
////        self.presenter?.HITAPI(api: DoctoryAPI.catagoryList.rawValue, params: nil, methodType: .GET, modelClass: CategoryList.self, token: true)
//        self.loader.isHidden = false
//    }
    
//    func cancelAppointment(id : String){
//        self.presenter?.HITAPI(api: Base.cancelAppointment.rawValue, params: ["id" : id], methodType: .POST, modelClass: CommonModel.self, token: true)
//    }
}


extension CategoryListController : UISearchBarDelegate {
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        self.push(id: Storyboard.Ids.SearchViewController, animation: true)
        return false
    }
    
    
   func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
          print("end searching --> Close Keyboard")
          self.doctorSearchBar.endEditing(true)
      }
    
    
}


extension UISearchBar {

func change(textFont : UIFont?) {

    for view : UIView in (self.subviews[0]).subviews {

        if let textField = view as? UITextField {
            textField.font = textFont
        }
    }
} }
