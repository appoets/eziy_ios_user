//
//  VisitedDoctorsViewController.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 31/01/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import ObjectMapper

class VisitedDoctorsViewController: UIViewController {
    
    @IBOutlet weak var backbtn: UIImageView!
    @IBOutlet weak var visitedDoctorsTable: UITableView!
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var noDataLabel: UILabel!
    
   
    
    var visitedDoctors : [VisitorResponseData] = [VisitorResponseData]()
        
    var isUpdate : Bool = false
    lazy var loader : UIView = {
        return createActivityIndicator(self.view.window ?? self.view)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        9876543210
        let tap = UITapGestureRecognizer(target: self, action: #selector(tappedMe))
        backbtn.addGestureRecognizer(tap)
        backbtn.isUserInteractionEnabled = true
        visitedDoctorsTable.delegate = self
        visitedDoctorsTable.dataSource = self
        initialLoads()
    }
    @objc func tappedMe()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
//        self.navigationController?.navigationBar.isHidden = false
        self.doctorPresenter?.getvistiordoctor()

    }

    
    private func localize(){
        self.noDataLabel.text = Constants.string.noDataLabel.localize()
    }
    


}
//
////MARK: View Setups
extension VisitedDoctorsViewController {

    private func initialLoads() {
//        setupNavigationBar()
        registerCell()
        self.localize()
        self.noDataView.isHidden = false
        self.visitedDoctorsTable.isHidden = true
    }

    private func setupNavigationBar() {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = Constants.string.visitedDoctors.localize()
    }

    private func registerCell() {
        self.visitedDoctorsTable.tableFooterView = UIView()
        self.visitedDoctorsTable.register(UINib(nibName: XIB.Names.UpcomingTableviewCell, bundle: nil), forCellReuseIdentifier: XIB.Names.UpcomingTableviewCell)
        
    }
}

////MARK: Tabelview delegates and datasources
//
extension VisitedDoctorsViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.visitedDoctors.count
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.UpcomingTableviewCell, for: indexPath) as! UpcomingTableviewCell
    
        self.populateData(cell: cell, data: self.visitedDoctors[indexPath.row],index: indexPath.row)
        return cell
    }

    func populateData(cell : UpcomingTableviewCell , data : VisitorResponseData,index:Int){
        
        cell.labelDate.text = dateConvertor(data.scheduled_at ?? "", _input: .date_time, _output: .DM)
        cell.labelTime.text = dateConvertor(data.scheduled_at ?? "", _input: .date_time, _output: .N_hour)
        cell.labeldoctorName.text = "Dr.\(data.provider?.first_name ?? "") \(data.provider?.last_name ?? "")"
        cell.labelSubtitle.text = "\(data.doctor_profile?.doctor_profile_details?.location ?? "")"
        cell.selectionStyle = .none
        cell.makeVideoCallButton.tag = index
        cell.buttonCancel.isHidden = true
        cell.labelStatus.isHidden = false

        if data.status ?? "" == "CANCELLED"{
            cell.labelStatus.backgroundColor = UIColor.red.withAlphaComponent(0.2) //UIColor(named: "LightGreen") //
            cell.labelStatus.textColor = UIColor.red.withAlphaComponent(0.2)//UIColor(named: "ConfirmedGreenColor") //UIColor(named: "")
        }else{
            cell.labelStatus.backgroundColor = UIColor(named: "LightGreen") //UIColor.LightGreen
            cell.labelStatus.textColor = UIColor(named: "ConfirmedGreenColor") //UIColor.AppBlueColor
        }
        if data.status == "CHECKEDOUT"{
            cell.labelStatus.text = ("Consulted").uppercased()
        }else {
        cell.labelStatus.text = data.status ?? "".uppercased()
        }

    }

//
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = AppointmentDetailsViewController.initVC(storyBoardName: .doctor, vc: AppointmentDetailsViewController.self, viewConrollerID: Storyboard.Ids.AppointmentDetailsViewController)
        vc.firstname = "\(visitedDoctors.first?.provider?.first_name ?? "")\(visitedDoctors.first?.provider?.last_name ?? "")\(",")\(visitedDoctors.first?.doctor_profile?.doctor_profile_details?.qualification ?? "")"
        vc.location = "\(visitedDoctors.first?.doctor_profile?.doctor_profile_details?.location ?? "")"
        vc.sheduledate = "\(dateConvertor(visitedDoctors.first?.scheduled_at ?? "", _input: .date_time, _output: .DM))\(",")\(dateConvertor(visitedDoctors.first?.scheduled_at ?? "", _input: .date_time, _output: .N_hour))"
        vc.profileimage = visitedDoctors.first?.provider?.picture ?? ""
        vc.experiences = "\(visitedDoctors.first?.doctor_profile?.doctor_profile_details?.specialist ?? "")"
        vc.requestid = visitedDoctors.first?.id ?? 0
        vc.doctorprofileid = visitedDoctors.first?.doctor_profile?.doctor_profile_id ?? 0
        vc.providerid = visitedDoctors.first?.doctor_profile?.provider_id ?? 0
        self.push(from: self, ToViewContorller: vc)

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 100
    }
    

}

//Api calls
extension VisitedDoctorsViewController : DoctorPresenterToDoctorViewProtocol{
    
    func getvistiordoctor(visitor: VisitorEntity) {
        self.visitedDoctors = visitor.responseData ?? []
        if self.visitedDoctors.count != 0{
            self.noDataView.isHidden = true
            self.visitedDoctorsTable.isHidden = false
        }else{
            self.noDataView.isHidden = false
        }
       
        visitedDoctorsTable.reloadData()
    }
    

}

