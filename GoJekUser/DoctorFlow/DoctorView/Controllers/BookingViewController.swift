//
//  BookingViewController.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 21/02/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import CLWeeklyCalendarView
import ObjectMapper

@available(iOS 13.0, *)
class BookingViewController: UIViewController {
    
    @IBOutlet weak var purposeOfVisit: UILabel!
    @IBOutlet weak var followUpLabel: UILabel!
    @IBOutlet weak var consultationLabel: UILabel!
    @IBOutlet weak var daySlotLabel: UILabel!
    
    @IBOutlet weak var doctorImg : UIImageView!
    @IBOutlet weak var doctorNameLbl : UILabel!
    @IBOutlet weak var splstLbl : UILabel!
    @IBOutlet weak var addressLbl : UILabel!
    @IBOutlet weak var followupView : UIView!
    @IBOutlet weak var consultantView : UIView!
    @IBOutlet weak var followupImg : UIImageView!
    @IBOutlet weak var consultantImg : UIImageView!
    @IBOutlet weak var proceedBtn : UIButton!
    @IBOutlet weak var calendarView: CLWeeklyCalendarView!
    @IBOutlet weak var timeCollectionCV: UICollectionView!
    @IBOutlet weak var timeCV: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backbtn: UIView!
    var isFollowUp = false
//    var doctorProfile : DoctorListEntity?
//    var searchDoctor : Search_doctors = Search_doctors()
    var docProfile : DoctorListEntity?
    var MainDoctorDetail : DoctorDetailModel?
    var doctorDetail : ResponseDatasssss?
    var timeSlot : [Timing] = [Timing]()
    var bookingreq : BookingReq = BookingReq()
    var bookingmodel : BookingModel?
    var isFromSearch : Bool = false
    var isfromFavourite : Bool = false
    var selectedTime : String = ""
    var selectedTimeID : String = ""
//    var favouriteDoctor : Favourite_Doctors?
    var scheduleDate = Date()
    var scheduleDateString : String = String()
    var selectedDate : String = ""{
        didSet{
            self.selectedTime = ""
            self.gettimeSlot(day: selectedDate)
            self.timeCV.reloadData()
        }
    }
    var categoryId : Int = 0
    var timeArr = ["9:30","8:30","8:30","8:30","8:30","8:30","8:30","8:30"]
    var merdianArr = ["AM","PM","AM","PM","AM","PM","AM","PM"]
    var sessionList = [Constants.string.morning.localize(), Constants.string.evening.localize()]
    var morningSlot = ["7:30", "8:00", "9:00", "9:30", "10:30", "11:00"]  //[String]()
    var eveningSlot = ["5:00", "5:30", "6:30", "7:00", "7:30", "8:30", "9:30"] //[String]()
    var time = String()
    var selectedIndex:IndexPath!
    var selectedDay : String = String()
    
    var doctorID : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialLoad()
        self.backbtn.addTap {
            self.popOrDismiss(animation: true)
        }
        
        if doctorID != nil {
            self.doctorPresenter?.getDoctorsDetail(id: self.doctorID ?? 0)
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
        self.registerCell()
       
    }
    
    func initialLoad()  {
        self.setupNavigation()
        self.setupAction()
        self.populateData()
        self.setCalendarView()
        self.localize()
    }
    
    func setupNavigation(){
        self.followupImg.image = UIImage(named: "RadioON")
        self.consultantImg.image = UIImage(named: "ic_radio_empty")
        self.bookingreq.booking_for = "follow_up"
//        self.followupImg.changeTintColor(color: .AppBlueColor)
//        self.consultantImg.changeTintColor(color: .AppBlueColor)
        self.isFollowUp = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.doctorImg.makeRoundedCorner()
    }
    
    private func registerCell(){
        self.timeCV.delegate = self
        self.timeCV.dataSource = self
        self.timeCV.register((UINib(nibName: "TimeCell", bundle: nil)), forCellWithReuseIdentifier: "TimeCell")
        self.timeCollectionCV.delegate = self
        self.timeCollectionCV.dataSource = self
        self.timeCollectionCV.register((UINib(nibName: "TimeCell", bundle: nil)), forCellWithReuseIdentifier: "TimeCell")
        
    
    }
    
    func setupAction(){
//
      
        
        self.followupImg.addTap {
            self.followupImg.image = UIImage(named: "RadioON")
            self.consultantImg.image = UIImage(named: "RadioOFF")
//            self.followupImg.changeTintColor(color: UIColor.AppBlueColor)
//            self.consultantImg.changeTintColor(color: UIColor.AppBlueColor)
            self.bookingreq.booking_for = "follow_up"
            self.isFollowUp = true
        }

        
        self.followupView.addTap {
            self.followupImg.image = UIImage(named: "RadioON")
            self.consultantImg.image = UIImage(named: "RadioOFF")
//            self.followupImg.changeTintColor(color: UIColor.AppBlueColor)
//            self.consultantImg.changeTintColor(color: UIColor.AppBlueColor)
            self.bookingreq.booking_for = "follow_up"
            self.isFollowUp = true
        }
        
        self.consultantImg.addTap {
            self.followupImg.image = UIImage(named: "RadioOFF")
            self.consultantImg.image = UIImage(named: "RadioON")
//            self.followupImg.changeTintColor(color: UIColor.AppBlueColor)
//            self.consultantImg.changeTintColor(color: UIColor.AppBlueColor)
            self.bookingreq.booking_for = "consultation"
             self.isFollowUp = false
        }

        self.consultantView.addTap {
            self.followupImg.image = UIImage(named: "RadioOFF")
            self.consultantImg.image = UIImage(named: "RadioON")
//            self.followupImg.changeTintColor(color: UIColor.AppBlueColor)
//            self.consultantImg.changeTintColor(color: UIColor.AppBlueColor)
            self.bookingreq.booking_for = "consultation"
             self.isFollowUp = false
        }
        
        self.proceedBtn.addTap {
            if self.scheduleDate.interval(ofComponent: .day, fromDate: Date()) < 0{
                 showToast(msg: "Please select today or upcoming date only")
            }else{
//                let view = TimePickerAlert.getView
//                view.alertdelegate = self
//                view.schduleDate = self.scheduleDate
//                AlertBuilder().addView(fromVC: self , view).show()
                
                
                if self.selectedTime != ""{
                    self.bookingreq.scheduled_at = "\(self.scheduleDateString) \(self.selectedTime)"
                    self.bookingreq.service_id = "\(self.categoryId)"
                    let vc = PatientDetailViewController.initVC(storyBoardName: .doctor, vc: PatientDetailViewController.self, viewConrollerID: Storyboard.Ids.PatientDetailViewController)
                    vc.bookingreq = self.bookingreq
                    vc.categoryId = self.categoryId
                    vc.MainDoctorDetail = self.MainDoctorDetail
                    vc.isfromSearch = self.isFromSearch
                    vc.isFollowup = self.isFollowUp
                    
                    vc.schedlueDate = self.scheduleDateString
                    vc.schedlueTime = self.selectedTimeID
                    self.push(from: self, ToViewContorller: vc)
                }else{
                    showToast(msg: "Please selected Time")
                }
                
            }
            
        }
    }
    
    func setCalendarView() {
        self.calendarView.calendarAttributes = [
            CLCalendarBackgroundImageColor : UIColor(named: "LightBackgroundGrey") ?? .white,
            CLCalendarPastDayNumberTextColor : UIColor.AppBlueColor,
            CLCalendarFutureDayNumberTextColor : UIColor.AppBlueColor,
            CLCalendarCurrentDayNumberTextColor :  UIColor.AppBlueColor,
            CLCalendarCurrentDayNumberBackgroundColor : UIColor(named: "LightBackgroundGrey") ?? .white,
//            CLCalendarFont:UIFont(name:FontCustom.meduim.rawValue, size: 14) ?? UIFont(),

            CLCalendarSelectedDayNumberTextColor : UIColor.white,
            CLCalendarSelectedDayNumberBackgroundColor : UIColor.appPrimaryColor,
            CLCalendarSelectedCurrentDayNumberTextColor : UIColor.white,
            CLCalendarSelectedCurrentDayNumberBackgroundColor : UIColor.AppBlueColor,
            
            CLCalendarDayTitleTextColor :UIColor.AppBlueColor,
            CLCalendarWeekStartDay : 1]
        self.calendarView.delegate = self
        
        
    }
    func populateData(){
        let url = URL(string:docProfile?.responseData?.provider_service?.first?.picture ?? "")
        guard let url = url else {return}
                let data = try? Data(contentsOf: url)
        self.doctorImg.image = UIImage(data: data ?? Data())
       
        self.doctorNameLbl.text = "\(docProfile?.responseData?.provider_service?.first?.first_name ?? "")\(docProfile?.responseData?.provider_service?.first?.last_name ?? "")"
        
        
    }

    

    
    private func localize(){
        self.purposeOfVisit.text = Constants.string.purposeOfVisit.localize()
        self.followUpLabel.text = Constants.string.followUp.localize()
        self.consultationLabel.text = Constants.string.consultation.localize()
        self.proceedBtn.setTitle(Constants.string.proceed.localize(), for: .normal)
        self.daySlotLabel.text = Constants.string.daySlot.localize()
        
        
    }
    
}

@available(iOS 13.0, *)
extension BookingViewController : CLWeeklyCalendarViewDelegate{
    func dailyCalendarViewDidSelect(_ date: Date!) {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "MM/dd/yyyy"
        self.scheduleDate = date
        self.scheduleDateString = dateFormatterGet.string(from: date)
        
        
            let dateFormatterGet1 = DateFormatter()
        dateFormatterGet1.dateFormat = "EEEE"
            print("SelectedDate",dateFormatterGet1.string(from: date))
        self.selectedDate = dateFormatterGet1.string(from: date)
        
    }
}
@available(iOS 13.0, *)
extension BookingViewController : AlertDelegate{
    func selectedDate(date: String, month: String, year: String, dob: String, alertVC: UIViewController) {
            
    }
       func selectedTime(time: String, alertVC: UIViewController) {
        self.bookingreq.scheduled_at = "\(self.scheduleDateString) \(time)"
        self.bookingreq.service_id = "\(self.categoryId)"
        let vc = PatientDetailViewController.initVC(storyBoardName: .doctor, vc: PatientDetailViewController.self, viewConrollerID: Storyboard.Ids.PatientDetailViewController)
        vc.bookingreq = self.bookingreq
        vc.categoryId = self.categoryId
           vc.MainDoctorDetail = self.MainDoctorDetail
           vc.schedlueDate = self.scheduleDateString
           vc.schedlueTime = self.selectedTimeID
//        vc.searchDoctor = self.searchDoctor
        vc.docProfile = self.docProfile
        vc.isfromSearch = self.isFromSearch
        vc.isFollowup = self.isFollowUp
        self.push(from: self, ToViewContorller: vc)
    }
    
}



@available(iOS 13.0, *)
extension BookingViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
        {
        
            if collectionView == self.timeCV{
                return self.timeSlot.count
            }else{
            return self.timeSlot.count
            }
        
        
        }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.timeCV{
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "TimeCell", for: indexPath) as! TimeCell
        cell.timeLbl.text = self.timeSlot[indexPath.item].start_time ?? ""
        cell.meridianLbl.isHidden = true
            if self.selectedTime == (self.timeSlot[indexPath.item].start_time ?? ""){
                cell.timeLbl.textColor = .whiteColor
                cell.meridianLbl.textColor = .white
                cell.timeView.borderColor = .AppBlueColor
                cell.timeView.backgroundColor = .AppBlueColor
            }else{
                cell.timeView.borderColor = .gray
                cell.timeView.backgroundColor = .white
                cell.timeLbl.textColor = .AppBlueColor
            }
       // cell.meridianLbl.text = self.merdianArr[indexPath.item]
           
        return cell
        }else{
            let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "TimeCell", for: indexPath) as! TimeCell
            cell.timeLbl.text = self.timeSlot[indexPath.item].start_time ?? ""
            if self.selectedTime == (self.timeSlot[indexPath.item].start_time ?? ""){
                cell.timeLbl.textColor = .whiteColor
                cell.meridianLbl.textColor = .white
                cell.timeView.borderColor = .AppBlueColor
                cell.timeView.backgroundColor = .AppBlueColor
            }
            cell.meridianLbl.isHidden = true
           // cell.meridianLbl.text = self.merdianArr[indexPath.item]

            return cell
        }
        
    }
    
  
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

//        let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
        
        return CGSize(width: collectionView.frame.width/6 , height: 70)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell  = collectionView.cellForItem(at: indexPath) as! TimeCell
        cell.timeLbl.textColor = .whiteColor
        cell.meridianLbl.textColor = .white
        cell.timeView.borderColor = .AppBlueColor
        cell.timeView.backgroundColor = .AppBlueColor
        self.selectedTime = self.timeSlot[indexPath.item].start_time ?? ""
        self.selectedTimeID = "\(self.timeSlot[indexPath.item].id ?? 0)"
        self.timeCV.reloadData()
    }
    
    
}


extension UIImageView{
    
    func changeTintColor(color:UIColor?)  {
        
        self.image = self.image?.withRenderingMode(.alwaysTemplate)
        self.tintColor = color

    }
}


@available(iOS 13.0, *)
extension BookingViewController:UITableViewDelegate, UITableViewDataSource{
    func  numberOfSections(in tableView: UITableView) -> Int {
        return sessionList.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let tableCell = tableView.dequeueReusableCell(withIdentifier: "ScheduleAppointmentCell") as! ScheduleAppointmentCell
        tableCell.sessionLbl.text =  sessionList[section]
        return tableCell.contentView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let tableCell = tableView.dequeueReusableCell(withIdentifier: "ScheduleSlotCell", for: indexPath) as! ScheduleSlotCell
        tableCell.selectionStyle = .none
        tableCell.bookedIndexpath = selectedIndex

        tableCell.slotIndex = indexPath.section
        tableCell.delegate = self
         tableCell.isbooked = false
        if selectedIndex != nil{
            tableCell.isbooked = (selectedIndex.section == indexPath.section) ? true:false
        }
       
        
       // tableCell.collectionView.reloadData()
        tableCell.contentView.layoutIfNeeded()
      //  tableCell.collectionView.layoutIfNeeded()
        return tableCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if morningSlot.count == 0 && eveningSlot.count == 0{
            return tableView.frame.height
        }
        else
        {
            return UITableView.automaticDimension
        }
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
}

class NoResultFoundCell: UITableViewCell {
    @IBOutlet weak var noResultFoundLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


class ScheduleAppointmentCell: UITableViewCell {

    @IBOutlet weak var sessionLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        Common.setFont(to: sessionLbl, isTitle: true, size: 16)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}



protocol ScheduleSlotCellProtocol{
    func selectedSlot(slotTime:String, indexpath:IndexPath)
//    func selectedSlots(slotObj: DoctorSlotData, indexpath:IndexPath)
}

class ScheduleSlotCell: UITableViewCell {

    @IBOutlet weak var collectionViewHeightConstrains: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    var delegate: ScheduleSlotCellProtocol?
    var slotList = [String]()
    var slotPeriod = String()
    var slotIndex : Int = -1
    var isbooked:Bool = false
    var bookedIndexpath:IndexPath!
    var slotsList = [DoctorSlotData]()
    var slotlist : [ResponseDatasss] = [] {
        didSet {
           // collectionView.reloadInMainThread()
        }
    }
    var timeSlot : [Timing] = [Timing]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension ScheduleSlotCell:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return slotList.count
//        return slotsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.width/5)
        return  CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ScheduleTimeCollectionViewCell", for: indexPath) as! ScheduleTimeCollectionViewCell
        //
        cell.baseView.layer.borderColor = UIColor.gray.cgColor
        cell.baseView.layer.borderWidth = 1
        cell.timeLbl.text = self.slotList[indexPath.row]//String(date24.dropLast(2))
        cell.sessionLbl.text = slotPeriod
        collectionViewHeightConstrains.constant = collectionView.contentSize.height
        collectionView.layoutIfNeeded()
        cell.contentView.layoutIfNeeded()
        cell.baseView.layoutIfNeeded()
        cell.baseView.layer.cornerRadius =  cell.baseView.frame.width/2//makeRoundedCorner()
        if let index = self.bookedIndexpath,index.section == slotIndex,index.row == indexPath.row{
            cell.baseView.backgroundColor = UIColor.appPrimaryColor
            cell.timeLbl.textColor = UIColor.white
            cell.sessionLbl.textColor = UIColor.white
        }
        else
        {
            cell.baseView.backgroundColor = UIColor.white
            cell.timeLbl.textColor = UIColor.AppBlueColor
            cell.sessionLbl.textColor = UIColor.AppBlueColor

        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let section = (slotPeriod == "am") ? 0: 1
        let bookedIndexpath =  IndexPath(row: indexPath.row, section: section)
       
        delegate?.selectedSlot(slotTime: "\(slotList[indexPath.row]) \(slotPeriod.uppercased())", indexpath: bookedIndexpath)
        
    }
}


struct DoctorSlotData: Mappable {
    
    var id: Int?
    var start_time: String?
    var end_time: String?
   
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        start_time <- map["start_time"]
        end_time <- map["end_time"]
    }
}


class ScheduleTimeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var sessionLbl: UILabel!
    @IBOutlet weak var baseView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        Common.setFont(to: sessionLbl, isTitle: false, size: 13)
        Common.setFont(to: timeLbl, isTitle: false, size: 13)
    }
}


@available(iOS 13.0, *)
extension BookingViewController:ScheduleSlotCellProtocol{
    

    func selectedSlot(slotTime: String,  indexpath: IndexPath) {
        print("slotTime:",slotTime)
        self.time = slotTime
        self.selectedIndex = indexpath
        self.tableView.reloadData()
    }
    
    
}
extension BookingViewController : DoctorPresenterToDoctorViewProtocol{
    
    func getDoctordeatil(doctor: DoctorDetailModel) {
        guard let data = doctor.responseData?.first else {return}
        self.doctorDetail = data
        self.gettimeSlot(day: selectedDate)
        self.timeCV.reloadData()
        self.MainDoctorDetail = doctor
        let url = data.provider?.picture ?? ""
        guard let url = URL(string: url) else {return}
        let dataImg = try? Data(contentsOf: url)
        self.doctorImg.image = UIImage(data: dataImg ?? Data())
       
        self.doctorNameLbl.text = "\(data.provider?.first_name ?? "")"
        self.splstLbl.text = "\(data.service?.service_subcategory_name ?? "")"
        self.addressLbl.text = "\(data.location ?? "")"
        
        
    }
    
    func gettimeSlot(day : String){
        timeSlot.removeAll()
        guard let profile = self.doctorDetail else {
            print("FailedNil")
            return}
        
        var timeForDay = profile.time_slot?.filter({$0.working_day == day}) ?? [Time_slot]()
        
        for i in timeForDay{
            timeSlot.append(i.timing?.first ?? Timing())
        }
        print("timeSlottimeSlottimeSlot",timeSlot.count)
    }
}
