//
//  AppointmentDetailViewController.swift
//  GoJekUser
//
//  Created by Santhosh on 04/05/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class AppointmentDetailViewController: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var specLabel: UILabel!
    @IBOutlet weak var locLabel: UILabel!
    @IBOutlet weak var quaLbl: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var cancelAppointmentButton: UIButton!
    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var statusLblView: UIView!
    @IBOutlet weak var meetdoctorButton: UIButton!
    @IBOutlet weak var listTableView: UITableView!
    var appointmnetDteail : Datass?
    var ratingView: RatingView?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let name = (appointmnetDteail?.provider?.first_name ?? "") + " " + (appointmnetDteail?.provider?.last_name ?? "")
        doctorNameLabel.text = name
        specLabel.text = appointmnetDteail?.doctor_profile?.doctor_profile_details?.specializations ?? ""
        quaLbl.text = appointmnetDteail?.doctor_profile?.doctor_profile_details?.qualification ?? ""
        locLabel.text = appointmnetDteail?.doctor_profile?.doctor_profile_details?.location ?? ""
        if (appointmnetDteail?.status ?? "") == "WAITING"{
            self.statusLblView.isHidden = true
        }else{
            self.statusLblView.isHidden = false
            self.statusLbl.text = (appointmnetDteail?.status ?? "") == "CANCELLED" ? "Meeting has cancelled" : "Meeting has completed"
        }
        self.profileImg.layer.cornerRadius = self.profileImg.frame.width / 2
        listTableView.register(UINib(nibName: "AppointmentDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "AppointmentDetailTableViewCell")
        profileImg.sd_setImage(with: URL(string: appointmnetDteail?.provider?.picture ?? ""), placeholderImage:#imageLiteral(resourceName: "ImagePlaceHolder"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
            // Perform operation.
            if (error != nil) {
                // Failed to load image
                self.profileImg.image = #imageLiteral(resourceName: "ImagePlaceHolder")
            } else {
                // Successful in loading image
                self.profileImg.image = image
            }
        })
        listTableView.delegate = self
        listTableView.dataSource = self
        backButton.addTarget(self, action: #selector(backButtonTaped), for: .touchUpInside)
        cancelAppointmentButton.addTarget(self, action: #selector(cancelButtonTapped), for: .touchUpInside)
        // Do any additional setup after loading the view.
        
        self.meetdoctorButton.addTap {
            let JitsiCallVC = UIStoryboard(name: "doctor", bundle: nil).instantiateViewController(identifier: "JitsiCallVC") as! JitsiCallVC
            JitsiCallVC.modalPresentationStyle = .fullScreen
            let data = self.appointmnetDteail

            let param : Parameters = ["room_id" : "room_id_\(data?.provider_id ?? 0)_\(data?.booking_id ?? "0")_\(data?.patient_id ?? 0)",
                                      "patient_id" : "\(data?.patient_id ?? 0)",
                                      "push_to" : "patient",
                                      "id" : "\(data?.id ?? 0)",
                                      "video" : "1",
                                      "hospital_id" : "\(data?.provider_id ?? 0)"]
            self.doctorPresenter?.appointmentsUpdate(param: param)
            
            
        }
    }
    private func getRatingView() -> RatingView? {
        guard let _ = self.ratingView else {
            if let ratingView = Bundle.main.loadNibNamed(Constant.RatingView, owner: self, options: [:])?.first as? RatingView {
                self.view.addSubview(ratingView)
                ratingView.translatesAutoresizingMaskIntoConstraints = false
                ratingView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
                ratingView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
                ratingView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
                ratingView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
                ratingView.show(with: .bottom, completion: nil)
                ratingView.layoutIfNeeded()
                self.ratingView = ratingView
            }
            return ratingView
        }
        return self.ratingView
    }
    
    func showRatingview(){
        if let _ = self.getRatingView() {
            let data = self.appointmnetDteail
            self.ratingView?.setValues(color: .doctorColor)
            self.ratingView?.idLabel.text = TaxiConstant.bookingId.localized + ":" + "\(String(describing: data?.booking_id ?? ""))"
            if let provider = data?.provider {
                self.ratingView?.userNameLabel.text = provider.first_name

                self.ratingView?.userNameImage.sd_setImage(with: URL(string: provider.picture ?? ""), placeholderImage:#imageLiteral(resourceName: "ImagePlaceHolder"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    // Perform operation.
                    if (error != nil) {
                        // Failed to load image
                        self.ratingView?.userNameImage.image = #imageLiteral(resourceName: "ImagePlaceHolder")
                    } else {
                        // Successful in loading image
                        self.ratingView?.userNameImage.image = image
                    }
                })
            }
            self.ratingView?.onClickSubmit = { [weak self] (rating, comments) in
                guard let self = self else {
                    return
                }
                print("Rating_commets",rating,comments)
                var rartingLocal = rating
                if rartingLocal > 3{
                    rartingLocal = 2
                }else if rartingLocal > 1{
                    rartingLocal = 1
                }else{
                    rartingLocal = 0
                }
                    let comment = comments == Constant.leaveComment.localized ? "" : comments
                let param: Parameters = ["id": data?.id ?? 0,
                                         "doctor_profile_id": data?.doctor_profile?.id ?? 0,
                                         "provider_id": data?.provider_id ?? 0,
                                             "description": comments,
                                         "rating" : rating]
                self.doctorPresenter?.ratingView(param: param)
              
            }
        }
    }
    
    @objc func backButtonTaped(){
        self.navigationController?.popViewController(animated: true)
    }
    @objc func cancelButtonTapped(){
        let param : Parameters
        param = ["id": appointmnetDteail?.id ?? 0]
        doctorPresenter?.cancelAppointments(id:param)
    }
    
    
    func updateApointment(call: CallStatusEntity) {
        let JitsiCallVC = UIStoryboard(name: "doctor", bundle: nil).instantiateViewController(identifier: "JitsiCallVC") as! JitsiCallVC
        JitsiCallVC.modalPresentationStyle = .fullScreen
        let data = self.appointmnetDteail
        JitsiCallVC.patientId = "\(data?.patient_id ?? 0)"
        JitsiCallVC.roomid = "room_id_\(data?.provider_id ?? 0)_\(data?.booking_id ?? "0")_\(data?.patient_id ?? 0)"
        JitsiCallVC.room_id = "room_id_\(data?.provider_id ?? 0)_\(data?.booking_id ?? "0")_\(data?.patient_id ?? 0)"
        JitsiCallVC.patient_id = "\(data?.patient_id ?? 0)"
        JitsiCallVC.id = "\(data?.id ?? 0)"
        JitsiCallVC.hospital_id = "\(data?.provider_id ?? 0)"
        JitsiCallVC.doctorViewRating = {(rating) in
            print("Rating done",rating)
            self.showRatingview()
        }
            self.present(JitsiCallVC, animated: true, completion: nil)
    }
    
    func updateRating(call: CallStatusEntity) {
        if self.removeView(viewObj: self.ratingView) {
            self.navigationController?.popToRootViewController(animated: true)
            self.ratingView = nil
        }
    }
    
    private func removeView(viewObj: UIView?) -> Bool {
        if let views = viewObj {
            views.removeFromSuperview()
            return true
        }
        return false
    }

}
extension AppointmentDetailViewController : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AppointmentDetailTableViewCell") as! AppointmentDetailTableViewCell
            cell.dateLabel.text = appointmnetDteail?.patient?.booking_for ?? ""
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AppointmentDetailTableViewCell") as! AppointmentDetailTableViewCell
            cell.dateLabel.text = appointmnetDteail?.scheduled_at ?? ""
            return cell
        }
        
       
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
            let headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 35))
            
            let headerCell = tableView.dequeueReusableCell(withIdentifier: "AppointmentDetailTableViewCell") as! AppointmentDetailTableViewCell
            headerCell.frame = headerView.bounds
            headerCell.dateLabel.text = "Booked At"
            headerView.backgroundColor = .lightGray
            headerView.addSubview(headerCell)
            return headerView
        }
        else if section == 1 {
            let headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 35))
            let headerCell = tableView.dequeueReusableCell(withIdentifier: "AppointmentDetailTableViewCell") as! AppointmentDetailTableViewCell
            headerCell.frame = headerView.bounds
            headerCell.dateLabel.text = "Scheduled At"
            headerView.backgroundColor = .lightGray
            headerView.addSubview(headerCell)
            return headerView
        }
        else {
            return UIView()
        }
        
    }

}

extension AppointmentDetailViewController : DoctorPresenterToDoctorViewProtocol {
    func cancelAppointments(success: SuccessEntity) {
        showToast(msg: "Meeting Cancelled Successfully")
        self.navigationController?.popToRootViewController(animated: true)
    }
}
