//
//  PatientDetailViewController.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 21/02/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import ObjectMapper
import IQKeyboardManagerSwift
import Alamofire

@available(iOS 13.0, *)
class PatientDetailViewController: UIViewController {

    @IBOutlet weak var doctorImg : UIImageView!
    @IBOutlet weak var doctorNameLbl : UILabel!
    @IBOutlet weak var clinicDetailLbl : UILabel!
    @IBOutlet weak var scrollInnerView : UIView!
    @IBOutlet weak var scrollView : UIScrollView!
//    @IBOutlet weak var backbtn: UIImageView!
    @IBOutlet weak var patientDetailLbl : UILabel!
    @IBOutlet weak var bookingforTitleLbl : UILabel!
    @IBOutlet weak var dateandtimeTitleLbl : UILabel!
    @IBOutlet weak var doctorInfoTitleLbl : UILabel!
    @IBOutlet weak var bookingforLbl : UILabel!
    @IBOutlet weak var dateandtimeLbl : UILabel!
    @IBOutlet weak var nameTxt : HoshiTextField!
    @IBOutlet weak var emailTxt : HoshiTextField!
    @IBOutlet weak var phonenumTxt : HoshiTextField!
    @IBOutlet weak var consultfor : HoshiTextField!
    @IBOutlet weak var paymentTypeLabel: UILabel!
    @IBOutlet weak var changePaymentButton: UIButton!
    @IBOutlet weak var selectedPaymentLabel: UILabel!
    @IBOutlet weak var cardNumLbl: UILabel!
    @IBOutlet weak var backbtn: UIImageView!
    @IBOutlet weak var confirmBtn : UIButton!
    
    var bookingreq : BookingReq = BookingReq()
    
    var MainDoctorDetail : DoctorDetailModel?
    var doctorProfile : Response?
    var categoryId : Int = 0
    var patientdetail : PatientAppoinmentEntity?
    var isfromSearch : Bool = false
    var isFollowup : Bool = false
    var invoiceView : InvoiceView!
    var selectedPaymentType : String = "CASH"
    var cardId : String = ""
    var docProfile : DoctorListEntity?
    var schedlueDate : String = ""
    var schedlueTime : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.nameTxt.text = "asdfgh"
//        self.emailTxt.text = "eme@demo.com"
//        self.phonenumTxt.text = "1234598765"
//        self.consultfor.text = "asdfgbnbvcxz"
        self.initialLoad()
        self.populateData()
        self.backbtn.addTap {
            self.popOrDismiss(animation: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.navigationBar.isHidden = false
        IQKeyboardManager.shared.enable = true

    }
    
    func initialLoad()  {
//        backbtn.addTap {
//            self.popOrDismiss(animation: true)
//        }
        self.setupNavigation()
        self.setupAction()
        self.localize()
        
//        self.setupFont()
         IQKeyboardManager.shared.enable = false
        self.changePaymentButton.addTarget(self, action: #selector(changePaymentAction(_sender:)), for: .touchUpInside)
//        self.scrollView.addSubview(self.scrollInnerView)
//        self.scrollView.contentSize = CGSize(width: self.view.frame.width, height: self.scrollInnerView.frame.height)
    }
    
    func setupNavigation(){
//        self.navigationController?.isNavigationBarHidden = false
//        self.navigationItem.title = Constants.string.enterPatientDetails.localize()
    }
    
    @IBAction private func changePaymentAction(_sender:UIButton){
        let vc = UIStoryboard(name: "Account", bundle: nil).instantiateViewController(withIdentifier: Storyboard.Ids.PaymentSelectViewController) as! PaymentSelectViewController
        vc.isChangePayment = true
        vc.modalPresentationStyle = .automatic
        vc.onClickPayment = {paymenttype,carddata in
            self.selectedPaymentType = paymenttype.rawValue
            self.cardId = carddata?.card_id ?? ""
            if self.selectedPaymentType == "CARD"{
                self.paymentTypeLabel.text = "XXXX-XXXX-XXXX-\(carddata?.last_four ?? "")"
               // self.cardNumLbl.text = "XXXX-XXXX-XXXX-\(carddata?.last_four ?? "")"
                  self.cardNumLbl.isHidden = true
            }else{
                self.paymentTypeLabel.text = paymenttype.rawValue
                self.cardNumLbl.isHidden = true
            }
        }
        vc.onSelectPaymentType = {(paymentType,card_id,lastfour) in
            self.selectedPaymentType = paymentType
            self.cardId = card_id
            if self.selectedPaymentType == "stripe"{
              self.paymentTypeLabel.text = "XXXX-XXXX-XXXX-\(lastfour)"
              self.cardNumLbl.text = "XXXX-XXXX-XXXX-\(lastfour)"
                self.cardNumLbl.isHidden = false
            }else{
              self.paymentTypeLabel.text = paymentType
                self.cardNumLbl.isHidden = true
            }
           
            
        }
        //let nav = UINavigationController(rootViewController: vc)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    private func localize(){
        self.patientDetailLbl.text = Constants.string.patientDetails.localize()
        self.nameTxt.placeholder = Constants.string.enterName.localize()
        self.emailTxt.placeholder = Constants.string.enterEmail.localize()
        self.phonenumTxt.placeholder = Constants.string.enterPhone.localize()
//        self.phonenumTxt.text = Constants.string.enterPhone.localize()
        self.bookingforTitleLbl.text = Constants.string.bookingFor.localize()
        self.dateandtimeTitleLbl.text = Constants.string.datetime.localize()
        self.doctorInfoTitleLbl.text = Constants.string.doctorInfo.localize()
        self.selectedPaymentLabel.text = Constants.string.selectedPaymentMode.localize()
        self.changePaymentButton.setTitle(Constants.string.change.localize(), for: .normal)
        self.confirmBtn.setTitle(Constants.string.confirm.localize(), for: .normal)
        
    }

     func setupAction(){
        self.confirmBtn.addTap {
            if self.validation(){
                guard let data = self.MainDoctorDetail?.responseData?.first else {return}
               
                var params = [String:Any]()
                if !self.isfromSearch{
                    params.updateValue("5", forKey: "consult_time")
                    params.updateValue("\(self.schedlueDate)", forKey: "schedule_date")
                    params.updateValue("\(self.schedlueTime)", forKey: "schedule_time")
//
                    params.updateValue("ONLINE", forKey: "appointment_type")
                    params.updateValue(self.nameTxt.text ?? "", forKey: "patient_name")
                    params.updateValue(self.emailTxt.text ?? "", forKey: "patient_email")
                    params.updateValue(self.phonenumTxt.text ?? "", forKey: "patient_contact_number")
                    params.updateValue(self.consultfor.text ?? "", forKey: "consult_for")
                    params.updateValue("Appointment", forKey: "description")
                    params.updateValue(self.isFollowup ? "follow_up" : "consultation", forKey: "booking_for")
//                    params.updateValue(UserDefaultConfig.PatientID, forKey: "selectedPatient")
                    params.updateValue("\(self.categoryId)", forKey: "service_id")
                    params.updateValue(self.selectedPaymentType  , forKey: "payment_mode")
                    params.updateValue(data.provider_id ?? 0  , forKey: "id")
                    if self.selectedPaymentType == "CARD"{
                        params.updateValue(self.cardId, forKey: "card_id")
                    }

                }
                
                self.doctorPresenter?.bookingdetail(id: data.id ?? 0, param: params)
            }
            }
        }
//    }
    
    func populateData(){
        
        self.dateandtimeLbl.text = self.bookingreq.scheduled_at
        self.bookingforLbl.text = self.bookingreq.booking_for.capitalized
        
            
            guard let data = self.MainDoctorDetail?.responseData?.first else {return}
            
            let url = data.provider?.picture ?? ""
            guard let url = URL(string: url) else {return}
            let dataImg = try? Data(contentsOf: url)
            self.doctorImg.image = UIImage(data: dataImg ?? Data())
           
            self.doctorNameLbl.text = "\(data.provider?.first_name ?? "")"
            self.clinicDetailLbl.text = "\(data.service?.service_subcategory_name ?? "")"
            
        //    self.bookingforLbl.text =  self.isFollowup ? "Follow up" : "Consultation"
           
     
        
    }
    
    func validation() -> Bool{
        if self.nameTxt.getText.isEmpty{
            showToast(msg: ErrorMessage.list.enterPatientName.localize())
            return false
        }else if self.emailTxt.getText.isEmpty || !self.emailTxt.getText.isValidEmail(){
            showToast(msg: ErrorMessage.list.enterEmail.localize())
            return false
        }else if self.phonenumTxt.getText.isEmpty || !self.phonenumTxt.getText.isPhoneNumber{
            showToast(msg: ErrorMessage.list.enterPhoneNumber.localize())
            return false
        }else if self.consultfor.getText.isEmpty{
            showToast(msg: "Enter Consultation details")
            return false
        }else{
            return true
        }
    }
}
//Api calls
@available(iOS 13.0, *)
extension PatientDetailViewController : DoctorPresenterToDoctorViewProtocol{
    
    func bookingdetail(PatientAppoinment: PatientAppoinmentEntity) {
        self.patientdetail = PatientAppoinment
        showToast(msg: PatientAppoinment.message ?? "")
        self.navigationController?.popToRootViewController(animated: true)
    }

    
    func showError(error: CustomError) {
        showToast(msg: error.localizedDescription)
    }
    
    
}


@available(iOS 13.0, *)
extension PatientDetailViewController : UITextFieldDelegate{
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       // Try to find next responder
       if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
          nextField.becomeFirstResponder()
       } else {
          // Not found, so remove keyboard.
          textField.resignFirstResponder()
       }
       // Do not add a line break
       return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
}

