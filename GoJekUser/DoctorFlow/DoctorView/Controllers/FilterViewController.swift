//
//  FilterViewController.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 31/01/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import ObjectMapper

class FilterViewController: UIViewController {

    @IBOutlet weak var fileterTableView: UITableView!
    @IBOutlet weak var applyView: UIView!
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var cancelButton : UIButton!
    var arr = ["","",""]
    var selectedAvailablity = String()
    var selectedPrice = String()
    var selectedGender :String = ""
    var selectedCategory = String()
    var orderCheckedStatus = [Int : [[Int] : Bool]]() // To update carton

    
    var hiddenSections = Set<Int>()
    var selectedData : [valueData]?
    var onClickDone : ((String,String,String)->Void)?
    var onClickSearch : ((Int,String,String,String)->Void)?
//    var dataSource : [Int:[[Int:Bool]]]?
    
    
    var selectedIndex : IndexPath?
//    var section = 0
    var isFromSearch : Bool = false
    var sectionHeader = [String]()
    var category : [Category] = [Category]()
    var availabitlityArr = [Constants.string.availableAnyDay.localize(),Constants.string.availableToday.localize(),Constants.string.available3days.localize(),Constants.string.availableComingWeekend.localize()]
    var genderArr = [Constants.string.male.localize(),Constants.string.female.localize()]
    var consultationArr = ["1-20","20-30","30+"]
    
    
    private var dataCells = [Int : [Int : (row : Int?, isSelect : Bool)]]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        sectionHeader = isFromSearch ?  [Constants.string.categories.localize(),Constants.string.availablity.localize(),Constants.string.gender.localize(),Constants.string.consulationFee.localize()]       : [Constants.string.availablity.localize(),Constants.string.gender.localize(),Constants.string.consulationFee.localize()]
        self.setUpnavigation()
        
//        self.fileterTableView.allowsMultipleSelection = true
        
        self.fileterTableView.register(UINib(nibName: "FilterCell", bundle: nil), forCellReuseIdentifier: "FilterCell")
        self.applyButton.setTitle(Constants.string.apply.localize(), for: .normal)
        self.cancelButton.setTitle(Constants.string.Cancel.localize(), for: .normal)
        self.applyButton.addTarget(self, action: #selector(applyAction(sender:)), for: .touchUpInside)
        self.cancelButton.addTarget(self, action: #selector(cancelAction(sender:)), for: .touchUpInside)
        self.initializeArr()


//        Common.setFont(to: self.navigationItem.title!, isTitle: true, size: 18)

        // Do any additional setup after loading the view.
    }
    
    private func initializeArr(){
        
        
        selectedData = [valueData]()
        var rowss = [rRow]()
        
        
        for (index,val) in availabitlityArr.enumerated(){
           
            rowss.append(rRow(valueName: val, row: index, isSelected: false))
        }
        isFromSearch ? selectedData?.append(valueData(sectionName: sectionHeader[1], row: rowss)) : selectedData?.append(valueData(sectionName: sectionHeader[0], row: rowss))
        rowss.removeAll()
        for (index,val) in genderArr.enumerated(){
            rowss.append(rRow(valueName: val, row: index, isSelected: false))
        }
        isFromSearch ?  selectedData?.append(valueData(sectionName: sectionHeader[2], row: rowss)) : selectedData?.append(valueData(sectionName: sectionHeader[1], row: rowss))
        rowss.removeAll()

        for (index,val) in consultationArr.enumerated(){
            rowss.append(rRow(valueName: val, row: index, isSelected: false))
        }
        isFromSearch ? selectedData?.append(valueData(sectionName: sectionHeader[2], row: rowss)) : selectedData?.append(valueData(sectionName: sectionHeader[2], row: rowss))
        
        rowss.removeAll()
        
        func getCatagoryList(){
//            self.presenter?.HITAPI(api: Base.catagoryList.rawValue, params: nil, methodType: .GET, modelClass: CategoryList.self, token: true)
        }
        
    }
    
    
    func setUpnavigation() {
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "AppBlueColor")
        self.navigationItem.title = Constants.string.filter.localize()

        Common.setFontWithType(to: self.navigationItem.title!, size: 18, type: .regular)
        Common.setFontWithType(to: self.applyButton, size: 14, type: .meduim)
        Common.setFontWithType(to: self.cancelButton, size: 14, type: .meduim)

    }


}
extension FilterViewController {
    
    @IBAction private func cancelAction(sender:UIButton){
        
        
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func applyAction(sender:UIButton){
        print(self.selectedAvailablity)
        print(self.selectedGender)
        print(self.selectedPrice)
        var availability_type = String()
        var seleGender = String()
        var price = String()
        if selectedAvailablity == Constants.string.availableAnyDay.localize(){
            availability_type = "AvailableAllDAY"
            
        }else if selectedAvailablity == Constants.string.availableToday.localize() {
            availability_type = "today"
            
        }else if selectedAvailablity == Constants.string.available3days.localize() {
            availability_type = "3w"
            
        }else {
           availability_type = "week"
        }
        
        seleGender = selectedGender
        
        if selectedPrice == "1-20"{
            price = "1-10"
            
        }else if selectedPrice == "20-30" {
            price = "20-30"
            
        }else if selectedPrice == "30+" {
            price = "30"
            
        }
        var catInt = Int()
        for (index,val) in category.enumerated(){
            if val.name == self.selectedCategory{
                catInt = category[index].id ?? 0
            }
        }
        
        isFromSearch ? self.onClickSearch?(catInt,availability_type,seleGender,price) : self.onClickDone?(availability_type,seleGender,price)
    }
    
}




@available(iOS 13.0, *)
extension FilterViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionHeader.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return 0.3
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = Bundle.main.loadNibNamed("FilterHeaderView", owner: self, options: nil)?.first as? FilterHeaderView
        headerView?.lbl.text = sectionHeader[section]
        headerView?.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50)
        headerView?.addTap {
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 0.3))
        view.backgroundColor = section != 2 ? UIColor(named: "TextForegroundColor") : .clear
        return view
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFromSearch{
            switch section {
            case 0 :
            return category.count
            case 1:
                return availabitlityArr.count
            case 2:
                return genderArr.count
            case 3:
                return consultationArr.count
            default:
                return 0
            }
            
        }else{
            switch section {
            case 0:
                return availabitlityArr.count
            case 1:
                return genderArr.count
            case 2:
                return consultationArr.count
            default:
                return 0
            }
            
        }
      


    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isFromSearch{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell", for: indexPath) as? FilterCell
            
            for _ in 0...(self.selectedData?[indexPath.section].row.count)! {
                if self.selectedData?[indexPath.section].row[indexPath.row].isSelected == true {
                    cell?.setCirlceImageForSelection = true
                    switch indexPath.section {
                    case 0:
                        self.selectedCategory = self.selectedData?[indexPath.section].row[indexPath.row].valueName ?? "0"
                    case 1:
                        self.selectedAvailablity = self.selectedData?[indexPath.section].row[indexPath.row].valueName ?? "0"
                    case 2:
                        self.selectedGender = self.selectedData?[indexPath.section].row[indexPath.row].valueName ?? "0"
                    case 3:
                        self.selectedPrice = self.selectedData?[indexPath.section].row[indexPath.row].valueName ?? "0"
                        
                    default:
                        break
                    }
                    

                }else{
                    cell?.setCirlceImageForSelection = false

                    
                }
            }
            cell?.titleLbl.text = self.selectedData?[indexPath.section].row[indexPath.row].valueName // genderArr[indexPath.row]
           
            return cell!
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell", for: indexPath) as? FilterCell
            
            for _ in 0...(self.selectedData?[indexPath.section].row.count)! {
                if self.selectedData?[indexPath.section].row[indexPath.row].isSelected == true {
                    cell?.setCirlceImageForSelection = true
                    switch indexPath.section {
                    case 0:
                        self.selectedAvailablity = self.selectedData?[indexPath.section].row[indexPath.row].valueName ?? "0"
                    case 1:
                        self.selectedGender = self.selectedData?[indexPath.section].row[indexPath.row].valueName ?? "0"
                    case 2:
                        
                        self.selectedPrice = self.selectedData?[indexPath.section].row[indexPath.row].valueName ?? "0"
                    default:
                        break
                    }
                    

                }else{
                    cell?.setCirlceImageForSelection = false

                    
                }
            }
            cell?.titleLbl.text = self.selectedData?[indexPath.section].row[indexPath.row].valueName // genderArr[indexPath.row]
           
            return cell!
        }
   
        }
         
//    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                    
        _ = 0
        _ = indexPath.row
//
//        if indexPath.section != 1 {
//
//            for i in 0..<(self.selectedData?[indexPath.section].row.count)! {
//                if dat1 != dat2 {
//                        self.selectedData?[indexPath.section].row[i].isSelected = false
//                    dat1 += 1
//                }else{
////                    cell?.setCirlceImageForSelection = false
//
//                    self.selectedData?[indexPath.section].row[i].isSelected = true
//                    dat1 += 1
//
//                }
//            }
//            self.fileterTableView.reloadData()
//
//        }else{
            
            if var data = self.selectedData?[indexPath.section].row[indexPath.row].isSelected{
                
                data = data ? false : true
                self.selectedData?[indexPath.section].row[indexPath.row].isSelected = data
            }
            self.fileterTableView.reloadData()

//        }
       
     }
    
}




struct valueData {
    
    var sectionName : String?
    var row : [rRow]
    
    
}

struct rRow {
    var valueName : String?
    var row : Int?
    var isSelected : Bool?

}

//extension FilterViewController : PresenterOutputProtocol{
//    func showSuccess(api: String, dataArray: [Mappable]?, dataDict: Mappable?, modelClass: Any) {
//        switch String(describing: modelClass) {
//            case model.type.CategoryList:
//                let data = dataDict as? CategoryList
//                self.category = data?.category ?? [Category]()
//                var rowss = [rRow]()
//                for (index,val) in self.category.enumerated(){
//                   
//                    rowss.append(rRow(valueName: val.name, row: index, isSelected: false))
//                }
//                selectedData?.append(valueData(sectionName: sectionHeader[0], row: rowss))
//                rowss.removeAll()
//                
//                self.fileterTableView.reloadInMainThread()
//                break
//           
//            default: break
//            
//        }
//    }
//    
//    func showError(error: CustomError) {
//        
//    }
//    
//    
//}

