//
//  DoctorHomeVC.swift
//  GoJekUser
//
//  Created by Shyamala's MacBook Pro on 20/01/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

    import UIKit
    import Foundation
    import ObjectMapper
    
    class DoctorHomeVC: UIViewController{
        
        @IBOutlet private var viewSideMenu : UIView!
        var dictionary =  [String:String]()
        
        @IBOutlet weak var navigatelabl: UILabel!
//        @IBOutlet weak var listTableView: UITableView!
        @IBOutlet weak var searchBar: UISearchBar!
        
        @IBOutlet weak var doctorcollection: UICollectionView!
        
        var selectedServiceID : Int?
        
        lazy var loader : UIView = {
            return createActivityIndicator(self.view.window ?? self.view)
        }()
        
        override var preferredStatusBarStyle: UIStatusBarStyle {
            return .lightContent
        }
        
        
        var titles = ["Find Doctors",/*"Chat",*/"Search Doctors","Visited Doctors","appointment","RelativesManagement","articles","medicalRecords"]
        var subTitles = ["Specialities",/*"Ask Question on health related",*/"Base on Hospitals","See you visited doctors","get the Appointments","ralative","write an articals","get the records"]
        var imageArray = ["finddoctors",/*"Group 9",*/"searchdoctors","visitedDoctors","articles","appointment","medicalRecords","RelativesManagement"]
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.initialLoads()
            registerCell()
            setupFont()
            placeholder()
            self.searchBar.delegate = self
        }
        
        
        
        override func viewWillAppear(_ animated: Bool) {
            self.navigationController?.isNavigationBarHidden = true
//            self.getProfileApi()
        }
        private func registerCell(){
           
            
            doctorcollection.register(UINib(nibName: XIB.Names.DoctorsListCell, bundle: .main), forCellWithReuseIdentifier: XIB.Names.DoctorsListCell)
            
        
        }
        
        
        
    }
    
    // MARK:- Methods
    
    extension DoctorHomeVC {
        
        private func initialLoads() {
            
            self.navigatelabl.text = Constants.string.navigatetitle.localize()
            
            self.viewSideMenu.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.sideMenuAction)))
            
            let image =  UIImageView(image: UIImage(imageLiteralResourceName: "google"))
            if #available(iOS 13.0, *) {
                searchBar.searchTextField.leftViewMode = .never
                searchBar.searchTextField.backgroundColor = .white
                searchBar.searchTextField.rightView = image
                searchBar.searchTextField.rightViewMode = .always
            } else {
                // Fallback on earlier versions
            }
           
        //        self.listTableView.register(UINib(nibName: XIB.Names.LogoCell, bundle: .main), forCellReuseIdentifier: XIB.Names.LogoCell)
        }
        func placeholder(){
            if #available(iOS 13.0, *) {
                searchBar.searchTextField.placeholder = Constants.string.searchDoctorPlaceHolder.localize()
            }
        }
        func setupFont(){
            Common.setFontWithType(to: self.navigatelabl, size: 25, type: .regular)
        
        }
        
        
        
        // MARK:- SideMenu Button Action
        
        @IBAction private func sideMenuAction(){
            
//            self.drawerController?.openSide(.left)
            self.navigationController?.popViewController(animated: true)
            self.viewSideMenu.addPressAnimation()
        }
        
        
    }
    
    
    // MARK:- Tableview Delegates
    
    
//    extension DoctorHomeVC : UITableViewDelegate, UITableViewDataSource
//    {
//        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//            return titles.count + 1
//        }
//
//        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//
//            if indexPath.row != titles.count {
//
//                let cell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.DoctorsListCell, for: indexPath) as! DoctorsListCell
//
//                cell.imgDoctor.image = UIImage(imageLiteralResourceName: self.imageArray[indexPath.row])
//                cell.labelName.text = titles[indexPath.row]
//                cell.labelSpeciality.text = subTitles[indexPath.row]
//                cell.selectionStyle  = .none
//                return cell
//
//            }
//            else
//            {
//
//                let cell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.LogoCell, for: indexPath) as! LogoCell
//                cell.selectionStyle  = .none
//                return cell
//
//            }
//
//        }
//
//        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
//        {
//
//            switch indexPath.row {
//                case 0:
//                let vc = storyboard?.instantiateViewController(withIdentifier: Storyboard.Ids.CategoryListController) as! CategoryListController
////                vc.id = AppManager.shared.getSelectedServices()?.id ?? 0
//                self.navigationController?.pushViewController(vc, animated: true)
//
//                case 1:
//                    self.push(id: Storyboard.Ids.ChatQuestionViewController, animation: true)
//                case 2:
//                    self.push(id: Storyboard.Ids.SearchViewController, animation: true)
//                case 3:
//                    self.push(id: Storyboard.Ids.VisitedDoctorsViewController, animation: true)
//                default:
//                    self.push(id: Storyboard.Ids.VisitedDoctorsViewController, animation: true)
//            }
//
//        }
//
//        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//            if indexPath.row == titles.count{
//                return 250
//            }else{
//
//                return 110
//
//            }
//
//
//        }
//
//
//    }

extension DoctorHomeVC : DoctorPresenterToDoctorViewProtocol {
    
}

//    //Api calls
//    extension DoctorHomeVC : PresenterOutputProtocol{
//        func showSuccess(api: String, dataArray: [Mappable]?, dataDict: Mappable?, modelClass: Any) {
//            switch String(describing: modelClass) {
//                case model.type.ProfileModel:
//                    let data = dataDict as? ProfileModel
//                    UserDefaultConfig.PatientID = (data?.patient?.id ?? 0).description
//                    profileDetali = data ?? ProfileModel()
//                    break
//
//                default: break
//
//            }
//        }
//
//        func showError(error: CustomError) {
//
//        }
//
//        func getProfileApi(){
//            self.presenter?.HITAPI(api: Base.profile.rawValue, params: nil, methodType: .GET, modelClass: ProfileModel.self, token: true)
//        }
//
//    }
    

    
    
    
extension DoctorHomeVC: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return titles.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
//            let cell = doctorcollection.dequeueReusableCell(withReuseIdentifier: XIB.Names.DoctorsListCell, for: indexPath) as! DoctorsListCell
        
        let cell = doctorcollection.dequeueReusableCell(withReuseIdentifier:XIB.Names.DoctorsListCell, for: indexPath as IndexPath) as! DoctorsListCell
//        let imageurl = self.imageArray[indexPath.row]
//        print(imageurl)
//        cell.imgDoctor.setURLImage(imageurl)
                        cell.imgDoctor.image = UIImage(imageLiteralResourceName: self.imageArray[indexPath.row])
//                 cell.setValues(name: imageArray[indexPath.row], imageString: imageArray[indexPath.row])
                        cell.LabelName.text = titles[indexPath.row]
                        cell.LabelSpeciality.text = subTitles[indexPath.row]
                        return cell
        
                    }
       
        
}
   
    
extension DoctorHomeVC:UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {
                        case 0:
                        let vc = storyboard?.instantiateViewController(withIdentifier: Storyboard.Ids.CategoryListController) as! CategoryListController
        //                vc.id = AppManager.shared.getSelectedServices()?.id ?? 0
                        self.navigationController?.pushViewController(vc, animated: true)
        
//                        case 1:
//                            self.push(id: Storyboard.Ids.ChatQuestionViewController, animation: true)
                        case 1:
                            self.push(id: Storyboard.Ids.SearchViewController, animation: true)
                        case 2:
                            self.push(id: Storyboard.Ids.VisitedDoctorsViewController, animation: true)
                        case 3:
                            self.push(id: Storyboard.Ids.AppointmentListViewController, animation: true)
                        case 4:
                            self.push(id: Storyboard.Ids.RelativeManagementViewController, animation: true)
                        case 5:
                            self.push(id: Storyboard.Ids.HealthFeedViewController, animation: true)
                        case 6:
                            self.push(id: Storyboard.Ids.MedicalRecordsViewController, animation: true)
            
                        default:
                            self.push(id: Storyboard.Ids.MedicalRecordsViewController, animation: true)
                    }
}
}
extension DoctorHomeVC: UICollectionViewDelegateFlowLayout {

        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
            let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
            let width:CGFloat = (doctorcollection.frame.size.width - space) / 2.0
            let height:CGFloat = (doctorcollection.frame.size.width - space) / 2.3
            return CGSize(width: width, height: height)
        }
    }
struct model {
    
    static let type = model()
    
    let RegisterModel = "RegisterModel"
    let LoginModel = "LoginModel"
    let Json4Swift_Base = "Json4Swift_Base"
    let MobileVerifyModel = "MobileVerifyModel"
    let SignupResponseModel = "SignupResponseModel"
    let AppointmentModel = "AppointmentModel"
    let CommonModel = "CommonModel"
    let CategoryList = "CategoryList"
    let DoctorsDetailModel = "DoctorsDetailModel"
    let ArticleModel = "ArticleModel"
    let MedicalRecordsModel = "MedicalRecordsModel"
    let DoctorsListModel = "DoctorsListModel"
    let ProfileModel = "ProfileModel"
    let FeedBackModel = "FeedBackModel"
    let BookingModel = "BookingModel"
    let PromoCodeEntity = "PromoCodeEntity"
    let ChatHistoryEntity = "ChatHistoryEntity"
    let TwilioAccess = "TwilioAccess"
    let MakeTwilioCall = "MakeTwilioCall"
    let Remainder = "Remainder"
    let RemainderSuccess = "RemainderSuccess"
    let UpdatedVistedDoctor = "UpdatedVistedDoctor"
    let CardSuccess = "CardSuccess"
    let AddMoneyModel = "AddMoneyModel"
    let CardsModel = "CardsModel"
    let FAQModel = "FAQModel"
    let RelativeManagement = "RelativeManagement"
    let CreateRelativeResponse = "CreateRelativeResponse"
    let GetDoctors = "GetDoctors"
    let AddMedicalRecordModel = "AddMedicalRecordModel"
    let ListMedicalRecord = "ListMedicalRecord"
    let PatientRecord = "PatientRecord"
}


    extension DoctorHomeVC : UISearchBarDelegate {
        
        func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
            self.push(id: Storyboard.Ids.SearchViewController, animation: true)
            return false
        }
        
        
       func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
              print("end searching --> Close Keyboard")
              self.searchBar.endEditing(true)
          }
        
        
    }

