//
//  DoctorDetailsController.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 21/02/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import ObjectMapper
import AVKit

//class DoctorDetailsController: UIViewController {
//
//    @IBOutlet weak var imageBackground: UIImageView!
//    @IBOutlet weak var btnFavourite: UIButton!
//    @IBOutlet weak var bookBtn: UIButton!
//    @IBOutlet weak var shareBtn: UIButton!
//    @IBOutlet weak var imgDoctor: UIImageView!
//    @IBOutlet weak var labelDoctorName: UILabel!
//    @IBOutlet weak var labelSpeciality: UILabel!
//    @IBOutlet weak var labelQualification: UILabel!
//    @IBOutlet weak var labelPercentage: UILabel!
//    @IBOutlet weak var labelExp: UILabel!
//    @IBOutlet weak var verifiedImg: UIImageView!
//    @IBOutlet weak var labelVerified: UILabel!
//    @IBOutlet weak var labelConsultationfee: UILabel!
//    @IBOutlet weak var buttonPlayVideo: UIButton!
//    @IBOutlet weak var availablityTitleLabel : UILabel!
//    @IBOutlet weak var serviceTitleLabel: UILabel!
//    @IBOutlet weak var specializationTitleLabel: UILabel!
//    @IBOutlet weak var locationTitleLabel: UILabel!
//
//    @IBOutlet weak var labelMonday: UILabel!
//    @IBOutlet weak var labelTue: UILabel!
//    @IBOutlet weak var labelWed: UILabel!
//    @IBOutlet weak var labelThu: UILabel!
//    @IBOutlet weak var labelFri: UILabel!
//    @IBOutlet weak var labelSat: UILabel!
//    @IBOutlet weak var labelSun: UILabel!
//    @IBOutlet weak var labelServices: UILabel!
//    @IBOutlet weak var servicesTV: UITableView!
//    @IBOutlet weak var labelSpecilization: UILabel!
//    @IBOutlet weak var specilizationTV: UITableView!
//   // @IBOutlet weak var timingTableView: UITableView!
//    @IBOutlet weak var labelLocation: UILabel!
//    @IBOutlet weak var labelClinicName: UILabel!
////    @IBOutlet weak var labelLocationValue: UILabel!
////    @IBOutlet weak var imgLocationPreview: UIImageView!
//    @IBOutlet weak var labelPhotos: UILabel!
//    @IBOutlet weak var photosCV: UICollectionView!
//    @IBOutlet weak var labelReviews: UILabel!
//    @IBOutlet weak var reviewsCV: UICollectionView!
//    @IBOutlet weak var specilizationTVHeight: NSLayoutConstraint!
//    @IBOutlet weak var specilizationVHeight: NSLayoutConstraint!
//
//    @IBOutlet weak var serviceTVHeight: NSLayoutConstraint!
//    @IBOutlet weak var serviceVHeight: NSLayoutConstraint!
//    @IBOutlet weak var timingHeight: NSLayoutConstraint!
//    @IBOutlet weak var scrollView: UIScrollView!
//    @IBOutlet weak var innerScrolllView: UIView!
//    @IBOutlet weak var staticMapImage: UIImageView!
//
////
//    //inital static Data
//
////    var searchDoctor = Search_doctors()
////    var docProfile = Doctor_profile()
////    var favouriteDoctor : Favourite_Doctors?
////    var isfromFavourite : Bool = false
////    var isFromSearchDoctor:Bool = true
////    var speciality : [String] = [String]()
////    var categoryID : Int = 0
////    var videoURL : NSURL?
////    var searchDoctors : ResponseDatasssss?
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        //View Setup
//       // self.initialLoads()
////        self.checkToday(day: Date().dayOfWeek()!)
//
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        self.navigationController?.isNavigationBarHidden = false
////        self.populateData()
//    }
//
//    override func viewWillLayoutSubviews() {
//        super.viewWillLayoutSubviews()
//    }
//
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        print("Hello")
//    }
//
////    private func localize(){
////        self.labelVerified.text = Constants.string.medicalVerified.localize()
////        self.availablityTitleLabel.text = Constants.string.availablity.localize()
////        self.labelMonday.text = Constants.string.mon.localize()
////        self.labelTue.text = Constants.string.tue.localize()
////        self.labelWed.text = Constants.string.wed.localize()
////        self.labelThu.text = Constants.string.thu.localize()
////        self.labelFri.text = Constants.string.fri.localize()
////        self.labelSat.text = Constants.string.sat.localize()
////        self.labelSun.text = Constants.string.sun.localize()
////        self.serviceTitleLabel.text = Constants.string.services.localize()
////        self.specializationTitleLabel.text = Constants.string.specialization.localize()
////        self.locationTitleLabel.text = Constants.string.location.localize()
////        self.bookBtn.setTitle(Constants.string.bookAppoitment.localize(), for: .normal)
////
////
////    }
//
//
////    func populateData(){
////
////        if !isFromSearchDoctor {
////            if isfromFavourite {
//////               let detail : Hospital = (self.favouriteDoctor?.hospital) ?? Hospital()
////               self.labelDoctorName.text = "\(detail.first_name ?? "") \(detail.last_name ?? "")"
////               self.imgDoctor.setURLImage(detail.doctor_profile?.profile_pic ?? "")
////                self.imgDoctor.makeRoundedCorner()
////                self.labelConsultationfee.text = Constants.string.consulationFee.localize() +  "$\(detail.doctor_profile?.fees ?? 0)"
////               self.labelQualification.text =  "\(Constants.string.consulationFee.localize())  " + "$ \(detail.doctor_profile?.speciality?.name ?? "")"
////               self.labelPercentage.text = "\(detail.feedback_percentage ?? "") %"
////                self.speciality.append(self.favouriteDoctor?.hospital?.doctor_profile?.speciality?.name ?? "")
////            //   self.imgLocationPreview.pin_setImage(from: URL(string: detail.clinic?.static_map ?? "")!)
////                self.labelClinicName.text = detail.clinic?.name ?? Constants.string.noClinicName.localize().capitalized
//////                self.labelLocationValue.text =  (detail.clinic?.address ?? Constants.string.noAddressFound.localize())
////                self.staticMapImage.getURLImage(detail.clinic?.static_map ?? "")
////               if (detail.is_favourite ?? "") == "false"{
////                   self.btnFavourite.setImage(UIImage(named: "love"), for: .normal)
////               }else{
////                   self.btnFavourite.setImage(UIImage(named: "love_red"), for: .normal)
////               }
////                if detail.doctor_profile?.profile_video == ""{
////                    self.buttonPlayVideo.isHidden = true
////                }else{
////                    self.buttonPlayVideo.isHidden = false
////                    let url = APPConstant.imageURL +  (detail.doctor_profile?.profile_video ?? "")
////                    self.videoURL = NSURL(string: url)
////                    self.buttonPlayVideo.addTarget(self, action: #selector(playProfileVideo(sender:)), for: .touchUpInside)
////                }
////    }else{        let detail : Hospital = (self.docProfile.hospital?.first) ?? Hospital()
////            self.labelDoctorName.text = "\(detail.first_name ?? "") \(detail.last_name ?? "")"
////            self.imgDoctor.setURLImage(detail.doctor_profile?.profile_pic ?? "")
////             self.imgDoctor.makeRoundedCorner()
////        self.labelConsultationfee.text = "\(Constants.string.consulationFee.localize())  " +  "$ \(self.docProfile.fees ?? 0)"
////            self.labelQualification.text = "\(self.docProfile.speciality?.name ?? "")"
////            self.labelPercentage.text = "\(detail.feedback_percentage ?? "") %"
////            self.speciality.append(self.docProfile.speciality?.name ?? "")
////         //   self.imgLocationPreview.pin_setImage(from: URL(string: detail.clinic?.static_map ?? "")!)
////            self.labelClinicName.text = detail.clinic?.name ??  Constants.string.noClinicName.localize().capitalized
//////            self.labelLocationValue.text = detail.clinic?.address ?? Constants.string.noAddressFound.localize()
////        self.staticMapImage.getURLImage(detail.clinic?.static_map ?? "")
////            if (detail.is_favourite ?? "") == "false"{
////                self.btnFavourite.setImage(UIImage(named: "love"), for: .normal)
////            }else{
////                self.btnFavourite.setImage(UIImage(named: "love_red"), for: .normal)
////            }
////            }
////            if self.docProfile.hospital?.first?.doctor_profile?.profile_video == ""{
////                self.buttonPlayVideo.isHidden = true
////            }else{
////                self.buttonPlayVideo.isHidden = false
////                let url = APPConstant.imageURL + (self.docProfile.profile_video ?? "")
////                self.videoURL = NSURL(string: url )
////                self.buttonPlayVideo.addTarget(self, action: #selector(playProfileVideo(sender:)), for: .touchUpInside)
////            }
////        }else{
////
////            let car = self.searchDoctor.timing?.count == 0 ? 0 : 70
////            let cr = 30 * (self.searchDoctor.timing?.count)! + 1
////            let cr2 = 30 * (self.searchDoctor.timing?.count)! + car
////
////            self.serviceTVHeight.constant = CGFloat(cr)
////            self.serviceVHeight.constant = CGFloat(cr2)
////
////            let specialityFloat1 = 40 * 1
////            let specialityFloat2 = (40 * 1) + 70
////
////            self.specilizationTVHeight.constant = CGFloat(specialityFloat1)
////            self.specilizationVHeight.constant = CGFloat(specialityFloat2)
//////            self.categoryID = self.searchDoctor.doctor_profile.
////
////                self.labelDoctorName.text = "\(self.searchDoctor.first_name ?? "")" + " " + "\(self.searchDoctor.last_name ?? "")"
////                self.imgDoctor.setURLImage(self.searchDoctor.doctor_profile?.profile_pic ?? "")
////                     self.imgDoctor.makeRoundedCorner()
////
////            self.labelConsultationfee.text = "\(Constants.string.consulationFee.localize())  "  +  "$ \(self.docProfile.fees ?? 0)"
////            self.labelQualification.text = "\(self.searchDoctor.doctor_profile?.speciality?.name ?? "")"
////                self.labelQualification.text = "\(self.searchDoctor.doctor_profile?.certification ?? "")".capitalized
////            if self.searchDoctor.doctor_profile?.profile_video == ""{
////                    self.buttonPlayVideo.isHidden = true
////                }else{
////                    self.buttonPlayVideo.isHidden = false
////                    let url = APPConstant.imageURL + (self.searchDoctor.doctor_profile?.profile_video ?? "")
////                    self.videoURL = NSURL(string: url )
////
////
////
////
////                }
////        //        self.labelPercentage.text = "\(self.docProfile.feedback?.first.) %"
////                self.speciality.append((self.searchDoctor.doctor_profile?.speciality?.name ?? "").capitalized)
////                 //   self.imgLocationPreview.pin_setImage(from: URL(string: detail.clinic?.static_map ?? "")!)
////            self.labelClinicName.text = self.searchDoctor.clinic?.name ?? Constants.string.noClinicName.localize().capitalized
//////                self.labelLocationValue.text = self.searchDoctor.clinic?.address ?? Constants.string.noAddressFound.localize()
////            self.staticMapImage.getURLImage(self.searchDoctor.clinic?.static_map ?? "")
////                if (self.searchDoctor.is_favourite ?? "") == "false"{
////                        self.btnFavourite.setImage(UIImage(named: "love"), for: .normal)
////                    }else{
////                        self.btnFavourite.setImage(UIImage(named: "love_red"), for: .normal)
////                    }
////                }
////
////
////            self.servicesTV.reloadInMainThread()
////            self.specilizationTV.reloadInMainThread()
////            self.timingTableView.reloadInMainThread()
////
////
////    }
//
////    @IBAction private func playProfileVideo(sender:UIButton){
////        let playerVC = AVPlayerViewController()
////        let player = AVPlayer(url: self.videoURL! as URL)
////        playerVC.player = player
////        self.present(playerVC, animated: true, completion: {
////            playerVC.player!.play()
////        })
////    }
//
//    func setupAction(){
////        self.btnFavourite.addTap {
////            if !self.isFromSearchDoctor{
////                if self.isfromFavourite{
////                    self.addRemoveFav(patient_id: UserDefaultConfig.PatientID, doctor_id: (self.favouriteDoctor?.hospital?.doctor_profile?.doctor_id ?? 0).description)
////                }else{
////            self.addRemoveFav(patient_id: UserDefaultConfig.PatientID, doctor_id:(self.docProfile.doctor_id ?? 0).description)
////            }
////            }else{
////                self.addRemoveFav(patient_id: UserDefaultConfig.PatientID, doctor_id:(self.searchDoctor.doctor_profile?.doctor_id ?? 0).description)
////            }
////            }
//
////        self.bookBtn.addTap {
////            let vc = BookingViewController.initVC(storyBoardName: .doctor, vc: BookingViewController.self, viewConrollerID: Storyboard.Ids.BookingViewController)
//////                     vc.docProfile = self.docProfile
//////                     vc.searchDoctor = self.searchDoctor
////                     vc.isFromSearch = self.isFromSearchDoctor
////                     vc.isfromFavourite = self.isfromFavourite
//////                     vc.favouriteDoctor = self.favouriteDoctor
////                     vc.categoryId = self.categoryID
////                     self.push(from: self, ToViewContorller: vc)
////
////        }
//    }
//}

///@available(iOS 13.0, *)
//extension DoctorDetailsController{
//
//    private func initialLoads() {
////        self.localize()
////        setupNavigationBar()
//////        self.setupTableViewCell()
//////        self.setupCollectionViewCell()
////        self.setupAction()
//    }
//
//    private func setupNavigationBar() {
////        self.navigationController?.isNavigationBarHidden = false
////        _ = UIBarButtonItem(image: #imageLiteral(resourceName: "share").resizeImage(newWidth: 20), style: .plain, target: self, action: #selector(shareAction))
////        _ = UIBarButtonItem(image: #imageLiteral(resourceName: "info").resizeImage(newWidth: 20), style: .plain, target: self, action: #selector(infoAction))
//////        self.navigationItem.rightBarButtonItems = [shareButton,infoButton]
//    }
//
//    @objc func shareAction() {
//        print("Share action here")
//    }
//
//    @objc func infoAction() {
//           print("Info action here")
//       }
//
//}

//@available(iOS 13.0, *)
//extension DoctorDetailsController : UITableViewDelegate,UITableViewDataSource{
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if tableView == self.servicesTV{
//            if isFromSearchDoctor{
//                return self.searchDoctor.doctor_service?.count ?? 0
//            }else{
//                if isfromFavourite{
//                    return self.favouriteDoctor?.hospital?.doctor_service?.count ?? 0
//                }else{
////                return self.docProfile.hospital?.first?.doctor_service?.count ?? 0
//                }
//            }
//
//
//        }else if tableView == self.specilizationTV{
//                return self.speciality.count
//        }else if tableView == self.timingTableView{
//            if isFromSearchDoctor{
//                if self.searchDoctor.timing?.count ?? 0 == 0{
//                    return 0
//                }
//                return 1//self.searchDoctor.timing?.count ?? 0
//            }else{
//                if isfromFavourite{
//                    if self.searchDoctor.timing?.count ?? 0 == 0{
//                        return 0
//                    }
//                    return 1//(self.favouriteDoctor?.hospital?.timing?.count ?? 0)
//                }else{
//                    if self.searchDoctor.timing?.count ?? 0 == 0{
//                        return 0
//                    }
//                return 1//(self.docProfile.hospital?.first?.timing?.count ?? 0)
//                }
//            }
//        }
//        else{
//            return 0
//        }
//    }
//
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if isFromSearchDoctor{
//        let cell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.ServiceSpecializationCell) as! ServiceSpecializationCell
//
//        if tableView == self.servicesTV{
////            cell.serviceLbl.text = self.searchDoctor.doctor_service?[indexPath.row].service?.name ?? ""
//        }else if tableView == self.specilizationTV{
//            cell.serviceLbl.text = self.speciality[indexPath.row]
//        }else if tableView == self.timingTableView{
//            if let timing : Timing = self.searchDoctor.timing?[indexPath.row]{
//                cell.dotImg.isHidden = false
//                cell.serviceLbl.text = "\(timing.start_time ?? ""):\(timing.end_time ?? "")"
////                self.setSeatchCountLbl(label: cell.serviceLbl, day: "\(timing.day ?? "") - " , timing: "\(timing.start_time ?? "") : \(timing.end_time ?? "")")
//            }
//        }
//
//        self.viewDidLayoutSubviews()
//        self.view.layoutIfNeeded()
//        return cell
//        }
//        else{
//            if isfromFavourite{
//                    let cell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.ServiceSpecializationCell) as! ServiceSpecializationCell
//
//                    if tableView == self.servicesTV{
////                        cell.serviceLbl.text = self.favouriteDoctor?.hospital?.doctor_service?[indexPath.row].service?.name ?? ""
////                        cell.lblLeadConstraints.constant = 10
//
//                    }else if tableView == self.specilizationTV{
//                        cell.serviceLbl.text = self.speciality[indexPath.row]
//                    }else if tableView == self.timingTableView{
//                        if let timing : Timing = self.favouriteDoctor?.hospital?.timing?[indexPath.row]{
////                            cell.dotImg.isHidden = true
////                            cell.lblLeadConstraints.constant = -50
//                            cell.serviceLbl.text = "\(timing.start_time ?? ""):\(timing.end_time ?? "")"
////                            self.setSeatchCountLbl(label: cell.serviceLbl, day: "\(timing.day ?? "") -" , timing: "\(timing.start_time ?? "") : \(timing.end_time ?? "")")
//                        }
//                    }
//
//                    self.viewDidLayoutSubviews()
//                    self.view.layoutIfNeeded()
//                    return cell
//            }else{
//        let cell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.ServiceSpecializationCell) as! ServiceSpecializationCell
//
//        if tableView == self.servicesTV{
////            cell.serviceLbl.text = self.docProfile.hospital?.first?.doctor_service?[indexPath.row].service?.name ?? ""
////            cell.lblLeadConstraints.constant = 10
//
//        }else if tableView == self.specilizationTV{
//            cell.serviceLbl.text = self.speciality[indexPath.row]
//        }else if tableView == self.timingTableView{
//            if let timing : Timing = self.docProfile.hospital?.first?.timing?[indexPath.row]{
//                cell.dotImg.isHidden = true
////                cell.lblLeadConstraints.constant = -10
//                cell.serviceLbl.text = "\(timing.start_time ?? "") : \(timing.end_time ?? "")"
////                self.setSeatchCountLbl(label: cell.serviceLbl, day: "\(timing.day ?? "") -" , timing: "\(timing.start_time ?? "") : \(timing.end_time ?? "")")
//            }
//        }
//
//        self.viewDidLayoutSubviews()
//        self.view.layoutIfNeeded()
//        return cell
//        }
//        }
//
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if tableView == self.timingTableView{
//            return 20
//        }else{
//        return 40
//        }
//    }
////
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        let footerView = createViewAllButtion()
//        footerView.addTap {
//
//            self.push(id: Storyboard.Ids.ServiceListViewController, animation: true)
//
//        }
//        return footerView
//    }
//
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        if isFromSearchDoctor {
//        if tableView == self.servicesTV{
//            if (self.searchDoctor.doctor_service?.count ?? 0) > 4{
//                return 40
//            }else{
//                return 0
//            }
//        }else if tableView == self.specilizationTV{
//
//            if speciality.count > 4{
//                return 40
//            }else{
//                return 0
//            }
//        }else if tableView == self.timingTableView{
//
//            return UITableView.automaticDimension
//
//            }
//
//
//
//    }else{
//            if isfromFavourite{
//                if tableView == self.servicesTV{
//                    if (self.favouriteDoctor?.hospital?.doctor_service?.count ?? 0) > 4{
//                        return 40
//                    }else{
//                        return 0
//                    }
//                }else if tableView == self.specilizationTV{
//
//                    if speciality.count > 4{
//                        return 40
//                    }else{
//                        return 0
//                    }
//
//                }
//                else if tableView == self.timingTableView{
//
//                    return UITableView.automaticDimension
//
//                    }
//
//            }else {
//        if tableView == self.servicesTV{
//            if (self.docProfile.hospital?.first?.doctor_service?.count ?? 0) > 4{
//                return 40
//            }else{
//                return 0
//            }
//        }else if tableView == self.specilizationTV{
//
//            if speciality.count > 4{
//                return 40
//            }else{
//                return 0
//            }
//
//        }
//        else if tableView == self.timingTableView{
//
//            return UITableView.automaticDimension
//
//            }
//
//    }
//        }
//        return 0
//    }
//
//    func setupTableViewCell(){
//        self.servicesTV.registerCell(withId: XIB.Names.ServiceSpecializationCell)
//        self.specilizationTV.registerCell(withId: XIB.Names.ServiceSpecializationCell)
//        self.timingTableView.registerCell(withId: XIB.Names.ServiceSpecializationCell)
//    }
//
//
//    func createViewAllButtion() -> UIView{
//        let customView = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
//        customView.backgroundColor = .clear
//        let viewLabel = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 30))
//
//
//        Common.setFontWithType(to: viewLabel, size: 12, type: .regular)
//        //Common.setFont(to: viewLabel,isTitle : true)
//        viewLabel.text = "View all >"
//        viewLabel.textAlignment = .center
//        viewLabel.textColor = UIColor(named: "AppBlueColor")
//        customView.addSubview(viewLabel)
//        return customView
//    }
//
//
//    func setSeatchCountLbl(label : UILabel,day : String , timing : String){
//        let attrs1 = [NSAttributedString.Key.font : UIFont.init(name: FontCustom.regular.rawValue, size: 14), NSAttributedString.Key.foregroundColor : UIColor.red.withAlphaComponent(0.8)]
//
//        let attrs2 = [NSAttributedString.Key.font : UIFont.init(name: FontCustom.regular.rawValue, size: 14), NSAttributedString.Key.foregroundColor : UIColor(named: "TextBlackColor")]
//
//        let attributedString1 = NSMutableAttributedString(string: "\(day) ", attributes:attrs1 as [NSAttributedString.Key : Any])
//
//        let attributedString2 = NSMutableAttributedString(string: timing, attributes:attrs2 as [NSAttributedString.Key : Any])
//
//        attributedString1.append(attributedString2)
//        label.attributedText = attributedString1
//    }
//}
//
//
//@available(iOS 13.0, *)
//extension DoctorDetailsController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        if isFromSearchDoctor{
//        if collectionView == self.photosCV{
//            return  10
//        }else if collectionView == self.reviewsCV{
//            return  self.searchDoctor.feedback?.count ?? 0
//        }
//        return  0
//        }else{
//        if collectionView == self.photosCV{
//            return  10
//        }else if collectionView == self.reviewsCV{
//            return  self.docProfile.hospital?.first?.feedback?.count ?? 0
//        }
//        return  0
//        }
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        if collectionView == self.photosCV{
//            let photoCell = collectionView.dequeueReusableCell(withReuseIdentifier: XIB.Names.PhotosCell, for: indexPath) as! PhotosCell
//            return photoCell
//        }else if collectionView == self.reviewsCV{
//            let reviewCell = collectionView.dequeueReusableCell(withReuseIdentifier: XIB.Names.ReviewCell, for: indexPath) as! ReviewCell
//            if isFromSearchDoctor{
//            self.populateFeedBackData(cell: reviewCell, feedback: self.searchDoctor.feedback?[indexPath.row] ?? Feedback())
//            } else{
//                self.populateFeedBackData(cell: reviewCell, feedback: self.docProfile.hospital?.first?.feedback?[indexPath.row] ?? Feedback())
//            }
//            return reviewCell
//        }
//
//        let photoCell = collectionView.dequeueReusableCell(withReuseIdentifier: XIB.Names.PhotosCell, for: indexPath) as! PhotosCell
//        return photoCell
//    }
//
//    func populateFeedBackData(cell : ReviewCell , feedback : Feedback){
//        cell.nameLbl.text = feedback.patient?.first_name ?? ""
//        cell.dateLbl.text = dateConvertor(feedback.created_at ?? "", _input: .date_time, _output: .DM)
//        cell.deasesLbl.text = feedback.visited_for ?? ""
//        cell.reviewLbl.text = feedback.comments ?? ""
//
//        if (feedback.experiences ?? "") == "LIKE"{
//            cell.thumpsupImg.setImage("like")
//        }else{
//            cell.thumpsupImg.setImage("dislike")
//        }
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        if collectionView == self.photosCV{
//            return CGSize(width: 100, height: 100)
//        }else if collectionView == self.reviewsCV{
//            return CGSize(width:  (UIScreen.main.bounds.width / 2) + 10, height: 120)
//        }
//
//        return CGSize(width: 200, height: 100)
//    }
//
//    func setupCollectionViewCell(){
//        self.photosCV.registerCell(withId: XIB.Names.PhotosCell)
//        self.reviewsCV.registerCell(withId: XIB.Names.ReviewCell)
//        self.photosCV.reloadData()
//         self.reviewsCV.reloadData()
//    }
//    func checkToday(day:String){
//        switch day {
//        case "Monday":
//            self.labelMonday.textColor = .red
//            break
//        case "Tuesday":
//            self.labelTue.textColor = .red
//            break
//        case "Wednesday":
//            self.labelWed.textColor = .red
//            break
//        case "Thursday":
//            self.labelThu.textColor = .red
//           break
//        case "Friday":
//            self.labelFri.textColor = .red
//            break
//        case "Saturday":
//            self.labelSat.textColor = .red
//            break
//        case "Sunday":
//            self.labelSun.textColor = .red
//            break
//
//        default:
//        break
//        }
//    }
//}
//
//
////Api calls
//@available(iOS 13.0, *)
//extension DoctorDetailsController : DoctorPresenterToDoctorViewProtocol{
//    func showSuccess(api: String, dataArray: [Mappable]?, dataDict: Mappable?, modelClass: Any) {
//        switch String(describing: modelClass) {
//            case model.type.CommonModel:
//                let data = dataDict as? CommonModel
//                if (data?.message ?? "") == "Favourite Doctor Added"{
//                    self.btnFavourite.setImage(UIImage(named: "love_red"), for: .normal)
//                    if isFromSearchDoctor{
//                    self.searchDoctor.is_favourite = "true"
//                    }else{
//                        self.docProfile.hospital?[0].is_favourite = "true"
//                    }
//                }else{
//                    self.btnFavourite.setImage(UIImage(named: "love"), for: .normal)
//                                        if isFromSearchDoctor{
//                    self.searchDoctor.is_favourite = "false"
//                    }else{
//                        self.docProfile.hospital?[0].is_favourite = "false"
//                    }
//                }
//                break
//
//            default: break
//
//        }
//    }
//
//    func showError(error: CustomError) {
//
//    }
//
////    func addRemoveFav(patient_id : String , doctor_id : String){
////        self.presenter?.HITAPI(api: Base.fav.rawValue, params: ["patient_id" : patient_id , "doctor_id" : doctor_id], methodType: .POST, modelClass: CommonModel.self, token: true)
////    }
//
//}
//extension NSLayoutConstraint {
//    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
//        return NSLayoutConstraint(item: self.firstItem!, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
//    }
//}
//
//extension Date {
//    func dayOfWeek() -> String? {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "EEEE"
//        return dateFormatter.string(from: self).capitalized
//        // or use capitalized(with: locale) if you want
//    }
//}
//



class DoctorDetailsControllers: UIViewController {
    
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var backgroundImageVIew: UIImageView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var likeSymbolImage: UIImageView!
    @IBOutlet weak var doctorName: UILabel!
    @IBOutlet weak var backImg: UIView!
    @IBOutlet weak var availableTimeLabel: UILabel!
    @IBOutlet weak var doctorQualificationLabel: UILabel!
    @IBOutlet weak var expirenenceLabel: UILabel!
    @IBOutlet weak var buttonplayVideo: UIButton!
    @IBOutlet weak var medicalVerifiedImage: UIImageView!
    @IBOutlet weak var medicalVerfiedLabel: UILabel!
    @IBOutlet weak var counsaltatntFeeLabel: UILabel!
    @IBOutlet weak var availabilityLabel: UILabel!
    @IBOutlet weak var mondayLabel: UILabel!
    @IBOutlet weak var tuesdayLabel: UILabel!
    @IBOutlet weak var wednesdayLabel: UILabel!
    @IBOutlet weak var thursdayLabel: UILabel!
    @IBOutlet weak var fridayLabel: UILabel!
    @IBOutlet weak var saturdayLabel: UILabel!
    @IBOutlet weak var sundayLabel: UILabel!
    @IBOutlet weak var serviceTableView: UITableView!
    @IBOutlet weak var serviceTitleLabel: UILabel!
    @IBOutlet weak var timeTableView: UITableView!
    @IBOutlet weak var specialationTitleLabel: UILabel!
    @IBOutlet weak var specializationTableView: UITableView!
    @IBOutlet weak var locationTitleLabel: UILabel!
    @IBOutlet weak var hospitalLocationLabel: UILabel!
    @IBOutlet weak var locationImage: UIImageView!
    @IBOutlet weak var bookButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    
    var docatorDetails : ResponseDatasssss?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backImg.addTap {
            self.navigationController?.popViewController(animated: true)
        }
        print("Daaataa",docatorDetails)
        setValues()
        setDelegate()
        
                self.bookButton.addTap {
                    let vc = BookingViewController.initVC(storyBoardName: .doctor, vc: BookingViewController.self, viewConrollerID: Storyboard.Ids.BookingViewController)
                    vc.doctorID = self.docatorDetails?.provider_id ?? 0
        //                     vc.docProfile = self.docProfile
        //                     vc.searchDoctor = self.searchDoctor
                             vc.isFromSearch = true
        //                     vc.favouriteDoctor = self.favouriteDoctor
                    vc.categoryId = self.docatorDetails?.service?.service_category_id ?? 0
                             self.push(from: self, ToViewContorller: vc)
        
                }
    }
    
    
}
extension DoctorDetailsControllers {
    func setValues(){
        
        let name = (docatorDetails?.provider?.first_name ?? "") + (docatorDetails?.provider?.last_name ?? "")
        self.doctorName.text = name 
        timeTableView.contentSize.height = 90
        self.doctorQualificationLabel.text = docatorDetails?.qualification ?? ""
        if (docatorDetails?.time_slot?.count ?? 0) > 0{
            let timing = (docatorDetails?.time_slot?[0].timing?[0].start_time ?? "") + "-" + (docatorDetails?.time_slot?[0].timing?[0].end_time ?? "")
            self.availableTimeLabel.text = timing
        }
        self.expirenenceLabel.text = "\(docatorDetails?.experience ?? 0)" + "Years"
        self.counsaltatntFeeLabel.text =  "Consultant fee" +   "   \(docatorDetails?.consultation_fee ?? 0)"
        self.likeButton.setImage(UIImage(named: "Heartt"), for: .normal)
        self.medicalVerifiedImage.image = UIImage(named: "Verified")
       
        let url = URL(string:docatorDetails?.provider?.picture ?? "")
        let data = try? Data(contentsOf: url!)
        self.profileImage.image = UIImage(data: data!)
        self.profileImage.layer.cornerRadius = self.profileImage.frame.width / 2
        self.profileImage.borderLineWidth = 1
        self.profileImage.borderColor = .appGreenColor
        self.locationImage.image = UIImage(named: "googl_ED")
    }
    
    func setDelegate(){
        self.serviceTableView.delegate = self
        self.serviceTableView.dataSource = self
        
        self.specializationTableView.delegate = self
        self.specializationTableView.dataSource = self
        
        self.timeTableView.delegate = self
        self.timeTableView.dataSource = self
        
        self.serviceTableView.register(UINib(nibName: "ServiceSpecializationCell", bundle: nil), forCellReuseIdentifier: "ServiceSpecializationCell")
        self.specializationTableView.register(UINib(nibName: "ServiceSpecializationCell", bundle: nil), forCellReuseIdentifier: "ServiceSpecializationCell")
        self.timeTableView.register(UINib(nibName: "ServiceSpecializationCell", bundle: nil), forCellReuseIdentifier: "ServiceSpecializationCell")
        
    }
}

extension DoctorDetailsControllers : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
       
        if tableView == serviceTableView {
            return 1
        }else if tableView == timeTableView {
            return 1
        }else if tableView == specializationTableView {
            return 1
        }
        else {
            return 0
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == serviceTableView {
            return 1
        }else if tableView == timeTableView {
            return 1
        }else if tableView == specializationTableView {
            return 1
        }
        else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        if tableView == timeTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceSpecializationCell") as! ServiceSpecializationCell
            if (docatorDetails?.time_slot?.count ?? 0 ) > 0{
            let timing = (docatorDetails?.time_slot?[indexPath.row].timing?[indexPath.row].start_time ?? "") + "-" + (docatorDetails?.time_slot?[indexPath.row].timing?[indexPath.row].end_time ?? "")
            cell.serviceLbl.text = timing
            }
            return cell
        }
        else if tableView == specializationTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceSpecializationCell", for: indexPath) as! ServiceSpecializationCell
//            dequeueReusableCell(withIdentifier: "ServiceSpecializationCell") as! ServiceSpecializationCell
            cell.serviceLbl.text = docatorDetails?.specializations ?? ""
            return cell
        }
        else if tableView == serviceTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceSpecializationCell") as! ServiceSpecializationCell
            cell.serviceLbl.text = docatorDetails?.service?.service_subcategory_name ?? ""
            return cell
        }else {
            return UITableViewCell()
        }
    }
}
