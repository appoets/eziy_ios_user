//
//  AppointmentListViewController.swift
//  GoJekUser
//
//  Created by Santhosh on 02/05/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit

class AppointmentListViewController: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var appointmentList : AppointmentList?
   
    override func viewDidLoad() {
        super.viewDidLoad()
        backButton.addTarget(self, action: #selector(backButtonTaped), for: .touchUpInside)
        listTableView.register(nibName: "AppointmentListTableViewCell")
        listTableView.delegate = self
        listTableView.dataSource = self
        getList()
     }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @objc func backButtonTaped(){
        self.navigationController?.popViewController(animated: true)
    }
 
}

extension AppointmentListViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appointmentList?.responseData?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AppointmentListTableViewCell") as! AppointmentListTableViewCell
        cell.dateLabel.text = appointmentList?.responseData?.data?[indexPath.row].scheduled_at ?? ""
        cell.dayLabel.text = appointmentList?.responseData?.data?[indexPath.row].consult_time ?? ""
        cell.nameLabel.text = "\(appointmentList?.responseData?.data?[indexPath.row].provider?.first_name ?? "") \(appointmentList?.responseData?.data?[indexPath.row].provider?.last_name ?? "")"
        cell.viewButton.tag = indexPath.row
        cell.viewButton.addTarget(self, action: #selector(viewButtonTapped(sender:)), for: .touchUpInside)
        cell.idLabel.text = appointmentList?.responseData?.data?[indexPath.row].booking_id ?? ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    @objc func viewButtonTapped(sender:UIButton) {
        let vc = storyboard?.instantiateViewController(identifier: "AppointmentDetailViewController") as! AppointmentDetailViewController
        vc.appointmnetDteail = appointmentList?.responseData?.data?[sender.tag]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}


extension AppointmentListViewController : DoctorPresenterToDoctorViewProtocol {
    func getList(){
        doctorPresenter?.getAppointmentList()
        
    }
    func getAppointmentList(getAppointmentList: AppointmentList) {
        appointmentList = getAppointmentList
        listTableView.reloadData()
        
    }
}

