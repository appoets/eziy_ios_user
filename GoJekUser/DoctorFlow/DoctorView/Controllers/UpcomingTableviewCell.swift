//
//  UpcomingTableviewCellTableViewCell.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 31/01/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit

class UpcomingTableviewCell: UITableViewCell {
    
    
    @IBOutlet weak var labelDate: UILabel!
    
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labeldoctorName: UILabel!
    
    @IBOutlet weak var labelSubtitle: UILabel!
    @IBOutlet weak var buttonCancel: UIButton!
    @IBOutlet weak var labelStatus: PaddingLabel!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var makeVideoCallButton: UIButton!
    
    @IBOutlet weak var statusWidth: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.localize()
    }
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.roundCorners(cornerRadius: 6, cornerView: dateView)
//        self.roundCorners(cornerRadius: 6, cornerView: labelStatus)

//        self.dateView.roundCorners(corners: [.topRight,.bottomRight], radius: 6)
//        self.labelStatus.roundCorners(corners: [.bottomLeft,.topLeft], radius: 6)
        self.dateView.borderLineWidth = 1.0
        
        
    }
    
    private func localize(){
        self.buttonCancel.setTitle(Constants.string.Cancel.localize(), for: .normal)
    }

    func roundCorners(cornerRadius: Double,cornerView:UIView) {
        cornerView.layer.cornerRadius = CGFloat(cornerRadius)
        cornerView.clipsToBounds = true
        cornerView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
    }
    
    
    
}
