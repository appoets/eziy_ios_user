//
//  SearchViewController.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 31/01/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import ObjectMapper

var searchTextWord = String()
var isFromSearchcontroller = false

@available(iOS 13.0, *)
class SearchViewController: UIViewController, UISearchBarDelegate,UISearchDisplayDelegate {
    
    @IBOutlet weak var searchBar : UISearchBar!
    @IBOutlet weak var backbutton: UIView!
    @IBOutlet weak var searchResult : UITableView!
    @IBOutlet weak var searchCountLbl : UILabel!
    
    var searchDoctors : [ResponseDatasssss] = []{
        didSet{
            searchResult.reloadInMainThread()
        }
    }
    var isUpdate : Bool = false
    var isFromSearch : Bool = false
    lazy var loader : UIView = {
        return createActivityIndicator(self.view.window ?? self.view)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initailSetup()
        searchBar.delegate = self
        searchResult.delegate = self
        searchResult.dataSource = self
        self.doctorPresenter?.getsearchdeatil(count: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //        self.navigationController?.isNavigationBarHidden = false
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        self.isUpdate = false
        //        self.isFromSearch = false
        //        self.searchDoctors.removeAll()
    }
    
    func initailSetup(){
        self.setupTableViewCell()
        self.backbutton.addTap {
            self.popOrDismiss(animation: true)
        }
        Common.setFontWithType(to: self.searchCountLbl, size: 14, type: .regular)
    }
    func setupTableViewCell(){
        self.searchResult.registerCell(withId: XIB.Names.SearchCell)
    }
    
    
    
    @IBAction private func filterAction(_sender:UIButton){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: Storyboard.Ids.FilterViewController) as! FilterViewController
        vc.isFromSearch = true
        //        vc.onClickSearch = { (catagoryID,availablity,gender,price) in
        //            if availablity == "AvailableAllDAY"{
        //                let url = "/api/patient/doctor_catagory/\(catagoryID)?gender=\(gender)&fees=\(price)"
        //                self.presenter?.HITAPI(api: url, params: nil, methodType: .GET, modelClass: DoctorsDetailModel.self, token: true)
        //                self.isUpdate = false
        //            }else{
        //            let url = "/api/patient/doctor_catagory/\(catagoryID)?availability_type=\(availablity)&gender=\(gender)&fees=\(price)"
        //            self.presenter?.HITAPI(api: url, params: nil, methodType: .GET, modelClass: DoctorsDetailModel.self, token: true)
        //            }
        //            self.isUpdate = false
        //            vc.dismiss(animated: true, completion: nil)
        //
        //
        //        }
        let navController = UINavigationController(rootViewController: vc)
        self.navigationController?.present(navController, animated: true, completion: nil)
    }
    
    
    
    // MARK:- set Multiple color for single lable
    
    func setSeatchCountLbl(resultCount : Int = 0){
        let attrs1 = [NSAttributedString.Key.font : UIFont.init(name: FontCustom.regular.rawValue, size: 14), NSAttributedString.Key.foregroundColor : UIColor(named: "TextBlackColor")]
        
        let attrs2 = [NSAttributedString.Key.font : UIFont.init(name: FontCustom.regular.rawValue, size: 14), NSAttributedString.Key.foregroundColor : UIColor(named: "AppDarkColor")]
        
        let attributedString1 = NSMutableAttributedString(string:Constants.string.searchDoctors.localize(), attributes:attrs1 as [NSAttributedString.Key : Any])
        
        let attributedString2 = NSMutableAttributedString(string:"(\(resultCount) \(Constants.string.resultsFound.localize())", attributes:attrs2 as [NSAttributedString.Key : Any])
        
        attributedString1.append(attributedString2)
        self.searchCountLbl.attributedText = attributedString1
    }
    
}



@available(iOS 13.0, *)
extension SearchViewController : UITableViewDelegate,UITableViewDataSource{
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchDoctors.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.SearchCell) as! SearchCell
        let search : ResponseDatasssss = self.searchDoctors[indexPath.row]
        
        cell.docNameLbl.text = "\(search.provider?.first_name ?? "" )\(search.provider?.last_name ?? "" )"
        cell.docSpecialtLbl.text = search.specializations ?? "".capitalized
        if search.provider?.picture != nil{
            let url = URL(string:search.provider?.picture ?? "")
            let data = try? Data(contentsOf: url!)
            cell.docImage.image = UIImage(data: data!)
        }
        
        return cell
        
    }
    
//    func SearchCellAction(cell : SearchCell,indexPathForID:String?){
//        cell.contentView.addTap {
//            self.push(id: Storyboard.Ids.DoctorDetailsController, animation: true)
//        }
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(identifier: "DoctorDetailsControllers") as! DoctorDetailsControllers
        vc.docatorDetails = searchDoctors[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.searchDoctors.count >= 10 {
            if !isFromSearch{
                if indexPath.row == self.searchDoctors.count - 3{
                    isUpdate = true
                    let count = self.searchDoctors[self.searchDoctors.count - 1].id ?? 0
                    if isFromSearch == true{
                        print("Nothiinnggg")
                    }else {
                      //  self.doctorPresenter?.getsearchdeatil(count: count )
                    }
                    
                    
                }
            }
        }
    }
    
    
}



////Api calls
//@available(iOS 13.0, *)
@available(iOS 13.0, *)
extension SearchViewController : DoctorPresenterToDoctorViewProtocol{
    
    func getsearchdeatil(searchmodel: searchmodel) {
        self.searchDoctors = searchmodel.responseData?.data ?? []
        
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        isFromSearchcontroller = true
        self.isFromSearch = true
        searchTextWord = searchBar.text ?? ""
        self.doctorPresenter?.getsearchdeatil(count: 0)
        
        
    }
}



