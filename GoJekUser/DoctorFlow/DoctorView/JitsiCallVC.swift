//
//  JitsiCallVC.swift
//  GoJekUser
//
//  Created by Shyamala's MacBook Pro on 08/09/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import JitsiMeetSDK
import ObjectMapper
import Alamofire

class JitsiCallVC: UIViewController {

    var isFromReceiveCall = false
    var patientId = ""
    //var hospitalId = 0
    var roomid = String()
    var room_id : String = ""
    var hospital_id : String = ""
    var patient_id : String = ""
    var id : String = ""
    var doctorViewRating : ((String)->())?
    
    @IBOutlet var joinBtn : UIButton!

    
    fileprivate var pipViewCoordinator: PiPViewCoordinator?
    fileprivate var jitsiMeetView: JitsiMeetView?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let defaultOptions = JitsiMeetConferenceOptions.fromBuilder { (builder) in
           
            builder.serverURL = URL(string: "https://meet.jit.si")
             builder.setFeatureFlag("welcomepage.enabled", withValue: false)
           
        }
        
        JitsiMeet.sharedInstance().defaultConferenceOptions = defaultOptions
    
        connetCall()
    }
    
    
    override func viewWillTransition(to size: CGSize,
                                     with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        let rect = CGRect(origin: CGPoint.zero, size: size)
       
        pipViewCoordinator?.resetBounds(bounds: rect)
    }
    
    
 
    @IBAction private func callJoin(sender: UIButton){
        
        
    }
    
    func connetCall(){
        let room: String = self.room_id
        guard room.count > 1 else {
            return
        }
        let jitsiMeetView = JitsiMeetView()
        jitsiMeetView.delegate = self
        self.jitsiMeetView = jitsiMeetView
        let options = JitsiMeetConferenceOptions.fromBuilder { (builder) in
            builder.room = room
        }
        jitsiMeetView.join(options)
        pipViewCoordinator = PiPViewCoordinator(withView: jitsiMeetView)
        pipViewCoordinator?.configureAsStickyView(withParentView: view)
        jitsiMeetView.alpha = 0
        pipViewCoordinator?.show()
        
        
    }
    
    fileprivate func cleanUp() {
        jitsiMeetView?.removeFromSuperview()
        jitsiMeetView = nil
        pipViewCoordinator = nil
    }
}

extension JitsiCallVC : DoctorPresenterToDoctorViewProtocol{
    func initiateCall(){
    
        
        let param : Parameters = ["room_id" : room_id,
                                  "video" : "1",
                                  "hospital_id" : hospital_id,
                                  "patient_id" : patient_id,
                                  "id" : id,
                                  "push_to" : "doctor"]
//        self.homePresenter?.chatFiler(param: param)
    }
    
    
    func endCall(){
        let param : Parameters = ["id" : id]
        self.doctorPresenter?.endCall(param: param)
    }
    
    func acceptRequestSuccess(acceptRequestEntity: HomeEntity) {
        print("Respone")
    }
}

extension JitsiCallVC: JitsiMeetViewDelegate {
    
    func ready(toClose data: [AnyHashable : Any]!) {
        self.pipViewCoordinator?.hide() { _ in
            self.cleanUp()
            self.doctorViewRating?("Completed")
            self.dismiss(animated: true)
        }
    }
    
    func enterPicture(inPicture data: [AnyHashable : Any]!) {
        self.initiateCall()
        self.pipViewCoordinator?.enterPictureInPicture()
    }
}

