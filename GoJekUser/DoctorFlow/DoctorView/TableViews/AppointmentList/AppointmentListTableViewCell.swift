//
//  AppointmentListTableViewCell.swift
//  GoJekUser
//
//  Created by Santhosh on 02/05/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit

class AppointmentListTableViewCell: UITableViewCell {

    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var viewButton: UIButton!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
