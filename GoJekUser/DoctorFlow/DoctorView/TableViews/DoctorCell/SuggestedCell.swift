//
//  SuggestedCellTableViewCell.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 31/01/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit

class SuggestedCell : UITableViewCell{
    
    @IBOutlet weak var titleLbl : UILabel!
    @IBOutlet weak var offerPriceLbl : UILabel!
    @IBOutlet weak var offerPriceView : UIView!
    @IBOutlet weak var priceLbl : UILabel!
    @IBOutlet weak var allSplistView : UIView!
    @IBOutlet weak var allSplistLbl : UILabel!
    @IBOutlet weak var suggetionView : UIView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupFont()
        self.selectionStyle = .none
    }
    
    
    func setupFont(){
        self.allSplistLbl.text = Constants.string.allspecialities.localize()
//        Common.setFont(to: self.titleLbl, isTitle: true, size: 20)
//        Common.setFont(to: self.offerPriceLbl, isTitle: false, size: 15)
//        Common.setFont(to: self.priceLbl, isTitle: false, size: 18)
//        Common.setFont(to: self.allSplistLbl, isTitle: true, size: 20)
    }
}
