//
//  TimeCell.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 21/02/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit

class TimeCell: UICollectionViewCell {
    
    @IBOutlet weak var timeLbl : UILabel!
    @IBOutlet weak var meridianLbl : UILabel!
    @IBOutlet weak var timeView : UIView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.timeView.makeRoundedCorner()
        
        
    }

}

