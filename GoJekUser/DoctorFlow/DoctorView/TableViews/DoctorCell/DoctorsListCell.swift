//
//  DoctorsListCell.swift
//  GoJekUser

//  Created by Nivedha's MacBook Pro on 09/02/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit

class DoctorsListCell: UICollectionViewCell {
    
    @IBOutlet weak var cellOuterView: UIView!
//    @IBOutlet weak var accountContentLabel: UILabel!
//    @IBOutlet weak var accountImage: UIImageView!
    
    @IBOutlet weak var imgDoctor: UIImageView!
    @IBOutlet weak var LabelSpeciality: UILabel!
    @IBOutlet weak var LabelName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.LabelName.text = ""
//        Common.setFontWithType(to: LabelSpeciality, size: 10, type: .regular)
        Common.setFontWithType(to: LabelName, size: 13.5, type: .meduim )

//            Common.setFont(to: labelSpeciality, isTitle: false, size: 12)
//            Common.setFont(to: labelName, isTitle: true, size: 16)
        imgDoctor.makeRoundedCorner()
    }
    
//    private func setDarkMode(){
//        self.contentView.backgroundColor = .boxColor
//     }
    func setValues(name:String, imageString: String) {
        
        imgDoctor.image = UIImage(named: imageString)
        imgDoctor.imageTintColor(color1: .white)
    }
    
//    override func layoutSubviews() {
//self.bgView.addShadow(color: .lightGray, opacity: 0.4, offset: CGSize(width: -0.5,height: 1.0), radius: 2, rasterize: false, maskToBounds: false)
//        super.layoutSubviews()
//        self.cellOuterView.setCornerRadius()
//    }
    

}
