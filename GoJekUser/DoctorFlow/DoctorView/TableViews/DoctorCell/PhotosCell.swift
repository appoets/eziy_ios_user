//
//  PhotosCell.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 31/01/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit

class PhotosCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImage : UIImageView!
    @IBOutlet weak var photoView : UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        self.photoView.clipsToBounds = true
//        self.photoView.layer.masksToBounds = true
        DispatchQueue.main.async {
            self.photoView.layer.cornerRadius = self.photoView.frame.width/2

        }
    }
}


