//
//  AppointmentDetailTableViewCell.swift
//  GoJekUser
//
//  Created by Santhosh on 04/05/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit

class AppointmentDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
