//
//  ServiceSpecializationCell.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 22/02/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit

class ServiceSpecializationCell: UITableViewCell {

    @IBOutlet weak var serviceLbl : UILabel!
    @IBOutlet weak var dotImg : UIImageView!
    @IBOutlet weak var outerView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.setTextFonts()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    private func setTextFonts() {
           Common.setFont(to: serviceLbl)
    }
    
}

