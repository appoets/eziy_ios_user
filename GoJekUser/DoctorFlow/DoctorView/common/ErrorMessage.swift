//
//  ErrorMessage.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 21/02/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import Foundation

struct ErrorMessage {
    
    static let list = ErrorMessage()
    
    let serverError = "Server Could not be reached.Try Again"
    let notReachable = "The Internet connection appears to be offline."
    let enterEmail = "Enter Email Id"
    let enterValidEmail = "Enter Valid Email Id"
    let enterPassword = "Enter Password"
    let enterConfirmPassword = "Enter Confirm Password"
    let enterName = "Enter Your First Name"
    let enterLastName = "Enter Last Name"
    let enterPhoneNumber = "Enter Phone Number"
    let enterCity = "Enter city"
    let agreeTheTerms = "Agree The Terms and Condition"
    let enterOTP = "Enter OTP"
    let enterNewPwd = "Enter New Password"
    let enterOldPassword = "Enter Old Password"
    let enterConfirmPwd = "Enter Confirm Password"
    let EnterCountry = "Enter Country code"
    let enterPatientName = "Enter Patient Name"
    let enterDetails =  "Enter Details"
    let enterInstruction = "Enter Instruction/Description"
    let enterDocterConsulted =  "Enter Doctor Consulted"
    let enterImage =  "Add Prescription Image"
    let enterRemainder = "Enter Remainder Text"
    let enterDate = "Enter Date"
    let enterTime = "Enter Time"
    let enterValidAmount = "Enter Valid amount to add wallet"
    let enterGender = "Enter Gender"
    let enterDOB = "Enter DOB"
    let enterBloodGroup = "Enter Blood Group"
    let enterMartialStatus = "Enter Martial Status"
    let enterHeight = "Enter Height"
    let enterWeight = "Enter Weight"
    let enterEmergencyContact = "Enter Emergency Contact"
    let enterLocation = "Enter Location"
    let enterAllergies = "Enter Allergies"

}

