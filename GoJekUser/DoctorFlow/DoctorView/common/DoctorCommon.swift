//
//  Global.swift
//  User
//
//  Created by imac on 1/1/18.
//  Copyright © 2018 Appoets. All rights reserved.
//

import UIKit
import Foundation
import PopupDialog
import AudioToolbox


//MARK:- Show Alert
internal func showAlert(message : String?, handler : ((UIAlertAction) -> Void)? = nil)->UIAlertController{
    
    let alert = UIAlertController(title: APPConstant.appName, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title:  "Ok", style: .default, handler: handler))
    alert.view.tintColor = .doctorColor
    return alert
}


//MARK:- Show Alert With Action

func showAlert(message : String?, okHandler : (()->Void)?, fromView : UIViewController){
    
    /* let alert = UIAlertController(title: AppName,
     message: message,
     preferredStyle: .alert)
     let okAction = UIAlertAction(title: Constants.string.OK, style: .default, handler: okHandler)
     
     let cancelAction = UIAlertAction(title: Constants.string.Cancel, style: .destructive, handler: nil)
     
     alert.addAction(okAction)
     alert.addAction(cancelAction)
     alert.view.tintColor = .primary */
    
    let alert = PopupDialog(title: message, message: nil)
    let okButton =  PopupDialogButton(title: "OK".localize(), action: {
        okHandler?()
        alert.dismiss()
    })
    alert.transitionStyle = .zoomIn
    alert.addButton(okButton)
    fromView.present(alert, animated: true, completion: nil)
    
}

func showAlert(message : String?, btnHandler : ((Int)->Void)?, fromView : UIViewController){
    
    /* let alert = UIAlertController(title: AppName,
     message: message,
     preferredStyle: .alert)
     let okAction = UIAlertAction(title: Constants.string.OK, style: .default, handler: okHandler)
     
     let cancelAction = UIAlertAction(title: Constants.string.Cancel, style: .destructive, handler: nil)
     
     alert.addAction(okAction)
     alert.addAction(cancelAction)
     alert.view.tintColor = .primary */
    
    let alert = PopupDialog(title: message, message: nil)
    let firstButton =  PopupDialogButton(title: Constants.string.Yes.localize(), action: {
        btnHandler?(1)
        alert.dismiss()
    })
    let secondButton =  PopupDialogButton(title: Constants.string.No.localize(), action: {
        btnHandler?(2)
        alert.dismiss()
    })
    alert.transitionStyle = .zoomIn
    alert.addButton(firstButton)
    alert.addButton(secondButton)
    fromView.present(alert, animated: true, completion: nil)
    
}


//MARK:- ShowLoader

internal func createActivityIndicator(_ uiView : UIView)->UIView{
    
    let container: UIView = UIView(frame: CGRect.zero)
    container.layer.frame.size = uiView.frame.size
    container.center = CGPoint(x: uiView.bounds.width/2, y: uiView.bounds.height/2)
    container.backgroundColor = UIColor(white: 0.2, alpha: 0.3)
    
    let loadingView: UIView = UIView()
    loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
    loadingView.center = container.center
    loadingView.backgroundColor = UIColor(white:0.1, alpha: 0.7)
    loadingView.clipsToBounds = true
    loadingView.layer.cornerRadius = 10
    loadingView.layer.shadowRadius = 5
    loadingView.layer.shadowOffset = CGSize(width: 0, height: 4)
    loadingView.layer.opacity = 2
    loadingView.layer.masksToBounds = false
    loadingView.layer.shadowColor = UIColor.doctorColor.cgColor
    
    let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    actInd.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
    actInd.clipsToBounds = true
    if #available(iOS 13.0, *) {
        actInd.style = UIActivityIndicatorView.Style.large
    } else {
        // Fallback on earlier versions
    }
    
    actInd.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
    loadingView.addSubview(actInd)
    container.addSubview(loadingView)
    container.isHidden = true
    uiView.addSubview(container)
    actInd.startAnimating()
    
    return container
    
}




//internal func storeInUserDefaults(){
//
//    let data = NSKeyedArchiver.archivedData(withRootObject: User.main)
//    UserDefaults.standard.set(data, forKey: Keys.list.userData)
//    UserDefaults.standard.synchronize()
//
//    print("Store in UserDefaults--", UserDefaults.standard.value(forKey: Keys.list.userData) ?? "Failed")
//}

// Retrieve from UserDefaults
internal func retrieveUserData()->Bool{

     if UserDefaultConfig.Token.isEmpty{
        return false
    }else{
        
        return true
    }
    
    return false
    
}

// Clear UserDefaults
//internal func clearUserDefaults(){
//
//    User.main = initializeUserData()  // Clear local User Data
//    UserDefaults.standard.set(nil, forKey: Keys.list.userData)
//    UserDefaults.standard.removeVolatileDomain(forName: Bundle.main.bundleIdentifier!)
//    UserDefaults.standard.synchronize()
//    UserDefaultConfig.Token = ""
//    print("Clear UserDefaults--", UserDefaults.standard.value(forKey: Keys.list.userData) ?? "Success")
//
//}

func toastSuccess(_ view:UIView,message:NSString,smallFont:Bool,isPhoneX:Bool, color:UIColor){
    var labelView = UIView()
    if(isPhoneX){
        labelView = UILabel(frame: CGRect(x: 0,y: 0,width:view.frame.size.width, height: 60))
    }else{
        labelView = UILabel(frame: CGRect(x: 0,y: 0,width:view.frame.size.width, height: 60))
    }
    labelView.backgroundColor = color
    
    //UIColor(red: 35/255, green: 86/255, blue: 142/255, alpha: 1)
    
    
    let  toastLabel = UILabel(frame: CGRect(x: 0,y:(labelView.frame.size.height/2)-20,width:view.frame.size.width, height: labelView.frame.size.height/2))
    toastLabel.textColor = UIColor.white
    toastLabel.textAlignment = NSTextAlignment.center
    toastLabel.numberOfLines = 2
    if(smallFont){
        // toastLabel.font = UIFont.boldSystemFont(ofSize: 10)
        toastLabel.font = UIFont(name: "Avenir Next Medium", size: 14)
    }else{
        // toastLabel.font = toastLabel.font.withSize(14)
        toastLabel.font = UIFont(name: "Avenir Next Medium", size: 18)
    }
    
    labelView.addSubview(toastLabel)
    view.addSubview(labelView)
    toastLabel.text = message as String
    labelView.alpha = 1.0
    let deadlineTime = DispatchTime.now() + .seconds(2)
    DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
        labelView.alpha = 0.0
        labelView.removeFromSuperview()
    }
}

//
//internal func forceLogout(with message : String? = nil) {
//
////    clearUserDefaults()
//    UserDefaultConfig.Token = ""
//    UserDefaultConfig.UserName = ""
//    UserDefaultConfig.PatientID = ""
//    UIApplication.shared.windows.last?.rootViewController?.popOrDismiss(animation: true)
//    UIApplication.shared.windows.first?.rootViewController = Router.createModule()
//    UIApplication.shared.windows.first?.makeKeyAndVisible()
//
//
//
//
//    if message != nil {
//        UIApplication.shared.windows.last?.rootViewController?.view.makeToast(message, duration: 2, position: .center, title: nil, image: nil, style: ToastStyle(), completion: nil)
//    }
//
//}

// Initialize User

//internal func initializeUserData()->User
//{
//    return User()
//}


func setLocalization(language : Language){
    
    if let path = Bundle.main.path(forResource: language.rawValue, ofType: "lproj"), let bundle = Bundle(path: path) {
        
        currentBundle = bundle
        
    } else {
        currentBundle = .main
    }
    
    
}


func vibrate(sound: defaultSystemSound) {
    AudioServicesPlaySystemSoundWithCompletion(SystemSoundID(sound.rawValue)) {
        // do what you'd like now that the sound has completed playing
    }
 }



//struct CommonModel : Mappable {
//    var status : Bool?
//    var message : String?
//    
//    init?(map: Map) {
//        
//    }
//    
//    mutating func mapping(map: Map) {
//        
//        status <- map["status"]
//        message <- map["message"]
//    }
//}






struct Constants {
    
    static let string = Constants()
    
    let Done = "Done"
    let Back = "Back"
    let noDevice = "no device"
    let manual = "manual"
    let OK = "OK"
    let Cancel = "Cancel"
    let NA = "NA"
    let MobileNumber = "Mobile Number"
    let next = "Next"
    let selectSource = "Select Source"
    let camera = "Camera"
    let photoLibrary = "Photo Library"
    let appointments =  "Appointments"
    let onlineConsultations =  "Online Consultations"
    let favDoctor = "Favourite Doctor"
    let medicalRecords = "Medical Records"
    let reminder = "Remainder"
    let logout = "Logout"
    let wallet = "Wallet"
    let articles = "Articles"
    let relativesManagement = "Relatives Management"
    let settings = "Settings"
    let faqAndAdmin = "FAQ/Admin Contact"
    let Yes = "Yes"
    let No = "No"
    let cannotMakeCallAtThisMoment = "Cannot make call at this moment"
    let couldnotOpenEmailAttheMoment = "Could not open Email at the moment."
    let areYouSureWantToLogout = "Are you want to logout?"
    let addanallerygy = "Add an allergy"
    let apply = "Apply"
    let addCardPayments = "Add card for payments"
    let paymentMethods = "Payment Methods"
    let yourCards = "Your Cards"
    let continueText = "Continue"
    let enterMobileNumber = "Enter Your Mobile Number"
    let whatisFirstName = "What's Your Name"
    let firstName = "First Name"
    let lastName = "Last Name"
    let whatisEmail = "What's Your Email Address?"
    let emailAddress = "Email Address"
    let confirmGender = "Confirm Gender"
    let male = "Male"
    let female = "Female"
    let other = "Other"
    let whatsYourBirthday = "What is Your Birthday?"
    let enterOTP = "Enter OTP"
    let sendOTP = "We just send 4 digit code to Your Phone Number"
    let findDoctors = "Find Doctors"
    let chat = "Chat"
    let searchDoctors = "Search Doctors"
    let visitedDoctors = "Visited Doctors"
    let specialities = "Specialities"
    let askQuestions = "Ask Question on health related"
    let basedOnHos = "Based on Hospitals"
    let seeVisitDrs = "See doctors your visited"
    let searchDoctorPlaceHolder = "Search For Doctors"
    let bookAppoitment = "Book Appointment"

    let call = "Call"
    let consulationFee = "Consultation Fees"
    let purposeOfVisit = "Purpose Of Visit"
    let followUp = "Follow Up"
    let consultation = "Consultation"
    let morning = "Morning"
    let evening = "Evening"
    let booking = "Booking"
    let proceed = "Proceed"
    let daySlot = "Day Slot"
    let enterPatientDetails = "Enter patient detail"
    let patientDetails = "Patient Details"
    let bookingFor = "Booking For"
    let enterName = "Enter Name"
    let enterEmail = "Enter Email"
    let enterPhone = "Enter Phone Number"
    let datetime = "Date & Time"
    let doctorInfo = "Doctor Info"
    let selectedPaymentMode = "Selected Payment Mode"
    let change = "Change"
    let confirm = "Confirm"
    let availablity = "Availabilty"
    let gender = "Gender"
    let availableAnyDay = "Available anyday"
    let availableToday = "Available today"
    let available3days = "Available next 3 days"
    let availableComingWeekend = "Available coming weekend"
    let filter = "Filter"
    let chatwithDoctor = "Chat With a Doctor"
    let symptomsTitle = "Let us know your symptoms or Health Problem"
    let chooseSpeciality = "Choose Suggested Speciality"
    let summary = "Summary"
    let verifiedDoctor = "Verified Orthopedic Specialists online now"
    let chatOut = "One of them will chat of you shortly"
    let fee = "Fee"
    let havePromocode = "Having a PromoCode?"
    let moneyBack = "100% Money back, if no response not for emerygency use"
    let proceedtoPay = "Proceed To Pay"
    let searchforDoctors = "Search results for Doctors "
    let resultsFound = "results found"
    let medicalVerified = "Medical Registration Verified"
    let noClinicName = "No Clinic Name"
    let noAddressFound = "No Address Found"
    let mon = "Mon"
    let tue = "Tue"
    let wed = "Wed"
    let thu = "Thu"
    let fri = "Fri"
    let sat = "Sat"
    let sun = "Sun"
    let services = "Services"
    let specialization = "Specialization"
    let location = "Location"
    let noDataLabel = "No Data Available"
    let upcomming = "Upcomming"
    let previous = "Previous"
    let onlineConsultation = "Online Consultants"
    let favDoctors = "Favourite Doctors"
    let noMedicalRecords = "No Medical Records Found"
    let meidcalRecords = "Medical Records"
    let doctors = "Doctors"
    let medicalRecordsGivenDoctor = "Medical Records given by Doctors"
    let niltext = "Nil"
    let patientRecord = "Patient Medical Records"
    let createdBy = "Created By"
    let testTaken = "Test Taken/Previous Prescription"
    let addImageForMedicalRecord = "Add Image of the Medical Record"
    let instructionMeical = "Description/Instruction Given"
    let doctorConsulted = "Doctor Consulted"
    let instructionGiven = "Instruction Given"
    let doctorName = "Doctor Name"
    let submit = "Submit"
    let medicalRecordAdded = "Medical Record Added Successfully"
    let noRemainderAvailable = "No Remainders"
    let remainder = "Remainder"
    let addRemainder = "Add Remainder"
    let time = "Time"
    let remainderName = "Remainder Name"
    let selectDate = "Select Date"
    let successfullyAdded = "SucessFully Added"
    let date = "Date"
    let alarm = "Alarm"
    let notifyMe = "Notify Me"
    let walletBalance = "Wallet Balance"
    let availableBalance = "Available Balance"
    let addAmountToWallet = "Add Amount To Wallet"
    let enterAmount = "Enter Amount"
    let addMoney = "Add Money"
    let addCardsForPayment = "Add Cards For Payment"
    let paymentView = "Payment View"
    let paymentModes = "Payment Modes"
    let availableCards = "Available Cards"
    let noArticleAvailable = "No Articles Available Currently"
    let publishedArticles = "Published Articles"
    let healthArticle = "Health Article"
    let yourProfile = "Your Profile"
    let personal = "Personal"
    let medical = "Medical"
    let lifestyle = "Lifestyle"
    let alcholHabit = "Do you have the habit of consuming alcohol?"
    let smokingHabit = "Do you have the habit of smoking?"
    let profileUpdated = "Profile Updated Sucessfully"
    let single = "Single"
    let married = "Married"
    let others = "Others"
    let bloodGroup = "Blood Group"
    let dob = "Date Of Birth"
    let martialStatus = "Martial Status"
    let weight = "Weight"
    let height = "Height"
    let emergencyContact = "Emergency Contact"
    let allergies = "Allergies"
    let currentMedication = "Current Medication"
    let pastMedication = "Past Medication"
    let chronicDiseases = "Chronic Diseases"
    let injuries = "Injuries"
    let surgeries = "Suregeries"
    let activity = "Activity"
    let foodPreference = "Food Preference"
    let occupation = "Occupation"
    let showRemainderNotification = "Show Reminder Notification"
    let aboutTxt = "About TeleHealth"
    let rateUsTxt = "Rate Us On AppStore"
    let chatDescription = "Let us know your symptoms or health problems"
    let invoice = "Invoice"
    let specialityFees = "Speciality Fees";
    let consultingFees = "Consulting Fees";
    let discount = "Discount";
    let grossTotal = "Gross Total";
    let totalPaid = "Total Paid";
    let allspecialities = "See All specialities";
    let visitedFor = "Visited For"
    let categories = "Categories"
    let navigatetitle = "Doctor"



}

//Defaults Keys

struct Keys {
    
    static let list = Keys()
    let userData = "userData"
    
    let id = "id"
    let name = "name"
    let accessToken = "access_token"
    let latitude = "latitude"
    let lontitude = "lontitude"
    let coOrdinates = "coOrdinates"
    let firstName = "firstName"
    let lastName = "lastName"
    let picture = "picture"
    let email = "email"
    let mobile = "mobile"
    
}






// Devices

enum DeviceType : String, Codable {
    
    case ios = "ios"
    case android = "android"
    
}



enum defaultSystemSound : Float {
    case peek = 1519
    case pop = 1520
    case cancelled = 1521
    case tryAgain = 1102
    case Failed = 1107
}





import UIKit
import MessageUI
import KWDrawerController
import Lottie

class Common {
    
    class func isValid(email : String)->Bool{
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@","[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}")
        return emailTest.evaluate(with: email)
        
    }
    
    class func getBackButton()->UIBarButtonItem{
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        return backItem// This will show in the next view controller being pushed
    }
 
    
//    class func setDrawerController()->UIViewController {
//
//          let drawerController =  DrawerController()
//
//           if let sideBarController = Router.main.instantiateViewController(withIdentifier: Storyboard.Ids.SideBarTableViewController) as? SideBarTableViewController  {
//               //let drawerSide : DrawerSide = selectedLanguage == .arabic ? .right : .left
//               let mainController = Router.main.instantiateViewController(withIdentifier: Storyboard.Ids.LaunchNavigationController)
//               drawerController.modalPresentationStyle = .currentContext
//               drawerController.setViewController(sideBarController, for: .left)
//               drawerController.setViewController(sideBarController, for: .right)
//               drawerController.setViewController(mainController, for: .none)
//               drawerController.getSideOption(for: .left)?.isGesture = true
//               drawerController.getSideOption(for: .right)?.isGesture = true
//           }
//           return drawerController
//       }
    
    class func getCurrentCode()->String?{
        
       return (Locale.current as NSLocale).object(forKey:  NSLocale.Key.countryCode) as? String
  
    }
    
    
    
    //MARK:- Get Countries from JSON
    
    class func getCountries()->[Country]{
        
        var source = [Country]()
        
        if let data = NSData(contentsOfFile: Bundle.main.path(forResource: "countryCodes", ofType: "json") ?? "") as Data? {
            do{
                source = try JSONDecoder().decode([Country].self, from: data)
                
            } catch let err {
                print(err.localizedDescription)
            }
        }
        return source
    }
    
    
    
//    class func getRefreshControl(intableView tableView : UITableView, tintcolorId  : Int = UIColor.doctorColor., attributedText text : NSAttributedString? = nil)->UIRefreshControl{
//
//        let rc = UIRefreshControl()
//        rc.tintColorId = tintcolorId
//        rc.attributedTitle = text
//        tableView.addSubview(rc)
//        return rc
//
//    }
    
    // MARK:- Set Font
    
    class func setFont(to field :Any, isTitle : Bool = false, size : CGFloat = 0) {
        
        let customSize = size > 0 ? size : (isTitle ? 16 : 14)
        let font = UIFont(name: isTitle ? FontType.bold.rawValue : FontType.medium.rawValue, size: customSize)

        switch (field.self) {
        case is UITextField:
            (field as? UITextField)?.font = font
        case is UILabel:
            (field as? UILabel)?.font = font
        case is UIButton:
            (field as? UIButton)?.titleLabel?.font = font
        case is UITextView:
            (field as? UITextView)?.font = font
        default:
            break
        }
    }
    
    class func setFontWithType(to field :Any, size : CGFloat = 0, type fontType:FontCustom = .meduim) {
        
        let customSize = size > 0 ? size : 14
        let font = UIFont(name: fontType.rawValue, size: customSize)

        switch (field.self) {
        case is UITextField:
            (field as? UITextField)?.font = font
        case is UILabel:
            (field as? UILabel)?.font = font
        case is UIButton:
            (field as? UIButton)?.titleLabel?.font = font
        case is UITextView:
            (field as? UITextView)?.font = font
        default:
            break
        }
    }

    
   
    
    // MARK:- Make Call
    class func call(to number : String?) {
        
        if let providerNumber = number, let url = URL(string: "tel://\(providerNumber)"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
//            UIScreen.main.focusedView?.make(toast: Constants.string.cannotMakeCallAtThisMoment.localize())
        }
        
    }
    
    // MARK:- Send Email
    class func sendEmail(to mailId : [String], from view : UIViewController & MFMailComposeViewControllerDelegate) {
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = view
            mail.setToRecipients(mailId)
            view.present(mail, animated: true)
        } else {
//            UIScreen.main.focusedView?.make(toast: Constants.string.couldnotOpenEmailAttheMoment.localize())
        }
        
    }
    
    // MARK:- Send Message
    
    class func sendMessage(to numbers : [String], text : String, from view : UIViewController & MFMessageComposeViewControllerDelegate) {
        
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = text
            controller.recipients = numbers
            controller.messageComposeDelegate = view
            view.present(controller, animated: true, completion: nil)
        }
    }
    
    // MARK:- Bussiness Image Url
    class func getImageUrl (for urlString : String?)->String {
        
        return APPConstant.baseUrl+"/storage/"+String.removeNil(urlString)
    }
    
    
    //MARK: Timestamp fomater
    class func formateDate(date: String?) -> String? {
        
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale // save locale temporarily
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = dateFormatter.date(from: nullStringToEmpty(string: date))!
        dateFormatter.dateFormat = "dd-MM-yyyy" //hh:mm:ss"
        dateFormatter.locale = tempLocale // reset the locale
        let dateString = dateFormatter.string(from: date)
        print("EXACT_DATE : \(dateString)")
        
        return nullStringToEmpty(string: dateString)
    }
    
    
    //Create QRCode Image
    class func CreateQrCodeForyourString (string:String)-> UIImage{
        let stringData = string.data(using: .utf8, allowLossyConversion: false)
        let filter = CIFilter(name: "CIQRCodeGenerator")
        filter?.setValue(stringData, forKey: "inputMessage")
        filter?.setValue("H", forKey: "inputCorrectionLevel")
        let qrCIImage = filter?.outputImage
        let colorFilter = CIFilter(name: "CIFalseColor")!
        colorFilter.setDefaults()
        colorFilter.setValue(qrCIImage, forKey: "inputImage")
        colorFilter.setValue(CIColor(red: 0, green: 0, blue: 0), forKey: "inputColor0")
        colorFilter.setValue(CIColor(red: 1, green: 1, blue: 1), forKey: "inputColor1")
        
        let codeImage = UIImage(ciImage: (colorFilter.outputImage!.transformed(by: CGAffineTransform(scaleX: 5, y: 5))))
        return codeImage
    }
    
}


public func nullStringToEmpty(string: String?) -> String {
    
    if string == nil {
        return ""
    }
    else {
        return string!
    }
}


//MARK: Timestamp fomater
extension Date {
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}


//Image tint colour change
public func withRenderingMode(originalImage: UIImage, imgView: UIImageView, imgTintColur: UIColor) {
    
    let tintedImage = originalImage.withRenderingMode(.alwaysTemplate)
    imgView.tintColor = imgTintColur
    imgView.image = tintedImage
    
}

func showToast(msg : String , bgcolor : UIColor = UIColor.gray) {
    let window = UIApplication.shared.keyWindow!
    let toastLabel = PaddingLabel()
    
    toastLabel.backgroundColor = bgcolor.withAlphaComponent(0.8)
    toastLabel.textColor = UIColor.white
    toastLabel.translatesAutoresizingMaskIntoConstraints = false
    toastLabel.textAlignment = .center;
    toastLabel.font = UIFont.systemFont(ofSize: 14)
    toastLabel.text = msg
    
    toastLabel.alpha = 1.0
    toastLabel.numberOfLines = 0
    toastLabel.lineBreakMode = .byWordWrapping
    toastLabel.layer.cornerRadius = 10;
    toastLabel.clipsToBounds  =  true
    toastLabel.removeFromSuperview()
    window.addSubview(toastLabel)
    toastLabel.bringSubviewToFront(window)
    NSLayoutConstraint.activate([
        toastLabel.leadingAnchor.constraint(greaterThanOrEqualTo: window.leadingAnchor, constant: 20),
        toastLabel.trailingAnchor.constraint(lessThanOrEqualTo: window.trailingAnchor,constant: -20),
        toastLabel.bottomAnchor.constraint(greaterThanOrEqualTo: window.bottomAnchor, constant:  -100),
        toastLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 30),
        toastLabel.widthAnchor.constraint(greaterThanOrEqualToConstant: 150),
        toastLabel.centerXAnchor.constraint(equalTo: window.centerXAnchor)
        
    ])
    UIView.animate(withDuration: 3.0, delay: 0.0, options: .curveEaseOut, animations: {
        toastLabel.alpha = 0.0
    }, completion: {(isCompleted) in
        toastLabel.removeFromSuperview()
    })
}

func dateConvertor(_ date: String, _input: DateTimeFormate, _output: DateTimeFormate) -> String
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = _input.rawValue
    let dates = dateFormatter.date(from: date)
    dateFormatter.dateFormat = _output.rawValue
    _ = dateFormatter.string(from: dates ?? Date())
    
    return  dateFormatter.string(from: dates ?? Date())
}

enum DateTimeFormate : String{
    case DMY_Time = "dd MMM YYYY, EEE hh:mm a"
    case DMY = "dd MMM YYYY"
    case MY = "MM/YYYY"
    case R_hour = "HH:mm"
    case N_hour = "hh:mm a"
    case date_time = "yyyy-MM-dd HH:mm:ss"
    case date_time_Z = "yyyy-MM-dd HH:mm:ss z"
    case DM = "dd MMM"
    case DMYhy = "dd-MMM-YYYY"
    case YMD = "yyyy-MM-dd"
}

class PaddingLabel: UILabel {
    
    @IBInspectable var topInset: CGFloat = 10.0
    @IBInspectable var bottomInset: CGFloat = 10.0
    @IBInspectable var leftInset: CGFloat = 10.0
    @IBInspectable var rightInset: CGFloat = 10.0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.insetBy(dx: topInset, dy: leftInset))
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            var contentSize = super.intrinsicContentSize
            contentSize.height += topInset + bottomInset
            contentSize.width += leftInset + rightInset
            return contentSize
        }
    }
}
