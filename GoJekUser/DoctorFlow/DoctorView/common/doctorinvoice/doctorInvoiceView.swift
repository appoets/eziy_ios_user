//
//  InvoiceView.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 22/02/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit

class doctorInvoiceView: UIView {

    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var specialityFeesLabel : UILabel!
    @IBOutlet weak var categoryFeesLabel : UILabel!
    @IBOutlet weak var discountLabel : UILabel!
    @IBOutlet weak var grossFessLabel : UILabel!
    @IBOutlet weak var totalFeesLabel : UILabel!
    @IBOutlet weak var specialityFeesValueLabel : UILabel!
    @IBOutlet weak var categoryFeesValueLabel : UILabel!
    @IBOutlet weak var discountValueLabel : UILabel!
    @IBOutlet weak var grossValueLabel : UILabel!
    @IBOutlet weak var totalValueLabel : UILabel!
    @IBOutlet weak var doneButton : UIButton!
    @IBOutlet weak var discountView : UIView!
    
    

    var onClickDone : (()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initalLoads()
    }

}


extension doctorInvoiceView {
    
    private func initalLoads(){
        self.setDesigns()
        self.localize()
        
    }
    
    private func setDesigns(){
        Common.setFontWithType(to: titleLabel, size: 17, type: .semiBold)
        Common.setFontWithType(to: specialityFeesLabel, size: 14, type: .light)
        Common.setFontWithType(to: categoryFeesLabel, size: 14, type: .light)
        Common.setFontWithType(to: discountLabel, size: 14, type: .light)
        Common.setFontWithType(to: grossFessLabel, size: 14, type: .light)
        Common.setFontWithType(to: totalFeesLabel, size: 14, type: .light)
        Common.setFontWithType(to: specialityFeesValueLabel, size: 14, type: .meduim)
        Common.setFontWithType(to: categoryFeesValueLabel, size: 14, type: .meduim)
        Common.setFontWithType(to: discountValueLabel, size: 14, type: .meduim)
        Common.setFontWithType(to: grossValueLabel, size: 14, type: .meduim)
        Common.setFontWithType(to: totalValueLabel, size: 14, type: .meduim)
        Common.setFontWithType(to: doneButton, size: 14, type: .meduim)
        self.doneButton.addTarget(self, action: #selector(doneAction(sender:)), for: .touchUpInside)
        
    }
    
//    func populateData(invoice:doctorInvoice?){
//        self.specialityFeesValueLabel.text = APPConstant.currency + "\(invoice?.speciality_fees ?? 0)"
//        self.categoryFeesValueLabel.text = APPConstant.currency + "\(invoice?.consulting_fees ?? 0)"
//        if invoice?.discount == 0{
//            self.discountView.isHidden = true
//        }else{
//        self.discountValueLabel.text =  APPConstant.currency + "\(invoice?.discount ?? 0)"
//            self.discountView.isHidden = false
//        }
//        self.grossValueLabel.text =  APPConstant.currency + "\(invoice?.gross ?? 0)"
//        self.totalValueLabel.text = APPConstant.currency + "\(invoice?.total_pay ?? 0)"
//    }
    
    @IBAction private func doneAction(sender:UIButton){
        self.onClickDone?()
    }
    private func localize(){
        self.titleLabel.text = Constants.string.invoice.localize()
        self.specialityFeesLabel.text = Constants.string.specialityFees.localize()
        self.categoryFeesLabel.text = Constants.string.consultingFees.localize()
        self.discountLabel.text = Constants.string.discount.localize()
        self.grossFessLabel.text = Constants.string.grossTotal.localize()
        self.totalFeesLabel.text = Constants.string.totalPaid.localize()
    }
    
}

