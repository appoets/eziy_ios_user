//
//  CommonModel.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 22/02/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import Foundation
import ObjectMapper

struct CommonModel : Mappable {
    var status : Bool?
    var message : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        status <- map["status"]
        message <- map["message"]
    }
}

