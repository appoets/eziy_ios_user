//
//  HealthFeedDetailsViewController.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 21/02/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import ObjectMapper

class HealthFeedDetailsViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timelineLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var titleImage: UIImageView!
    @IBOutlet weak var healthArticleTitleLabel: UILabel!
    
    var titleText : String = ""
      var descriptionText : String = ""
    var timeText : String = ""
    var imageTitle : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        initialLoads()
    }
    

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
       
    }
    

}

extension HealthFeedDetailsViewController {
    
    func initialLoads() {
        setupNavigationBar()
        self.localize()
    }
    private func setupNavigationBar() {
         self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = Constants.string.articles.localize()
         Common.setFont(to: self.navigationItem.title!, isTitle: true, size: 18)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
         self.navigationController?.navigationBar.isTranslucent = false
         self.navigationController?.navigationBar.barTintColor = UIColor(named: "AppBlueColor")
        self.timelineLabel.text = timeText
        self.titleLabel.text = titleText
        self.descriptionTextView.text = descriptionText
        self.titleImage.setURLImage(imageTitle)
    }
    
    private func localize(){
        self.healthArticleTitleLabel.text = Constants.string.healthArticle.localize()
    }
}


