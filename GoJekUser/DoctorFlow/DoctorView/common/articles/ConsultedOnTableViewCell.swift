//
//  ConsultedOnTableViewCell.swift
//  GoJekUser
//
//  Created by ABIRAMI R on 10/05/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit

class ConsultedOnTableViewCell: UITableViewCell {

    @IBOutlet weak var downloadPdfBtn: UIButton!
    @IBOutlet weak var dateLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.downloadPdfBtn.setTitle("", for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
