//
//  MedicalRecordsViewController.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 21/02/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import ObjectMapper

class MedicalRecordsViewController: UIViewController {

    @IBOutlet weak var listTable: UITableView!
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var downloadMedicalView: UIView!
    @IBOutlet weak var addMedicalRecordButton: UIButton!
    
//     var medical  = [Medicals]()
    var medicalEntity : MedicalRecordsEntity?
    lazy var loader  : UIView = {
        return createActivityIndicator(self.view.window ?? self.view)
       }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.navigationController?.navigationBar.isHidden = false
//        self.getMedicalRecords()
//        self.setupNavigationBar()
        self.noDataView.isHidden = true
        self.initialLoadss()
        self.navigationController?.isNavigationBarHidden = false
        self.title = "Medical Records"
    }

}
extension MedicalRecordsViewController {
    func initialLoadss(){
        self.doctorPresenter?.getMedicalRecords()
        self.registerTableViewCell()
    }
    func registerTableViewCell(){
        self.listTable.register(UINib(nibName: XIB.Names.FavDoctorTableViewCell, bundle: nil), forCellReuseIdentifier: XIB.Names.FavDoctorTableViewCell)
    }
}
extension MedicalRecordsViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.medicalEntity?.responseData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.FavDoctorTableViewCell, for: indexPath) as! FavDoctorTableViewCell
        let getMedicalData = self.medicalEntity?.responseData?[indexPath.row]
        guard let url = URL(string: getMedicalData?.provider?.picture ?? "") else { return UITableViewCell() }
        do{
        if let data = try? Data(contentsOf: url){
            cell.doctorImage.image = UIImage(data: data)
        }
        }
        catch{
            
        }
        cell.labelName.text = getMedicalData?.provider?.first_name ?? "" + ((getMedicalData?.provider?.last_name)!)
        cell.labelSpeciality.text = getMedicalData?.service?.service_subcategory_name ?? ""
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "DetailMedicalRecordVc") as! DetailMedicalRecordVc
        vc.medicalEntity = self.medicalEntity
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}


//extension MedicalRecordsViewController {
//    func initialLoads() {
//        registerCell()
//
//        self.addMedicalRecordButton.addTarget(self, action: #selector(addAction(sender:)), for: .touchUpInside)
//
//    }
//    private func setupNavigationBar() {
//         self.navigationController?.isNavigationBarHidden = false
//        self.navigationItem.title = Constants.string.medicalRecords.localize()
//         Common.setFont(to: self.navigationItem.title!, isTitle: true, size: 18)
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
//         self.navigationController?.navigationBar.isTranslucent = false
//         self.navigationController?.navigationBar.barTintColor = UIColor(named: "AppBlueColor")
//        self.noDataLabel.text = Constants.string.noMedicalRecords.localize()
//    }
//
//    func registerCell(){
////        self.listTable.registerCell(withId: XIB.Names.FavDoctorTableViewCell)
//        self.listTable.register(UINib(nibName: XIB.Names.FavDoctorTableViewCell, bundle: nil), forCellReuseIdentifier: XIB.Names.FavDoctorTableViewCell)
//    }
//
//    @IBAction private func addAction(sender:UIButton){
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: Storyboard.Ids.AddMedicalRecordViewController) as! AddMedicalRecordViewController
//        vc.navigationController?.setNavigationBarHidden(true, animated: true)
//        self.navigationController?.pushViewController(vc, animated: true)
//    }
//}
//
//extension MedicalRecordsViewController : UITableViewDelegate,UITableViewDataSource {
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
////        if self.medical.count > 0{
////            return self.medical.count + 1
////        }
//        return 1//self.medical.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        let cell = self.listTable.dequeueReusableCell(withIdentifier: XIB.Names.FavDoctorTableViewCell, for: indexPath) as! FavDoctorTableViewCell
////        if indexPath.row == self.medical.count{
////            cell.doctorImage.setURLImage(self.medical[indexPath.row].doctor_profile?.profile_pic ?? "")
////            cell.labelName.text = "Other Doctors"
////            cell.labelSpeciality.text = "Nil"
////            return cell
//////        }else{
////        cell.doctorImage.setURLImage(self.medical[indexPath.row].doctor_profile?.profile_pic ?? "")
////        cell.labelName.text = "\(self.medical[indexPath.row].first_name ?? Constants.string.other.localize()) \(self.medical[indexPath.row].last_name ?? Constants.string.doctors.localize())"
////        cell.labelSpeciality.text = "\(self.medical[indexPath.row].doctor_profile?.speciality?.name ?? Constants.string.niltext.localize())"
//        return cell
////        }
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
////        if indexPath.row == self.medical.count{
////            let recordVC = self.storyboard?.instantiateViewController(withIdentifier: Storyboard.Ids.PatientRecordsViewController) as! PatientRecordsViewController
////            recordVC.doctorId = 0
////            self.navigationController?.pushViewController(recordVC, animated: true)
////        }else{
//        let recordVC = self.storyboard?.instantiateViewController(withIdentifier: Storyboard.Ids.PatientRecordsViewController) as! PatientRecordsViewController
//        recordVC.doctorId = self.medical[indexPath.row].id ?? 0
//        self.navigationController?.pushViewController(recordVC, animated: true)
////        }
//
//    }
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//
//        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 0.3))
//        view.backgroundColor = section != 2 ? UIColor(named: "TextForegroundColor") : .clear
//        return view
//    }
//
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//
//        return 50
//    }
//
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//
//        return 0.3
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 80
//    }
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//        let headerView = Bundle.main.loadNibNamed("FilterHeaderView", owner: self, options: nil)?.first as? FilterHeaderView
//        headerView?.lbl.text = Constants.string.medicalRecordsGivenDoctor.localize()
//        headerView?.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50)
//        return headerView
//    }
//
//}
////Api calls
extension MedicalRecordsViewController : DoctorPresenterToDoctorViewProtocol{
    
    func getMedicalRecords(medicalRecordsEntity: MedicalRecordsEntity) {
        self.medicalEntity = medicalRecordsEntity
        self.listTable.reloadData()
        if self.medicalEntity?.responseData?.count == 0{
            self.noDataView.isHidden = false
        }else{
            self.noDataView.isHidden = true
        }
    }
}

