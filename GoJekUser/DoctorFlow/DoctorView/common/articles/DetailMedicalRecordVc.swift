//
//  DetailMedicalRecordVc.swift
//  GoJekUser
//
//  Created by ABIRAMI R on 10/05/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit

class DetailMedicalRecordVc: UIViewController {
    @IBOutlet weak var detailTableView: UITableView!
    var medicalEntity : MedicalRecordsEntity?
    var sectionHeader = ["","Consulted on","Download medical records"]
    var titlee = ""
    lazy var loader  : UIView = {
        return createActivityIndicator(self.view.window ?? self.view)
       }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.detailTableView.reloadData()

      
    }
    
    override func viewWillAppear(_ animated: Bool) {

        self.initialLoads()
        self.detailTableView.reloadData()
        self.navigationController?.isNavigationBarHidden = false
//        self.title = ""
    }
    


}
extension DetailMedicalRecordVc{
    func initialLoads(){
        self.registerTableViewCell()
        setnavigation()
    }
    func setnavigation(){
//        let backBtn = UIBarButtonItem(image: UIImage(named: "back_white"),
//                                      style: .plain,
//                                      target: self,
//                                      action: #selector(UINavigationController.popViewController(animated:)))
//        navigationController?.navigationItem.leftBarButtonItem = backBtn
//        navigationController?.navigationBar.tintColor = .white
        let leftBarButton = UIBarButtonItem.init(image: UIImage(named: "back_white"), style:
        .plain, target: self, action: #selector(leftBarButtonActn))
        navigationController?.navigationItem.rightBarButtonItem = leftBarButton
        navigationController?.navigationBar.tintColor = .white
//        navigationController?.title = titlee
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
//        navigationController?.interactivePopGestureRecognizer?.delegate = self
        
    }
    @objc func leftBarButtonActn(){
        self.navigationController?.popViewController(animated: true)
    }
    func registerTableViewCell(){
        self.detailTableView.register(UINib(nibName: XIB.Names.FavDoctorTableViewCell, bundle: nil), forCellReuseIdentifier: XIB.Names.FavDoctorTableViewCell)
        self.detailTableView.register(UINib(nibName: XIB.Names.ConsultedOnTableViewCell, bundle: nil), forCellReuseIdentifier: XIB.Names.ConsultedOnTableViewCell)
    }
}
extension DetailMedicalRecordVc : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
        let cell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.FavDoctorTableViewCell, for: indexPath) as! FavDoctorTableViewCell
            let getMedicalData = self.medicalEntity?.responseData?[indexPath.row]
            guard let url = URL(string: getMedicalData?.provider?.picture ?? "") else { return UITableViewCell() }
            do{
                if let data = try? Data(contentsOf: url){
                    cell.doctorImage.image = UIImage(data: data)
                }
            }
            catch{
                
            }
//            self.titlee = getMedicalData?.provider?.first_name ?? "" + ((getMedicalData?.provider?.last_name)!)
            cell.labelName.text = getMedicalData?.provider?.first_name ?? "" + ((getMedicalData?.provider?.last_name)!)
            cell.labelSpeciality.text = getMedicalData?.service?.service_subcategory_name ?? ""
        return cell
        }else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.ConsultedOnTableViewCell, for: indexPath) as! ConsultedOnTableViewCell
            cell.downloadPdfBtn.isHidden = true
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd hh:mm:ss"

            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "d MMM yyyy, E h:mm a"

            guard let date = dateFormatterGet.date(from: self.medicalEntity?.responseData?[indexPath.row].checkedout_at ?? "") else { return UITableViewCell() }
            
            cell.dateLbl.text = dateFormatterPrint.string(from: date)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.ConsultedOnTableViewCell, for: indexPath) as! ConsultedOnTableViewCell
            cell.dateLbl.isHidden = true
            cell.downloadPdfBtn.setTitle("Download pdf", for: .normal)
            cell.downloadPdfBtn.layer.borderColor = UIColor(red: 119/255, green: 205/255, blue: 241/255, alpha: 1).cgColor
            cell.downloadPdfBtn.layer.borderWidth = 1
            cell.downloadPdfBtn.setTitleColor(UIColor(red: 119/255, green: 205/255, blue: 241/255, alpha: 1), for: .normal)
            return cell
        }
       
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = Bundle.main.loadNibNamed("FilterHeaderView", owner: self, options: nil)?.first as? FilterHeaderView
        headerView?.lbl.text = sectionHeader[section]
        headerView?.backgroundColor = UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1)
        headerView?.lbl.textColor = .black
        headerView?.lbl.font = .systemFont(ofSize: 17, weight: .semibold )
        headerView?.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50)
//        headerView?.addTap {
//        }
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }else{
            return 50
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 60
        }else if indexPath.section == 1{
            return 40
        }else{
            return 80
        }
    }
    
    
}
