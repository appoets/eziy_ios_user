//
//  UXViewController.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 21/03/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit

extension UIViewController{
    func present<T : UIViewController>(vc : T, _ isFullyPresent : YesNoType = .no){
        let vcl = vc
        if isFullyPresent == .no{
            vcl.modalPresentationStyle = .custom
        }else{
            vcl.modalPresentationStyle = .fullScreen
        }
        self.present(vc, animated: false, completion: nil)
    }
}
