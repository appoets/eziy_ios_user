//
//  DoctorPresenter.swift
//  GoJekUser
//
//  Created by apple on 20/02/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit
import Alamofire
import CloudKit


class DoctorPresenter: DoctorViewToDoctorPresenterProtocol {
    func ratingView(param: Parameters) {
        doctorInteractor?.ratingView(param: param)
    }
    
    func appointmentsUpdate(param: Parameters) {
        doctorInteractor?.appointmentsUpdate(param: param)
    }
    
    func endCall(param: Parameters) {
        doctorInteractor?.endCall(param: param)
    }
    
    func getDoctorsDetail(id: Int) {
        doctorInteractor?.getDoctorsDetail(id: id)
    }
    
    func visitorpost(param: Parameters, id: Int, doctor_profile_id: Int, provider_id: Int) {
        doctorInteractor?.visitorpost(param: param,id: id,doctor_profile_id: doctor_profile_id,provider_id: provider_id)
    }
    
   
    
    func getvistiordoctor() {
        doctorInteractor?.getvistiordoctor()
    }
    
   
    func cancelAppointments(id: Parameters) {
        doctorInteractor?.cancelAppointments(id: id)
    }
    func getAppointmentList() {
        doctorInteractor?.getAppointmentList()
    }
    
    func getsearchdeatil(count: Int) {
        doctorInteractor?.getsearchdeatil(count: count)
    }
    
   
    
    func getRelativeManagementList() {
        doctorInteractor?.getRelativeManagementList()
    }
    
    func PostRelativeManagement(PostRelativeManagement: Parameters, imageData: [String : Data]?) {
        doctorInteractor?.PostRelativeManagement(PostRelativeManagement: PostRelativeManagement, imageData: imageData)
    }
    
    func relativeManagement() {
        
    }
    
    func bookingdetail(id: Int, param: Parameters) {
        doctorInteractor?.bookingdetail(id: id, param: param)
    }
    
    func getArticles() {
        doctorInteractor?.getArticles()
    }
    
    func getslot(id: Int) {
        doctorInteractor?.getslot(id: id)
    }
    
    func getDoctorsList(id: Int) {
        doctorInteractor?.getDoctorsList(id: id)
    }
    
    func getfinddoctor(id: Int) {
        doctorInteractor?.getfinddoctor(id: id)
    }
    
    
//    func getvistiordoctor(count: Int) {
//        doctorInteractor?.getvistiordoctor(count: count)
//    }
    
    
    func getCategoryList(id: Int) {
        doctorInteractor?.getCategoryList(id: id)
    }
    func cancelappoinment(id: String){
        doctorInteractor?.cancelappoinment(id: id)
    }
    func getMedicalRecords() {
        doctorInteractor?.getMedicalRecords()
    }
    
    
   
    var doctorView: DoctorPresenterToDoctorViewProtocol?
    var doctorInteractor: DoctorPresenterToDoctorInteractorProtocol?
    var doctorRouter: DoctorPresenterToDoctorRouterProtocol?
    
}

extension DoctorPresenter: DoctorInteractorToDoctorPresenterProtocol {
    
    func updateRating(call: CallStatusEntity) {
        doctorView?.updateRating(call: call)
    }
    
    func updateApointment(call: CallStatusEntity) {
        doctorView?.updateApointment(call: call)
    }
    
    func endCallStatus(call: CallStatusEntity) {
        doctorView?.endCallStatus(call: call)
    }
    
    
    func getDoctordeatil(doctor: DoctorDetailModel) {
        doctorView?.getDoctordeatil(doctor: doctor)
    }
    
    func visitorpost(visitors: VisitorupdateModel) {
        doctorView?.visitorpost(visitors: visitors)
    }
    
    func getvistiordoctor(visitor: VisitorEntity) {
        doctorView?.getvistiordoctor(visitor: visitor)
    }
    
   
    func cancelAppointments(success: SuccessEntity) {
        doctorView?.cancelAppointments(success: success)
    }
    
   
    func getAppointmentList(getAppointmentList: AppointmentList) {
        doctorView?.getAppointmentList(getAppointmentList: getAppointmentList)
    }
    
    func getsearchdeatil(searchmodel: searchmodel) {
        doctorView?.getsearchdeatil(searchmodel: searchmodel)
    }
    
    
    func PostRelativeManagement(PostRelativeManagement: PostRelativeManagementEntity) {
        doctorView?.PostRelativeManagement(PostRelativeManagement: PostRelativeManagement)
    }
    func RelativeManagement(RelativeManagement: RelativeManagementEntity) {
        doctorView?.RelativeManagement(RelativeManagement: RelativeManagement)
    }
    
    func getRelativeManagementList(data: Relatives) {
        doctorView?.getRelativeManagementList(data: data)
    }
    func getArticels(data: ArticleModel) {
        doctorView?.getArticels(data: data)
    }
    
    func bookingdetail(PatientAppoinment: PatientAppoinmentEntity) {
        doctorView?.bookingdetail(PatientAppoinment: PatientAppoinment)
    }
    
    func getslot(BookingModels: BookingModel) {
        doctorView?.getslot(BookingModels: BookingModels)
    }
    
    func getDoctorsList(DoctorListEntity: DoctorListEntity) {
        doctorView?.getDoctorsList(DoctorListEntity: DoctorListEntity)
    }
    
    func getfinddoctor(SubCategoryEntity: SubCategoryEntity) {
        doctorView?.getfinddoctor(SubCategoryEntity: SubCategoryEntity)
    }
    
//    func vistordoctorlist(ProfileModel: UpdatedVistedDoctor) {
//        doctorView?.vistordoctorlist(ProfileModel:ProfileModel)
//    }
    
    func cancelappoinment(data: Category) {
        doctorView?.cancelappoinment(data: data)
    }
    
    func categoryList(data: CategoryList) {
        doctorView?.categoryList(data: data)
    }
    func getMedicalRecords(medicalRecordsEntity: MedicalRecordsEntity) {
        doctorView?.getMedicalRecords(medicalRecordsEntity: medicalRecordsEntity)
    }
    
    
    
    

}
