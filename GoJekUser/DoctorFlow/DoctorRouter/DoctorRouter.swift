//
//  DoctorRouter.swift
//  GoJekUser
//
//  Created by apple on 20/02/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit

class DoctorRouter: DoctorPresenterToDoctorRouterProtocol {
    
    static func createDoctorModule() -> UIViewController {
        let doctorDetailViewController  = doctorStoryboard.instantiateViewController(withIdentifier: Storyboard.Ids.DoctorHomeVC) as! DoctorHomeVC
        let doctorPresenter: DoctorViewToDoctorPresenterProtocol & DoctorInteractorToDoctorPresenterProtocol = DoctorPresenter()
        let doctorInteractor: DoctorPresenterToDoctorInteractorProtocol = DoctorInteractor()
        let doctorRouter: DoctorPresenterToDoctorRouterProtocol = DoctorRouter()
        doctorDetailViewController.doctorPresenter = doctorPresenter
        doctorPresenter.doctorView = doctorDetailViewController
        doctorPresenter.doctorRouter = doctorRouter
        doctorPresenter.doctorInteractor = doctorInteractor
        doctorInteractor.doctorPresenter = doctorPresenter
        return doctorDetailViewController
    }
    
   
    static var doctorStoryboard: UIStoryboard {
        return UIStoryboard(name:"doctor",bundle: Bundle.main)
    }
}



