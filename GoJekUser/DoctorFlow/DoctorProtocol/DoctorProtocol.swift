//
//  DoctorProtocol.swift
//  GoJekUser
//
//  Created by Shyamala's MacBook Pro on 24/01/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import Alamofire

var doctorPresenterObject: DoctorViewToDoctorPresenterProtocol?

//MARK:- Doctor presenter Doctor view protocol

protocol DoctorPresenterToDoctorViewProtocol: class {
    
    func RelativeManagement(RelativeManagement: RelativeManagementEntity)
    func PostRelativeManagement(PostRelativeManagement: PostRelativeManagementEntity)
    func bookingdetail(PatientAppoinment: PatientAppoinmentEntity)
    func categoryList(data:CategoryList)
    func getslot(BookingModels: BookingModel)
    func getfinddoctor(SubCategoryEntity: SubCategoryEntity)
    func cancelappoinment(data:Category)
    func getDoctorsList(DoctorListEntity:DoctorListEntity)
    func getArticels(data : ArticleModel)
    func getRelativeManagementList(data : Relatives)
    func getMedicalRecords(medicalRecordsEntity : MedicalRecordsEntity)
    func getsearchdeatil(searchmodel : searchmodel)
    func getAppointmentList(getAppointmentList : AppointmentList)
    func cancelAppointments(success : SuccessEntity)
    func getvistiordoctor(visitor : VisitorEntity)
    func visitorpost(visitors : VisitorupdateModel)
    func getDoctordeatil(doctor : DoctorDetailModel)
    
    func endCallStatus(call: CallStatusEntity)
    func updateApointment(call: CallStatusEntity)
    func updateRating(call: CallStatusEntity)

}

extension DoctorPresenterToDoctorViewProtocol {
    
    var doctorPresenter: DoctorViewToDoctorPresenterProtocol? {
        get {
            doctorPresenterObject?.doctorView = self
            return doctorPresenterObject
        }
        set(newValue) {
            doctorPresenterObject = newValue
        }
    }
    
    func RelativeManagement(RelativeManagement: RelativeManagementEntity) {return}
    func PostRelativeManagement(PostRelativeManagement: PostRelativeManagementEntity) {return}
    func bookingdetail(PatientAppoinment: PatientAppoinmentEntity){return}
    func categoryList(data:CategoryList) {return}
    func getslot(BookingModels: BookingModel) {return}
    func getfinddoctor(SubCategoryEntity: SubCategoryEntity){return}
    func getDoctorsList(DoctorListEntity:DoctorListEntity){return}
    func cancelappoinment(data:Category){return}
    func getArticels(data : ArticleModel){return}
    func getRelativeManagementList(data : Relatives){return}
    func getMedicalRecords(medicalRecordsEntity : MedicalRecordsEntity){return}
    func getsearchdeatil(searchmodel : searchmodel){return}
    func getAppointmentList(getAppointmentList : AppointmentList) {return}
    func cancelAppointments(success : SuccessEntity) { return }
    func getvistiordoctor(visitor : VisitorEntity) { return }
    func visitorpost(visitors : VisitorupdateModel) { return }
    func getDoctordeatil(doctor : DoctorDetailModel){ return }
    
    func endCallStatus(call: CallStatusEntity){return}
    func updateApointment(call: CallStatusEntity){return}
    func updateRating(call: CallStatusEntity){return}
}

//MARK:- Doctor Interactor to Doctor Presenter Protocol

protocol DoctorInteractorToDoctorPresenterProtocol: class {
    func RelativeManagement(RelativeManagement: RelativeManagementEntity)
    func PostRelativeManagement(PostRelativeManagement: PostRelativeManagementEntity)
    func bookingdetail(PatientAppoinment: PatientAppoinmentEntity)
    func categoryList(data:CategoryList)
    func getslot(BookingModels: BookingModel)
    func getfinddoctor(SubCategoryEntity: SubCategoryEntity)
    func getDoctorsList(DoctorListEntity:DoctorListEntity)
    func cancelappoinment(data:Category)
    func getArticels(data : ArticleModel)
    func getRelativeManagementList(data : Relatives)
    func getMedicalRecords(medicalRecordsEntity : MedicalRecordsEntity)
    func getsearchdeatil(searchmodel : searchmodel)
    func getDoctordeatil(doctor : DoctorDetailModel)
    func getAppointmentList(getAppointmentList : AppointmentList)
    func cancelAppointments(success : SuccessEntity)
    func getvistiordoctor(visitor : VisitorEntity)
    func visitorpost(visitors : VisitorupdateModel)
    func endCallStatus(call: CallStatusEntity)
    func updateApointment(call: CallStatusEntity)
    func updateRating(call: CallStatusEntity)

}

//MARK:- Doctor Presenter to Doctor Interactor Protocol

protocol DoctorPresenterToDoctorInteractorProtocol: class {
    
    var doctorPresenter: DoctorInteractorToDoctorPresenterProtocol? { get set }
    func getRelativeManagementList()
    func PostRelativeManagement(PostRelativeManagement: Parameters, imageData: [String:Data]?)
    func bookingdetail(id: Int, param: Parameters)
    func getCategoryList(id : Int)
    func getslot(id: Int)
    func getfinddoctor(id : Int)
    func relativeManagement()
    func getDoctorsList(id : Int)
    func getDoctorsDetail(id : Int)
    func cancelappoinment(id : String)
    func getArticles()
    func getMedicalRecords()
    func getsearchdeatil(count:Int)
    func getAppointmentList()
    func cancelAppointments(id:Parameters)
    func getvistiordoctor()
    func visitorpost(param: Parameters, id: Int, doctor_profile_id:Int, provider_id:Int)
    func endCall(param: Parameters)
    func ratingView(param: Parameters)
    func appointmentsUpdate(param:Parameters)
}

//MARK:- Doctor view to Doctor presenter protocol

protocol DoctorViewToDoctorPresenterProtocol: class {
    
    var doctorView: DoctorPresenterToDoctorViewProtocol? { get set}
    var doctorInteractor: DoctorPresenterToDoctorInteractorProtocol? { get set }
    var doctorRouter: DoctorPresenterToDoctorRouterProtocol? { get set }
    func getRelativeManagementList()
    func PostRelativeManagement(PostRelativeManagement: Parameters, imageData: [String:Data]?)
    func bookingdetail(id: Int, param: Parameters)
    func relativeManagement()
    func getCategoryList(id : Int)
    func getslot(id: Int)
    func getfinddoctor(id : Int)
    func getDoctorsList(id : Int)
    func getDoctorsDetail(id : Int)
    func cancelappoinment(id : String)
    func getArticles()
    func getMedicalRecords()
    func getsearchdeatil(count:Int)
    func getAppointmentList()
    func cancelAppointments(id:Parameters)
    func appointmentsUpdate(param:Parameters)
    func getvistiordoctor()
    func ratingView(param: Parameters)
    func visitorpost(param: Parameters, id: Int, doctor_profile_id:Int, provider_id:Int)
func endCall(param: Parameters)
}

//MARK:- Doctor Presenter to Doctor Router Protocol

protocol DoctorPresenterToDoctorRouterProtocol {
    static func createDoctorModule() -> UIViewController
}


