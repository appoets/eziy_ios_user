//
//  ArticleModel.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 21/02/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import Foundation
import ObjectMapper

struct Article : Mappable {
    var id : Int?
    var hospital_id : Int?
    var name : String?
    var article_image : [ArticleImage]?
    var description : String?
    var status : String?
    var created_at : String?
    var updated_at : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        hospital_id <- map["hospital_id"]
        name <- map["title"]
        article_image <- map["article_image"]
        description <- map["description"]
        status <- map["status"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
    }
    
}


struct ArticleModel : Mappable {
    var article : [Article]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        article <- map["responseData"]
    }
    
}



struct ArticleImage : Mappable {
    var id : Int?
    var article_id : Int?
    var description : String?
    var image : String?
    var created_at : String?
    var days : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        article_id <- map["article_id"]
        description <- map["description"]
        image <- map["image"]
        created_at <- map["created_at"]
        days <- map["days"]
    }
    
}
