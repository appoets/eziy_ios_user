//
//  SubCategoryEntity.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 16/03/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import Foundation
import ObjectMapper

struct SubCategoryEntity : Mappable {
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : [ResponseData]?
    var error : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }

}
struct ResponseData : Mappable {
    var id : Int?
    var service_category_id : Int?
    var service_subcategory_name : String?
    var picture : String?
    var service_subcategory_order : Int?
    var service_subcategory_status : Int?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        service_category_id <- map["service_category_id"]
        service_subcategory_name <- map["service_subcategory_name"]
        picture <- map["picture"]
        service_subcategory_order <- map["service_subcategory_order"]
        service_subcategory_status <- map["service_subcategory_status"]
    }

}

