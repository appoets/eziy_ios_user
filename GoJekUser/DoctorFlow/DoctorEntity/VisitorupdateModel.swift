//
//  VisitorupdateModel.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 17/05/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import Foundation
import ObjectMapper

struct VisitorupdateModel : Mappable {
    
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : updatResponseData?
    var error : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }

}
struct updatResponseData : Mappable {
    var id : Int?
    var doctor_request_id : Int?
    var doctor_profile_id : String?
    var provider_id : String?
    var user_id : Int?
    var company_id : Int?
    var description : String?
    var like : String?
    var created_at : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        doctor_request_id <- map["doctor_request_id"]
        doctor_profile_id <- map["doctor_profile_id"]
        provider_id <- map["provider_id"]
        user_id <- map["user_id"]
        company_id <- map["company_id"]
        description <- map["description"]
        like <- map["like"]
        created_at <- map["created_at"]
    }

}


struct CallStatusEntity: Mappable {
    var statusCode : String?
    var title : String?
    var message : String?
    var error : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        error <- map["error"]
    }

}
