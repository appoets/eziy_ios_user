//
//  BookingModel.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 21/02/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import Foundation
import ObjectMapper




struct BookingModel : Mappable {
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : [ResponseDatasss]?
    var error : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }

}



struct ResponseDatasss : Mappable {
    var id : Int?
    var doctor_profile_id : Int?
    var provider_id : Int?
    var working_day : String?
    var time_slot : Int?
    var slots_available_status : Int?
    var timing : [Timings]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        doctor_profile_id <- map["doctor_profile_id"]
        provider_id <- map["provider_id"]
        working_day <- map["working_day"]
        time_slot <- map["time_slot"]
        slots_available_status <- map["slots_available_status"]
        timing <- map["timing"]
    }

}




struct Timings : Mappable {
    var id : Int?
    var company_id : Int?
    var start_time : String?
    var end_time : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        company_id <- map["company_id"]
        start_time <- map["start_time"]
        end_time <- map["end_time"]
    }

}
struct BookingReq : Codable {
    var doctor_id : String = ""
    var booking_for : String = ""
    var scheduled_at : String = ""
    var consult_time : String = ""
    var appointment_type : String = ""
    var description : String = ""
    var selectedPatient : String = ""
    var service_id : String = ""

}



struct Relatives : Mappable {
    var id : Int?
    var patient_id : Int?
    var first_name : String?
    var last_name : String?
    var phone : String?
    var username : String?
    var secondary_mobile : String?
    var other_id : String?
    var payment_mode : String?
    var device_token : String?
    var device_id : String?
    var device_type : String?
    var login_by : String?
    var social_unique_id : String?
    var wallet_balance : Int?
    var rating : String?
    var email : String?
    var otp : Int?
    var stripe_cust_id : String?
    var regn_id : String?
    var email_verified : Int?
    var email_token : String?
    var email_verified_at : String?
    var created_at : String?
    var updated_at : String?
    var deleted_at : String?
    var profile : Profile?
    
    init() {
        
    }

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        patient_id <- map["patient_id"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        phone <- map["phone"]
        username <- map["username"]
        secondary_mobile <- map["secondary_mobile"]
        other_id <- map["other_id"]
        payment_mode <- map["payment_mode"]
        device_token <- map["device_token"]
        device_id <- map["device_id"]
        device_type <- map["device_type"]
        login_by <- map["login_by"]
        social_unique_id <- map["social_unique_id"]
        wallet_balance <- map["wallet_balance"]
        rating <- map["rating"]
        email <- map["email"]
        otp <- map["otp"]
        stripe_cust_id <- map["stripe_cust_id"]
        regn_id <- map["regn_id"]
        email_verified <- map["email_verified"]
        email_token <- map["email_token"]
        email_verified_at <- map["email_verified_at"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        deleted_at <- map["deleted_at"]
        profile <- map["profile"]
    }

}


struct CreateRelativeResponse : Mappable {
    var message : String?
    var patient_relative : Patient_relative?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        message <- map["message"]
        patient_relative <- map["patient_relative"]
    }

}


struct Profile : Mappable {
    var id : Int?
    var patient_id : String?
    var age : String?
    var gender : String?
    var dob : String?
    var blood_group : String?
    var profile_pic : String?
    var description : String?
    var refered_by : String?
    var occupation : String?
    var groups : String?
    var address : String?
    var street : String?
    var locality : String?
    var city : String?
    var country : String?
    var postal_code : String?
    var created_at : String?
    var updated_at : String?
    var deleted_at : String?
    var merital_status : String?
    var height : String?
    var weight : String?
    var emergency_contact : String?
    var allergies : String?
    var current_medications : String?
    var past_medications : String?
    var chronic_diseases : String?
    var injuries : String?
    var surgeries : String?
    var smoking : String?
    var alcohol : String?
    var activity : String?
    var food : String?
    
    init() {
        
    }
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        patient_id <- map["patient_id"]
        age <- map["age"]
        gender <- map["gender"]
        dob <- map["dob"]
        blood_group <- map["blood_group"]
        profile_pic <- map["profile_pic"]
        description <- map["description"]
        refered_by <- map["refered_by"]
        occupation <- map["occupation"]
        groups <- map["groups"]
        address <- map["address"]
        street <- map["street"]
        locality <- map["locality"]
        city <- map["city"]
        country <- map["country"]
        postal_code <- map["postal_code"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        deleted_at <- map["deleted_at"]
        merital_status <- map["merital_status"]
        height <- map["height"]
        weight <- map["weight"]
        emergency_contact <- map["emergency_contact"]
        allergies <- map["allergies"]
        current_medications <- map["current_medications"]
        past_medications <- map["past_medications"]
        chronic_diseases <- map["chronic_diseases"]
        injuries <- map["injuries"]
        surgeries <- map["surgeries"]
        smoking <- map["smoking"]
        alcohol <- map["alcohol"]
        activity <- map["activity"]
        food <- map["food"]
    }
    
}



struct Patient_relative : Mappable {
    var id : Int?
    var patient_id : Int?
    var first_name : String?
    var last_name : String?
    var phone : String?
    var username : String?
    var secondary_mobile : String?
    var other_id : String?
    var payment_mode : String?
    var device_token : String?
    var device_id : String?
    var device_type : String?
    var login_by : String?
    var social_unique_id : String?
    var wallet_balance : Int?
    var rating : String?
    var email : String?
    var otp : Int?
    var stripe_cust_id : String?
    var regn_id : String?
    var email_verified : Int?
    var email_token : String?
    var email_verified_at : String?
    var created_at : String?
    var updated_at : String?
    var deleted_at : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        patient_id <- map["patient_id"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        phone <- map["phone"]
        username <- map["username"]
        secondary_mobile <- map["secondary_mobile"]
        other_id <- map["other_id"]
        payment_mode <- map["payment_mode"]
        device_token <- map["device_token"]
        device_id <- map["device_id"]
        device_type <- map["device_type"]
        login_by <- map["login_by"]
        social_unique_id <- map["social_unique_id"]
        wallet_balance <- map["wallet_balance"]
        rating <- map["rating"]
        email <- map["email"]
        otp <- map["otp"]
        stripe_cust_id <- map["stripe_cust_id"]
        regn_id <- map["regn_id"]
        email_verified <- map["email_verified"]
        email_token <- map["email_token"]
        email_verified_at <- map["email_verified_at"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        deleted_at <- map["deleted_at"]
    }

}

struct MedicalRecordsEntity : Mappable{
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : [ResponseDataEntity]?
    var error : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }

}
struct ResponseDataEntity : Mappable{
    var checkedout_at : String?
    var provider : ProviderDetails?
    var service : ServiceData?
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        checkedout_at <- map["checkedout_at"]
        service <- map["service"]
        provider <- map["provider"]
    }
}
struct ServiceData : Mappable{
    var id : Int?
    var service_category_id : Int?
    var service_subcategory_name : String?
    var picture : String?
    var service_subcategory_order : Int?
    var service_subcategory_status : Int?
    
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        id <- map["id"]
        service_category_id <- map["service_category_id"]
        service_subcategory_name <- map["service_subcategory_name"]
        picture <- map["picture"]
        service_subcategory_order <- map["service_subcategory_order"]
        service_subcategory_status <- map["service_subcategory_status"]
    }
    
}
