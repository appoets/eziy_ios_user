//
//  DoctorListEntity.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 16/03/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import Foundation
import ObjectMapper

struct  DoctorListEntity : Mappable {
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : Response?
    var error : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }

}
struct Response : Mappable {
    var provider_service : [doctorProvider_service]?
    var currency : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        provider_service <- map["provider_service"]
        currency <- map["currency"]
    }

}
struct doctorProvider_service : Mappable {
    var id : Int?
    var first_name : String?
    var last_name : String?
    var picture : String?
    var location : String?
    var rating : Int?
    var city_id : Int?
    var latitude : Double?
    var longitude : Double?
    var consultation_fee : Int?
    var specialist : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        picture <- map["picture"]
        rating <- map["rating"]
        city_id <- map["city_id"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        consultation_fee <- map["consultation_fee"]
        location <- map["location"]
        specialist <- map["specialist"]
    }

}

