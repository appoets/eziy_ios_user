//
//  RelativeManagementEntity.swift
//  GoJekUser
//
//  Created by Senthil Kumar T on 04/04/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import Foundation
import ObjectMapper

struct RelativeManagementEntity : Mappable {
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : [RelativeManagementListEntity]?
    var error : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }

}

struct RelativeManagementListEntity : Mappable {
    var id : Int?
    var name : String?
    var picture : String?
    var email : String?
    var contact_number : String?
    var gender : String?
    var dob : String?
    var blood_group : String?
    var martial_status : String?
    var height : String?
    var weight : String?
    var emergency_contact : String?
    var allergies : String?
    var current_medications : String?
    var past_medications : String?
    var chronic_disease : String?
    var injury : String?
    var surgery : String?
    var smoking_habits : String?
    var alcohol_consumption : String?
    var activity_level : String?
    var food_preference : String?
    var occupation : String?
    var created_at : String?
    var age : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        name <- map["name"]
        picture <- map["picture"]
        email <- map["email"]
        contact_number <- map["contact_number"]
        gender <- map["gender"]
        dob <- map["dob"]
        blood_group <- map["blood_group"]
        martial_status <- map["martial_status"]
        height <- map["height"]
        weight <- map["weight"]
        emergency_contact <- map["emergency_contact"]
        allergies <- map["allergies"]
        current_medications <- map["current_medications"]
        past_medications <- map["past_medications"]
        chronic_disease <- map["chronic_disease"]
        injury <- map["injury"]
        surgery <- map["surgery"]
        smoking_habits <- map["smoking_habits"]
        alcohol_consumption <- map["alcohol_consumption"]
        activity_level <- map["activity_level"]
        food_preference <- map["food_preference"]
        occupation <- map["occupation"]
        created_at <- map["created_at"]
        age <- map["age"]
    }

}


struct PostRelativeManagementEntity : Mappable {
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : PostRelativeManagementResponseData?
    var error : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }

}

struct PostRelativeManagementResponseData : Mappable {
    var name : String?
    var email : String?
    var contact_number : String?
    var dob : String?
    var blood_group : String?
    var martial_status : String?
    var height : String?
    var weight : String?
    var emergency_contact : String?
    var allergies : String?
    var current_medications : String?
    var past_medications : String?
    var chronic_disease : String?
    var injury : String?
    var surgery : String?
    var smoking_habits : String?
    var alcohol_consumption : String?
    var activity_level : String?
    var food_preference : String?
    var occupation : String?
    var created_at : String?
    var id : Int?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        name <- map["name"]
        email <- map["email"]
        contact_number <- map["contact_number"]
        dob <- map["dob"]
        blood_group <- map["blood_group"]
        martial_status <- map["martial_status"]
        height <- map["height"]
        weight <- map["weight"]
        emergency_contact <- map["emergency_contact"]
        allergies <- map["allergies"]
        current_medications <- map["current_medications"]
        past_medications <- map["past_medications"]
        chronic_disease <- map["chronic_disease"]
        injury <- map["injury"]
        surgery <- map["surgery"]
        smoking_habits <- map["smoking_habits"]
        alcohol_consumption <- map["alcohol_consumption"]
        activity_level <- map["activity_level"]
        food_preference <- map["food_preference"]
        occupation <- map["occupation"]
        created_at <- map["created_at"]
        id <- map["id"]
    }

}
