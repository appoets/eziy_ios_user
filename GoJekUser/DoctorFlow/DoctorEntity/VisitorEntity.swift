//
//  VisitorEntity.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 17/05/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import Foundation
import ObjectMapper

struct VisitorEntity : Mappable {
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : [VisitorResponseData]?
    var error : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }

}
struct VisitorResponseData : Mappable {
    var id : Int?
    var booking_id : String?
    var admin_service : String?
    var user_id : Int?
    var patient_id : Int?
    var provider_id : Int?
    var service_id : Int?
    var city_id : Int?
    var country_id : Int?
    var promocode_id : String?
    var admin_id : String?
    var time_slot_id : Int?
    var scheduled_at : String?
    var scheduled_end : String?
    var engaged_at : String?
    var checkin_at : String?
    var user_checked_in : String?
    var checkedout_at : String?
    var user_checked_out : String?
    var consult_time : String?
    var doctor_profile_timings_id : Int?
    var currency : String?
    var use_wallet : Int?
    var status : String?
    var cancelled_by : String?
    var cancel_reason : String?
    var payment_mode : String?
    var paid : Int?
    var timezone : String?
    var created_type : String?
    var created_at : String?
    var provider : VisitoProvider?
    var doctor_profile : VisitoDoctor_profile?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        booking_id <- map["booking_id"]
        admin_service <- map["admin_service"]
        user_id <- map["user_id"]
        patient_id <- map["patient_id"]
        provider_id <- map["provider_id"]
        service_id <- map["service_id"]
        city_id <- map["city_id"]
        country_id <- map["country_id"]
        promocode_id <- map["promocode_id"]
        admin_id <- map["admin_id"]
        time_slot_id <- map["time_slot_id"]
        scheduled_at <- map["scheduled_at"]
        scheduled_end <- map["scheduled_end"]
        engaged_at <- map["engaged_at"]
        checkin_at <- map["checkin_at"]
        user_checked_in <- map["user_checked_in"]
        checkedout_at <- map["checkedout_at"]
        user_checked_out <- map["user_checked_out"]
        consult_time <- map["consult_time"]
        doctor_profile_timings_id <- map["doctor_profile_timings_id"]
        currency <- map["currency"]
        use_wallet <- map["use_wallet"]
        status <- map["status"]
        cancelled_by <- map["cancelled_by"]
        cancel_reason <- map["cancel_reason"]
        payment_mode <- map["payment_mode"]
        paid <- map["paid"]
        timezone <- map["timezone"]
        created_type <- map["created_type"]
        created_at <- map["created_at"]
        provider <- map["provider"]
        doctor_profile <- map["doctor_profile"]
    }

}
struct VisitoDoctor_profile : Mappable {
    var id : Int?
    var doctor_profile_id : Int?
    var provider_id : Int?
    var working_day : String?
    var time_slot : Int?
    var slots_available_status : Int?
    var doctor_profile_details : VisitoDoctor_profile_details?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        doctor_profile_id <- map["doctor_profile_id"]
        provider_id <- map["provider_id"]
        working_day <- map["working_day"]
        time_slot <- map["time_slot"]
        slots_available_status <- map["slots_available_status"]
        doctor_profile_details <- map["doctor_profile_details"]
    }

}
struct VisitoProvider : Mappable {
    var id : Int?
    var unique_id : String?
    var first_name : String?
    var last_name : String?
    var payment_mode : String?
    var email : String?
    var country_code : String?
    var iso2 : String?
    var currency : String?
    var currency_symbol : String?
    var mobile : String?
    var gender : String?
    var device_token : String?
    var device_id : String?
    var device_type : String?
    var login_by : String?
    var social_unique_id : String?
    var latitude : Double?
    var longitude : Double?
    var current_location : String?
    var stripe_cust_id : String?
    var wallet_balance : Int?
    var is_online : Int?
    var is_assigned : Int?
    var rating : Int?
    var status : String?
    var is_service : Int?
    var is_document : Int?
    var is_bankdetail : Int?
    var is_profile : Int?
    var is_address : Int?
    var picture_draft : String?
    var admin_id : String?
    var payment_gateway_id : String?
    var otp : String?
    var language : String?
    var picture : String?
    var qrcode_url : String?
    var referral_unique_id : String?
    var referal_count : Int?
    var country_id : Int?
    var state_id : Int?
    var city_id : Int?
    var zone_id : String?
    var activation_status : Int?
    var airport_at : String?
    var enable_doctor : Int?
    var current_ride_vehicle : String?
    var current_store : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        unique_id <- map["unique_id"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        payment_mode <- map["payment_mode"]
        email <- map["email"]
        country_code <- map["country_code"]
        iso2 <- map["iso2"]
        currency <- map["currency"]
        currency_symbol <- map["currency_symbol"]
        mobile <- map["mobile"]
        gender <- map["gender"]
        device_token <- map["device_token"]
        device_id <- map["device_id"]
        device_type <- map["device_type"]
        login_by <- map["login_by"]
        social_unique_id <- map["social_unique_id"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        current_location <- map["current_location"]
        stripe_cust_id <- map["stripe_cust_id"]
        wallet_balance <- map["wallet_balance"]
        is_online <- map["is_online"]
        is_assigned <- map["is_assigned"]
        rating <- map["rating"]
        status <- map["status"]
        is_service <- map["is_service"]
        is_document <- map["is_document"]
        is_bankdetail <- map["is_bankdetail"]
        is_profile <- map["is_profile"]
        is_address <- map["is_address"]
        picture_draft <- map["picture_draft"]
        admin_id <- map["admin_id"]
        payment_gateway_id <- map["payment_gateway_id"]
        otp <- map["otp"]
        language <- map["language"]
        picture <- map["picture"]
        qrcode_url <- map["qrcode_url"]
        referral_unique_id <- map["referral_unique_id"]
        referal_count <- map["referal_count"]
        country_id <- map["country_id"]
        state_id <- map["state_id"]
        city_id <- map["city_id"]
        zone_id <- map["zone_id"]
        activation_status <- map["activation_status"]
        airport_at <- map["airport_at"]
        enable_doctor <- map["enable_doctor"]
        current_ride_vehicle <- map["current_ride_vehicle"]
        current_store <- map["current_store"]
    }

}
struct VisitoDoctor_profile_details : Mappable {
    var id : Int?
    var company_id : Int?
    var provider_id : Int?
    var dob : String?
    var qualification : String?
    var experience : Int?
    var consultation_fee : Int?
    var specialist : String?
    var picture : String?
    var medical_picture : String?
    var clinic_photos : String?
    var schedule_date : String?
    var service : String?
    var specializations : String?
    var location : String?
    var latitude : Double?
    var longitude : Double?
    var status : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        company_id <- map["company_id"]
        provider_id <- map["provider_id"]
        dob <- map["dob"]
        qualification <- map["qualification"]
        experience <- map["experience"]
        consultation_fee <- map["consultation_fee"]
        specialist <- map["specialist"]
        picture <- map["picture"]
        medical_picture <- map["medical_picture"]
        clinic_photos <- map["clinic_photos"]
        schedule_date <- map["schedule_date"]
        service <- map["service"]
        specializations <- map["specializations"]
        location <- map["location"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        status <- map["status"]
    }

}

