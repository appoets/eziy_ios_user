//
//  DoctorInteractor.swift
//  GoJekUser
//
//  Created by apple on 20/02/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit
import Alamofire

class DoctorInteractor: DoctorPresenterToDoctorInteractorProtocol {
    func ratingView(param: Parameters) {
        WebServices.shared.requestToApi(type: CallStatusEntity.self, with:  DoctoryAPI.reviews, urlMethod: .post, showLoader: true,params: param) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.updateRating(call: response)
            }}

    }
    
    func appointmentsUpdate(param: Parameters) {
        WebServices.shared.requestToApi(type: CallStatusEntity.self, with:  DoctoryAPI.updateAppointment, urlMethod: .post, showLoader: true,params: param) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.updateApointment(call: response)
            }}

    }
    
    func endCall(param: Parameters) {
        WebServices.shared.requestToApi(type: CallStatusEntity.self, with:  DoctoryAPI.endCall, urlMethod: .post, showLoader: true,params: nil) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.endCallStatus(call: response)
            }}
    }
    
    func getDoctorsDetail(id: Int) {
        var  url = String()
        
            url = DoctoryAPI.docDetail + "?id=\(id)"
            
       print("URL_Detail",url)
        WebServices.shared.requestToApi(type: DoctorDetailModel.self, with:  url, urlMethod: .get, showLoader: true,params: nil) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.getDoctordeatil(doctor: response)
            }}
    }
    
    func visitorpost(param: Parameters, id: Int, doctor_profile_id: Int, provider_id: Int) {
        let url = DoctoryAPI.visitorupdate + "?id=\(id)&doctor_profile_id=\(doctor_profile_id)&provider_id=\(provider_id)"
        WebServices.shared.requestToApi(type: VisitorupdateModel.self, with:  url, urlMethod: .post, showLoader: true,params: param) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.visitorpost(visitors: response)
            }}
    }
    
    func visitorpost(param: Parameters, id: Int) {
        let url = DoctoryAPI.visitorupdate + "?id=\(id)"
        WebServices.shared.requestToApi(type: VisitorupdateModel.self, with:  url, urlMethod: .post, showLoader: true,params: param) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.visitorpost(visitors: response)
            }}
    }
    

    
  
    
   
    func cancelAppointments(id: Parameters) {
        WebServices.shared.requestToApi(type: SuccessEntity.self, with: DoctoryAPI.cancelAppointmentss, urlMethod: .post, showLoader: true,params: id) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.cancelAppointments(success: response)
            }}
    }
    
    
    
    func PostRelativeManagement(PostRelativeManagement: Parameters, imageData: [String : Data]?) {
        WebServices.shared.requestToImageUpload(type: PostRelativeManagementEntity.self, with: DoctoryAPI.postRelativesList, imageData: imageData, showLoader: true, params: PostRelativeManagement, accessTokenAdd: false) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let responseValue = response?.value {
                self.doctorPresenter?.PostRelativeManagement(PostRelativeManagement: responseValue)
            }
        }
    }
    
    func getRelativeManagementList() {
        let url = DoctoryAPI.getRelativesList
        WebServices.shared.requestToApi(type: RelativeManagementEntity.self, with:  url, urlMethod: .get, showLoader: true,params: nil) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.RelativeManagement(RelativeManagement: response)
            }}
    }
    
    func relativeManagement() {
        let url = DoctoryAPI.articles
        WebServices.shared.requestToApi(type: ArticleModel.self, with:  url, urlMethod: .get, showLoader: true,params: nil) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.getArticels(data: response)
            }}
    }
    func getsearchdeatil(count:Int){
        var  url = String()
        if isFromSearchcontroller == true {
            url = DoctoryAPI.search + "?name=\(searchTextWord)"
            isFromSearchcontroller = false
        }else {
            url = DoctoryAPI.search
        }
        WebServices.shared.requestToApi(type: searchmodel.self, with:  url, urlMethod: .get, showLoader: true,params: nil) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.getsearchdeatil(searchmodel: response)
            }}
    }
    func getAppointmentList() {
       
        let url = DoctoryAPI.getAppointmentList
        WebServices.shared.requestToApi(type: AppointmentList.self, with:  url, urlMethod: .get, showLoader: true,params: nil) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.getAppointmentList(getAppointmentList: response)
            }}
    }
    
    func getArticles() {
        let url = DoctoryAPI.articles
        WebServices.shared.requestToApi(type: ArticleModel.self, with:  url, urlMethod: .get, showLoader: true,params: nil) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.getArticels(data: response)
            }}
    }
    
    func bookingdetail(id: Int, param: Parameters) {
        let url = DoctoryAPI.booking //+ "?id=\(id)"
        print("Param",param)
        WebServices.shared.requestToApi(type: PatientAppoinmentEntity.self, with:  url, urlMethod: .post, showLoader: true,params: param) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.bookingdetail(PatientAppoinment: response)
            }}
    }
    
    func getslot(id: Int){
        let url = DoctoryAPI.slot + "?provider_id=\(id)&schedule_date=03/21/2022"
        WebServices.shared.requestToApi(type: BookingModel.self, with:  url, urlMethod: .get, showLoader: false,params: nil) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
//                self.doctorPresenter?.getDoctorsList(DoctorListEntity: response)
                self.doctorPresenter?.getslot(BookingModels: response)
            }}
    }
    
    func getDoctorsList(id: Int) {
        let url = DoctoryAPI.subcatorgylist + "?id=\(id)"
        WebServices.shared.requestToApi(type: DoctorListEntity.self, with:  url, urlMethod: .get, showLoader: false,params: nil) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.getDoctorsList(DoctorListEntity: response)
            }}
    }
    
    func getfinddoctor(id: Int) {
        let url = DoctoryAPI.doctorServiceSubCategory + "\(id)" + "?city_id=3659"
        
        WebServices.shared.requestToApi(type: SubCategoryEntity.self, with:  url, urlMethod: .get, showLoader: false,params: nil) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.getfinddoctor(SubCategoryEntity: response)
            }}
        
    }
    

    
    func getvistiordoctor() {
        let url = DoctoryAPI.getvisitedDoctor

        WebServices.shared.requestToApi(type: VisitorEntity.self, with:  url, urlMethod: .get, showLoader: false,params: nil) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                
                self.doctorPresenter?.getvistiordoctor(visitor: response)
            }}
    }
    
    func cancelappoinment(id: String) {
//        let url = DoctoryAPI.doctorServiceSubCategory + "\(id)"
        let url = DoctoryAPI.cancelAppointment + "\(id)"
        WebServices.shared.requestToApi(type: Category.self, with:  url, urlMethod: .post, showLoader: false,params: nil) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.cancelappoinment(data: response)
            }}
    }
    
    func getCategoryList(id: Int) {
        let url = DoctoryAPI.doctorServiceSubCategory + "\(id)"
        WebServices.shared.requestToApi(type: CategoryList.self, with:  url, urlMethod: .get, showLoader: false,params: nil) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.categoryList(data: response)
            }}
    }
    func getMedicalRecords() {
        WebServices.shared.requestToApi(type: MedicalRecordsEntity.self, with: DoctoryAPI.getMedicalRec, urlMethod: .get, showLoader: false,params: nil) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.getMedicalRecords(medicalRecordsEntity: response)
            }}
    }
   
   
    
  
    var doctorPresenter: DoctorInteractorToDoctorPresenterProtocol?
    
  
}

