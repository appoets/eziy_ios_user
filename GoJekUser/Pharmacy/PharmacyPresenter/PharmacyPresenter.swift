//
//  PharmacyPresenter.swift
//  GoJekUser
//
//  Created by Shyamala's MacBook Pro on 22/11/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import Foundation

class PharmacyPresenter : PharmacyViewToPharmacyPresenterProtocol{
    
    
    var pharmacyView: PharmacyPresenterToPharmacyViewProtocol?
    
    var pharmacyInteractor: PharmacyPresenterToPharmacyInteractorProtocol?
    
    var pharmacyRouter: PharmacyPresenterToPharmacyRouterProtocol?
    
    func sendData(data: String) {
        pharmacyInteractor?.sendData(data: data)
    }
    
}

extension PharmacyPresenter : PharmacyInteractorToPharmacyPresenterProtocol{
    func getData(data: String) {
        pharmacyView?.getData(data: data)
    }
    
    
}
