//
//  PharmacyFilter.swift
//  GoJekUser
//
//  Created by Shyamala's MacBook Pro on 23/11/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit
//PharmacyFilter
class PharmacyFilter: UIViewController {
    
    @IBOutlet weak var filterTableView: UITableView!
    @IBOutlet weak var applyFiterButton: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var headingLabel: UILabel!
    
    @IBOutlet weak var closeBgButton: UIButton!
    @IBOutlet weak var bottomLine: UIView!
    
    var appliedFilter:Bool = false
    var cusineList:[String] = ["Chennai,India" , "Delhi, India" , "Vellore,India" , "Bangalore,India"]
    var showRestaurantList = ["25% - 30%","20% - 25%","15% - 20%"]
    var qfilterArr = NSMutableArray()
    var filterArr = NSMutableArray()
    weak var delegate: PharmacyFilterDelegate?
    var isFoodie:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialLoads()
    }
    
    
}

//MARK: - Methods

extension PharmacyFilter {
    private func initialLoads()  {
        self.closeBgButton.addTarget(self, action: #selector(tapCloseButton), for: .touchUpInside)
        self.resetButton.addTarget(self, action: #selector(tapResetButton), for: .touchUpInside)
        applyFiterButton.addTarget(self, action: #selector(tapapplyFilterButton), for: .touchUpInside)
        self.filterTableView.register(nibName: FoodieConstant.FoodieFilterTableViewCell)
        self.closeButton.setImage(UIImage(named: Constant.closeImage), for: .normal)
        self.applyFiterButton.backgroundColor = .pharmacyColor
        self.applyFiterButton.setCornerRadius()
        resetButton.setTitleColor(.lightGray, for: .normal)
        bottomLine.backgroundColor = .veryLightGray
        
        setFont()
        setLocalize()
        foodiePresenter?.getCusineList(Id: AppManager.shared.getSelectedServices()?.menu_type_id ?? 0)
        setDarkMode()
    }
    
    
    private func setDarkMode(){
        self.view.backgroundColor = .white
        self.closeButton.tintColor = .blackColor
        
//        filterTableView.layer.cornerRadius = 50
        filterTableView.layer.masksToBounds = false
        filterTableView.layer.shadowColor = UIColor.black.withAlphaComponent(0.4).cgColor // any value you want
        filterTableView.layer.shadowOpacity = 0.5 // any value you want
        filterTableView.layer.shadowRadius = 5// any value you want
        filterTableView.layer.shadowOffset = CGSize(width: 1, height: 1)

    }
    
    func setLocalize(){
        self.headingLabel.text = FoodieConstant.filters.localized.uppercased()
        self.resetButton.setTitle(FoodieConstant.clearAll.localized.capitalized, for: .normal)
        applyFiterButton.setTitle(FoodieConstant.done.localized.uppercased(), for: .normal)
    }
    
    func setFont(){
        applyFiterButton.titleLabel?.font = UIFont.setCustomFont(name: .medium, size: .x16)
        resetButton.titleLabel?.font = UIFont.setCustomFont(name: .medium, size: .x12)
        headingLabel.font = UIFont.setCustomFont(name: .bold, size: .x20)
        headingLabel.textColor = .pharmacyColor
    }
    
    
    @objc func tapCloseButton() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func tapapplyFilterButton() {
        
        for item in qfilterArr {
            let itemStr = item as! String
            if itemStr == FoodieConstant.pureVeg {
                qfilterArr.remove(item)
                qfilterArr.add(RestaurantType.veg.rawValue)
            }else if itemStr == FoodieConstant.nonVeg{
                qfilterArr.remove(item)
                qfilterArr.add(RestaurantType.nonveg.rawValue)
            }else if itemStr == FoodieConstant.freeDelivery {
                qfilterArr.remove(item)
                qfilterArr.add(RestaurantType.freedelivery.rawValue)
            }
        }
        let filterstr = filterArr.componentsJoined(by: ",")
        let qFilterstr = qfilterArr.componentsJoined(by: ",")
        
        delegate?.applyFilterAction(filterArr: filterstr, qfilter: qFilterstr)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @objc func tapResetButton() {
        filterArr.removeAllObjects()
        qfilterArr.removeAllObjects()
        filterTableView.reloadData()
    }
}

//MARK: - Tableview Delegate Datasource

extension PharmacyFilter: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = self.filterTableView.cellForRow(at: indexPath) as! FoodieFilterTableViewCell
        cell.isSelectedItem = !cell.isSelectedItem
        
      
    }
}

extension PharmacyFilter: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
            return section == 0 ? showRestaurantList.count: cusineList.count
     
    }
    

    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return  40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30))
        headerView.backgroundColor = .white
        
        let label = UILabel(frame: CGRect(x: 10, y: 10, width: headerView.frame.width-40, height: headerView.frame.height))
        label.font = UIFont.setCustomFont(name: .medium, size: .x18)
        
            label.text = section == 0 ? "Offers" : "Country"
      
        headerView.addSubview(label)
        headerView.clipsToBounds = false
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:FoodieFilterTableViewCell = self.filterTableView.dequeueReusableCell(withIdentifier: FoodieConstant.FoodieFilterTableViewCell, for: indexPath) as! FoodieFilterTableViewCell

    
            if indexPath.section == 0 {
                cell.titleLabel.text = self.showRestaurantList[indexPath.row]
                if qfilterArr.contains(self.showRestaurantList[indexPath.row]) {
                    cell.isSelectedItem = true
                }else{
                    cell.isSelectedItem = false
                }
            }else{
                if filterArr.contains(cusineList[indexPath.row]) {
                    cell.isSelectedItem = true
                }else{
                    cell.isSelectedItem = false
                    
                }
                cell.titleLabel.text = self.cusineList[indexPath.row]
               
            }
     
        cell.clipsToBounds = false
        return cell
    }
    
}
extension PharmacyFilter: FoodiePresenterToFoodieViewProtocol{
    func cusineListResponse(getCusineListResponse: CusineListEntity) {
      
    }
}

// MARK: - Protocol
protocol PharmacyFilterDelegate: class {
    func applyFilterAction(filterArr:String,qfilter:String)
}
