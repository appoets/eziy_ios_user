//
//  PharmacyHomeViewController.swift
//  GoJekUser
//
//  Created by Shyamala's MacBook Pro on 22/11/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit

class PharmacyHomeViewController: UIViewController {
    
    @IBOutlet weak var backBtn : UIButton!
    @IBOutlet weak var cartBtn : UIButton!
    @IBOutlet weak var titleLbl : UILabel!
    @IBOutlet weak var pharmacyTable : UITableView!
    
    
    var headerView : PharmacyHeaderView?

    override func viewDidLoad() {
        super.viewDidLoad()
        pharmacyPresenter?.sendData(data: "Hello Pharmacy")
        
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initialLoads()
        self.setupTableView()
        self.setupAction()
        
    }
}

extension PharmacyHomeViewController{
    
    private func initialLoads() {
        
        self.titleLbl.text = PharmacyConstant.Pharmacy.localized
        self.titleLbl.font = .setCustomFont(name: .bold, size: .x18)
        self.backBtn.setTitle("", for: .normal)
        self.cartBtn.setTitle("", for: .normal)
        self.titleLbl.text = "All Pharmacy"
        
    }
    
    func setupAction(){
        self.backBtn.addTap {
            self.navigationController?.popViewController(animated: true)
        }
        
        self.cartBtn.addTap {
            
        }
    }
}

extension PharmacyHomeViewController : UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else if section == 1{
            return 10
        }else{
            return 1
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyConstant.PharmacyOfferCell, for: indexPath) as! PharmacyOfferCell
            cell.contentView.backgroundColor = .clear
            return cell
        }else if indexPath.section == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyConstant.RecommandedTableCell, for: indexPath) as! RecommandedTableCell
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyConstant.PharmacyListCell, for: indexPath) as!
            PharmacyListCell
            cell.contentView.addTap {
                let vc = PharmacyRouter.pharmacyStoryboard.instantiateViewController(withIdentifier: PharmacyConstant.PharmacyDetailVC) as! PharmacyDetailVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 220
        }else  if indexPath.section == 2{
            return 280
        }
        else{
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }else{
            return 50
        }
    }
    
//
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        let views =  Bundle.main.loadNibNamed(PharmacyConstant.PharmacyHeaderView, owner: self, options: [:])?.first as! PharmacyHeaderView
        views.frame = CGRect(x: 0, y: 0, width:self.view.bounds.width - 400, height: 10)
        views.backgroundColor = .clear
        headerView.addSubview(views)
        if section == 1{
            views.titleLbl.text = "Recent Searches"
            views.seeAllLbl.addTap {
                let vc = PharmacyRouter.pharmacyStoryboard.instantiateViewController(withIdentifier: PharmacyConstant.PharmacyListVC) as! PharmacyListVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            views.titleLbl.text = "Recommended".uppercased()
        }
        headerView.clipsToBounds = true
        return headerView
    }
    

    func setupTableView(){
        self.pharmacyTable.delegate = self
        self.pharmacyTable.dataSource = self
        self.pharmacyTable.register(nibName: PharmacyConstant.PharmacyOfferCell)
        self.pharmacyTable.register(nibName: PharmacyConstant.RecommandedTableCell)
        self.pharmacyTable.register(nibName: PharmacyConstant.PharmacyListCell)
        
        
    }
    
  
}

extension PharmacyHomeViewController : PharmacyPresenterToPharmacyViewProtocol{
    func getData(data: String) {
        print("GettData is",data)
    }
}


