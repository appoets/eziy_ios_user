//
//  MedicineVC.swift
//  GoJekUser
//
//  Created by Shyamala's MacBook Pro on 23/11/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit

class MedicineVC: UIViewController,UITextViewDelegate {
    
    @IBOutlet weak var backBtn : UIButton!
    @IBOutlet weak var cartBtn : UIButton!
    @IBOutlet weak var titleLbl : UILabel!
    @IBOutlet weak var yourUIViewView : UITextView!
    @IBOutlet weak var termLabel : UILabel!
    
    let termText = "By register, I agree to ... Terms of $Service an@d $Private Policy"
    let URLText = "By register, I https://www.avanderlee.com/swift/url-components/ to ... Termhttps://stackoverflow.com/questions/28079123/how-to-check-validity-of-url-in-swifts of @Ser#vice an@d #Private #Policy"
    let term = "Terms of Service"
    let policy = "Private Policy"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLbl.font = .setCustomFont(name: .bold, size: .x18)
        self.backBtn.setTitle("", for: .normal)
        self.cartBtn.setTitle("", for: .normal)
        
      
        let formattedText = String.format(strings: termText.findMentionText(),
                                            boldFont: UIFont.boldSystemFont(ofSize: 15),
                                            boldColor: UIColor.blue,
                                            inString: termText,
                                            font: UIFont.systemFont(ofSize: 15),
                                            color: UIColor.black)
        termLabel.attributedText = String.formats(strings:  URLText.finURLMentionText(), inString: URLText)
        termLabel.numberOfLines = 0
//        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTermTapped))
//        termLabel.addGestureRecognizer(tap)
        termLabel.isUserInteractionEnabled = true
        termLabel.textAlignment = .center
        
//        let formattedURLText = String.format(strings: URLText.finURLMentionText(),
//                                            boldFont: UIFont.boldSystemFont(ofSize: 14),
//                                             boldColor: UIColor.blue.withAlphaComponent(0.3),
//                                            inString: URLText,
//                                            font: UIFont.systemFont(ofSize: 14),
//                                            color: UIColor.black)
        yourUIViewView.attributedText = String.formats(strings:  URLText.finURLMentionText(), inString: URLText)
        let taps = UITapGestureRecognizer(target: self, action: #selector(handleTermssTapped))
        yourUIViewView.addGestureRecognizer(taps)
//        yourUIViewView.isUserInteractionEnabled = true
        yourUIViewView.isEditable = false
        yourUIViewView.textAlignment = .center
        
        guard let htmlData = NSString(string: "go to <a href=\"http://www.google.com\">google</a> and search for it").data(using: String.Encoding.unicode.rawValue) else { return }
            
        DispatchQueue.main.async {
            do {

//            let attributedString = try NSAttributedString(data: htmlData, options: [Nsd: NSHTMLTextDocumentType], documentAttributes: nil)
//                self.yourUIViewView.isSelectable = true
//                self.yourUIViewView.dataDetectorTypes = .link
//                self.yourUIViewView.attributedText = attributedString
//                self.yourUIViewView.delegate = self
                
                let attributedString = NSMutableAttributedString(string: "Want to learn iOS? You should visit the best source of free iOS tutorials! sdfghjkl oqihdlkqbsdj,sb")
                
                attributedString.addAttribute(.link, value: "https://www.hackingwithswift.com", range: NSRange(location: 19, length: 55))
                
                
                attributedString.addAttribute(.link, value: "https://www.google.com", range: NSRange(location: 57, length: 20))

//                self.yourUIViewView.attributedText = attributedString

            } catch {
                print("Cannot setup link with \(htmlData)!")
            }
        }
    }


    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        // Return NO if you don't want iOS to open the link
        return true
    }
    
    
    @objc func handleTermssTapped(gesture: UITapGestureRecognizer) {
        let termString = URLText as NSString
        let termRange = termString.range(of: term)
        let policyRange = termString.range(of: policy)
     
        let tapLocation = gesture.location(in: yourUIViewView)
        let index = yourUIViewView.indexOfAttributedTextCharacterAtPoint(point: tapLocation)
        for i in self.URLText.findMentionText(){
            if checkRange(termString.range(of: i), contain: index){
                print("asdfghjk",i)
               
                return
            }
        }
    }
    
    @objc func handleTermTapped(gesture: UITapGestureRecognizer) {
        let termString = termText as NSString
        let termRange = termString.range(of: term)
        let policyRange = termString.range(of: policy)

        let tapLocation = gesture.location(in: termLabel)
        let index = termLabel.indexOfAttributedTextCharacterAtPoint(point: tapLocation)
        for i in self.termText.findMentionText(){
            if checkRange(termString.range(of: i), contain: index){
                print("asdfghjk",i)
                return
            }
        }

//        if checkRange(termRange, contain: index) == true {
////            handleViewTermOfUse()
//            print("asdfghjk")
//            return
//        }
//
//        if checkRange(policyRange, contain: index) {
////            handleViewPrivacy()
//            print("policyRangepolicyRangepolicyRange")
//            return
//        }
    }
    
    func checkRange(_ range: NSRange, contain index: Int) -> Bool {
        return index > range.location && index < range.location + range.length
    }
}

extension UILabel {
    func indexOfAttributedTextCharacterAtPoint(point: CGPoint) -> Int {
        assert(self.attributedText != nil, "This method is developed for attributed string")
        let textStorage = NSTextStorage(attributedString: self.attributedText!)
        let layoutManager = NSLayoutManager()
        textStorage.addLayoutManager(layoutManager)
        let textContainer = NSTextContainer(size: self.frame.size)
        textContainer.lineFragmentPadding = 0
        textContainer.maximumNumberOfLines = self.numberOfLines
        textContainer.lineBreakMode = self.lineBreakMode
        layoutManager.addTextContainer(textContainer)

        let index = layoutManager.characterIndex(for: point, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return index
    }
    
    
}

extension UITextView{
    func indexOfAttributedTextCharacterAtPoint(point: CGPoint) -> Int {
        assert(self.attributedText != nil, "This method is developed for attributed string")
        let textStorage = NSTextStorage(attributedString: self.attributedText!)
        let layoutManager = NSLayoutManager()
        textStorage.addLayoutManager(layoutManager)
        let textContainer = NSTextContainer(size: self.frame.size)
        textContainer.lineFragmentPadding = 0
//        textContainer.maximumNumberOfLines = self.numberOfLines
//        textContainer.lineBreakMode = self.lineBreakMode
        layoutManager.addTextContainer(textContainer)

        let index = layoutManager.characterIndex(for: point, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return index
    }
}

extension String {
    static func format(strings: [String],
                    boldFont: UIFont = UIFont.boldSystemFont(ofSize: 14),
                    boldColor: UIColor = UIColor.blue,
                    inString string: String,
                    font: UIFont = UIFont.systemFont(ofSize: 14),
                    color: UIColor = UIColor.black) -> NSAttributedString {
        let attributedString =
            NSMutableAttributedString(string: string,
                                    attributes: [
                                        NSAttributedString.Key.font: font,
                                        NSAttributedString.Key.foregroundColor: color])
        let boldFontAttribute = [NSAttributedString.Key.font: boldFont, NSAttributedString.Key.foregroundColor: boldColor]
        for bold in strings {
            attributedString.addAttributes(boldFontAttribute, range: (string as NSString).range(of: bold))
        }
        return attributedString
    }
}

extension String {
    func findMentionText() -> [String] {
        var arr_hasStrings:[String] = []
        let regex = try? NSRegularExpression(pattern: "(@[a-zA-Z0-9_\\p{Arabic}\\p{N}]*)", options: [])
        if let matches = regex?.matches(in: self, options:[], range:NSMakeRange(0, self.count)) {
            for match in matches {
                arr_hasStrings.append(NSString(string: self).substring(with: NSRange(location:match.range.location, length: match.range.length)))
            }
        }
        return arr_hasStrings
    }
    
    func finURLText() -> [String] {
        var arr_hasStrings:[String] = []
        
        let regExs = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
        let regex = try? NSRegularExpression(pattern: regExs, options: [])
        if let matches = regex?.matches(in: self, options:[], range:NSMakeRange(0, self.count)) {
            for match in matches {
                arr_hasStrings.append(NSString(string: self).substring(with: NSRange(location:match.range.location, length: match.range.length)))
            }
        }
        return arr_hasStrings
    }
    
    func finURLMentionText() -> ([String],[String]) {
        var arr_URLStrings:[String] = []
        var arr_hasStrings:[String] = []
       
        let regExs = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
        let regex = try? NSRegularExpression(pattern: regExs, options: [])
        if let matches = regex?.matches(in: self, options:[], range:NSMakeRange(0, self.count)) {
            for match in matches {
                arr_URLStrings.append(NSString(string: self).substring(with: NSRange(location:match.range.location, length: match.range.length)))
            }
        }
        
        
        let regexs = try? NSRegularExpression(pattern: "(#[a-zA-Z0-9_\\p{Arabic}\\p{N}]*)", options: [])
        if let matches = regexs?.matches(in: self, options:[], range:NSMakeRange(0, self.count)) {
            for match in matches {
                arr_hasStrings.append(NSString(string: self).substring(with: NSRange(location:match.range.location, length: match.range.length)))
            }
        }
//        let commonArry : [String] = arr_URLStrings + arr_hasStrings
        return (arr_URLStrings,arr_hasStrings)
    }
    
    static func formats(strings: ([String],[String]),
                    boldFont: UIFont = UIFont.boldSystemFont(ofSize: 14),
                    boldColor: UIColor = UIColor.blue,
                    inString string: String,
                    font: UIFont = UIFont.systemFont(ofSize: 14),
                    color: UIColor = UIColor.black) -> NSAttributedString {
        let attributedString =
            NSMutableAttributedString(string: string,
                                    attributes: [
                                        NSAttributedString.Key.font: font,
                                        NSAttributedString.Key.foregroundColor: color])
        let boldFontAttribute = [NSAttributedString.Key.font: boldFont, NSAttributedString.Key.foregroundColor: boldColor]
        let boldsFontAttribute = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: UIColor.red]
        for bold in strings.0 {
            attributedString.addAttribute(.link, value: bold, range:  (string as NSString).range(of: bold))
       }
        for bolds in strings.1 {
            attributedString.addAttributes(boldsFontAttribute, range: (string as NSString).range(of: bolds))
        }
        return attributedString
    }
   
}
