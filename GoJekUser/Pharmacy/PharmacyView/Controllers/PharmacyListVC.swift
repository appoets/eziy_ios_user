//
//  PharmacyListVC.swift
//  GoJekUser
//
//  Created by Shyamala's MacBook Pro on 23/11/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit

class PharmacyListVC: UIViewController {

        
    @IBOutlet weak var backBtn : UIButton!
    @IBOutlet weak var titleLbl : UILabel!
    @IBOutlet weak var pharmacyTable : UITableView!
    @IBOutlet weak var filterLbl : UILabel!
    @IBOutlet weak var searchLbl : UILabel!
    @IBOutlet weak var filterView : UIView!
    @IBOutlet weak var searchView : UIView!
        
    
    var shopArrList:[ShopsListData] = []
        var headerView : PharmacyHeaderView?

        override func viewDidLoad() {
            super.viewDidLoad()
           
        }
       
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            self.initialLoads()
            self.setupTableView()
            self.setupAction()
        }
    }

    extension PharmacyListVC{
        
        private func initialLoads() {
            self.titleLbl.text = PharmacyConstant.Pharmacy.localized
            self.titleLbl.font = .setCustomFont(name: .bold, size: .x18)
            self.backBtn.setTitle("", for: .normal)
            
            self.filterView.addShadow(radius: 10, color: .pharmacyColor)
            self.filterView.layer.cornerRadius = 5
            self.searchView.addShadow(radius: 10, color: .pharmacyColor)
            self.searchView.layer.cornerRadius = 5
            self.filterLbl.font = .setCustomFont(name: .bold, size: .x18)
            self.searchLbl.font = .setCustomFont(name: .bold, size: .x18)
            self.filterLbl.text = "Filter"
            self.searchLbl.text = "Search"
            
        }
        
        func setupAction(){
            self.backBtn.addTap {
                self.navigationController?.popViewController(animated: true)
            }
            
            self.filterView.addTap {
                let pharmacyVC = PharmacyRouter.pharmacyStoryboard.instantiateViewController(withIdentifier: PharmacyConstant.PharmacyFilter) as! PharmacyFilter
                
                if #available(iOS 15.0, *) {
//                    if let presentationController = pharmacyVC.presentationController as? UISheetPresentationController {
//                        presentationController.detents = [.medium(),.large()]
//                    }
                } else {
                    pharmacyVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                }
                self.navigationController?.present(pharmacyVC, animated: true, completion: nil)
            }
           
        }
    }

    extension PharmacyListVC : UITableViewDelegate,UITableViewDataSource{
       
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.shopArrList.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            if shopArrList.count == 0 {
               let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyflowConstant.EmptyShopTableCell, for: indexPath) as! EmptyShopTableCell
               return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyflowConstant.PharmacyListCell, for: indexPath) as! PharmacyListCell
               cell.setShopListData(data: shopArrList[indexPath.row])
                cell.contentView.addTap {
                    let vc = PharmacyRouter.pharmacyStoryboard.instantiateViewController(withIdentifier: PharmacyConstant.PharmacyDetailVC) as! PharmacyDetailVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }
               return cell
           }
        }
        
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
                return 60
        }
        
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let headerView = UIView()
            let views =  Bundle.main.loadNibNamed(PharmacyConstant.TableHeaderView, owner: self, options: [:])?.first as! TableHeaderView
            views.frame = CGRect(x: 0, y: 0, width:self.view.bounds.width - 300, height: 10)
            headerView.addSubview(views)
            views.titleLbl.text = "All Products".capitalized
            views.countLbl.text = "\(self.shopArrList.count) Products available"
            headerView.clipsToBounds = true
            return headerView
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                return UITableView.automaticDimension
        }
        
        func setupTableView(){
            self.pharmacyTable.delegate = self
            self.pharmacyTable.dataSource = self
            self.pharmacyTable.register(nibName: PharmacyConstant.PharmacyListCell)
        }
    }

    extension PharmacyListVC : PharmacyPresenterToPharmacyViewProtocol{
        func getData(data: String) {
            print("GettData is",data)
        }
    }


