//
//  OfferCollectionCell.swift
//  GoJekUser
//
//  Created by Shyamala's MacBook Pro on 22/11/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit

class OfferCollectionCell: UICollectionViewCell {

    @IBOutlet weak var offerView : UIView!
    @IBOutlet weak var offerImg : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.offerView.addShadow(radius: 10, color: .gray)
        self.offerView.layer.cornerRadius = 10
        self.offerView.layer.masksToBounds = true
    }

}
