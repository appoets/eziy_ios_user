//
//  RecommandedTableCell.swift
//  GoJekUser
//
//  Created by Shyamala's MacBook Pro on 22/11/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit

class RecommandedTableCell: UITableViewCell {
    
    @IBOutlet weak var recommandedCollection : UICollectionView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupCollection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension RecommandedTableCell : UICollectionViewDelegate , UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PharmacyConstant.RecommandedCell, for: indexPath) as! RecommandedCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width / 1.8, height: 300)
    }
    
    func setupCollection(){
        self.recommandedCollection.delegate = self
        self.recommandedCollection.dataSource = self
        self.recommandedCollection.register(nibName: PharmacyConstant.RecommandedCell)
    }
    
    
    
}
