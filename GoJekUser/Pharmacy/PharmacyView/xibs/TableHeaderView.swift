//
//  TableHeaderView.swift
//  GoJekUser
//
//  Created by Shyamala's MacBook Pro on 23/11/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit

class TableHeaderView: UIView {

    @IBOutlet weak var titleLbl : UILabel!
    @IBOutlet weak var countLbl : UILabel!
    @IBOutlet weak var contentViews : UIView!

    override  func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    func setupView(){
        self.titleLbl.font = .setCustomFont(name: .bold, size: .x18)
        self.titleLbl.textColor = .black
        self.countLbl.textColor = .darkGray
        self.countLbl.font = .setCustomFont(name: .bold, size: .x14)
    }

}
