//
//  PharmacyHeaderView.swift
//  GoJekUser
//
//  Created by Shyamala's MacBook Pro on 23/11/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit

class PharmacyHeaderView: UIView {
    
    @IBOutlet weak var titleLbl : UILabel!
    @IBOutlet weak var seeAllLbl : UILabel!
    @IBOutlet weak var contentViews : UIView!

    override  func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    func setupView(){
        self.titleLbl.font = .setCustomFont(name: .bold, size: .x18)
        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue]
        let underlineAttributedString = NSAttributedString(string: "See All", attributes: underlineAttribute)
        self.seeAllLbl.attributedText = underlineAttributedString
        self.seeAllLbl.textColor = .foodieColor
        self.seeAllLbl.font = .setCustomFont(name: .bold, size: .x16)
    }
}
