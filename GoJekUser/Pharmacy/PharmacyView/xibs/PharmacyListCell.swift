//
//  PharmacyListCell.swift
//  GoJekUser
//
//  Created by Shyamala's MacBook Pro on 23/11/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit
import SDWebImage

class PharmacyListCell: UITableViewCell {
    
    @IBOutlet weak var pharmacyOuterView : UIView!
    @IBOutlet weak var pharmacyImg : UIImageView!
    @IBOutlet weak var pharmacyName : UILabel!
    @IBOutlet weak var pharmacyAddress : UILabel!
    @IBOutlet weak var pharmacyrating : UILabel!
    @IBOutlet weak var pharmacyRatingView : FloatRatingView!
    @IBOutlet weak var pharmacyOfferLbl : UILabel!
    @IBOutlet weak var pharmacyTimingLbl : UILabel!
    @IBOutlet weak var pharmacyLocaLbl : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
        self.setupDummy()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setupView(){
        self.pharmacyImg.layer.cornerRadius = 10
        self.pharmacyName.font = .setCustomFont(name: .bold, size: .x18)
        self.pharmacyAddress.font = .setCustomFont(name: .light, size: .x12)
        self.pharmacyAddress.textColor = .darkGray
        self.pharmacyrating.font = .setCustomFont(name: .medium, size: .x14)
        self.pharmacyOfferLbl.font = .setCustomFont(name: .medium, size: .x14)
        self.pharmacyOfferLbl.textColor = .green
        self.pharmacyTimingLbl.textColor = .green
        self.pharmacyTimingLbl.font = .setCustomFont(name: .medium, size: .x14)
        self.pharmacyLocaLbl.font = .setCustomFont(name: .medium, size: .x14)
        self.pharmacyLocaLbl.textColor = .foodieColor
    }
    
    func setupDummy(){
        self.pharmacyName.text = "Motherhood Pharmacy"
        self.pharmacyLocaLbl.text = "Chennai , India"
        self.pharmacyTimingLbl.text = "30 mins"
        self.pharmacyrating.text = "4.2"
        self.pharmacyOfferLbl.text = "( 25% off )"
        self.pharmacyAddress.text = "no 23 , 9 Ponniaamman kovil st, Avvai Colony, Chennai."
        
    }
    
    func setShopListData(data: ShopsListData){
 
        pharmacyImg.sd_setImage(with: URL(string: data.picture ?? ""), placeholderImage: #imageLiteral(resourceName: "ImagePlaceHolder"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                // Perform operation.
                if (error != nil) {
                    // Failed to load image
                    self.pharmacyImg.image = #imageLiteral(resourceName: "ImagePlaceHolder")
                } else {
                    // Successful in loading image
                    self.pharmacyImg.image = image
                }
            })

        pharmacyName.text = data.store_name
        pharmacyAddress.text = data.store_location
        if data.offer_percent != 0 {
            pharmacyOfferLbl.text = (data.offer_percent?.toString() ?? "0") + FoodieConstant.offerPercent.localized
            pharmacyOfferLbl.isHidden = false
        }
        else {
            pharmacyOfferLbl.isHidden = true
        }
        
         
        let rateValue = Double(data.rating ?? 0).rounded(.awayFromZero)
        pharmacyrating.text = rateValue.toString()
        if data.rating == nil || data.rating == 0 {
        self.pharmacyrating.isHidden = true
        }else{
            self.pharmacyrating.isHidden = false
        }
        
        if data.estimated_delivery_time == nil && data.estimated_delivery_time == "" {
//            dotView.isHidden = true
            pharmacyTimingLbl.isHidden = true
        }else{
//            dotView.isHidden = false
            pharmacyTimingLbl.isHidden = false
            pharmacyTimingLbl.text = (data.estimated_delivery_time ?? "") + FoodieConstant.mins.localized

        }
        pharmacyTimingLbl.isHidden = data.storetype?.category != FoodieConstant.food
//        dotView.isHidden = data.storetype?.category != FoodieConstant.food
        
//        if data.shopstatus == "CLOSED" {
//            closeView.isHidden = false
//        }else{
//            closeView.isHidden = true
//
//        }
        
    }
}
