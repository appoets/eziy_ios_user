//
//  PharmacyConstant.swift
//  GoJekUser
//
//  Created by Shyamala's MacBook Pro on 22/11/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import Foundation

struct PharmacyConstant {
    static let PharmacyHomeViewController = "PharmacyHomeViewController"
    static let PharmacyListVC = "PharmacyListVC"
    static let PharmacyFilter = "PharmacyFilter"
    static let PharmacyDetailVC = "PharmacyDetailVC"
    
    static let OfferCollectionCell = "OfferCollectionCell"
    static let PharmacyOfferCell = "PharmacyOfferCell"
    static let RecommandedCell = "RecommandedCell"
    static let RecommandedTableCell = "RecommandedTableCell"
    static let PharmacyHeaderView = "PharmacyHeaderView"
    static let PharmacyListCell = "PharmacyListCell"
    static let PharmacyDetailCell = "PharmacyDetailCell"
    static let PrescriptionCell = "PrescriptionCell"
    static let TableHeaderView = "TableHeaderView"
    static let PharmacyflowCartViewController = "PharmacyflowCartViewController"
    
    static let Pharmacy = "Pharmacy"
    static let uploadPrescription = "Upload Prescriptions"
}
