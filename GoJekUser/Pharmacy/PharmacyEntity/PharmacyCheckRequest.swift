//
//  PharmacyCheckRequest.swift
//  GoJekUser
//
//  Created by Shyamala's MacBook Pro on 02/12/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import Foundation
import ObjectMapper

struct PharmacyCheckRequestEntity : Mappable {
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : PharmacyCheckRequestResponseData?
    var error : [String]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }
    
}

struct PharmacyCheckRequestResponseData : Mappable {
    var response_time : String?
//    var data : [Str]?
    var sos : String?
    var order_otp : String?
    var emergency : [Emergency]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        response_time <- map["response_time"]
//        data <- map["data"]
        sos <- map["sos"]
        emergency <- map["emergency"]
        order_otp <- map["order_otp"]
    }
    
}
