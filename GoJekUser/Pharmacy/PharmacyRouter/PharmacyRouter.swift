//
//  PharmacyRouter.swift
//  GoJekUser
//
//  Created by Shyamala's MacBook Pro on 22/11/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit


class PharmacyRouter: PharmacyPresenterToPharmacyRouterProtocol {
    static func createPharmacyModule() -> UIViewController {
        let pharmacyDetailViewController  = pharmacyStoryboard.instantiateViewController(withIdentifier: PharmacyConstant.PharmacyHomeViewController) as! PharmacyHomeViewController
        let pharmacyPresenter: PharmacyViewToPharmacyPresenterProtocol & PharmacyInteractorToPharmacyPresenterProtocol = PharmacyPresenter()
        let pharmacyInteractor: PharmacyPresenterToPharmacyInteractorProtocol = PharmacyInteractor()
        let pharmacyRouter: PharmacyPresenterToPharmacyRouterProtocol = PharmacyRouter()
        pharmacyDetailViewController.pharmacyPresenter = pharmacyPresenter
        pharmacyPresenter.pharmacyView = pharmacyDetailViewController
        pharmacyPresenter.pharmacyRouter = pharmacyRouter
        pharmacyPresenter.pharmacyInteractor = pharmacyInteractor
        pharmacyInteractor.pharmacyPresenter = pharmacyPresenter
        return pharmacyDetailViewController
    }
    
    
//    static func createPharmacyOrderStatusModule(isHome: Bool?, orderId: Int?,isChat: Bool?,isFromOrder: Bool) -> UIViewController {
//
//           let pharmacyOrderStatusViewController  = pharmacyStoryboard.instantiateViewController(withIdentifier: PharmacyConstant.PharmacyOrderStatusViewController) as! PharmacyOrderStatusViewController
//           let pharmacyPresenter: PharmacyViewToPharmacyPresenterProtocol & PharmacyInteractorToPharmacyPresenterProtocol = PharmacyPresenter()
//           let pharmacyInteractor: PharmacyPresenterToPharmacyInteractorProtocol = PharmacyInteractor()
//           let pharmacyRouter: PharmacyPresenterToPharmacyRouterProtocol = PharmacyRouter()
//           pharmacyOrderStatusViewController.pharmacyPresenter = pharmacyPresenter
//           pharmacyPresenter.pharmacyView = pharmacyOrderStatusViewController
//           pharmacyPresenter.pharmacyRouter = pharmacyRouter
//           pharmacyPresenter.pharmacyInteractor = pharmacyInteractor
//           pharmacyInteractor.pharmacyPresenter = pharmacyPresenter
//            pharmacyOrderStatusViewController.isFromOrderPage = isFromOrder
//           pharmacyOrderStatusViewController.isHome = isHome ?? false
//           orderRequestId = orderId ?? 0
//           pharmacyOrderStatusViewController.OrderfromChatNotification = isChat ?? false
//           return pharmacyOrderStatusViewController
//       }
    
    static var pharmacyStoryboard: UIStoryboard {
        return UIStoryboard(name:"pharmacy",bundle: Bundle.main)
    }
}



