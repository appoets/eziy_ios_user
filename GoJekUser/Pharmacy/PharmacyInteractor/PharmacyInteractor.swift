//
//  PharmacyInteractor.swift
//  GoJekUser
//
//  Created by Shyamala's MacBook Pro on 22/11/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import Foundation
import Alamofire

class PharmacyInteractor : PharmacyPresenterToPharmacyInteractorProtocol{
   
    
  
    var pharmacyPresenter: PharmacyInteractorToPharmacyPresenterProtocol?
    
    func sendData(data: String) {
        pharmacyPresenter?.getData(data: data)
    }
    
}
