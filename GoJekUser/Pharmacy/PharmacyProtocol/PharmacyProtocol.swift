//
//  PharmacyProtocol.swift
//  GoJekUser
//
//  Created by Shyamala's MacBook Pro on 22/11/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import Foundation
import Alamofire

var pharmacyPresenterObject: PharmacyViewToPharmacyPresenterProtocol?

//MARK:- Pharmacy presenter Pharmacy view protocol

protocol PharmacyPresenterToPharmacyViewProtocol: class {
    
    
    func getData(data : String)

}

extension PharmacyPresenterToPharmacyViewProtocol {
    
    var pharmacyPresenter: PharmacyViewToPharmacyPresenterProtocol? {
        get {
            pharmacyPresenterObject?.pharmacyView = self
            return pharmacyPresenterObject
        }
        set(newValue) {
            pharmacyPresenterObject = newValue
        }
    }
    
    
    func getData(data : String) {return}
    


}

//MARK:- Pharmacy Interactor to Pharmacy Presenter Protocol

protocol PharmacyInteractorToPharmacyPresenterProtocol: class {
    
    
    func getData(data : String)


}

//MARK:- Pharmacy Presenter to Pharmacy Interactor Protocol

protocol PharmacyPresenterToPharmacyInteractorProtocol: class {
    
    var pharmacyPresenter: PharmacyInteractorToPharmacyPresenterProtocol? { get set }
    
    
    func sendData(data : String)
    

}

//MARK:- Pharmacy view to Pharmacy presenter protocol

protocol PharmacyViewToPharmacyPresenterProtocol: class {
    
    var pharmacyView: PharmacyPresenterToPharmacyViewProtocol? { get set}
    var pharmacyInteractor: PharmacyPresenterToPharmacyInteractorProtocol? { get set }
    var pharmacyRouter: PharmacyPresenterToPharmacyRouterProtocol? { get set }
    
    func sendData(data : String)
    
}

//MARK:- Pharmacy Presenter to Pharmacy Router Protocol

protocol PharmacyPresenterToPharmacyRouterProtocol {
    static func createPharmacyModule() -> UIViewController
}



