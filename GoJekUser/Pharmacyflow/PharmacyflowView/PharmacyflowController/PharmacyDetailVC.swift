//
//  PharmacyDetailVC.swift
//  GoJekUser
//
//  Created by Shyamala's MacBook Pro on 23/11/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class PharmacyDetailVC: UIViewController {
    
    @IBOutlet weak var backBtn : UIButton!
    @IBOutlet weak var cartBtn : UIButton!
    @IBOutlet weak var titleLbl : UILabel!
    @IBOutlet weak var cartCountLbl : UILabel!
    @IBOutlet weak var cartCountView : UIView!
    @IBOutlet weak var pharmacyDetailTable : UITableView!
    
    var shopDetail: ShopDetail?
    var productList: [FoodieDetailProduct]?
    var AllproductList: [FoodieDetailProduct] = []
    var pharmacyDetail : PharmacyflowDetailEntity?
    var pharmacyflowCartList: CartListResponse?
    var priceSymbol = AppManager.shared.getUserDetails()
    var pharmacyflowAddOnsView: PharmacyflowAddOns?
    var totalStoreCount = 0
    var cateSelectionId = 0
    
    var isClosed = false
    var isPharmacyflow = false
    var restaurentId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLbl.text = "Apollo Pharmacy"
        self.titleLbl.font = .setCustomFont(name: .bold, size: .x18)
        self.cartCountLbl.font = .setCustomFont(name: .bold, size: .x12)
        self.backBtn.setTitle("", for: .normal)
        self.cartBtn.setTitle("", for: .normal)
        self.setupTableview()
        self.setupAction()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//
        navigationController?.isNavigationBarHidden = true
//        hideTabBar()
        
        getItemAvailable(filter: .all)
    }
    
    func setupAction(){
        self.backBtn.addTap {
            self.navigationController?.popViewController(animated: true)
            
        }
        
        self.cartCountView.addTap {
            let pharmacyflowcartVC = PharmacyflowRouter.pharmacyflowStoryboard.instantiateViewController(withIdentifier: PharmacyflowConstant.PharmacyflowCartViewController)
            self.navigationController?.pushViewController(pharmacyflowcartVC, animated: true)
        }
    }
}

extension PharmacyDetailVC : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else{
            return self.productList?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyConstant.PharmacyDetailCell, for: indexPath) as! PharmacyDetailCell
          
     
            DispatchQueue.main.async {
                self.titleLbl.text = self.shopDetail?.storeName ?? ""
                cell.pharmacyAddressLbl.text = self.shopDetail?.storeLocation ?? ""
                cell.pharmacytimingLbl.text = "\(self.pharmacyDetail?.responseData?.estimatedDeliveryTime ?? "0") Mins"
                let rateValue = Double(self.shopDetail?.rating ?? 0).rounded(.awayFromZero)
                cell.pharmacyratingLbl.text = rateValue.toString()
                cell.ratingView.rating = rateValue
                
                cell.pharmacyImg.sd_setImage(with: URL(string: self.shopDetail?.picture ?? "") , placeholderImage:UIImage.init(named: PharmacyflowConstant.imagePlaceHolder),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    // Perform operation.
                    if (error != nil) {
                        // Failed to load image
                        cell.pharmacyImg.image = UIImage.init(named: PharmacyflowConstant.imagePlaceHolder)
                    } else {
                        // Successful in loading image
                        cell.pharmacyImg.image = image
                    }
                })
               
            }
            
            cell.prescriptionsView.addTap {
                self.showImage(isRemoveNeed: nil, with:{ (image) in
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrisciptionVC") as! PrisciptionVC
                    vc.prescriptionImg = image
                    vc.shopDetail = self.shopDetail
                    self.navigationController?.pushViewController(vc, animated: true)
//                    cell.prescriptionsImg.image = image
                })
            }
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyConstant.PrescriptionCell, for: indexPath) as! PrescriptionCell
            if let data = self.productList?[indexPath.row]{
                cell.titleLbl.text = data.itemName
                cell.descLbl.text = data.itemDescription
                cell.priceLbl.text = data.itemPrice?.setCurrency()
                cell.offerLbl.text = "\(data.offer ?? 0)% off"
                
                cell.itemsaddView.tag = indexPath.row
                cell.itemsaddView.delegate = self
                cell.itemsaddView.currentType = .Order
                cell.pharmacyImg.sd_setImage(with: URL(string: data.picture ?? "") , placeholderImage:UIImage.init(named: PharmacyflowConstant.imagePlaceHolder),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    // Perform operation.
                    if (error != nil) {
                        // Failed to load image
                        cell.pharmacyImg.image = UIImage.init(named: PharmacyflowConstant.imagePlaceHolder)
                    } else {
                        // Successful in loading image
                        cell.pharmacyImg.image = image
                    }
                })
                
                var quantity = 0
                for quantityVal in data.itemcart ?? [] {
                    quantity = quantity+(quantityVal.quantity ?? 0)
                }
                cell.itemsaddView.count = quantity
            }
           
            cell.contentsView.addTap {
//                let vc = PharmacyRouter.pharmacyStoryboard.instantiateViewController(withIdentifier: "MedicineVC") as! MedicineVC
//                self.navigationController?.pushViewController(vc, animated: true)
            }
            return cell
        }
    }
    
    func setupTableview(){
        self.pharmacyDetailTable.delegate = self
        self.pharmacyDetailTable.dataSource = self
        self.pharmacyDetailTable.register(nibName: PharmacyConstant.PharmacyDetailCell)
        self.pharmacyDetailTable.register(nibName: PharmacyConstant.PrescriptionCell)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 200
        }else{
            return 185
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }else{
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        let views =  Bundle.main.loadNibNamed(PharmacyConstant.TableHeaderView, owner: self, options: [:])?.first as! TableHeaderView
        views.frame = CGRect(x: 0, y: 0, width:self.view.bounds.width - 300, height: 10)
        headerView.addSubview(views)
        if section == 0{
            
        }else{
            views.titleLbl.text = "All Products".capitalized
            views.countLbl.text = "\(self.productList?.count ?? 0) Products available"
        }
        headerView.clipsToBounds = true
        return headerView
    }
}


extension PharmacyDetailVC : PharmacyflowPresenterToPharmacyflowViewProtocol{
    //API Call
    private func getItemAvailable(filter: RestaurantType) {
        let param: Parameters = [PharmacyflowConstant.search: "",
                                 PharmacyflowConstant.Pqfilter: filter.rawValue]
        pharmacyflowPresenter?.getStoresDetail(with: restaurentId ?? 0, param: param)
    }
    
    
    func getStoresDetailResponse(pharmacyflowDetailEntity: PharmacyflowDetailEntity) {
        //Product list with reload tableview
        AllproductList = pharmacyflowDetailEntity.responseData?.products ?? []
        productList = AllproductList
        shopDetail = pharmacyflowDetailEntity.responseData
        
        pharmacyDetail = pharmacyflowDetailEntity
        
       
        DispatchQueue.main.async {
            self.pharmacyDetailTable.reloadData()
        }
       
        //itemListTableView.reloadData()
        self.title = pharmacyflowDetailEntity.responseData?.storeName
        //Display tableview data
        
        totalStoreCount = shopDetail?.totalstorecart ?? 0
        var count = 0
        for data in AllproductList{
            
            count = count + (data.itemcart?.first?.quantity ?? 0)
        }
        if count == 0{
            self.cartCountView.isHidden = true
        }else{
            self.cartCountView.isHidden = false
            self.cartCountLbl.text = "\(count)"
        }
        
        //API Call
        pharmacyflowPresenter?.getCartList()
    }
    
    func getCartListResponse(cartListEntity: PharmacyflowCartListEntity) {
        var count = 0
        count = cartListEntity.responseData?.carts?.count ?? 0
        if count == 0{
            self.cartCountView.isHidden = true
        }else{
            self.cartCountView.isHidden = false
            self.cartCountLbl.text = "\(count)"
        }
    }
}


//MARK: - PharmacyflowAddOnsProtocol

extension PharmacyDetailVC: PharmacyflowAddOnsProtocol{
    
    private func addOnsButton(index: Int,tag: Int,isplus: Bool) { //count tag-cell tag
        if pharmacyflowAddOnsView == nil, let pharmacyflowAddOnsView = Bundle.main.loadNibNamed(PharmacyflowConstant.PharmacyflowAddOns, owner: self, options: [:])?.first as? PharmacyflowAddOns {
            
            self.pharmacyflowAddOnsView?.addonsItem.removeAllObjects()
            
            pharmacyflowAddOnsView.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: self.view.frame.width, height: self.view.frame.height))
            pharmacyflowAddOnsView.delegate = self
            self.pharmacyflowAddOnsView = pharmacyflowAddOnsView
            self.view.addSubview(pharmacyflowAddOnsView)
        }
        pharmacyflowAddOnsView?.onClickClose = { [weak self] in
            guard let self = self else {
                return
            }
            self.pharmacyflowAddOnsView?.dismissView(onCompletion: {
                self.pharmacyflowAddOnsView = nil
            })
        }
        guard let productDetail = productList?[tag] else {return}
        
        pharmacyflowAddOnsView?.index = index
        pharmacyflowAddOnsView?.tagCount = tag
        pharmacyflowAddOnsView?.isCartPage = false
        
        pharmacyflowAddOnsView?.isplus = isplus
        pharmacyflowAddOnsView?.AddonsArr = productDetail.itemsaddon ?? []
        pharmacyflowAddOnsView?.itemNameLabel.text = productDetail.itemName
        
        pharmacyflowAddOnsView?.itemImageView.sd_setImage(with: URL(string: productDetail.picture ?? "") , placeholderImage:UIImage.init(named: PharmacyflowConstant.imagePlaceHolder),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
            // Perform operation.
            if (error != nil) {
                // Failed to load image
                self.pharmacyflowAddOnsView?.itemImageView.image = UIImage.init(named: PharmacyflowConstant.imagePlaceHolder)
            } else {
                // Successful in loading image
                self.pharmacyflowAddOnsView?.itemImageView.image = image
            }
        })
        totalStoreCount = 1
        
        pharmacyflowAddOnsView?.itemPriceLabel.text = Double(productDetail.product_offer ?? 0).setCurrency()
        let addOnsCount = productDetail.itemsaddon?.count ?? 0
        for _ in 0..<addOnsCount{
            pharmacyflowAddOnsView?.addonsItem.add("")
            
        }
        pharmacyflowAddOnsView?.addOnsTableView.reloadData()
    }
    
    func ondoneAction(addonsItem: NSMutableArray,indexPath:Int,tag:Int,isplus: Bool){
        let cell:PrescriptionCell = pharmacyDetailTable.cellForRow(at: IndexPath(row: tag, section: 1)) as? PrescriptionCell ?? PrescriptionCell()
        guard let productDetail = productList?[tag] else {return}
        let cardId = productDetail.itemcart?.first?.id ?? 0
        pharmacyflowAddOnsView?.dismissView(onCompletion: {
            self.pharmacyflowAddOnsView = nil
            
            var addOnsArr:[String] = []
            for i in 0..<addonsItem.count {
                if let addonsStr = addonsItem[i] as? String {
                    if !addonsStr.isEmpty {
                        addOnsArr.append(addonsStr)
                    }
                }
            }
            
            let addOnsStr = addOnsArr.joined(separator: ",")
            if isplus {
                cell.itemsaddView.count = cell.itemsaddView.count + 1
                if ((Int(self.cartCountLbl.text ?? "0") ?? 0) + 1) == 0{
                    self.cartCountView.isHidden = true
                }else{
                    self.cartCountView.isHidden = false
                    self.cartCountLbl.text = "\((Int(self.cartCountLbl.text ?? "0") ?? 0) + 1)"
                }
            }else{
                cell.itemsaddView.count = cell.itemsaddView.count - 1
                if ((Int(self.cartCountLbl.text ?? "0") ?? 0) - 1) == 0{
                    self.cartCountView.isHidden = true
                }else{
                    self.cartCountView.isHidden = false
                    self.cartCountLbl.text = "\((Int(self.cartCountLbl.text ?? "0") ?? 0) - 1)"
                }
            }
            if cell.itemsaddView.count == 0 {
                if cardId != 0 {
                    let param: Parameters = [PharmacyflowConstant.cartId: cardId]
                    self.pharmacyflowPresenter?.postRemoveCart(param: param)
                }
            }
            else {
                let param: Parameters = [PharmacyflowConstant.itemId: productDetail.id!,
                                         PharmacyflowConstant.qty: 1,
                                         PharmacyflowConstant.addons: addOnsStr,
                                         PharmacyflowConstant.repeatVal: 0,
                                         PharmacyflowConstant.Pcustomize: 0]
                self.pharmacyflowPresenter?.postAddToCart(param: param)
            }
        })
    }
}



//MARK: - PlusMinusDelegates
extension PharmacyDetailVC: PlusMinusDelegates {
    
    func countChange(count: Int, tag: Int, isplus: Bool) {
        if guestLogin() {
            
            print("Count \(count) Tag \(tag)")
            let cell:PrescriptionCell = pharmacyDetailTable.cellForRow(at: IndexPath(row: tag, section: 1)) as? PrescriptionCell ?? PrescriptionCell()
            guard let productDetail = productList?[tag] else {return}
            let cardId = productDetail.itemcart?.first?.id ?? 0
            
            func changeItemValue() {
                self.totalStoreCount = 1
                if isplus {
                    cell.itemsaddView.count = cell.itemsaddView.count + 1
                    if ((Int(self.cartCountLbl.text ?? "0") ?? 0) + 1) == 0{
                        self.cartCountView.isHidden = true
                    }else{
                        self.cartCountView.isHidden = false
                        self.cartCountLbl.text = "\((Int(self.cartCountLbl.text ?? "0") ?? 0) + 1)"
                    }
                } else {
                    cell.itemsaddView.count = cell.itemsaddView.count - 1
                    if ((Int(self.cartCountLbl.text ?? "0") ?? 0) - 1) == 0{
                        self.cartCountView.isHidden = true
                    }else{
                        self.cartCountView.isHidden = false
                        self.cartCountLbl.text = "\((Int(self.cartCountLbl.text ?? "0") ?? 0) - 1)"
                    }
                }
                
                if cell.itemsaddView.count == 0 {
                    if cardId != 0 {
                        
                        let param: Parameters = [PharmacyflowConstant.cartId: cardId]
                        self.pharmacyflowPresenter?.postRemoveCart(param: param)
                    }
                }
                else {
                    var param: Parameters = [:]
                    
                    if cardId != 0 {
                        param = [PharmacyflowConstant.itemId: productDetail.id!,
                                 PharmacyflowConstant.cartId: cardId,
                                 PharmacyflowConstant.qty: cell.itemsaddView.count,
                                 PharmacyflowConstant.repeatVal: 1,
                                 PharmacyflowConstant.Pcustomize: 0]
                    } else {
                        param = [PharmacyflowConstant.itemId: productDetail.id!,
                                 PharmacyflowConstant.qty: cell.itemsaddView.count,
                                 PharmacyflowConstant.repeatVal: 0,
                                 PharmacyflowConstant.Pcustomize: 0]
                    }
                    self.pharmacyflowPresenter?.postAddToCart(param: param)
                }
            }
            
            //Validation of item
            if totalStoreCount == 0 && (shopDetail?.usercart ?? 0) > 0 {
                AppAlert.shared.simpleAlert(view: self, title: "", message: PharmacyflowConstant.anotherRestaurant.localized, buttonOneTitle: Constant.SYes.localized, buttonTwoTitle: Constant.SNo.localized)
                AppAlert.shared.onTapAction = { [weak self] tag in
                    guard let self = self else {
                        return
                    }
                    if tag == 0 {
                        if productDetail.itemcart?.count == 0 {
                            if productDetail.itemsaddon?.count != 0 {
                                self.addOnsButton(index: count,tag: tag,isplus: isplus)
                            }else{
                                changeItemValue()
                            }
                        }else{
                            changeItemValue()
                        }
                    }
                }
            }else {
                if productDetail.itemsaddon?.count != 0 {
                    if !isplus {
                        if  let quantity = productDetail.itemcart?.count, quantity == 1, cardId != 0  {
                            cell.itemsaddView.count = cell.itemsaddView.count - 1
                            if cell.itemsaddView.count == 0 {
                                let param: Parameters = [PharmacyflowConstant.cartId: cardId]
                                self.pharmacyflowPresenter?.postRemoveCart(param: param)
                            } else {
                                let param: Parameters = [PharmacyflowConstant.itemId: productDetail.id!,
                                                         PharmacyflowConstant.cartId: cardId,
                                                         PharmacyflowConstant.qty: cell.itemsaddView.count,
                                                         PharmacyflowConstant.repeatVal: 1,
                                                         PharmacyflowConstant.Pcustomize: 0]
                                self.pharmacyflowPresenter?.postAddToCart(param: param)
                            }
                        } else {
                            let pharmacyflowcartVC = PharmacyflowRouter.pharmacyflowStoryboard.instantiateViewController(withIdentifier: PharmacyflowConstant.PharmacyflowCartViewController)
                            navigationController?.pushViewController(pharmacyflowcartVC, animated: true)
                        }
                    } else {
                        if cell.itemsaddView.count > 0 {
                            AppAlert.shared.simpleAlert(view: self, title: "", message: PharmacyflowConstant.repeatLast.localized, buttonOneTitle: Constant.SChoose.localized, buttonTwoTitle: Constant.SRepeat.localized)
                            AppAlert.shared.onTapAction = { [weak self] alertTag in
                                guard let self = self else {
                                    return
                                }
                                switch alertTag {
                                case 0:
                                    self.addOnsButton(index: count,tag: tag,isplus: isplus)
                                    break
                                case 1:
                                    cell.itemsaddView.count = cell.itemsaddView.count + 1
                                    let param: Parameters = [PharmacyflowConstant.itemId: productDetail.id!,
                                                             PharmacyflowConstant.cartId: 0,
                                                             PharmacyflowConstant.qty: 0,
                                                             PharmacyflowConstant.repeatVal: 1,
                                                             PharmacyflowConstant.Pcustomize: 0]
                                    self.pharmacyflowPresenter?.postAddToCart(param: param)
                                    break
                                default:
                                    break
                                }
                            }
                        } else {
                            self.addOnsButton(index: count,tag: tag,isplus: isplus)
                        }
                    }
                } else {
                    changeItemValue()
                }
            }
        }
    }
}
