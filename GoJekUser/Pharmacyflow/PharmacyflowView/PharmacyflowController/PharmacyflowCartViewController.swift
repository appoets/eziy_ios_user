//
//  PharmacyflowCartViewController.swift
//  GoJekUser
//
//  Created by Thiru on 07/03/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class PharmacyflowCartViewController: UIViewController {
    
    //MARK: - IBOutlet
    @IBOutlet weak var cartListTableView: UITableView!
    @IBOutlet weak var scheduleButton: UIButton!
    @IBOutlet weak var placeOrderButtton: UIButton!
    @IBOutlet weak var bottomView: UIStackView!
    @IBOutlet weak var labelItemNotAvailable: UILabel!

    var ScheduleView: PharmacyflowScheduleTimeView!
    var addNoteView: PharmacyflowAddNoteView!
    var orderPlacePoupView: OrderPlacePopup!
    var pharmacyflowCheckOut: PharmacyflowCheckoutResponseData?
    var pharmacyflowAddOnsView: PharmacyflowAddOns?
    
    var pharmacyflowCartList: CartListResponse?
    var pharmacyflowProductList: [Cart] = Array()
    var priceSymbol = AppManager.shared.getUserDetails()
    var couponView: CouponView?
    var promoCodeListArr:[PromocodeData] = []
    var addressDetail:AddressResponseData!
    
    var addonsArr:[String] = []
    var addressId: Int?
    var paymentMode: String = "CASH"
    var isWallet = 0
    var selectedCardId = "0"
    var doorStep = 0

    var orderType = ""
    var promoCodeId = 0
    let userDetail = AppManager.shared.getUserDetails()
    var addressDatasource: [AddressResponseData] = Array()
    var selectedPromo:PromocodeData?

    
    // View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = PharmacyflowConstant.cart.localized
        initialLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.hideTabBar()
    }
}

//MARK: - LocalMethod

extension PharmacyflowCartViewController {
    
    private func initialLoad() {
        self.setNavigationTitle()
        self.setLeftBarButtonWith(color: .blackColor)
        self.view.backgroundColor = .veryLightGray
        DispatchQueue.main.async {
            self.scheduleButton.setCornerRadiuswithValue(value: 15)
            self.placeOrderButtton.setCornerRadiuswithValue(value: 15)
        }
        placeOrderButtton.backgroundColor = .pharmacyflowColor
        placeOrderButtton.setTitle(PharmacyflowConstant.placeOrder.localized.uppercased(), for: .normal)
        placeOrderButtton.addTarget(self, action: #selector(placeOrderAction), for: .touchUpInside)
        placeOrderButtton.setTitleColor(.white, for: .normal)
        scheduleButton.titleLabel?.font = UIFont.setCustomFont(name: .bold, size: .x16)
        scheduleButton.addTarget(self, action: #selector(scheduleViewAction), for: .touchUpInside)
        placeOrderButtton.titleLabel?.font = UIFont.setCustomFont(name: .bold, size: .x16)
        self.cartListTableView.register(UINib(nibName: PharmacyflowConstant.PharmacyflowItemsTableViewCell, bundle: nil), forCellReuseIdentifier: PharmacyflowConstant.PharmacyflowItemsTableViewCell)
        self.cartListTableView.register(UINib(nibName: PharmacyflowConstant.CartPageTableViewCell, bundle: nil), forCellReuseIdentifier: PharmacyflowConstant.CartPageTableViewCell)
        self.cartListTableView.register(UINib(nibName: PharmacyflowConstant.PharmacyflowDeliveryTableViewCell, bundle: nil), forCellReuseIdentifier: PharmacyflowConstant.PharmacyflowDeliveryTableViewCell)
        
        self.cartListTableView.backgroundColor = .veryLightGray
        self.view.backgroundColor = .veryLightGray
        labelItemNotAvailable.font = UIFont.setCustomFont(name: .medium, size: .x12)
        labelItemNotAvailable.textColor = .lightGray
        labelItemNotAvailable.isHidden = true
        labelItemNotAvailable.text = PharmacyflowConstant.checkoutItem.localized
        self.scheduleButton.isHidden = false
        scheduleButton.setTitle(PharmacyflowConstant.TSchedule.localized.uppercased(), for: .normal)
        scheduleButton.setTitleColor(.white, for: .normal)
        scheduleButton.backgroundColor = .DarkBlueColor
        //Get Address List
        self.pharmacyflowPresenter?.getSavedAddress()
        //API Call
        var param = ["order_type":orderType]
        param[PharmacyflowConstant.userAddressId] = "\(addressId ?? 0)"
        
        self.pharmacyflowPresenter?.getCartList(param: param)
        
        self.pharmacyflowPresenter?.getSavedAddress()

        self.orderType = orderByType.delivery.rawValue

        setDarkMode()
    }
    
    func setDarkMode(){
        self.view.backgroundColor = .backgroundColor
        self.cartListTableView.backgroundColor = .backgroundColor
    }
    
    private func showDimView(view: UIView) {
        let dimView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        dimView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        let dimViewTap = UITapGestureRecognizer(target: self, action: #selector(tapDimView))
        dimView.addGestureRecognizer(dimViewTap)
        dimView.addSubview(view)
        self.view.addSubview(dimView)
    }
    
    @objc func tapDimView() {
        if couponView != nil {
            couponView?.superview?.removeFromSuperview() // dimview
            couponView?.dismissView(onCompletion: {
                self.couponView = nil
            })
        }
    }
    
    private func navigateToManageAddressView() {
        let manageAddressController = AccountRouter.accountStoryboard.instantiateViewController(withIdentifier: AccountConstant.ManageAddressController) as! ManageAddressController
        manageAddressController.delegate = self
        manageAddressController.isFromCartView = true
        manageAddressController.addressDetail = self.addressDetail

        self.navigationController?.pushViewController(manageAddressController, animated: true)
    }
    
    private func navigateToPaymetView() {
        let paymentVC = AccountRouter.accountStoryboard.instantiateViewController(withIdentifier: AccountConstant.PaymentSelectViewController) as! PaymentSelectViewController
        paymentVC.isChangePayment = true
        paymentVC.selectCardId = selectedCardId
        paymentVC.onClickPayment = { [weak self] (type,cardEntity) in
            guard let self = self else {
                return
            }
            let cell = self.cartListTableView.cellForRow(at: [2,0]) as? PharmacyflowDeliveryTableViewCell
            cell?.paymentTypeLabel.text = type.rawValue
            if type == .CARD {
                cell?.cardLabel.text = Constant.cardPrefix + (cardEntity?.last_four ?? "")
                self.selectedCardId = cardEntity?.card_id ?? "0"
                self.paymentMode = "CARD"
            }else{
                self.paymentMode = "CASH"
                cell?.cardLabel.text = ""
            }
            
            if self.orderType == orderByType.delivery.rawValue &&  self.paymentMode == "CARD" {
                cell?.doorStepButton.isHidden = false
            }else{
                cell?.doorStepButton.isHidden = true
            }
        }
        self.navigationController?.pushViewController(paymentVC, animated: true)
    }
    
    private func updateCartDetailView() {
        //Display tableview data
        self.pharmacyflowProductList = pharmacyflowCartList?.carts ?? []
        labelItemNotAvailable.isHidden = true
        placeOrderButtton.setTitle(PharmacyflowConstant.placeOrder.localized.uppercased(), for: .normal)
        for item in pharmacyflowProductList {
            if item.product?.status == 0 {
                labelItemNotAvailable.isHidden = false
                placeOrderButtton.setTitle(PharmacyflowConstant.itemNotAvail.localized.uppercased(), for: .normal)
            }
        }
     UIView.transition(with: cartListTableView,
     duration: 0.35,
     options: .curveEaseIn,
     animations: { self.cartListTableView.reloadData() })
        
        if let count = pharmacyflowCartList?.carts?.count, count>0 {
            self.bottomView.isHidden = false
            cartListTableView.backgroundView = nil
            
        }
        else {
            self.bottomView.isHidden = true
            cartListTableView.setBackgroundImageAndTitle(imageName: PharmacyflowConstant.cauldron, title: PharmacyflowConstant.cartEmpty.localized,tintColor: .black)
        }
    }
    
    private func placeOrderSuccess() {
        if self.orderPlacePoupView == nil {
            self.orderPlacePoupView = Bundle.main.loadNibNamed(PharmacyflowConstant.OrderPlacePopup, owner: self, options: [:])?.first as? OrderPlacePopup
            let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
            self.orderPlacePoupView.frame = CGRect(x: 0, y: 0, width: (window?.frame.width)!, height: (window?.frame.height)!)
            self.orderPlacePoupView.showMessage(orderID: self.pharmacyflowCheckOut?.store_order_invoice_id ?? "")
            window?.addSubview(self.orderPlacePoupView)
            self.orderPlacePoupView?.show(with: .bottom, completion: nil)
        }
        
        self.orderPlacePoupView.onClickClose = { [weak self] in
            guard let self = self else {
                return
            }
            self.orderPlacePoupView.removeFromSuperview()
            self.orderPlacePoupView = nil
            let pharmacyflowcartVC = PharmacyflowRouter.pharmacyflowStoryboard.instantiateViewController(withIdentifier: PharmacyflowConstant.PharmacyflowOrderStatusViewController) as! PharmacyflowOrderStatusViewController
            orderRequestId = self.pharmacyflowCheckOut?.id ?? 0
            self.navigationController?.pushViewController(pharmacyflowcartVC, animated: true)
        }
    }
}

extension PharmacyflowCartViewController {
    
    @objc func placeOrderAction() {
         if addressId == nil && self.orderType == orderByType.delivery.rawValue {
            AppAlert.shared.simpleAlert(view: self, title: PharmacyflowConstant.Address.localized, message: nil)
            
        }
        else {
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd.MM.yyyy"
            let dateresult = formatter.string(from: date)
      

            var param: Parameters = [PharmacyflowConstant.Pwallet: isWallet,
                                     PharmacyflowConstant.paymentMode: paymentMode,
                                     PharmacyflowConstant.orderType: orderType,
                                     PharmacyflowConstant.Pleave_at_door:doorStep ]
            
            if pharmacyflowCartList?.storeType != PharmacyflowConstant.food {
                param[PharmacyflowConstant.deliveryDate] = dateresult
            }
            
            if(orderType == orderByType.delivery.rawValue)
            {
                param[PharmacyflowConstant.userAddressId] = addressId!
            }

            
            if promoCodeId != 0 {
                param[PharmacyflowConstant.promocodeId] = promoCodeId
                
            }
            if selectedCardId != "0" {
                param[PharmacyflowConstant.cardId] = selectedCardId
            }
            
            self.pharmacyflowPresenter?.postOrderCheckout(param: param)
        }
       
    }
    
    
    @objc func scheduleViewAction() {
        
        if self.ScheduleView == nil {
            self.ScheduleView = Bundle.main.loadNibNamed(PharmacyflowConstant.PharmacyflowScheduleTimeView, owner: self, options: [:])?.first as? PharmacyflowScheduleTimeView
            
            self.ScheduleView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            
            self.view.addSubview(self.ScheduleView!)
            self.ScheduleView?.show(with: .bottom, completion: nil)
        }
        self.ScheduleView.onClickClose = { [weak self] in
            guard let self = self else {
                return
            }
            self.ScheduleView.removeFromSuperview()
            self.ScheduleView = nil
        }
        self.ScheduleView.onClickSchedule = { [weak self]  (dates , times) in
            guard let self = self else {
                return
            }
            self.ScheduleView.removeFromSuperview()
            self.ScheduleView = nil
            let indexPath = IndexPath(row: 0, section: 2)
            let cell = self.cartListTableView.cellForRow(at: indexPath) as! PharmacyflowDeliveryTableViewCell
            cell.scheduleatValueLabel.text = "\(dates) , \(times)"
        }
    }
    
    @objc func addNoteViewAction() {
        
        if self.addNoteView == nil {
            self.addNoteView = Bundle.main.loadNibNamed(PharmacyflowConstant.PharmacyflowAddNoteView, owner: self, options: [:])?.first as? PharmacyflowAddNoteView
            
            self.addNoteView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            
            self.view.addSubview(self.addNoteView!)
            self.addNoteView?.show(with: .bottom, completion: nil)
        }
        self.addNoteView.onClickClose = { [weak self] in
            guard let self = self else {
                return
            }
            self.addNoteView.removeFromSuperview()
            self.addNoteView = nil
        }
        self.addNoteView.onClickSubmit = { [weak self] in
            guard let self = self else {
                return
            }
            self.addNoteView.removeFromSuperview()
            self.addNoteView = nil
            // self.headerView.isRemoveNote = true
        }
    }
}

//MARK: - UITableViewDataSource
extension PharmacyflowCartViewController: ManageAddressDelegate {
    func addressValue(addressArr: [AddressResponseData]) {
        print("Hello")
        let cell = self.cartListTableView.cellForRow(at: [2,0]) as? PharmacyflowDeliveryTableViewCell
     
        if addressArr.count == 0 {
            cell?.addressTypeLabel.text = Constant.noSavedAddress.localized
            cell?.addressStringLabel.text = .empty
            cell?.addressChangeButton.setTitle(PharmacyflowConstant.add.localized.uppercased(), for: .normal)
            var param = ["order_type":self.orderType]
                                                      if(self.orderType == orderByType.delivery.rawValue)
                                                                             {
                                                                              param[PharmacyflowConstant.userAddressId] = "0"
                                                                             }
                                                                   self.pharmacyflowPresenter?.getCartList(param: param)
        }else{
            for i in 0..<addressArr.count {
                let address = addressArr[i]
                if address.id == self.addressId {
                    cell?.addressTypeLabel.text = address.address_type ?? PharmacyflowConstant.homeAddress.localized
                    cell?.addressStringLabel.text = address.locationAddress()
                    cell?.addressChangeButton.setTitle(PharmacyflowConstant.change.localized.uppercased(), for: .normal)
                    var param = ["order_type":self.orderType]
                               if(self.orderType == orderByType.delivery.rawValue)
                                                      {
                                                       param[PharmacyflowConstant.userAddressId] = "\(self.addressId ?? 0)"
                                                      }
                        self.pharmacyflowPresenter?.getCartList(param: param)
                    
                }else{
                    var param = ["order_type":self.orderType]
                                            if(self.orderType == orderByType.delivery.rawValue)
                                                                   {
                                                                    param[PharmacyflowConstant.userAddressId] = "0"
                                                                   }
                                                         self.pharmacyflowPresenter?.getCartList(param: param)
                    cell?.addressTypeLabel.text = Constant.noSavedAddress.localized
                    cell?.addressStringLabel.text = .empty
                    cell?.addressChangeButton.setTitle(PharmacyflowConstant.add.localized.uppercased(), for: .normal)
                }
            }
        }
    }
    
    func selectedAddress(address: AddressResponseData) {
        self.addressId = address.id
        self.addressDetail = address
        let cell = self.cartListTableView.cellForRow(at: [2,0]) as? PharmacyflowDeliveryTableViewCell
        var param = ["order_type":self.orderType]
                    if(self.orderType == orderByType.delivery.rawValue)
                                           {
                                            param[PharmacyflowConstant.userAddressId] = "\(self.addressId ?? 0)"
                                           }
                                 self.pharmacyflowPresenter?.getCartList(param: param)
        DispatchQueue.main.async {
            cell?.addressTypeLabel.text = address.address_type ?? PharmacyflowConstant.homeAddress.localized
            cell?.addressStringLabel.text = address.locationAddress()
        }
        if  cell?.addressStringLabel.text == "" {
            cell?.addressChangeButton.setTitle(PharmacyflowConstant.add.localized.uppercased(), for: .normal)
        }else{
            cell?.addressChangeButton.setTitle(PharmacyflowConstant.change.localized.uppercased(), for: .normal)
            
        }
    }
}

//MARK: - UITableViewDataSource
extension PharmacyflowCartViewController: UITableViewDataSource {
    @objc func tapDeleteAction(sender:UIButton) {
        let productList = pharmacyflowProductList[sender.tag]
     //   let productDetail = productList.product
        let param: Parameters = [PharmacyflowConstant.cartId: productList.id ?? 0]
                  self.pharmacyflowPresenter?.postRemoveCart(param: param)
           }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = pharmacyflowCartList?.carts?.count, count>0 {
            if section == 0  {
                return 1
            }else if section == 1  {
                return pharmacyflowProductList.count
            }else{
                return 1
                
            }
        }
        return 0
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyflowConstant.CartPageTableViewCell, for: indexPath) as! CartPageTableViewCell
            
            let shopDetail = pharmacyflowCartList?.carts?.first?.store
            DispatchQueue.main.async {
                cell.nameLabel.text = shopDetail?.store_name ?? ""
                cell.descrLabel.text = shopDetail?.storetype?.name ?? ""
                let rateValue = Double(shopDetail?.rating ?? 0).rounded(.awayFromZero)
                cell.ratingLabel.text = rateValue.toString()
                
                cell.reastaurantLogoImageView.sd_setImage(with: URL(string: shopDetail?.picture ?? "") , placeholderImage:#imageLiteral(resourceName: "ImagePlaceHolder"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                                                        // Perform operation.
                                                        if (error != nil) {
                                                            // Failed to load image
                                                          cell.reastaurantLogoImageView.image = #imageLiteral(resourceName: "ImagePlaceHolder")
                                                        } else {
                                                            // Successful in loading image
                                                          cell.reastaurantLogoImageView.image = image
                                }
                    })
            }
            return cell
        }else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyflowConstant.PharmacyflowItemsTableViewCell, for: indexPath) as! PharmacyflowItemsTableViewCell
            cell.itemsaddView.tag = indexPath.row
            if #available(iOS 13.0, *) {
                cell.backgroundColor = .systemBackground
            } else {
                // Fallback on earlier versions
                cell.backgroundColor = .veryLightGray

            }
            cell.backgroundColor = .backgroundColor
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.itemsaddView.delegate = self
            cell.itemsaddView.tag = indexPath.row
            cell.customizeButton.tag = indexPath.row
            cell.pharmacyflowAddonsgaDelegate = self
            let productList = pharmacyflowProductList[indexPath.row]
            
            let productDetail = productList.product
            cell.itemNameLabel.text = productDetail?.item_name
            cell.delBtn.tag = indexPath.row
            cell.delBtn.addTarget(self, action: #selector(tapDeleteAction), for: .touchUpInside)
            if productDetail?.is_veg == "Non Veg"
                  {
                     cell.isVeg  = false
                  }else{
                      cell.isVeg  = true

                  }
            
            if productDetail?.quantity != nil && productDetail?.quantity != 0 {
                       cell.qtyLabel.isHidden = false
                cell.qtyLabel.text = "Qty \(String(productDetail?.quantity ?? 0))"
                if productDetail?.unit != nil  {
                    cell.qtyLabel.text = (cell.qtyLabel.text ?? "") + " " + "\(String(productDetail?.unit?.name ?? ""))"
                   }
                   }else{
                       cell.qtyLabel.isHidden = true
                   }
            
            if productDetail?.status == 0 {
                cell.itemDisableVw.isHidden = false
                cell.overView.isHidden = true
               
            }else{
                cell.itemDisableVw.isHidden = true
                cell.overView.isHidden = false
            }
            
            cell.itemsaddView.count = productList.quantity ?? 0
            cell.priceLabel.text = "\(self.priceSymbol?.currency ?? "") \(productList.totalItemPrice ?? 0.0)"
            
            
            cell.itemImageView.sd_setImage(with: URL(string: productDetail?.picture ?? ""), placeholderImage:#imageLiteral(resourceName: "ImagePlaceHolder"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                                                                   // Perform operation.
                                                                   if (error != nil) {
                                                                       // Failed to load image
                                                                     cell.itemImageView.image = #imageLiteral(resourceName: "ImagePlaceHolder")
                                                                   } else {
                                                                       // Successful in loading image
                                                                     cell.itemImageView.image = image
                                                                   }
                                                               })
            
            
            if productList.cartaddon?.count == 0 {
                cell.customizableLabel.isHidden = true
                cell.addonsLabel.isHidden = true
                cell.customizeButton.isHidden = true
            }else{
                cell.addonsLabel.isHidden = false
                cell.customizableLabel.isHidden = false
                cell.customizeButton.isHidden = false
                
            }
            let font = UIFont.setCustomFont(name: .medium, size: .x14)

            let cartDetails = self.getCartAddOnValue(values: productList.cartaddon ?? [])
            let height = cell.heightForView(text: cartDetails, font: font, width: 100.0)
            cell.heightConstraint.constant = height + 10
            cell.addonsLabel.text = cartDetails
            cell.layoutIfNeeded()
            if productList.is_prescription == 1{
                
                    cell.isprescription.isHidden = false
                cell.isprescriptionImg.sd_setImage(with: URL(string: productList.prescription?.prescription_image ?? ""), placeholderImage:UIImage.init(named: PharmacyflowConstant.imagePlaceHolder),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    // Perform operation.
                    if (error != nil) {
                        // Failed to load image
                        cell.isprescriptionImg.image = UIImage.init(named: PharmacyflowConstant.imagePlaceHolder)
                    } else {
                        // Successful in loading image
                        cell.isprescriptionImg.image = image
                    }
                })
            }else{
                cell.isprescription.isHidden = true
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyflowConstant.PharmacyflowDeliveryTableViewCell, for: indexPath) as! PharmacyflowDeliveryTableViewCell
            
            let wallet = AppManager.shared.getUserDetails()?.wallet_balance
            if (wallet  ?? 0) > 0 {
                cell.useWalletAmount.setTitle(PharmacyflowConstant.useWallet.localized + "(" + (wallet?.setCurrency().localized ?? "") + ")", for: .normal)
            }else{
                cell.useWalletAmount.isHidden = true
            }
            
            DispatchQueue.main.async {
                cell.addressTypeLabel.text = self.addressDetail?.address_type ?? Constant.noSavedAddress.localized
                cell.addressStringLabel.text = self.addressDetail?.locationAddress()
            }
            if self.addressDetail != nil {
                cell.addressChangeButton.setTitle(PharmacyflowConstant.change.localized.uppercased(), for: .normal)
                
            }else{
                cell.addressChangeButton.setTitle(PharmacyflowConstant.add.localized.uppercased(), for: .normal)
                
            }
            
            
            if orderType == orderByType.delivery.rawValue &&  self.paymentMode == "CARD" {
                cell.doorStepButton.isHidden = false
            }else{
                cell.doorStepButton.isHidden = true

            }
            
            if self.orderType == orderByType.delivery.rawValue {
                cell.deliveryAddress.isHidden = false
                cell.deliveryChargeView.isHidden = false
                cell.deliveryImageView.image = UIImage(named: Constant.circleFullImage)?.imageTintColor(color1: .pharmacyflowColor)
                cell.takeawayImageView.image = UIImage(named: Constant.circleImage)?.imageTintColor(color1: .pharmacyflowColor)
            }else{
                cell.deliveryAddress.isHidden = true
                cell.deliveryChargeView.isHidden = true
                cell.deliveryImageView.image = UIImage(named: Constant.circleImage)?.imageTintColor(color1: .pharmacyflowColor)
                cell.takeawayImageView.image = UIImage(named: Constant.circleFullImage)?.imageTintColor(color1: .pharmacyflowColor)
            }
            
            let currency = self.userDetail?.currency ?? ""
            cell.discountValueLabel.text = "-\(currency)\(self.pharmacyflowCartList?.shopDiscount ?? 0)"
  
            cell.totalValueLabel.text = "\(currency)\(self.pharmacyflowCartList?.payable ?? 0)"
            cell.itemtotalValueLbl.text = "\(currency)\(self.pharmacyflowCartList?.totalItemPrice ?? 0.0)"
            cell.deliveryValueLabel.text = "\(currency)\(self.pharmacyflowCartList?.deliveryCharges ?? 0.0)"
            cell.paymentTypeLabel.text  = PharmacyflowConstant.cash.localized
            cell.cardLabel.text = ""
            cell.taxLabelValue.text = "\(currency)\(self.pharmacyflowCartList?.shopGstAmount ?? 0)"
            cell.storePackageAmountValue.text = "\(currency)\(self.pharmacyflowCartList?.shopPackageCharge ?? 0.0)"
            cell.promoCodeView.isHidden = true
            cell.deliveryViewButtonAction = { [weak self] action in
                guard let self = self else {
                    return
                }
                switch action {
                case DeliveryViewAction.addressChange:
                    self.navigateToManageAddressView()
                    break
                case DeliveryViewAction.paymentChange:
                    self.navigateToPaymetView()
                    break
                case DeliveryViewAction.couponChange:
                    self.scheduleButton.isHidden = false
                    break
                    
                case DeliveryViewAction.deliveryType:
                    self.orderType = orderByType.delivery.rawValue
                    cell.deliveryAddress.isHidden = false
                    cell.deliveryChargeView.isHidden = false
                    
                    cell.deliveryImageView.image = UIImage(named: Constant.circleFullImage)?.imageTintColor(color1: .pharmacyflowColor)
                    cell.takeawayImageView.image = UIImage(named: Constant.circleImage)?.imageTintColor(color1: .pharmacyflowColor)
                    if self.orderType == orderByType.delivery.rawValue &&  self.paymentMode == "CARD" {
                        cell.doorStepButton.isHidden = false
                    }else{
                        cell.doorStepButton.isHidden = true
                        
                    }
                    cell.updateConstraints()
                    self.view.layoutIfNeeded()
                    self.cartListTableView.reloadData()
                    
                    //  self.paymentMode = "CASH"
                    var param = ["order_type":self.orderType]
                    if(self.orderType == orderByType.delivery.rawValue)
                    {
                        param[PharmacyflowConstant.userAddressId] = "\(self.addressId ?? 0)"
                    }
                    self.pharmacyflowPresenter?.getCartList(param: param)
                    break
                case DeliveryViewAction.takeAwayType:
                    self.orderType = orderByType.takeAway.rawValue
                    cell.deliveryAddress.isHidden = true
                    cell.deliveryChargeView.isHidden = true
                    cell.deliveryImageView.image = UIImage(named: Constant.circleImage)?.imageTintColor(color1: .pharmacyflowColor)
                    cell.takeawayImageView.image = UIImage(named: Constant.circleFullImage)?.imageTintColor(color1: .pharmacyflowColor)
                    if self.orderType == orderByType.delivery.rawValue &&  self.paymentMode == "CARD" {
                        cell.doorStepButton.isHidden = false
                    }else{
                        cell.doorStepButton.isHidden = true
                        
                    }
                    cell.updateConstraints()
                    self.view.layoutIfNeeded()
                    self.cartListTableView.reloadData()
                    
                    var param = ["order_type":self.orderType]
                    if(self.orderType == orderByType.delivery.rawValue) {
                        param[PharmacyflowConstant.userAddressId] = "\(self.addressId ?? 0)"
                    }
                    self.pharmacyflowPresenter?.getCartList(param: param)
                    
                    break
                case DeliveryViewAction.useWallet:
                    if cell.useWalletAmount.imageView?.image?.isEqual(to: UIImage(named: Constant.squareFill) ?? UIImage()) ?? false {
                        self.isWallet = 0
                        cell.useWalletAmount.setImage(UIImage(named: Constant.sqaureEmpty), for: .normal)
                    }else{
                        self.isWallet = 1
                        cell.useWalletAmount.setImage(UIImage(named: Constant.squareFill), for: .normal)
                    }
                    break
                case DeliveryViewAction.doorStep:
                    if cell.doorStepButton.imageView?.image?.isEqual(to: UIImage(named: Constant.squareFill) ?? UIImage()) ?? false {
                        self.doorStep = 0
                        cell.doorStepButton.setImage(UIImage(named: Constant.sqaureEmpty), for: .normal)
                    }else{
                        self.doorStep = 1
                        cell.doorStepButton.setImage(UIImage(named: Constant.squareFill), for: .normal)
                    }
                    break
                case DeliveryViewAction.showCoupon:
                    if self.couponView == nil, let couponView = Bundle.main.loadNibNamed(Constant.CouponView, owner: self, options: [:])?.first as? CouponView {
                        let viewHeight = ((self.view.frame.height )/100)*30 //30% of view
                        couponView.frame = CGRect(origin: CGPoint(x: 0, y: (self.view.frame.height)-viewHeight), size: CGSize(width: (self.view.frame.width), height: viewHeight))
                        self.couponView = couponView
                        couponView.setValues(color: .pharmacyflowColor)
                        couponView.set(values: self.promoCodeListArr )
                        couponView.show(with: .bottom, completion: nil)
                        if let selectedCoupon = self.selectedPromo {
                            self.couponView?.isSelectedPromo(values: selectedCoupon)
                        }
                        self.showDimView(view: couponView)
                    }
                    
                    // selected coupon stored in globally
                    self.couponView?.applyCouponAction = { [weak self] selectedPromocode in
                        guard let self = self else {
                            return
                        }
                        self.couponView?.superview?.dismissView(onCompletion: {
                            self.couponView = nil
                        })
                        self.selectedPromo = selectedPromocode
                        
                        self.promoCodeId = selectedPromocode?.id ?? 0
                        self.pharmacyflowPresenter?.getPromoCodeCartList(promoCodeStr: selectedPromocode?.id?.toString() ?? "")
                        
                    }
                    break
                }
            }
            return cell
        }
    }
    
    func reload(tableView: UITableView) {

        let contentOffset = tableView.contentOffset
        tableView.reloadData()
        tableView.layoutIfNeeded()
        tableView.setContentOffset(contentOffset, animated: false)

    }
    
    func getCartAddOnValue(values: [Cartaddon]) -> (String) {
        var cartName:String = ""
        for cart in values {
            cartName = cartName + (cart.addon_name ?? "") + ","
        }
        cartName = String(cartName.dropLast())
        return (cartName)
    }
}

//MARK: - UITableviewDelegate
extension PharmacyflowCartViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 200
        }
        return UITableView.automaticDimension
    }
}


//MARK: - PlusMinusDelegates
extension PharmacyflowCartViewController: PlusMinusDelegates {
    
    func countChange(count: Int, tag: Int,isplus: Bool) {
        print("Count \(count) Tag \(tag)")
        let productDetail = pharmacyflowProductList[tag]
        let cell:PharmacyflowItemsTableViewCell = self.cartListTableView.cellForRow(at: IndexPath(row: tag, section: 1)) as? PharmacyflowItemsTableViewCell ?? PharmacyflowItemsTableViewCell()
        
        if isplus {
            cell.itemsaddView.count = cell.itemsaddView.count + 1
            
        }else{
            cell.itemsaddView.count = cell.itemsaddView.count - 1
        }
        
        if cell.itemsaddView.count == 0 {
            let param: Parameters = [PharmacyflowConstant.cartId: productDetail.id!]
            self.pharmacyflowPresenter?.postRemoveCart(param: param)
        }else {
            let param: Parameters = [PharmacyflowConstant.itemId: (productDetail.product?.id ?? 0),
                                     PharmacyflowConstant.cartId: productDetail.id ?? 0,
                                     PharmacyflowConstant.qty: cell.itemsaddView.count,
                                     PharmacyflowConstant.repeatVal: 1,
                                     PharmacyflowConstant.Pcustomize: 0]
            self.pharmacyflowPresenter?.postAddToCart(param: param)
        }
    }
}


//MARK: - FilterViewDelegate
extension PharmacyflowCartViewController: CartHeaderViewDelegate {
    
    func addRemoveNote() {
    }
    
    func editNote() {
        addNoteViewAction()
    }
}

//MARK: - PharmacyflowPresenterToPharmacyflowViewProtocol
extension PharmacyflowCartViewController: PharmacyflowPresenterToPharmacyflowViewProtocol {
    
    func getCartListResponse(cartListEntity: PharmacyflowCartListEntity) {
        self.pharmacyflowCartList = cartListEntity.responseData
        self.updateCartDetailView()
        let params:Parameters = [XuberInput.totalAmount : cartListEntity.responseData?.payable ?? 0,XuberInput.storeID : pharmacyflowCartList?.carts?.first?.storeId ?? 0]
        self.pharmacyflowPresenter?.getPromoCodeList(param: params)
    }
    
    func postAddToCartResponse(addCartEntity: PharmacyflowCartListEntity) {
        self.pharmacyflowCartList = addCartEntity.responseData
        //API Call
        var param = ["order_type":orderType]
        if(orderType == orderByType.delivery.rawValue)
                        {
                            param[PharmacyflowConstant.userAddressId] = "\(addressId ?? 0)"
                        }
        self.pharmacyflowPresenter?.getCartList(param: param)
        self.updateCartDetailView()
    }
    
    func postRemoveCartResponse(cartListEntity: PharmacyflowCartListEntity) {
        self.pharmacyflowCartList = cartListEntity.responseData
        //API Call
        var param = ["order_type":orderType]
        if(orderType == orderByType.delivery.rawValue)
                        {
                            param[PharmacyflowConstant.userAddressId] = "\(addressId ?? 0)"
                        }
              self.pharmacyflowPresenter?.getCartList(param: param)
        self.updateCartDetailView()
    }
    
    func postOrderCheckoutResponse(checkoutEntity: PharmacyflowCheckoutEntity) {
        pharmacyflowCheckOut = checkoutEntity.responseData
        if pharmacyflowCheckOut == nil{
            if checkoutEntity.statusCode == "205" {
                //API Call
                var param = ["order_type":orderType]
                if(orderType == orderByType.delivery.rawValue)
                                {
                                    param[PharmacyflowConstant.userAddressId] = "\(addressId ?? 0)"
                                }
                self.pharmacyflowPresenter?.getCartList(param: param)
                self.updateCartDetailView()
                
                AppAlert.shared.simpleAlert(view: self, title: "", message: PharmacyflowConstant.checkoutItem.localized,buttonTitle: Constant.SOk.localized)

            }else{
                // ToastManager.show(title: checkoutEntity.message ?? "", state: .error)
                AppAlert.shared.simpleAlert(view: self, title: "", message: (checkoutEntity.message ?? "").localized,buttonTitle: Constant.SOk.localized)
                
            }
        }else{
            self.placeOrderSuccess()
        }
    }
    
    func getSavedAddressResponse(addressList: SavedAddressEntity) {
        let addressList = addressList.responseData ?? []
        if let count = pharmacyflowCartList?.carts?.count, count>0 {
            
            
            addressDetail = addressList.first
            self.addressId = addressDetail?.id
            let cell = self.cartListTableView.cellForRow(at: [2,0]) as? PharmacyflowDeliveryTableViewCell
            DispatchQueue.main.async {
                cell?.addressTypeLabel.text = self.addressDetail?.address_type ?? Constant.noSavedAddress.localized
                cell?.addressStringLabel.text = self.addressDetail?.locationAddress()
            }
            if self.addressDetail != nil {
                cell?.addressChangeButton.setTitle(PharmacyflowConstant.change.localized.uppercased(), for: .normal)
                
            }else{
                cell?.addressChangeButton.setTitle(PharmacyflowConstant.add.localized.uppercased(), for: .normal)
                
            }
           UIView.transition(with: cartListTableView,
            duration: 0.35,
            options: .curveEaseIn,
            animations: { self.cartListTableView.reloadData() })
        }
         var param = ["order_type":orderType]
          if(orderType == orderByType.delivery.rawValue)
                      {
                         param[PharmacyflowConstant.userAddressId] = "\(addressId ?? 0)"
                     }
          self.pharmacyflowPresenter?.getCartList(param: param)
        
    }
    
    func getPromoCodeResponse(getPromoCodeResponse: PromocodeEntity) {
        self.promoCodeListArr = getPromoCodeResponse.responseData ?? []
        
        if let count = pharmacyflowCartList?.carts?.count, count>0 {
            
            let cell = cartListTableView.cellForRow(at: [2,0]) as? PharmacyflowDeliveryTableViewCell
            
            if self.promoCodeListArr.count == 0 {
                cell?.couponOuterView.isHidden = true
            }else{
                cell?.couponOuterView.isHidden = false
            }
        }
        //Get Address List
    }
    
    func postPromoCodeCartResponse(cartListEntity: PharmacyflowCartListEntity) {
        
        let cell = self.cartListTableView.cellForRow(at: [2,0]) as? PharmacyflowDeliveryTableViewCell
        
        let currency = self.userDetail?.currency ?? ""
        
        if self.promoCodeListArr.count == 0 || cartListEntity.responseData?.promocodeAmount == 0.0 {
            cell?.viewCouponButton.setTitle(Constant.viewCoupon.localized.uppercased(), for: .normal)
            cell?.offerCouponAmt.isHidden = true
        }else{
            cell?.viewCouponButton.setTitle("-\(currency)\(cartListEntity.responseData?.promocodeAmount ?? 0)", for: .normal)
            cell?.offerCouponAmt.isHidden = false
        }
        
        
        
        cell?.totalValueLabel.text = "\(currency)\(cartListEntity.responseData?.payable ?? 0)"
        
    }
}
extension PharmacyflowCartViewController : ShowAddonsDelegates {
    func addonsCustomize(tag: Int) {
        
        self.addOnsButton(tag: tag)
    }
}

extension PharmacyflowCartViewController : PharmacyflowAddOnsProtocol{
    func ondoneAction(addonsItem: NSMutableArray,indexPath:Int,tag:Int,isplus: Bool) {
        let cell:PharmacyflowItemsTableViewCell = self.cartListTableView.cellForRow(at: IndexPath(row: tag, section: 1)) as? PharmacyflowItemsTableViewCell ?? PharmacyflowItemsTableViewCell()
        let productDetail = pharmacyflowProductList[tag]
        let cardId = productDetail.id ?? 0
        self.pharmacyflowAddOnsView?.dismissView(onCompletion: {
            self.pharmacyflowAddOnsView = nil
            var addOnsArr: [String] = []
            for i in 0..<addonsItem.count {
                if let addonsStr = addonsItem[i] as? String {
                    if !addonsStr.isEmpty {
                        addOnsArr.append(addonsStr)
                    }
                }
            }
            
            let addOnsStr = addOnsArr.joined(separator: ",")
            print(addOnsStr)
            
             if cardId != 0 {
                if cell.itemsaddView.count == 0 {
                    let param: Parameters = [PharmacyflowConstant.cartId: cardId]
                    self.pharmacyflowPresenter?.postRemoveCart(param: param)
                }
                else {
                    let param: Parameters = [PharmacyflowConstant.itemId: (productDetail.product?.id ?? 0),
                                             PharmacyflowConstant.cartId: cardId,
                                             PharmacyflowConstant.qty: cell.itemsaddView.count,
                                             PharmacyflowConstant.addons: addOnsStr,
                                             PharmacyflowConstant.repeatVal: 0,
                                             PharmacyflowConstant.Pcustomize: 1]
                    self.pharmacyflowPresenter?.postAddToCart(param: param)
                }
            }
        })
    }
    
    func addOnsButton(tag: Int) { //count tag-cell tag
        if self.pharmacyflowAddOnsView == nil, let pharmacyflowAddOnsView = Bundle.main.loadNibNamed(PharmacyflowConstant.PharmacyflowAddOns, owner: self, options: [:])?.first as? PharmacyflowAddOns {
            self.pharmacyflowAddOnsView?.addonsItem.removeAllObjects()
            pharmacyflowAddOnsView.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: self.view.frame.width, height: self.view.frame.height))
            pharmacyflowAddOnsView.delegate = self
            self.pharmacyflowAddOnsView = pharmacyflowAddOnsView
            self.view.addSubview(pharmacyflowAddOnsView)
        }
        let productDetail = pharmacyflowProductList[tag]

        self.pharmacyflowAddOnsView?.tagCount = tag
        self.pharmacyflowAddOnsView?.isCartPage = true
        self.pharmacyflowAddOnsView?.CartAddonsArr = productDetail.cartaddon ?? []
        self.pharmacyflowAddOnsView?.AddonsArr = productDetail.product?.itemsaddon ?? []
        self.pharmacyflowAddOnsView?.itemNameLabel.text = productDetail.product?.item_name
        
        let addOnsCount = productDetail.product?.itemsaddon?.count ?? 0
               for _ in 0..<addOnsCount{
                   self.pharmacyflowAddOnsView?.addonsItem.add("")
                   
               }
        

        
        for i in 0..<(addOnsCount) {
            if let firstSuchElement = productDetail.cartaddon?.first(where: { $0.store_item_addons_id == productDetail.product?.itemsaddon?[i].id }) {
                print(firstSuchElement) // 4
                self.pharmacyflowAddOnsView?.addonsItem.replaceObject(at: i, with: productDetail.product?.itemsaddon?[i].id?.toString() ?? "")

            }else{
                self.pharmacyflowAddOnsView?.addonsItem.replaceObject(at: i, with: "")
            }
        }
        
        self.pharmacyflowAddOnsView?.onClickClose = { [weak self] in
            guard let self = self else {
                return
            }
            self.pharmacyflowAddOnsView?.dismissView(onCompletion: {
                self.pharmacyflowAddOnsView = nil
            })
        }
        

        
        
        self.pharmacyflowAddOnsView?.itemImageView.sd_setImage(with: URL(string: productDetail.product?.picture ?? ""), placeholderImage:UIImage.init(named: PharmacyflowConstant.imagePlaceHolder),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
            // Perform operation.
            if (error != nil) {
                // Failed to load image
                self.pharmacyflowAddOnsView?.itemImageView.image = UIImage.init(named: PharmacyflowConstant.imagePlaceHolder)
            } else {
                // Successful in loading image
                self.pharmacyflowAddOnsView?.itemImageView.image = image
            }
        })
        
        self.pharmacyflowAddOnsView?.itemPriceLabel.text = Double(productDetail.itemPrice ?? 0).setCurrency()
       
        self.pharmacyflowAddOnsView?.addOnsTableView.reloadData()
    }
}




