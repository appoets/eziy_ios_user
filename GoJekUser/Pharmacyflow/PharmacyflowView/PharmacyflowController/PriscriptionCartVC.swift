//
//  PriscriptionCartVC.swift
//  GoJekUser
//
//  Created by Shyamala's MacBook Pro on 05/01/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import SDWebImage

class PriscriptionCartVC: UIViewController {
    
    @IBOutlet weak var pharmacyDetailView : UIView!
    @IBOutlet weak var pharmacyNameLbl : UILabel!
    @IBOutlet weak var pharmacyAddressLbl : UILabel!
    @IBOutlet weak var pharmacyRatingLbl : UILabel!
    @IBOutlet weak var titleLbl : UILabel!
    @IBOutlet weak var backBtn : UIButton!
    @IBOutlet weak var pharmacyImg : UIImageView!
    @IBOutlet weak var priscriptionImg : UIImageView!
    @IBOutlet weak var prsNameLbl : UILabel!
    @IBOutlet weak var priceLbl : UILabel!
    @IBOutlet weak var confirmBtn : UIButton!
    @IBOutlet weak var infoLbl : UILabel!
    
    var prescripitionImg : UIImage?
    var shopDetail: ShopDetail?
    var cartData : PharmacyflowCartListEntity?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        self.setupData()
        self.setupAction()
    }
    
    func setupView(){
        self.confirmBtn.backgroundColor = .pharmacyColor
        self.confirmBtn.setTitleColor(.white, for: .normal)
        self.confirmBtn.setTitle("Confirm Order", for: .normal)
        self.confirmBtn.layer.cornerRadius = 10
        self.pharmacyDetailView.layer.cornerRadius = 20
        self.pharmacyDetailView.addShadow(radius: 10, color: .gray)
//        self.pharmacyRatingLbl.text = "5.0"
        self.pharmacyImg.layer.cornerRadius = 10
        self.priscriptionImg.layer.cornerRadius = 10
        self.infoLbl.font = .setCustomFont(name: .medium, size: .x13)
        self.infoLbl.text = "Selected Pharmacy will send you the total amount after your confirmation"
        self.prsNameLbl.text = "The amount for your Prescripition"
        self.priceLbl.text = "$ 20.0"
        self.backBtn.setTitle("", for: .normal)
    }
    
    func setupData(){
        self.priscriptionImg.image = prescripitionImg
//        self.titleLbl.text = self.shopDetail?.storeName ?? ""
        self.titleLbl.text = ""
        self.pharmacyNameLbl.text = self.shopDetail?.storeName ?? ""
        self.pharmacyAddressLbl.text = self.shopDetail?.storeLocation ?? ""
        let rateValue = Double(self.shopDetail?.rating ?? 0).rounded(.awayFromZero)
//        self.pharmacyratingLbl.text = rateValue.toString()
//        self.ratingView.rating = rateValue
        
        self.pharmacyImg.sd_setImage(with: URL(string: self.shopDetail?.picture ?? "") , placeholderImage:UIImage.init(named: PharmacyflowConstant.imagePlaceHolder),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
            // Perform operation.
            if (error != nil) {
                // Failed to load image
                self.pharmacyImg.image = UIImage.init(named: PharmacyflowConstant.imagePlaceHolder)
            } else {
                // Successful in loading image
                self.pharmacyImg.image = image
            }
        })
    }
    
    func setupAction(){
        self.confirmBtn.addTap {
            let pharmacyflowcartVC = PharmacyflowRouter.pharmacyflowStoryboard.instantiateViewController(withIdentifier: PharmacyflowConstant.PharmacyflowCartViewController)
            self.navigationController?.pushViewController(pharmacyflowcartVC, animated: true)
        }
    }

}
