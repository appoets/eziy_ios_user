//
//  PharmacyflowOrderStatusViewController.swift
//  GoJekUser
//
//  Created by Thiru on 09/03/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

//var orderRequestId = 0


class PharmacyflowOrderStatusViewController: UIViewController {
    
    //Outlets
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var orderIDLabel:UILabel!
    @IBOutlet weak var otpLabel:UILabel!
    @IBOutlet weak var orderStatusTableView:UITableView!
    
    @IBOutlet weak var cancelButton: UIButton!
    var ratingView: PharmacyflowRatingView!
    var pharmacyflowCurrentRequest:PharmacyflowOrderDetailEntity?
    var phoneNumber:String? = "" //Phone number for user and restaurent
    var isHome = false
    var tableView: CustomTableView?
    var cancelReasonData: [ReasonData]?
    var isFromOrderPage = false
      var OrderfromChatNotification: Bool? = false
      var isAppFrom =  false
    
     //For chat_order
     var isAppPresentTapOnPush:Bool = false // avoiding multiple screens redirectns,if same push comes multiple times
     var isChatAlreadyPresented:Bool = false
    var orderStatus: pharmacyflowOrderStatus = .none {
        didSet {
            if orderStatus == .completed {
                self.showRatingView()
            }
        }
    }
    var cellHeights: [IndexPath : CGFloat] = [:]
    var delegate : UpdateOrderHistoryDelegate?
    var lat = 0.0
    var long = 0.0
    
    
    //ViewLifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
         isAppFrom = true
        initialLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden  = true
        self.hideTabBar()
        ChatPushClick.shared.isOrderPushClick = true
        NotificationCenter.default.addObserver(self, selector: #selector(enterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        BackGroundRequestManager.share.stopBackGroundRequest()
        isAppPresentTapOnPush = false
        self.isChatAlreadyPresented = false
        orderIDLabel.adjustsFontSizeToFitWidth = true
        //For chat_order
        NotificationCenter.default.addObserver(self, selector: #selector(isChatPushRedirection), name: Notification.Name(pushNotificationType.chat_order.rawValue), object: nil)
        
        FLocationManager.shared.start { (info) in
            print(info.longitude ?? 0.0)
            print(info.latitude ?? 0.0)
            self.lat = info.latitude ?? 0.0
            self.long = info.longitude ?? 0.0
        }
        
        if OrderfromChatNotification == true {
            OrderfromChatNotification = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                if self.isAppPresentTapOnPush == false {
                    self.isAppPresentTapOnPush = true
                    self.isChatAlreadyPresented = false
                }
                else {
                    self.isChatAlreadyPresented = true
                }
                self.tapMessage()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
         FLocationManager.shared.stop()
     }
    
    func socketAndBgTaskSetUp() {
        if let requestData = pharmacyflowCurrentRequest?.responseData {
            BackGroundRequestManager.share.startBackGroundRequest(type: .ModuleWise, roomId: SocketUtitils.construtRoomKey(requestID: "\(requestData.id ?? 0)", serviceType: .order), listener: .Order)
            BackGroundRequestManager.share.requestCallback =  { [weak self] in
                guard let self = self else {
                    return
                }
                self.checkOrderDetailsApi()
            }
        } else {
            checkOrderDetailsApi()
        }
    }
    
    func checkOrderDetailsApi() {
        pharmacyflowPresenter?.getOrderStatus(Id: orderRequestId)
    }
}

extension PharmacyflowOrderStatusViewController {
    
    private func initialLoad() {
        self.socketAndBgTaskSetUp()
        LoadingIndicator.show()
        self.pharmacyflowPresenter?.getReasons(param: [XuberInput.type: ServiceType.orders.currentType])
        self.cancelButton.isHidden = true
        otpLabel.adjustsFontSizeToFitWidth = true
        self.orderStatusTableView.register(UINib(nibName: PharmacyflowConstant.PharmacyflowOrderStatusTableViewCell, bundle: nil), forCellReuseIdentifier: PharmacyflowConstant.PharmacyflowOrderStatusTableViewCell)
        self.orderStatusTableView.register(UINib(nibName: PharmacyflowConstant.PharmacyflowDelvieryPersonTableViewCell, bundle: nil), forCellReuseIdentifier: PharmacyflowConstant.PharmacyflowDelvieryPersonTableViewCell)
        self.orderStatusTableView.register(UINib(nibName: PharmacyflowConstant.PharmacyflowOrderDetailTableViewCell,bundle: nil), forCellReuseIdentifier: PharmacyflowConstant.PharmacyflowOrderDetailTableViewCell)
        self.orderStatusTableView.register(UINib(nibName: PharmacyflowConstant.DeliveryChargeTableViewCell,bundle: nil), forCellReuseIdentifier: PharmacyflowConstant.DeliveryChargeTableViewCell)
        self.orderStatusTableView.register(UINib(nibName: PharmacyflowConstant.ShopDetailTableViewCell,bundle: nil), forCellReuseIdentifier: PharmacyflowConstant.ShopDetailTableViewCell)
        setFont()
        backButton.addTarget(self, action: #selector(tapBack), for: .touchUpInside)
        cancelButton.setTitleColor(.pharmacyflowColor, for: .normal)
        cancelButton.setTitle(PharmacyflowConstant.OCancel.localized.uppercased(), for: .normal)
        cancelButton.addTarget(self, action: #selector(tapCancelButton), for: .touchUpInside)
        if CommonFunction.checkisRTL() {
            backButton.imageView?.transform = backButton.transform.rotated(by: .pi)
        }
        setDarkMode()
    }
    
    private func setDarkMode(){
        self.view.backgroundColor = .backgroundColor
        self.navigationView.backgroundColor = .boxColor
        self.backButton.tintColor = .blackColor
    }
    
    @objc private func enterForeground() {
           isAppFrom = false
        if let _ = pharmacyflowCurrentRequest {
            pharmacyflowCurrentRequest = nil
        }
        BackGroundRequestManager.share.resetBackGroudTask()
        socketAndBgTaskSetUp()
    }
    
    private func setFont(){
        self.orderIDLabel.font = UIFont.setCustomFont(name: .medium, size: .x16)
        self.otpLabel.font = UIFont.setCustomFont(name: .medium, size: .x16)
        cancelButton.titleLabel?.font =  UIFont.setCustomFont(name: .bold, size: .x14)
    }
    @objc func tapCancelButton(){
        showCancelTable()
    }
    
    @objc func tapBack() {
        ChatPushClick.shared.clear()

        if isFromOrderPage {
            self.navigationController?.popViewController(animated: true)
        }else{
            let appDelegate = UIApplication.shared.delegate! as! AppDelegate
            appDelegate.window?.rootViewController = TabBarController().listTabBarController()
            appDelegate.window?.makeKeyAndVisible()
        }
        
    }
    
    func showRatingView() {
        if self.ratingView == nil, let ratingView = Bundle.main.loadNibNamed(Constant.PharmacyflowRatingView, owner: self, options: [:])?.first as? PharmacyflowRatingView {
            ratingView.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: self.view.frame.width, height: self.view.frame.height))
            self.ratingView = ratingView
            self.ratingView.setValues(color: .pharmacyflowColor)
            self.view.addSubview(ratingView)
            ratingView.show(with: .bottom, completion: nil)
        }
        DispatchQueue.main.async {
            let name = (self.pharmacyflowCurrentRequest?.responseData?.provider?.first_name ?? "") + (self.pharmacyflowCurrentRequest?.responseData?.provider?.last_name ?? "")
            self.ratingView?.userNameLabel.text = name
         
            
            self.ratingView.userNameImage.sd_setImage(with: URL(string: self.pharmacyflowCurrentRequest?.responseData?.provider?.picture ?? ""), placeholderImage: UIImage(named: Constant.userPlaceholderImage),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                       // Perform operation.
                       if (error != nil) {
                           // Failed to load image
                           self.ratingView.userNameImage.image = UIImage(named: Constant.userPlaceholderImage)
                       } else {
                           // Successful in loading image
                           self.ratingView.userNameImage.image = image
                       }
                   })
            
            if self.pharmacyflowCurrentRequest?.responseData?.order_type == "TAKEAWAY" {
                self.ratingView?.providerRateView.isHidden = true
                self.ratingView?.ratingProviderView.isHidden = true
                self.ratingView?.rateDriverLabel.isHidden = true
            }else{
                self.ratingView?.providerRateView.isHidden = false
                self.ratingView?.ratingProviderView.isHidden = false
                self.ratingView?.rateDriverLabel.isHidden = false
            }
            self.ratingView?.ratingCountLabel.text = (self.pharmacyflowCurrentRequest?.responseData?.provider?.rating?.rounded(.awayFromZero))?.toString()
        }
        self.ratingView?.onClickSubmit = { [weak self] (rating, comments, shopRating) in
            guard let self = self else {
                return
            }
            self.ratingView?.dismissView(onCompletion: {
                self.ratingView = nil
                var comment = ""
                if comments == Constant.leaveComment.localized {
                    comment = ""
                }
                else {
                    comment = comments
                }
                let param: Parameters = [PharmacyflowConstant.PRequestId: self.pharmacyflowCurrentRequest?.responseData?.id ?? 0,
                                         PharmacyflowConstant.Prating:rating,
                                         PharmacyflowConstant.Pcomment: comment,
                                         PharmacyflowConstant.Pshopid: self.pharmacyflowCurrentRequest?.responseData?.store?.id ?? 0,
                                         PharmacyflowConstant.Pshoprating:shopRating]
                
                self.pharmacyflowPresenter?.userRatingParam(param: param)
                
            })
        }
    }
}

//MARK: - TableViewDataSources
extension PharmacyflowOrderStatusViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        if self.pharmacyflowCurrentRequest?.responseData != nil {
            
            if self.pharmacyflowCurrentRequest?.responseData?.order_type == "TAKEAWAY"  {
                count = 4
                return count
            }
            if self.pharmacyflowCurrentRequest?.responseData?.provider == nil {
                count = 3
                return count
            }
            if self.pharmacyflowCurrentRequest?.responseData?.provider != nil {
                count = 4
                return count
            }
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0  {
            let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyflowConstant.PharmacyflowOrderStatusTableViewCell, for: indexPath) as! PharmacyflowOrderStatusTableViewCell
            cell.orderType = self.pharmacyflowCurrentRequest?.responseData?.order_type ?? ""
            cell.orderReadystatus = self.pharmacyflowCurrentRequest?.responseData?.order_ready_status ?? 0
            cell.orderStatus = self.orderStatus
            return cell
        } else {
            if self.pharmacyflowCurrentRequest?.responseData?.order_type == "TAKEAWAY" {
                if indexPath.row == 1 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyflowConstant.ShopDetailTableViewCell, for: indexPath) as! ShopDetailTableViewCell
                    cell.shopNameLabel.text = self.pharmacyflowCurrentRequest?.responseData?.store?.store_name
                    cell.shopAddressLabel.text = self.pharmacyflowCurrentRequest?.responseData?.pickup?.store_location
                    cell.callButton.setTitle(self.pharmacyflowCurrentRequest?.responseData?.pickup?.contact_number, for: .normal)
                    
                    cell.onTapMatTrack = { [weak self] in
                        guard let self = self else {
                            return
                        }
                        self.redirectToGoogleMap()
                    }
                    // cell.mapTrackButton.isHidden = true
                    cell.callButton.addTarget(self, action: #selector(self.tapCall), for: .touchUpInside)
                    return cell
                }else if indexPath.row == 2  {
                    let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyflowConstant.PharmacyflowOrderDetailTableViewCell, for: indexPath) as! PharmacyflowOrderDetailTableViewCell
                    
                    cell.orderItem = self.pharmacyflowCurrentRequest?.responseData?.invoice?.items ?? []
                    if (self.pharmacyflowCurrentRequest?.responseData?.delivery_date ?? "") == ""{
                        cell.deliveryDateLabel.text = ""
                    }
                    else{
                        let deliveryDate = self.pharmacyflowCurrentRequest?.responseData?.delivery_date ?? ""
                        cell.deliveryDateLabel.text = deliveryDate + " : " + PharmacyflowConstant.delivery_Date.localized
                    }
                    return cell
                }
                else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyflowConstant.DeliveryChargeTableViewCell, for: indexPath) as! DeliveryChargeTableViewCell
                    let data = self.pharmacyflowCurrentRequest?.responseData?.invoice
                    cell.deliveryChargeLabel.text = (data?.delivery_amount ?? 0)?.setCurrency()
                    if data?.promocode_id == 0 {
                        cell.couponView.isHidden = true
                    }else{
                        cell.couponView.isHidden = false
                        
                    }
                    cell.couponLabel.text = "- " + (data?.promocode_amount ?? 0).setCurrency()
                    cell.totalChargeLabel.text = (data?.payable ?? 0)?.setCurrency()
                    cell.taxAmt.text = (data?.tax_amount ?? 0)?.setCurrency()
                    cell.storePackageAmt.text = (data?.store_package_amount ?? 0)?.setCurrency()
                    if data?.discount == 0 {
                        cell.discountView.isHidden = true
                    }else{
                        cell.discountView.isHidden = false
                        
                    }
                    cell.itemTotalValueLabel.text = (data?.item_price ?? 0)?.setCurrency()
                    cell.discountValueLabel.text = "- " + ((data?.discount ?? 0)?.setCurrency() ?? String.empty)
                    return cell
                }
                
            }else if self.pharmacyflowCurrentRequest?.responseData?.provider == nil {
                if indexPath.row == 1  {
                    let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyflowConstant.PharmacyflowOrderDetailTableViewCell, for: indexPath) as! PharmacyflowOrderDetailTableViewCell
                    
                    cell.orderItem = self.pharmacyflowCurrentRequest?.responseData?.invoice?.items ?? []
                    if (self.pharmacyflowCurrentRequest?.responseData?.delivery_date ?? "") == ""{
                        cell.deliveryDateLabel.text = ""
                    }
                    else{
                        let deliveryDate = self.pharmacyflowCurrentRequest?.responseData?.delivery_date ?? ""
                        cell.deliveryDateLabel.text = deliveryDate + " : " + PharmacyflowConstant.delivery_Date.localized
                    }
                    return cell
                }
                else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyflowConstant.DeliveryChargeTableViewCell, for: indexPath) as! DeliveryChargeTableViewCell
                    let data = self.pharmacyflowCurrentRequest?.responseData?.invoice
                    cell.deliveryChargeLabel.text = (data?.delivery_amount ?? 0)?.setCurrency()
                    if data?.promocode_id == 0 {
                        cell.couponView.isHidden = true
                    }else{
                        cell.couponView.isHidden = false
                        
                    }
                    cell.couponLabel.text = "- " + ((data?.promocode_amount ?? 0)?.setCurrency() ?? String.empty)
                    cell.totalChargeLabel.text = (data?.payable ?? 0)?.setCurrency()
                    cell.taxAmt.text = (data?.tax_amount ?? 0)?.setCurrency()
                    cell.storePackageAmt.text = (data?.store_package_amount ?? 0)?.setCurrency()
                    if data?.discount == 0 {
                        cell.discountView.isHidden = true
                    }else{
                        cell.discountView.isHidden = false
                        
                    }
                    cell.discountValueLabel.text = "- " + ((data?.discount ?? 0)?.setCurrency() ?? String.empty)
                    cell.itemTotalValueLabel.text = (data?.item_price ?? 0)?.setCurrency()
                    
                    return cell
                }
            }else{
                if indexPath.row == 1 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyflowConstant.PharmacyflowDelvieryPersonTableViewCell, for: indexPath) as! PharmacyflowDelvieryPersonTableViewCell
                    
                    let name = (self.pharmacyflowCurrentRequest?.responseData?.provider?.first_name ?? "") + (self.pharmacyflowCurrentRequest?.responseData?.provider?.last_name ?? "")
                    cell.deliveryPersonNameLabel.text = name
                    cell.timeLabel.text = (self.pharmacyflowCurrentRequest?.responseData?.store?.estimated_delivery_time ?? "") + PharmacyflowConstant.Mins.localized
                    cell.phoneButton.setTitle(self.pharmacyflowCurrentRequest?.responseData?.provider?.mobile, for: .normal)
                    
                    
                    cell.profileImageView.sd_setImage(with: URL(string:  self.pharmacyflowCurrentRequest?.responseData?.provider?.picture ?? ""), placeholderImage: UIImage(named: Constant.userPlaceholderImage),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                        // Perform operation.
                        if (error != nil) {
                            // Failed to load image
                            cell.profileImageView.image = UIImage(named: Constant.userPlaceholderImage)
                        } else {
                            // Successful in loading image
                            cell.profileImageView.image = image
                        }
                    })
                    let dateFormatter1 = DateFormatter()
                    dateFormatter1.dateFormat = DateFormat.yyyy_MM_dd_T_HH_mm_ss_SSSSSS_Z
                    let date: Date? = dateFormatter1.date(from: UTCToLocal(date: self.pharmacyflowCurrentRequest?.responseData?.created_at ?? ""))
                    cell.onTapMatTrack = { [weak self] in
                        guard let self = self else {
                            return
                        }
                        let vc = PharmacyflowRouter.pharmacyflowStoryboard.instantiateViewController(withIdentifier: PharmacyflowConstant.PharmacyflowTrackingController) as! PharmacyflowTrackingController
                        vc.pharmacyflowCurrentRequest = self.pharmacyflowCurrentRequest
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    if let date = date {
                        cell.dateLabel.text = dateFormatter1.string(from: date)
                        
                        
                    }
                    cell.phoneButton.addTarget(self, action: #selector(self.tapCall), for: .touchUpInside)
                    cell.msgButton.addTarget(self, action: #selector(self.tapMessage), for: .touchUpInside)
                    return cell
                }else if indexPath.row == 2  {
                    let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyflowConstant.PharmacyflowOrderDetailTableViewCell, for: indexPath) as! PharmacyflowOrderDetailTableViewCell
                    cell.orderItem = self.pharmacyflowCurrentRequest?.responseData?.invoice?.items ?? []
                    if (self.pharmacyflowCurrentRequest?.responseData?.delivery_date ?? "") == ""{
                        cell.deliveryDateLabel.text = ""
                    }
                    else{
                        cell.deliveryDateLabel.text = "\(self.pharmacyflowCurrentRequest?.responseData?.delivery_date ?? "") : Delivery Date"
                    }
                    return cell
                }
                else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyflowConstant.DeliveryChargeTableViewCell, for: indexPath) as! DeliveryChargeTableViewCell
                    let data = self.pharmacyflowCurrentRequest?.responseData?.invoice
                    cell.deliveryChargeLabel.text = (data?.delivery_amount ?? 0)?.setCurrency()
                    if data?.promocode_id == 0 {
                        cell.couponView.isHidden = true
                    }else{
                        cell.couponView.isHidden = false
                    }
                    cell.couponLabel.text = "- " + ((data?.promocode_amount ?? 0)?.setCurrency() ?? String.empty)
                    cell.totalChargeLabel.text = (data?.payable ?? 0)?.setCurrency()
                    cell.taxAmt.text = (data?.tax_amount ?? 0)?.setCurrency()
                    cell.storePackageAmt.text = (data?.store_package_amount ?? 0)?.setCurrency()
                    if data?.discount == 0 {
                        cell.discountView.isHidden = true
                    }else{
                        cell.discountView.isHidden = false
                    }
                    cell.discountValueLabel.text = "- " + ((data?.discount ?? 0)?.setCurrency() ?? String.empty)
                    cell.itemTotalValueLabel.text = (data?.item_price ?? 0)?.setCurrency()
                    return cell
                }
            }
        }
    }
    
    //Redirect to google map
    private func redirectToGoogleMap() {
        let baseUrl = "comgooglemaps-x-callback://"
        if UIApplication.shared.canOpenURL(URL(string: baseUrl)!) {
           
            let directionsRequest = "comgooglemaps://?saddr=\(lat),\(long)&daddr=\(pharmacyflowCurrentRequest?.responseData?.pickup?.latitude ?? 0.0),\(pharmacyflowCurrentRequest?.responseData?.pickup?.longitude ?? 0.0)&directionsmode=driving"
            if let url = URL(string: directionsRequest) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        else {
            ToastManager.show(title: Constant.googleConstant.localized, state: .error)
        }
    }

    
    @objc func tapCall() {
        guard let _ = self.phoneNumber else {
            return
        }
        AppUtils.shared.call(to: self.phoneNumber)
    }
    //For chat
    @objc func isChatPushRedirection() {
        if isAppPresentTapOnPush == false {
            if isAppFrom == true {
                
                if self.isAppPresentTapOnPush == false {
                    self.isAppPresentTapOnPush = true
                    self.isChatAlreadyPresented = false
                }
                else {
                    self.isChatAlreadyPresented = true
                }
                self.tapMessage()
            }else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                    if self.isAppPresentTapOnPush == false {
                        self.isAppPresentTapOnPush = true
                        self.isChatAlreadyPresented = false
                    }
                    else {
                        self.isChatAlreadyPresented = true
                    }
                    //  self.isChatAlreadyPresented = true
                    self.tapMessage()
                }
            }
        }else{
            
        }
    }
    
    @objc func tapMessage(){
        let checkRequestDetail = self.pharmacyflowCurrentRequest?.responseData
        let providerDetail = checkRequestDetail?.provider
        let userDetail = checkRequestDetail?.user
        let chatView = ChatViewController()
        chatView.requestId = "\((checkRequestDetail?.id ?? 0))"
        chatView.chatRequestFrom = MasterServices.Order.rawValue
        chatView.userId = "\((userDetail?.id ?? 0))"
        chatView.userName = "\( userDetail?.first_name ?? "")" + " " + "\(userDetail?.last_name ?? "")"
        chatView.providerId = "\((providerDetail?.id ?? 0))"
        chatView.providerName = "\(providerDetail?.first_name ?? "")" + " " + "\(providerDetail?.last_name ?? "")"
        chatView.adminServiceId = "\(checkRequestDetail?.admin_service_id ?? "")"
        self.navigationController?.pushViewController(chatView, animated: true)
    }
}

//MARK: - UITableViewDelegate
extension PharmacyflowOrderStatusViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        guard let height = cellHeights[indexPath] else { return 260.0 }
        
        return height
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            if self.pharmacyflowCurrentRequest?.responseData?.order_type == "TAKEAWAY" {
                return 190 //60*3
            }else{
                return 320 //60*3
            }
            
        }else {
            if self.pharmacyflowCurrentRequest?.responseData?.order_type == "TAKEAWAY" {
                if indexPath.row == 1{
                    return 190 //(self.view.frame.height/100)*25
                }else if indexPath.row == 2 {
                    let numberOfItem = self.pharmacyflowCurrentRequest?.responseData?.invoice?.items?.count ?? 0
                    return CGFloat((numberOfItem+1)*55) // number of items
                }else if indexPath.row == 3 {
                    let data = self.pharmacyflowCurrentRequest?.responseData?.invoice
                    if data?.promocode_id == 0 {
                        return 312
                    }else{
                        return 345
                    }
                }
            }else if self.pharmacyflowCurrentRequest?.responseData?.provider == nil {
                if indexPath.row == 1 {
                    let numberOfItem = self.pharmacyflowCurrentRequest?.responseData?.invoice?.items?.count ?? 0
                    return CGFloat((numberOfItem+1)*55) // number of items
                }else if indexPath.row == 2 {
                    let data = self.pharmacyflowCurrentRequest?.responseData?.invoice
                    if data?.promocode_id == 0 {
                        return 312
                    }else{
                        return 345
                    }
                }
            }else{
                if indexPath.row == 1{
                    return 250 //(self.view.frame.height/100)*25
                }else if indexPath.row == 2 {
                    let numberOfItem = self.pharmacyflowCurrentRequest?.responseData?.invoice?.items?.count ?? 0
                    return CGFloat((numberOfItem+1)*55) // number of items
                }else if indexPath.row == 3 {
                    let data = self.pharmacyflowCurrentRequest?.responseData?.invoice
                    if data?.promocode_id == 0 {
                        return 312
                    }else{
                        return 345
                    }
                }
            }
        }
        return UITableView.automaticDimension
    }
}

extension PharmacyflowOrderStatusViewController {
    //MARK:- checkForProviderStatus
    
    func updateProvierDetails() {
        if self.pharmacyflowCurrentRequest?.responseData?.status == nil {
            self.orderStatus = .none
        }
        else {
            let invoiceId = self.pharmacyflowCurrentRequest?.responseData?.store_order_invoice_id ?? ""
            let otp = self.pharmacyflowCurrentRequest?.responseData?.order_otp ?? ""
            self.orderIDLabel.attributeString(string: PharmacyflowConstant.orderId.localized+invoiceId, range: NSRange(location: PharmacyflowConstant.orderId.count, length: invoiceId.count), color: .lightGray)
            self.otpLabel.attributeString(string: PharmacyflowConstant.otp+otp, range: NSRange(location: PharmacyflowConstant.otp.count, length: otp.count), color: .pharmacyflowColor)
            self.orderStatus = pharmacyflowOrderStatus(rawValue: self.pharmacyflowCurrentRequest?.responseData?.status ?? "") ?? .none
            
            if self.orderStatus == .ordered || self.orderStatus == .storeCancelled {
                self.cancelButton.isHidden = false
            }else{
                self.cancelButton.isHidden = true
            }
            
            if self.orderStatus == .cancelled {
                if self.isHome == true {
                    self.navigationController?.popViewController(animated: true)
                }else if self.isFromOrderPage {
                    self.delegate?.onRefreshOrderHistory(tag: self.isFromOrderPage)
                    self.navigationController?.popViewController(animated: true)
                }else{
                    
                    for vc in self.navigationController?.viewControllers ?? [UIViewController()] {
                        if let myViewCont = vc as? HomeViewController
                        {
                            self.navigationController?.popToViewController(myViewCont, animated: true)
                        }
                    }
                }
            }
            
            if self.pharmacyflowCurrentRequest?.responseData?.order_type == "TAKEAWAY" {
                self.phoneNumber = self.pharmacyflowCurrentRequest?.responseData?.pickup?.contact_number
            }else{
                self.phoneNumber = self.pharmacyflowCurrentRequest?.responseData?.provider?.mobile
            }
            
            DispatchQueue.main.async {
                self.orderStatusTableView.reloadData()
                self.orderStatusTableView.beginUpdates()
                self.orderStatusTableView.endUpdates()
            }
        }
    }
}

//MARK: - PharmacyflowPresenterToPharmacyflowViewProtocol
extension PharmacyflowOrderStatusViewController: PharmacyflowPresenterToPharmacyflowViewProtocol {
    
    func getUserRatingResponse(getUserRatingResponse: SuccessEntity){
        BackGroundRequestManager.share.resetBackGroudTask()
        ToastManager.show(title: Constant.RatingToast, state: .success)
        if isFromOrderPage {
            self.delegate?.onRefreshOrderHistory(tag: self.isFromOrderPage)
            
            self.navigationController?.popViewController(animated: true)
        }else{
            
            for vc in self.navigationController?.viewControllers ?? [UIViewController()] {
                if let myViewCont = vc as? HomeViewController
                {
                    self.navigationController?.popToViewController(myViewCont, animated: true)
                }
            }
        }
    }
    
    func pharmacyflowOrderStatusResponse(orderStatus: PharmacyflowOrderDetailEntity){
        if let requetsId = orderStatus.responseData?.id {
            orderRequestId = requetsId
            self.pharmacyflowCurrentRequest = orderStatus
            self.socketAndBgTaskSetUp()
           //self.otpLabel.isHidden = orderStatus.responseData?.self.pharmacyflowCurrentRequest?.re
            if (orderStatus.responseData?.invoice?.is_prescription ?? 0 ) == 1{
                if (orderStatus.responseData?.prescription_amount ?? 0) != 0{
                    print("Helloooooo")
                }
            }
            DispatchQueue.main.async {
                self.updateProvierDetails()
            }
            
           
        }
    }
    
    func getReasons(reasonEntity: ReasonEntity) {
        self.cancelReasonData = reasonEntity.responseData ?? []
        LoadingIndicator.hide()
    }
    func getCancelRequest(cancelEntity: SuccessEntity) {
    
        BackGroundRequestManager.share.resetBackGroudTask()
        ToastManager.show(title: cancelEntity.message ?? "", state: .success)
        LoadingIndicator.hide()
        
        if isFromOrderPage {
            self.delegate?.onRefreshOrderHistory(tag: self.isFromOrderPage)
            
            self.navigationController?.popViewController(animated: true)
        }else{
            
            for vc in self.navigationController?.viewControllers ?? [UIViewController()] {
                if let myViewCont = vc as? HomeViewController
                {
                    self.navigationController?.popToViewController(myViewCont, animated: true)
                }
            }
        }
    }
}

extension PharmacyflowOrderStatusViewController {
    
    func showCancelTable() {
        if self.tableView == nil, let tableView = Bundle.main.loadNibNamed(Constant.CustomTableView, owner: self, options: [:])?.first as? CustomTableView {
            let height = (self.view.frame.height/100)*35
            tableView.frame = CGRect(x: 20, y: (self.view.frame.height/2)-(height/2), width: self.view.frame.width-40, height: height)
            tableView.heading = Constant.chooseReason.localized
            self.tableView = tableView
            self.tableView?.setCornerRadiuswithValue(value: 10.0)
            var reasonArr:[String] = []
            for reason in self.cancelReasonData ?? [] {
                reasonArr.append(reason.reason ?? "")
            }
            if !reasonArr.contains(Constant.other) {
                reasonArr.append(Constant.other)
            }
            tableView.values = reasonArr
            tableView.show(with: .bottom, completion: nil)
            showDimView(view: tableView)
        }
        self.tableView?.onClickClose = { [weak self] in
            guard let self = self else {
                return
            }
            self.tableView?.superview?.dismissView(onCompletion: {
                self.tableView = nil
            })
        }
        self.tableView?.selectedItem = { [weak self] (selectedReason) in
            guard let self = self else {
                return
            }
            self.callCancelAPI(reason: selectedReason)
        }
    }
    
    func callCancelAPI(reason: String?) {
        var param:Parameters = [PharmacyflowConstant.Pid : self.pharmacyflowCurrentRequest?.responseData?.id ??  0]
        if reason !=  nil {
            param[PharmacyflowConstant.PCancelReason] = reason
        }
        LoadingIndicator.show()
        self.pharmacyflowPresenter?.cancelRequest(param: param)
    }
    
    func showDimView(view: UIView) {
        let dimView = UIView(frame: self.view.frame)
        dimView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        dimView.addSubview(view)
        self.view.addSubview(dimView)
    }
}


