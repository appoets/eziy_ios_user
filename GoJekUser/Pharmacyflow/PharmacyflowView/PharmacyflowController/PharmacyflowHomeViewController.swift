//
//  PharmacyflowHomeViewController.swift
//  GoJekUser
//
//  Created by Thiru on 27/02/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation

class PharmacyflowHomeViewController: UIViewController {
    
    //NavigationView
    @IBOutlet weak var filterBtnView: RoundedView!
    @IBOutlet weak var restaurantListTableView: UITableView!
    
    @IBOutlet weak var dishesButton: UIButton!
    @IBOutlet weak var restaurantButton: UIButton!
    @IBOutlet weak var tabView: UIView!

    
    //Variables
    var bottomLineView : UIView?
    
    var bannerView: HomeBannerView!
    var filterView: PharmacyflowScheduleTimeView!
    var shopArrList:[ShopsListData] = []
    var promoCodeListArr:[PromocodeData] = []
    private var mapViewHelper:GoogleMapsHelper?
    
    var lat = 0.0
    var long = 0.0
    var isPharmacyflow = false

    //MARK: View LifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
        initialLoads()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        hideTabBar()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        FLocationManager.shared.stop()
        navigationController?.isNavigationBarHidden = true
    }
    
    
}
extension PharmacyflowHomeViewController {
    
    private func initialLoads() {
        LoadingIndicator.show()
        
        restaurantListTableView.register(UINib(nibName: PharmacyflowConstant.RestaurantTableViewCell, bundle: nil), forCellReuseIdentifier: PharmacyflowConstant.RestaurantTableViewCell)
        restaurantListTableView.register(UINib(nibName: PharmacyflowConstant.EmptyShopTableCell, bundle: nil), forCellReuseIdentifier: PharmacyflowConstant.EmptyShopTableCell)
        
        self.restaurantListTableView.register(nibName: PharmacyConstant.PharmacyOfferCell)
        self.restaurantListTableView.register(nibName: PharmacyConstant.RecommandedTableCell)
        self.restaurantListTableView.register(nibName: PharmacyConstant.PharmacyListCell)
        
        FLocationManager.shared.start { (info) in
            print(info.longitude ?? 0.0)
            print(info.latitude ?? 0.0)
            self.lat = info.latitude ?? 0.0
            self.long = info.longitude ?? 0.0
            let param: Parameters = ["latitude":info.latitude ?? 0.0,
                                     "longitude":info.longitude ?? 0.0]
            self.pharmacyflowPresenter?.getListOfStores(Id: AppManager.shared.getSelectedServices()?.menu_type_id ?? 0, param: param)
        }
        
        showHomeBanner()
        let recong = UITapGestureRecognizer(target: self, action: #selector(tapFilter))
        filterBtnView.addGestureRecognizer(recong)
        title = ""
//        view.backgroundColor = .veryLightGray
        DispatchQueue.main.async {
            self.filterBtnView.setCornerRadius()
//            self.filterBtnView.backgroundColor = .pharmacyflowColor
//            self.filterBtnView.setCenterImage = UIImage(named: PharmacyflowConstant.ic_filter)//?.imageTintColor(color1: .white)
        }
        
        self.setNavigationTitle()
        self.setLeftBarButtonWith(color: .blackColor)
        
        
        
        let rightBarButton = UIBarButtonItem.init(image: UIImage.init(named: Constant.ic_search), style: .plain, target: self, action: #selector(rightBarButtonAction))
//        navigationItem.rightBarButtonItem = rightBarButton
        setDarkMode()
        
        self.restaurantButton.addTarget(self, action: #selector(resturantTapped), for: .touchUpInside)
        self.dishesButton.addTarget(self, action: #selector(dishesTapped), for: .touchUpInside)
        
        self.restaurantButton.setTitle(PharmacyflowConstant.restaurant.localized.uppercased(), for: .normal)
        self.dishesButton.setTitle(PharmacyflowConstant.dishes.localized.uppercased(), for: .normal)
        
        self.restaurantButton.setTitleColor(.pharmacyflowColor, for: .normal)
        self.dishesButton.setTitleColor(.lightGray, for: .normal)
        
        self.bottomLineView = UIView(frame: CGRect(x: 0, y: (self.tabView.frame.height - 5), width: (self.tabView.frame.width/2), height: 5))
        self.bottomLineView?.backgroundColor = .pharmacyflowColor
        self.tabView.addSubview(self.bottomLineView ?? UIView())
        
        self.tabView.backgroundColor = .clear
        
    }
    
    
    private func setDarkMode(){
//        self.view.backgroundColor = .backgroundColor
//        self.restaurantListTableView.backgroundColor = .backgroundColor
    }
    
    @objc func resturantTapped() {
        self.restaurantButton.setTitleColor(.pharmacyflowColor, for: .normal)
        self.dishesButton.setTitleColor(.lightGray, for: .normal)
        self.bottomLineView?.frame = CGRect(x: 0, y: (self.tabView.frame.height - 5), width: (self.tabView.frame.width/2), height: 5)
    }
    
    @objc func dishesTapped() {
        self.restaurantButton.setTitleColor(.lightGray, for: .normal)
        self.dishesButton.setTitleColor(.pharmacyflowColor, for: .normal)
        self.bottomLineView?.frame = CGRect(x: (self.tabView.frame.width/2), y: (self.tabView.frame.height - 5), width: (self.tabView.frame.width/2), height: 5)
    }
    
    //Left navigation bar button action
    @objc func rightBarButtonAction() {
         if guestLogin() {
        let pharmacyflowSearchViewController = PharmacyflowRouter.pharmacyflowStoryboard.instantiateViewController(withIdentifier: PharmacyflowConstant.PharmacyflowSearchViewController) as! PharmacyflowSearchViewController
        if shopArrList.first?.storetype?.category != PharmacyflowConstant.food {
            pharmacyflowSearchViewController.isPharmacyflowCatgory = false
        }else{
            pharmacyflowSearchViewController.isPharmacyflowCatgory = true
            
        }
        navigationController?.pushViewController(pharmacyflowSearchViewController, animated: true)
        }
    }
    
    private func showHomeBanner() {
        
        if self.bannerView == nil, let bannerView = Bundle.main.loadNibNamed(PharmacyflowConstant.HomeBannerView, owner: self, options: [:])?.first as? HomeBannerView {
            
            bannerView.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: self.view.frame.width, height: bannerView.frame.height))
            bannerView.delegate = self
            self.bannerView = bannerView
            
//            self.view.addSubview(bannerView)
            bannerView.show(with: .right, completion: nil)
        }
    }
    
    @objc func tapFilter() {
        
        let pharmacyflowItemsVC = PharmacyflowRouter.pharmacyflowStoryboard.instantiateViewController(withIdentifier: PharmacyflowConstant.PharmacyflowFilterController) as! PharmacyflowFilterController
        pharmacyflowItemsVC.delegate = self
        if(shopArrList.count != 0)
        {
        pharmacyflowItemsVC.isPharmacyflow = shopArrList.first?.storetype?.category == PharmacyflowConstant.food
        }
        else
        {
          pharmacyflowItemsVC.isPharmacyflow = isPharmacyflow
        }
        if #available(iOS 15.0, *) {
//            if let presentationController = pharmacyflowItemsVC.presentationController as? UISheetPresentationController {
//                presentationController.detents = [.medium(),.large()]
//            }
        } else {
            pharmacyflowItemsVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        }
        navigationController?.present(pharmacyflowItemsVC, animated: true, completion: nil)
    }
}

//MARK: UITableViewDelegate

extension PharmacyflowHomeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if shopArrList.count != 0 {
            let pharmacyflowItem = shopArrList[indexPath.row]

//                let pharmacyflowItemsVC = PharmacyflowRouter.pharmacyflowStoryboard.instantiateViewController(withIdentifier: PharmacyflowConstant.PharmacyflowItemsViewController) as! PharmacyflowItemsViewController
//                pharmacyflowItemsVC.restaurentId = pharmacyflowItem.id ?? 0
//                pharmacyflowItemsVC.isPharmacyflow = isPharmacyflow
//                if pharmacyflowItem.shopstatus != "CLOSED" {
//                    pharmacyflowItemsVC.isClosed = false
//                }
//                else
//               {
//                pharmacyflowItemsVC.isClosed = true
//               }
//                navigationController?.pushViewController(pharmacyflowItemsVC, animated: true)
         
            let pharmacyflowItemsVC = PharmacyflowRouter.pharmacyflowStoryboard.instantiateViewController(withIdentifier: PharmacyflowConstant.PharmacyDetailVC) as! PharmacyDetailVC
            pharmacyflowItemsVC.restaurentId = pharmacyflowItem.id ?? 0
            pharmacyflowItemsVC.isPharmacyflow = isPharmacyflow
            if pharmacyflowItem.shopstatus != "CLOSED" {
                pharmacyflowItemsVC.isClosed = false
            }
            else
           {
            pharmacyflowItemsVC.isClosed = true
           }
            navigationController?.pushViewController(pharmacyflowItemsVC, animated: true)
        }
    }
}

//MARK: - UITableViewDataSource

extension PharmacyflowHomeViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else if section == 1{
            var count = 0
            if shopArrList.count == 0 {
                count = 1
            }else{
                count = shopArrList.count
            }
            return count
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }else{
            return 55
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        let views =  Bundle.main.loadNibNamed(PharmacyConstant.PharmacyHeaderView, owner: self, options: [:])?.first as! PharmacyHeaderView
        views.frame = CGRect(x: 0, y: 0, width:self.view.bounds.width - 400, height: 10)
        views.backgroundColor = .clear
        headerView.addSubview(views)
        if section == 1{
            views.titleLbl.text = "Recent Searches"
            views.seeAllLbl.addTap {
                let vc = PharmacyRouter.pharmacyStoryboard.instantiateViewController(withIdentifier: PharmacyConstant.PharmacyListVC) as! PharmacyListVC
                vc.shopArrList = self.shopArrList
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            views.titleLbl.text = "Recommended".uppercased()
        }
        headerView.clipsToBounds = true
        return headerView
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: PharmacyConstant.PharmacyOfferCell, for: indexPath) as! PharmacyOfferCell
//            cell.contentView.backgroundColor = .clear
            return cell
        }else{
         if shopArrList.count == 0 {
            let cell = restaurantListTableView.dequeueReusableCell(withIdentifier: PharmacyflowConstant.EmptyShopTableCell, for: indexPath) as! EmptyShopTableCell
            return cell
         }else{
             let cell = restaurantListTableView.dequeueReusableCell(withIdentifier: PharmacyflowConstant.PharmacyListCell, for: indexPath) as! PharmacyListCell
            cell.setShopListData(data: shopArrList[indexPath.row])
            return cell
        }
        }
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//        return bannerView
//    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 220
        }else{
        return UITableView.automaticDimension
        }
    }
}

//MARK: - PharmacyflowPresenterToPharmacyflowViewProtocol

extension PharmacyflowHomeViewController: PharmacyflowPresenterToPharmacyflowViewProtocol {
    
    func getListOfStoresResponse(getStoreResponse: StoreListEntity) {
        let params:Parameters = ["" : ""]
        pharmacyflowPresenter?.getPromoCodeList(param: params)
        shopArrList = getStoreResponse.responseData ?? []
        title = APPConstant.appName + " " + (getStoreResponse.responseData?.first?.storetype?.name?.capitalized ?? "")
        isPharmacyflow = getStoreResponse.responseData?.first?.storetype?.category == PharmacyflowConstant.food
        bannerView.filterButton.isHidden = getStoreResponse.responseData?.first?.storetype?.category != PharmacyflowConstant.food
        restaurantListTableView.reloadData()
        LoadingIndicator.hide()
    }
    
    func getPromoCodeResponse(getPromoCodeResponse: PromocodeEntity) {

        promoCodeListArr = getPromoCodeResponse.responseData ?? []
        bannerView.promoCodeList = promoCodeListArr
        if bannerView.promoCodeList.count == 0 {
            bannerView.errorView.isHidden = false
        }else{
            bannerView.errorView.isHidden = true
        }
        bannerView.bannerCollectionView.reloadData()
        
    }
    
    func getFilterRestaurantResponse(getFilterRestaurantResponse: StoreListEntity) {
        shopArrList = getFilterRestaurantResponse.responseData ?? []

        restaurantListTableView.reloadData()
    }
}

//MARK: - HomeBannerViewDelegate

extension PharmacyflowHomeViewController: HomeBannerViewDelegate,PharmacyflowFilterControllerDelegate {
    func applyFilterAction(filterArr:String,qfilter:String){
        
        let param: Parameters = ["latitude":lat,
                                 "longitude":long]
        self.pharmacyflowPresenter?.getFilterRestaurant(Id: AppManager.shared.getSelectedServices()?.menu_type_id ?? 0, filter: filterArr, qFilter: qfilter, param: param)
        
    }
    
    func applyFilterAction(vegOrNonVeg:String) {
        
        let param: Parameters = ["latitude":lat, "longitude":long]
        self.pharmacyflowPresenter?.getFilterRestaurant(Id: AppManager.shared.getSelectedServices()?.menu_type_id ?? 0, filter: "", qFilter: vegOrNonVeg, param: param)
    }
}


