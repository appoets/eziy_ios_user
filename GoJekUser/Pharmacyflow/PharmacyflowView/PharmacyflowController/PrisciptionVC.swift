//
//  PrisciptionVC.swift
//  GoJekUser
//
//  Created by Shyamala's MacBook Pro on 05/01/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import Alamofire

class PrisciptionVC: UIViewController {
    
    @IBOutlet weak var acceptBtn : UIButton!
    @IBOutlet weak var cancelBtn : UIButton!
    @IBOutlet weak var backBtn : UIButton!
    @IBOutlet weak var img : UIImageView!
    
    @IBOutlet weak var yourorderOuterView : UIView!
    @IBOutlet weak var yourorderAddedView : UIView!
    @IBOutlet weak var yourorderAddedLbl : UILabel!
    @IBOutlet weak var gotocartBtn : UIButton!
    
    var shopDetail: ShopDetail?
    var prescriptionImg : UIImage?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.yourorderAddedLbl.font = .setCustomFont(name: .bold, size: .x24)
        self.gotocartBtn.setTitle("GO TO CART", for: .normal)
        
        self.yourorderAddedLbl.textColor = .white
        self.yourorderAddedLbl.text = "Your Order Added in Cart"
        self.gotocartBtn.layer.cornerRadius = 10
        self.gotocartBtn.backgroundColor = .darkGray
        self.yourorderAddedView.layer.cornerRadius = 20
        self.gotocartBtn.layer.cornerRadius = 20
        self.gotocartBtn.setTitleColor(.white, for: .normal)
        
        [self.acceptBtn,self.cancelBtn,self.backBtn].forEach { btn in
            btn?.setTitle("", for: .normal)
        }
        
        self.img.image = prescriptionImg
        
        self.yourorderOuterView.addTap {
            self.yourorderAddedView.isHidden = true
            self.yourorderOuterView.isHidden = true
        }
        
        self.acceptBtn.addTap {
            self.uploadPrescription()
        }
        self.cancelBtn.addTap {
            self.navigationController?.popViewController(animated: true)
        }
        self.gotocartBtn.addTap {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PriscriptionCartVC") as! PriscriptionCartVC
            vc.prescripitionImg = self.prescriptionImg
            vc.shopDetail = self.shopDetail
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension PrisciptionVC : PharmacyflowPresenterToPharmacyflowViewProtocol{
    func uploadPrescription()
    {
        let param: Parameters = [PharmacyflowConstant.is_prescription: "1",
                                     PharmacyflowConstant.prescriptionLength: "1",
                                     PharmacyflowConstant.shop_id: self.shopDetail?.id ?? 0]
        self.pharmacyflowPresenter?.uploadPrescription(params: param, imagedata: ["prescription[0]" : self.prescriptionImg?.pngData() ?? Data()])
    }
    
    func getCartListResponse(cartListEntity: PharmacyflowCartListEntity) {
            self.yourorderAddedView.isHidden = false
            self.yourorderOuterView.isHidden = false
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PriscriptionCartVC") as! PriscriptionCartVC
        vc.prescripitionImg = self.prescriptionImg
        vc.shopDetail = self.shopDetail
        vc.cartData = cartListEntity
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
