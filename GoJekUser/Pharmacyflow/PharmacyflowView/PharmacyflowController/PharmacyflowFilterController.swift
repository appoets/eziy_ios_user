//
//  PharmacyflowFilterController.swift
//  GoJekUser
//
//  Created by Ansar on 19/03/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit

class PharmacyflowFilterController: UIViewController {
    
    @IBOutlet weak var filterTableView: UITableView!
    @IBOutlet weak var applyFiterButton: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var headingLabel: UILabel!
    
    @IBOutlet weak var closeBgButton: UIButton!
    @IBOutlet weak var bottomLine: UIView!
    
    var appliedFilter:Bool = false
    var cusineList:[CusineResponseData] = []
    var showRestaurantList = [PharmacyflowConstant.pureVeg,PharmacyflowConstant.nonVeg,PharmacyflowConstant.freeDelivery]
    var qfilterArr = NSMutableArray()
    var filterArr = NSMutableArray()
    weak var delegate: PharmacyflowFilterControllerDelegate?
    var isPharmacyflow:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialLoads()
    }
    
    
}

//MARK: - Methods

extension PharmacyflowFilterController {
    private func initialLoads()  {
        self.closeBgButton.addTarget(self, action: #selector(tapCloseButton), for: .touchUpInside)
        self.resetButton.addTarget(self, action: #selector(tapResetButton), for: .touchUpInside)
        applyFiterButton.addTarget(self, action: #selector(tapapplyFilterButton), for: .touchUpInside)
        self.filterTableView.register(nibName: PharmacyflowConstant.PharmacyflowFilterTableViewCell)
        self.closeButton.setImage(UIImage(named: Constant.closeImage), for: .normal)
        self.applyFiterButton.backgroundColor = .pharmacyflowColor
        self.applyFiterButton.setCornerRadius()
        resetButton.setTitleColor(.lightGray, for: .normal)
        bottomLine.backgroundColor = .veryLightGray
        
        setFont()
        setLocalize()
        pharmacyflowPresenter?.getCusineList(Id: AppManager.shared.getSelectedServices()?.menu_type_id ?? 0)
        setDarkMode()
    }
    
    
    private func setDarkMode(){
        self.view.backgroundColor = .white
        self.closeButton.tintColor = .blackColor
        
//        filterTableView.layer.cornerRadius = 50
        filterTableView.layer.masksToBounds = false
        filterTableView.layer.shadowColor = UIColor.black.withAlphaComponent(0.4).cgColor // any value you want
        filterTableView.layer.shadowOpacity = 0.5 // any value you want
        filterTableView.layer.shadowRadius = 5// any value you want
        filterTableView.layer.shadowOffset = CGSize(width: 1, height: 1)

    }
    
    func setLocalize(){
        self.headingLabel.text = PharmacyflowConstant.filters.localized.uppercased()
        self.resetButton.setTitle(PharmacyflowConstant.clearAll.localized.capitalized, for: .normal)
        applyFiterButton.setTitle(PharmacyflowConstant.done.localized.uppercased(), for: .normal)
    }
    
    func setFont(){
        applyFiterButton.titleLabel?.font = UIFont.setCustomFont(name: .medium, size: .x16)
        resetButton.titleLabel?.font = UIFont.setCustomFont(name: .medium, size: .x12)
        headingLabel.font = UIFont.setCustomFont(name: .bold, size: .x20)
        headingLabel.textColor = .pharmacyflowColor
    }
    
    
    @objc func tapCloseButton() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func tapapplyFilterButton() {
        
        for item in qfilterArr {
            let itemStr = item as! String
            if itemStr == PharmacyflowConstant.pureVeg {
                qfilterArr.remove(item)
                qfilterArr.add(RestaurantType.veg.rawValue)
            }else if itemStr == PharmacyflowConstant.nonVeg{
                qfilterArr.remove(item)
                qfilterArr.add(RestaurantType.nonveg.rawValue)
            }else if itemStr == PharmacyflowConstant.freeDelivery {
                qfilterArr.remove(item)
                qfilterArr.add(RestaurantType.freedelivery.rawValue)
            }
        }
        let filterstr = filterArr.componentsJoined(by: ",")
        let qFilterstr = qfilterArr.componentsJoined(by: ",")
        
        delegate?.applyFilterAction(filterArr: filterstr, qfilter: qFilterstr)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @objc func tapResetButton() {
        filterArr.removeAllObjects()
        qfilterArr.removeAllObjects()
        filterTableView.reloadData()
    }
}

//MARK: - Tableview Delegate Datasource

extension PharmacyflowFilterController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = self.filterTableView.cellForRow(at: indexPath) as! PharmacyflowFilterTableViewCell
        cell.isSelectedItem = !cell.isSelectedItem
        
        if !isPharmacyflow {
            if cell.isSelectedItem {
                filterArr.add(cusineList[indexPath.row].id ?? 0)
            }else{
                if filterArr.contains(cusineList[indexPath.row].id ?? 0) {
                    filterArr.remove(cusineList[indexPath.row].id ?? 0)
                }
            }
        }else{
            if indexPath.section == 0 {
                if cell.isSelectedItem {
                    qfilterArr.add(self.showRestaurantList[indexPath.row])
                }else{
                    if qfilterArr.contains(self.showRestaurantList[indexPath.row]) {
                        qfilterArr.remove(cusineList[indexPath.row].id ?? 0)
                    }
                }
            }else{
                if cell.isSelectedItem {
                    filterArr.add(cusineList[indexPath.row].id ?? 0)
                }else{
                    if filterArr.contains(cusineList[indexPath.row].id ?? 0) {
                        filterArr.remove(cusineList[indexPath.row].id ?? 0)
                    }
                }
            }
        }
    }
}

extension PharmacyflowFilterController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return isPharmacyflow ? 2 : 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !isPharmacyflow {
            return cusineList.count
        }else{
            return section == 0 ? showRestaurantList.count: cusineList.count
        }
    }
    

    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return  isPharmacyflow ? (section == 0 ? 10 : 40) : 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30))
        headerView.backgroundColor = .white
        
        let label = UILabel(frame: CGRect(x: 10, y: 10, width: headerView.frame.width-40, height: headerView.frame.height))
        label.font = UIFont.setCustomFont(name: .medium, size: .x18)
        if !isPharmacyflow {
            label.text = PharmacyflowConstant.category.localized
        }else{
            label.text = section == 0 ? "" : PharmacyflowConstant.cuisine.localized
        }
        headerView.addSubview(label)
        headerView.clipsToBounds = false
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:PharmacyflowFilterTableViewCell = self.filterTableView.dequeueReusableCell(withIdentifier: PharmacyflowConstant.PharmacyflowFilterTableViewCell, for: indexPath) as! PharmacyflowFilterTableViewCell

        if !isPharmacyflow {
            if filterArr.contains(cusineList[indexPath.row].id ?? 0) {
                cell.isSelectedItem = true
            }else{
                cell.isSelectedItem = false
                
            }
            cell.setFilterData(data: cusineList[indexPath.row])
        }else{
            if indexPath.section == 0 {
                cell.titleLabel.text = self.showRestaurantList[indexPath.row]
                if qfilterArr.contains(self.showRestaurantList[indexPath.row]) {
                    cell.isSelectedItem = true
                }else{
                    cell.isSelectedItem = false
                }
            }else{
                if filterArr.contains(cusineList[indexPath.row].id ?? 0) {
                    cell.isSelectedItem = true
                }else{
                    cell.isSelectedItem = false
                    
                }
                cell.setFilterData(data: cusineList[indexPath.row])
            }
        }
        cell.clipsToBounds = false
        return cell
    }
    
}
extension PharmacyflowFilterController: PharmacyflowPresenterToPharmacyflowViewProtocol{
    func cusineListResponse(getCusineListResponse: CusineListEntity) {
        cusineList = getCusineListResponse.responseData ?? []
        filterTableView.reloadInMainThread()
    }
}

// MARK: - Protocol
protocol PharmacyflowFilterControllerDelegate: class {
    func applyFilterAction(filterArr:String,qfilter:String)
}
