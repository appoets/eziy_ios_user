//
//  PharmacyflowFilterTableViewCell.swift
//  GoJekUser
//
//  Created by Thiru on 11/03/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit

class PharmacyflowFilterTableViewCell: UITableViewCell {

    @IBOutlet weak var selectImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var isSelectedItem: Bool = false {
        didSet {
            selectImageView.image = UIImage(named: isSelectedItem ? Constant.radioFill : Constant.radioEmpty)
            if selectImageView.image == UIImage(named: Constant.radioEmpty) {
                selectImageView.imageTintColor(color1: .lightGray)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.font = UIFont.setCustomFont(name: .medium, size: .x14)
        titleLabel.textColor = .lightGray
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
       
    }
    
    func setFilterData(data: CusineResponseData){
        titleLabel.text = data.name
    }
    
}
