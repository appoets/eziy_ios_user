//
//  ViewCartView.swift
//  GoJekUser
//
//  Created by Thiru on 07/03/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit

class PharmacyflowViewCartView: UIView {
    
    //Outlets
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var extrachargeLabel: UILabel!
    
    @IBOutlet weak var dashedLineView: UIView!
    @IBOutlet weak var viewCartButton: UIButton!
    @IBOutlet weak var cartView: UIView!

    //Life Cycles
    override func awakeFromNib() {
        super.awakeFromNib()
        initialLoad()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
}
extension PharmacyflowViewCartView {
    
    private func initialLoad () {
        
        self.initialViewLoad()
        self.setCustomColor()
    }
    
    private func initialViewLoad() {
        
        self.viewCartButton.addTarget(self, action: #selector(ViewCartAction), for: .touchUpInside)
        self.viewCartButton.setImage(UIImage(named: FoodieConstant.ic_cartbag)?.imageTintColor(color1: .white), for: .normal)
     
        self.dashedLineView.addDashLine(strokeColor: .white, lineWidth: 0.8)
        
        if CommonFunction.checkisRTL() {
            self.viewCartButton.changeToRight(spacing: -10)
        }else {
            self.viewCartButton.changeToRight(spacing: 10)
        }
        
        self.setCornerRadiuswithValue(value: 5.0)
        
        self.extrachargeLabel.text = FoodieConstant.extraCharge.localized
        self.viewCartButton.setTitle(FoodieConstant.TViewcart.localized.uppercased(), for: .normal)

        self.viewCartButton.titleLabel?.font = .setCustomFont(name: .bold, size: .x16)
        self.priceLabel.font = .setCustomFont(name: .bold, size: .x16)
        self.extrachargeLabel.font = .setCustomFont(name: .bold, size: .x13)
        viewCartButton.titleLabel?.adjustsFontSizeToFitWidth = true
        extrachargeLabel.adjustsFontSizeToFitWidth = true
    }
    
    private func setCustomColor() {
        
        self.backgroundColor = .clear
        self.viewCartButton.tintColor = .white
        self.cartView.backgroundColor = .foodieColor
        self.viewCartButton.setTitleColor(.white, for: .normal)
        self.priceLabel.textColor = .white
        self.extrachargeLabel.textColor = .white
    }
    
    @objc func ViewCartAction() {
        
        let foodiecartVC = PharmacyflowRouter.pharmacyflowStoryboard.instantiateViewController(withIdentifier: PharmacyflowConstant.PharmacyflowCartViewController)
        UIApplication.topViewController()?.navigationController?.pushViewController(foodiecartVC, animated: true)
        
    }
}
