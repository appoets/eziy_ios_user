//
//  PharmacyDetailCell.swift
//  GoJekUser
//
//  Created by Shyamala's MacBook Pro on 23/11/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit

class PharmacyDetailCell: UITableViewCell {
    
    @IBOutlet weak var pharmacyImg : UIImageView!
    @IBOutlet weak var pharmacyratingLbl : UILabel!
    @IBOutlet weak var pharmacyAddressLbl : UILabel!
    @IBOutlet weak var pharmacyratingCountsLbl : UILabel!
    @IBOutlet weak var pharmacytimingLbl : UILabel!
    @IBOutlet weak var pharmacylocLbl : UILabel!
    @IBOutlet weak var pharmacyPrescriptionsLbl : UILabel!
    @IBOutlet weak var prescriptionsView : UIView!
    @IBOutlet weak var prescriptionsImg : UIImageView!
    @IBOutlet weak var outerView : UIView!
    @IBOutlet weak var ratingView : FloatRatingView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
        self.setupDummyData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setupView(){
        self.pharmacyImg.layer.cornerRadius = 10
        self.outerView.layer.cornerRadius = 10
        self.prescriptionsView.layer.cornerRadius = 10
        self.pharmacyAddressLbl.font = .setCustomFont(name: .bold, size: .x14)
        self.pharmacyAddressLbl.textColor = .white
        self.pharmacyratingLbl.font = .setCustomFont(name: .medium, size: .x14)
        self.pharmacyratingCountsLbl.font = .setCustomFont(name: .medium, size: .x14)
        self.pharmacytimingLbl.font = .setCustomFont(name: .medium, size: .x12)
        self.pharmacylocLbl.font = .setCustomFont(name: .medium, size: .x12)
        self.pharmacytimingLbl.textColor = .green
        self.pharmacylocLbl.textColor = .pharmacyColor
        self.pharmacyratingCountsLbl.textColor = .darkGray
        
        self.pharmacyPrescriptionsLbl.font = .setCustomFont(name: .light, size: .x12)
        self.pharmacyPrescriptionsLbl.text = PharmacyConstant.uploadPrescription
        
    }
    
    func setupDummyData(){
        self.pharmacyratingLbl.text = "4.2"
        self.pharmacyratingCountsLbl.text = "( 186 rating )"
        self.pharmacytimingLbl.text = "30 Mins"
        self.pharmacylocLbl.text = "Chennai, India"
        self.pharmacyAddressLbl.text = "No 12, Gandhi Mandapam , Chennai , India"
    }
    
}
