//
//  PharmacyflowOrderStatusTableViewCell.swift
//  GoJekUser
//
//  Created by Thiru on 09/03/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit

class PharmacyflowOrderStatusTableViewCell: UITableViewCell {
    
    @IBOutlet weak var orderStatusTableView: UITableView!
    var orderComplete:(()->Void)?
    
    var orderType = ""
    var orderReadystatus = 0
    
    var orderStatus: pharmacyflowOrderStatus = .searching {
        didSet {
            if orderStatus != .completed {
                self.orderStatusTableView.reloadInMainThread()
            }
        }
    }
    
    var statusTakeAwayImage = [PharmacyflowConstant.ic_process,PharmacyflowConstant.ic_delivery]
    var statusTakeAwayStr = [PharmacyflowConstant.orderProcess,PharmacyflowConstant.orderDeliver]
    
    var statusImage = [PharmacyflowConstant.ic_process,PharmacyflowConstant.cateringImage,PharmacyflowConstant.scooterImage,PharmacyflowConstant.ic_ondway,PharmacyflowConstant.ic_delivery]
    var statusStr = [PharmacyflowConstant.orderProcess.localized,PharmacyflowConstant.orderAssign.localized,PharmacyflowConstant.orderReach.localized,PharmacyflowConstant.orderOndway.localized,PharmacyflowConstant.orderDeliver.localized]
    var statusDesc = [PharmacyflowConstant.orderProcessDesc.localized,PharmacyflowConstant.orderOndwayDesc.localized,PharmacyflowConstant.orderDeliverDesc.localized]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialLoads()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

//MARK: - Methods

extension PharmacyflowOrderStatusTableViewCell  {
    private func initialLoads() {
        orderStatusTableView.delegate = self
        orderStatusTableView.dataSource = self
        orderStatusTableView.register(UINib(nibName: PharmacyflowConstant.OrderStatusCell, bundle: nil), forCellReuseIdentifier: PharmacyflowConstant.OrderStatusCell)
    }
    
   
}

//MARK: - Tableview delegate datasource


extension PharmacyflowOrderStatusTableViewCell: UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if orderType == "TAKEAWAY" {
            return 2
        }else{
            return 5
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  60
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.orderStatusTableView.dequeueReusableCell(withIdentifier: PharmacyflowConstant.OrderStatusCell, for: indexPath) as! OrderStatusCell
        if orderType == "TAKEAWAY" {
            cell.statusImage.image = UIImage(named: statusTakeAwayImage[indexPath.row])?.imageTintColor(color1: .pharmacyflowColor)
            cell.statusLabel.text = statusTakeAwayStr[indexPath.row]
            if orderStatus.rawValue == "ORDERED" {
                cell.statusDescLabel.text = PharmacyflowConstant.orderedStatus
                cell.isCurrentState = indexPath.row == 0
            }else if orderReadystatus == 0 && orderStatus.rawValue == "RECEIVED" {
                cell.statusDescLabel.text = PharmacyflowConstant.recievedStatus
                cell.isCurrentState = indexPath.row == 0
            }else if orderReadystatus == 1 && orderStatus.rawValue == "RECEIVED" {
                cell.statusDescLabel.text = PharmacyflowConstant.pickupStatus
                cell.isCurrentState = indexPath.row == 1
            }else if orderReadystatus == 1 && orderStatus.rawValue == "COMPLETED" {
                cell.statusDescLabel.text = PharmacyflowConstant.orderDeliverDesc
                cell.isCurrentState = indexPath.row == 1
            }else if orderReadystatus == 0 && orderStatus.rawValue == "STORECANCELLED" {
                cell.statusDescLabel.text = PharmacyflowConstant.orderedStatus
                cell.isCurrentState = indexPath.row == 0
            }
            
        }else{
            cell.statusImage.image = UIImage(named: statusImage[indexPath.row])?.imageTintColor(color1: .pharmacyflowColor)
            cell.statusLabel.text = statusStr[indexPath.row]
            cell.statusDescLabel.text = orderStatus.statusStr
            cell.isCurrentState = indexPath.row == orderStatus.index
        }
    
        return cell
    }

}
