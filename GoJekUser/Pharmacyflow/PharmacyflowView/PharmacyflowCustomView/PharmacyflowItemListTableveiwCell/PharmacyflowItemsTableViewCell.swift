//
//  PharmacyflowItemsTableViewCell.swift
//  GoJekUser
//
//  Created by Thiru on 07/03/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit

class PharmacyflowItemsTableViewCell: UITableViewCell {
    
    //MARK: - IBOutlet
    @IBOutlet weak var itemNotAvailableLabel: UILabel!
    @IBOutlet weak var itemNotAvailableView: UIView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var itemsaddView: PlusMinusView!
    @IBOutlet weak var customizableLabel: UILabel!
    @IBOutlet weak var addonsLabel: UILabel!
    @IBOutlet weak var customizeButton: UIButton!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var itemDisableLabel: UILabel!
    @IBOutlet weak var itemDisableVw: UIStackView!
    @IBOutlet weak var delBtn: UIButton!
    @IBOutlet weak var qtyLabel: UILabel!
    @IBOutlet weak var vegImgVw: UIImageView!
    @IBOutlet weak var imageOuterView: UIView!
    @IBOutlet weak var overView: UIStackView!
    @IBOutlet weak var isprescription : UIView!
    @IBOutlet weak var isprescriptionImg : UIImageView!


    var pharmacyflowAddonsgaDelegate: ShowAddonsDelegates?
    
    var isVeg: Bool = false {
          didSet {
              vegImgVw.image = UIImage(named: isVeg ? PharmacyflowConstant.ic_veg : PharmacyflowConstant.ic_nonveg)
          }
      }

    override func awakeFromNib() {
        super.awakeFromNib()
        initialLoad()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        itemsaddView.setCornerRadius()
//        itemImageView?.applyshadowWithCorner(containerView : imageOuterView, cornerRadious : 5.0)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

//MARK: - LocalMethod
extension PharmacyflowItemsTableViewCell {
    
    private func initialLoad() {
        itemDisableLabel.layer.borderColor = UIColor.lightGray.cgColor
        itemDisableLabel.layer.borderWidth = 1
        
        addonsLabel.isHidden = true
        customizeButton.isHidden = true
        customizeButton.setTitleColor(.pharmacyflowColor, for: .normal)
        itemsaddView.buttonColor = .pharmacyflowColor
        itemNotAvailableView.backgroundColor = .pharmacyflowColor
        contentView.backgroundColor = .backgroundColor
        priceLabel.font = UIFont.setCustomFont(name: .medium, size: .x16)
        customizableLabel.text = PharmacyflowConstant.Customizable.localized
        itemNotAvailableLabel.text = PharmacyflowConstant.itemNotAvailable.localized
        customizableLabel.font = UIFont.setCustomFont(name: .medium, size: .x12)
        customizableLabel.textColor = .lightGray
        itemNameLabel.font = UIFont.setCustomFont(name: .medium, size: .x16)
        priceLabel.font = UIFont.setCustomFont(name: .medium, size: .x14)
        customizeButton.titleLabel?.font = UIFont.setCustomFont(name: .medium, size: .x12)
        addonsLabel.font = UIFont.setCustomFont(name: .medium, size: .x14)
        itemNotAvailableLabel.font  = UIFont.setCustomFont(name: .medium, size: .x12)
        customizeButton.setTitle(PharmacyflowConstant.customize.localized, for: .normal)
//        customizeButton.setImage(UIImage(named: PharmacyflowConstant.ic_downarrow)?.imageTintColor(color1: .pharmacyflowColor), for: .normal)
        customizeButton.addTarget(self, action: #selector(tapCustomize(_:)), for: .touchUpInside)
//        if CommonFunction.checkisRTL() {
//            customizeButton.changeToRight(spacing: -6)
//        }else {
//            customizeButton.semanticContentAttribute = .forceRightToLeft
//            customizeButton.sizeToFit()
//            customizeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//
//        }
        qtyLabel.font = UIFont.setCustomFont(name: .medium, size: .x14)
        qtyLabel.textColor = .lightGray
        itemDisableLabel.font = UIFont.setCustomFont(name: .medium, size: .x12)
        delBtn.setBothCorner()
        itemDisableLabel.textAlignment = .center
        itemDisableLabel.text = PharmacyflowConstant.itemnotavail.localized
        itemDisableLabel.textColor = .red
          
        delBtn.setImage(UIImage(named:PharmacyflowConstant.ic_delete), for: .normal)
        delBtn.imageView?.tintColor = .lightGray
        addonsLabel.textColor = .lightGray
        itemNotAvailableLabel.textColor = .white

        DispatchQueue.main.async {
            self.backView.addShadow(radius: 5.0, color: UIColor.darkGray)
        }
        setDarkMode()
    }
    
    private func setDarkMode(){
        self.backView.backgroundColor = .veryLightGray
    }
    @objc func tapCustomize(_ sender: UIButton){
        
        pharmacyflowAddonsgaDelegate?.addonsCustomize(tag: sender.tag)
    }
    
     func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0,y: 0,width:  width,height:  CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text

        label.sizeToFit()
        return label.frame.height
    }
    
}
//protocol ShowAddonsDelegates {
//    
//    func addonsCustomize(tag: Int)
//}



