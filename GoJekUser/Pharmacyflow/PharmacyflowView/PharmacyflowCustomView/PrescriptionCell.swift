//
//  PrescriptionCell.swift
//  GoJekUser
//
//  Created by Shyamala's MacBook Pro on 23/11/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit

class PrescriptionCell: UITableViewCell {
    
    @IBOutlet weak var contentsView : UIView!
    @IBOutlet weak var pharmacyImg : UIImageView!
    @IBOutlet weak var titleLbl : UILabel!
    @IBOutlet weak var descLbl : UILabel!
    @IBOutlet weak var ratingLbl : UILabel!
    @IBOutlet weak var ratingCountLbl : UILabel!
    @IBOutlet weak var priceLbl : UILabel!
    @IBOutlet weak var offerLbl : UILabel!
    @IBOutlet weak var ratingview : FloatRatingView!
    @IBOutlet weak var addcardBtn : UIButton!
    @IBOutlet weak var itemsaddView: PlusMinusView!
    @IBOutlet weak var itemNotAvailableLabel: UILabel!
    @IBOutlet weak var itemNotAvailableView: UIView!

    var isItemAvailable = false{
        didSet{
            if(isItemAvailable == true){
                itemsaddView.isHidden = false
//                customizableLabel.isHidden = false
                itemNotAvailableView.isHidden = true
                self.backgroundView?.alpha = 1.0
            }
            else{
                itemsaddView.isHidden = true
//                customizableLabel.isHidden = true
                itemNotAvailableView.isHidden = false
                self.backgroundView?.alpha = 0.5
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
        self.setupDummyData()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setupView(){
        self.itemsaddView.buttonColor = .pharmacyColor
       
        self.titleLbl.font = .setCustomFont(name: .bold, size: .x15)
        self.descLbl.font = .setCustomFont(name: .medium, size: .x13)
        self.descLbl.textColor = .darkGray
        self.ratingLbl.font = .setCustomFont(name: .medium, size: .x12)
        self.ratingCountLbl.font = .setCustomFont(name: .medium, size: .x13)
        self.ratingCountLbl.textColor = .darkGray
        self.priceLbl.font = .setCustomFont(name: .bold, size: .x16)
        self.offerLbl.font = .setCustomFont(name: .bold, size: .x15)
        self.offerLbl.textColor = .green
        self.addcardBtn.setTitle("Add to cart", for: .normal)
        self.addcardBtn.backgroundColor = .clear
        self.addcardBtn.layer.borderColor = UIColor.pharmacyColor.cgColor
        self.addcardBtn.layer.borderWidth = 1
        self.addcardBtn.setTitleColor(.pharmacyColor, for: .normal)
        self.addcardBtn.layer.cornerRadius = 5
        self.pharmacyImg.layer.cornerRadius = 5
    }
    
    func setupDummyData(){
        self.titleLbl.text = "Burnol Cream"
        self.descLbl.text = "tube of 20 gm cream"
        self.ratingLbl.text = "4.2"
        self.ratingCountLbl.text = "(186 ratings)"
        self.priceLbl.text = "$71"
        self.offerLbl.text = "10% off"
    }
}

