//
//  PharmacyflowPresenter.swift
//  GoJekUser
//
//  Created by apple on 20/02/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit
import Alamofire


class PharmacyflowPresenter: PharmacyflowViewToPharmacyflowPresenterProtocol {
   
    var pharmacyflowView: PharmacyflowPresenterToPharmacyflowViewProtocol?
    var pharmacyflowInteractor: PharmacyflowPresenterToPharmacyflowInteractorProtocol?
    var pharmacyflowRouter: PharmacyflowPresenterToPharmacyflowRouterProtocol?
    
    func uploadPrescription(params: Parameters, imagedata: [String : Data]) {
        pharmacyflowInteractor?.uploadPrescription(params: params, imagedata: imagedata)
    }
    
    func getListOfStores(Id: Int, param: Parameters) {
        pharmacyflowInteractor?.getListOfStores(Id: Id, param: param)
    }
    func getPromoCodeList(param: Parameters) {
        pharmacyflowInteractor?.getPromoCodeList(param: param)
    }
    func searchRestaurantList(id: Int,type: String,searchStr: String,param: Parameters) {
        pharmacyflowInteractor?.searchRestaurantList(id: id,type: type,searchStr: searchStr,param: param)
    }
    func getCusineList(Id: Int){
        pharmacyflowInteractor?.getCusineList(Id: Id)
    }
    func getCartList(){
        pharmacyflowInteractor?.getCartList()
    }

   
    func getFilterRestaurant(Id: Int,filter: String,qFilter: String,param: Parameters){
        pharmacyflowInteractor?.getFilterRestaurant(Id: Id, filter: filter, qFilter: qFilter,param: param)

    }
    func userRatingParam(param: Parameters){
        pharmacyflowInteractor?.userRatingParam(param: param)
    }


    func getStoresDetail(with Id: Int, param: Parameters) {
        pharmacyflowInteractor?.getStoresDetail(with: Id, param: param)
    }
    
    func postAddToCart(param: Parameters) {
        pharmacyflowInteractor?.postAddToCart(param: param)
    }
    
    func getCartList(param: Parameters) {
        pharmacyflowInteractor?.getCartList(param: param)
    }
    
    func postRemoveCart(param: Parameters) {
        pharmacyflowInteractor?.postRemoveCart(param: param)
    }
    
    func postOrderCheckout(param: Parameters) {
        pharmacyflowInteractor?.postOrderCheckout(param: param)
    }
    
    func getSavedAddress() {
        pharmacyflowInteractor?.getSavedAddress()
    }
    func getOrderStatus(Id: Int){
        pharmacyflowInteractor?.getOrderStatus(Id: Id)

    }
    func getPromoCodeCartList(promoCodeStr: String){
        pharmacyflowInteractor?.getPromoCodeCartList(promoCodeStr: promoCodeStr)

    }
    
    func getReasons(param: Parameters) {
        pharmacyflowInteractor?.getReasons(param: param)
    }
    
    
    func cancelRequest(param: Parameters) {
        pharmacyflowInteractor?.cancelRequest(param: param)
    }

}

extension PharmacyflowPresenter: PharmacyflowInteractorToPharmacyflowPresenterProtocol {
    func getCancelError(response: Any) {
        pharmacyflowView?.getCancelError(response: response)
    }
    func postPromoCodeCartResponse(cartListEntity: PharmacyflowCartListEntity) {
        pharmacyflowView?.postPromoCodeCartResponse(cartListEntity: cartListEntity)
    }
    func getCancelRequest(cancelEntity: SuccessEntity) {
        pharmacyflowView?.getCancelRequest(cancelEntity: cancelEntity)
    }
    
    func getReasons(reasonEntity: ReasonEntity) {
        pharmacyflowView?.getReasons(reasonEntity: reasonEntity)
    }
    
    func pharmacyflowOrderStatusResponse(orderStatus: PharmacyflowOrderDetailEntity){
        pharmacyflowView?.pharmacyflowOrderStatusResponse(orderStatus: orderStatus)
    }
    
    
    func getUserRatingResponse(getUserRatingResponse: SuccessEntity) {
        pharmacyflowView?.getUserRatingResponse(getUserRatingResponse: getUserRatingResponse)
    }
    
    func getFilterRestaurantResponse(getFilterRestaurantResponse: StoreListEntity) {
        pharmacyflowView?.getFilterRestaurantResponse(getFilterRestaurantResponse: getFilterRestaurantResponse)
    }
    
   
    func searchRestaturantResponse(getSearchRestuarantResponse: SearchEntity) {
        pharmacyflowView?.searchRestaturantResponse(getSearchRestuarantResponse: getSearchRestuarantResponse)
    }
    
    func getPromoCodeResponse(getPromoCodeResponse: PromocodeEntity) {
        pharmacyflowView?.getPromoCodeResponse(getPromoCodeResponse: getPromoCodeResponse)
    }
    
    
    func getListOfStoresResponse(getStoreResponse: StoreListEntity) {
        pharmacyflowView?.getListOfStoresResponse(getStoreResponse: getStoreResponse)
    }
    
    func getStoresDetailResponse(pharmacyflowDetailEntity: PharmacyflowDetailEntity) {
        pharmacyflowView?.getStoresDetailResponse(pharmacyflowDetailEntity: pharmacyflowDetailEntity)
    }
    
    func postAddToCartResponse(addCartEntity: PharmacyflowCartListEntity) {
         pharmacyflowView?.postAddToCartResponse(addCartEntity: addCartEntity)
    }
  
    func cusineListResponse(getCusineListResponse: CusineListEntity){
        pharmacyflowView?.cusineListResponse(getCusineListResponse: getCusineListResponse)
    }
 
    func postRemoveCartResponse(cartListEntity: PharmacyflowCartListEntity) {
        pharmacyflowView?.postRemoveCartResponse(cartListEntity: cartListEntity)
    }
    
    func getCartListResponse(cartListEntity: PharmacyflowCartListEntity) {
        pharmacyflowView?.getCartListResponse(cartListEntity: cartListEntity)
    }
    
    func postOrderCheckoutResponse(checkoutEntity: PharmacyflowCheckoutEntity) {
        pharmacyflowView?.postOrderCheckoutResponse(checkoutEntity: checkoutEntity)
    }
    
    func getSavedAddressResponse(addressList: SavedAddressEntity) {
        pharmacyflowView?.getSavedAddressResponse(addressList: addressList)
    }
    
}
