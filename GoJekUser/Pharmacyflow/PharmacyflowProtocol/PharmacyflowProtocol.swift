//
//  PharmacyflowProtocol.swift
//  GoJekUser
//
//  Created by apple on 20/02/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit
import Alamofire

var pharmacyflowPresenterObject: PharmacyflowViewToPharmacyflowPresenterProtocol?

//MARK:- Pharmacyflow presenter Pharmacyflow view protocol

protocol PharmacyflowPresenterToPharmacyflowViewProtocol: class {
    
    func getListOfStoresResponse(getStoreResponse: StoreListEntity)
    func getStoresDetailResponse(pharmacyflowDetailEntity: PharmacyflowDetailEntity)
    func postAddToCartResponse(addCartEntity: PharmacyflowCartListEntity)
    func getCartListResponse(cartListEntity: PharmacyflowCartListEntity)
    func postRemoveCartResponse(cartListEntity: PharmacyflowCartListEntity)
    func postOrderCheckoutResponse(checkoutEntity: PharmacyflowCheckoutEntity)
    func getPromoCodeResponse(getPromoCodeResponse: PromocodeEntity)
    func searchRestaturantResponse(getSearchRestuarantResponse: SearchEntity)
    func cusineListResponse(getCusineListResponse: CusineListEntity)
    func getFilterRestaurantResponse(getFilterRestaurantResponse: StoreListEntity)
    func getUserRatingResponse(getUserRatingResponse: SuccessEntity)
    func getSavedAddressResponse(addressList: SavedAddressEntity)
    func pharmacyflowOrderStatusResponse(orderStatus: PharmacyflowOrderDetailEntity)
    func postPromoCodeCartResponse(cartListEntity: PharmacyflowCartListEntity)
    func getReasons(reasonEntity: ReasonEntity)
    func getCancelRequest(cancelEntity: SuccessEntity)
    func getCancelError(response: Any)


}

extension PharmacyflowPresenterToPharmacyflowViewProtocol {
    
    var pharmacyflowPresenter: PharmacyflowViewToPharmacyflowPresenterProtocol? {
        get {
            pharmacyflowPresenterObject?.pharmacyflowView = self
            return pharmacyflowPresenterObject
        }
        set(newValue) {
            pharmacyflowPresenterObject = newValue
        }
    }
    
    func getListOfStoresResponse(getStoreResponse: StoreListEntity) { return }
    func getStoresDetailResponse(pharmacyflowDetailEntity: PharmacyflowDetailEntity) { return }
    func postAddToCartResponse(addCartEntity: PharmacyflowCartListEntity) { return }
    func postRemoveCartResponse(cartListEntity: PharmacyflowCartListEntity) { return }
    func getCartListResponse(cartListEntity: PharmacyflowCartListEntity) { return }
    func postOrderCheckoutResponse(checkoutEntity: PharmacyflowCheckoutEntity) { return }
    func getPromoCodeResponse(getPromoCodeResponse: PromocodeEntity) { return }
    func searchRestaturantResponse(getSearchRestuarantResponse: SearchEntity) { return }
    func cusineListResponse(getCusineListResponse: CusineListEntity) { return }
    func getFilterRestaurantResponse(getFilterRestaurantResponse: StoreListEntity) { return }
    func getUserRatingResponse(getUserRatingResponse: SuccessEntity) { return }
    func getSavedAddressResponse(addressList: SavedAddressEntity) { return }
     func pharmacyflowOrderStatusResponse(orderStatus: PharmacyflowOrderDetailEntity) { return }
    func postPromoCodeCartResponse(cartListEntity: PharmacyflowCartListEntity) { return }
    func getReasons(reasonEntity: ReasonEntity) { return }
    func getCancelRequest(cancelEntity: SuccessEntity) { return }
    func getCancelError(response: Any) { return }


}

//MARK:- Pharmacyflow Interactor to Pharmacyflow Presenter Protocol

protocol PharmacyflowInteractorToPharmacyflowPresenterProtocol: class {
    
    func getListOfStoresResponse(getStoreResponse: StoreListEntity)
    func getStoresDetailResponse(pharmacyflowDetailEntity: PharmacyflowDetailEntity)
    func postAddToCartResponse(addCartEntity: PharmacyflowCartListEntity)
    func getCartListResponse(cartListEntity: PharmacyflowCartListEntity)
    func postRemoveCartResponse(cartListEntity: PharmacyflowCartListEntity)
    func postOrderCheckoutResponse(checkoutEntity: PharmacyflowCheckoutEntity)
    func getPromoCodeResponse(getPromoCodeResponse: PromocodeEntity)
    func searchRestaturantResponse(getSearchRestuarantResponse: SearchEntity)
    func cusineListResponse(getCusineListResponse: CusineListEntity)
    func getFilterRestaurantResponse(getFilterRestaurantResponse: StoreListEntity)
    func getUserRatingResponse(getUserRatingResponse: SuccessEntity)
    func getSavedAddressResponse(addressList: SavedAddressEntity)
    func pharmacyflowOrderStatusResponse(orderStatus: PharmacyflowOrderDetailEntity)
    func postPromoCodeCartResponse(cartListEntity: PharmacyflowCartListEntity)
    func getReasons(reasonEntity: ReasonEntity)
    func getCancelRequest(cancelEntity: SuccessEntity)
    func getCancelError(response: Any)


}

//MARK:- Pharmacyflow Presenter to Pharmacyflow Interactor Protocol

protocol PharmacyflowPresenterToPharmacyflowInteractorProtocol: class {
    
    var pharmacyflowPresenter: PharmacyflowInteractorToPharmacyflowPresenterProtocol? { get set }
    
    func getListOfStores(Id: Int, param: Parameters)
    func getStoresDetail(with Id: Int, param: Parameters)
    func postAddToCart(param: Parameters)
    func getCartList(param: Parameters)
    func getCartList()
    func postRemoveCart(param: Parameters)
    func postOrderCheckout(param: Parameters)
    func getPromoCodeList(param: Parameters)
    func searchRestaurantList(id: Int,type: String,searchStr: String,param: Parameters)
    func getCusineList(Id: Int)
    func getFilterRestaurant(Id: Int,filter: String,qFilter: String,param: Parameters)
    func userRatingParam(param: Parameters)
    func getSavedAddress()
    func getOrderStatus(Id: Int)
    func getPromoCodeCartList(promoCodeStr: String)
    func getReasons(param: Parameters)
    func cancelRequest(param: Parameters)
    func uploadPrescription(params : Parameters,imagedata : [String : Data])

}

//MARK:- Pharmacyflow view to Pharmacyflow presenter protocol

protocol PharmacyflowViewToPharmacyflowPresenterProtocol: class {
    
    var pharmacyflowView: PharmacyflowPresenterToPharmacyflowViewProtocol? { get set}
    var pharmacyflowInteractor: PharmacyflowPresenterToPharmacyflowInteractorProtocol? { get set }
    var pharmacyflowRouter: PharmacyflowPresenterToPharmacyflowRouterProtocol? { get set }
    
    func getListOfStores(Id: Int, param: Parameters)
    func getStoresDetail(with Id: Int, param: Parameters)
    func postAddToCart(param: Parameters)
    func getCartList(param: Parameters)
    func getCartList()
    func postRemoveCart(param: Parameters)
    func postOrderCheckout(param: Parameters)
    func getPromoCodeList(param: Parameters)
    func searchRestaurantList(id: Int,type: String,searchStr: String,param: Parameters)
    func getCusineList(Id: Int)
    func getFilterRestaurant(Id: Int,filter: String,qFilter: String,param: Parameters)
    func userRatingParam(param: Parameters)
    func getSavedAddress()
    func getOrderStatus(Id: Int)
    func getPromoCodeCartList(promoCodeStr: String)
    func getReasons(param: Parameters)
    func cancelRequest(param: Parameters)
    func uploadPrescription(params : Parameters,imagedata : [String : Data])
}

//MARK:- Pharmacyflow Presenter to Pharmacyflow Router Protocol

protocol PharmacyflowPresenterToPharmacyflowRouterProtocol {
    static func createPharmacyflowModule() -> UIViewController    
}



