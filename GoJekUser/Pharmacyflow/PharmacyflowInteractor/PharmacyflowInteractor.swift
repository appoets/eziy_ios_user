//
//  PharmacyflowInteractor.swift
//  GoJekUser
//
//  Created by apple on 20/02/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit
import Alamofire

class PharmacyflowInteractor: PharmacyflowPresenterToPharmacyflowInteractorProtocol {
  
    var pharmacyflowPresenter: PharmacyflowInteractorToPharmacyflowPresenterProtocol?
    
    func getListOfStores(Id: Int, param: Parameters) {
        
        WebServices.shared.requestToApi(type: StoreListEntity.self, with: "\(PharmacyflowAPI.storeList)/\(Id)", urlMethod: .get, showLoader: true, params: param,encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.pharmacyflowPresenter?.getListOfStoresResponse(getStoreResponse: response)
            }
        }
    }
    
    func getStoresDetail(with Id: Int, param: Parameters) {
        
        WebServices.shared.requestToApi(type: PharmacyflowDetailEntity.self, with: "\(PharmacyflowAPI.storeDetail)\(Id)", urlMethod: .get, showLoader: true, params: param, encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.pharmacyflowPresenter?.getStoresDetailResponse(pharmacyflowDetailEntity: response)
            }
        }
    }
    
    func postAddToCart(param: Parameters) {
        
        WebServices.shared.requestToApi(type: PharmacyflowCartListEntity.self, with: PharmacyflowAPI.addCart, urlMethod: .post, showLoader: true,  params: param) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.pharmacyflowPresenter?.postAddToCartResponse(addCartEntity: response)
            }
        }
    }
    
    func getCartList(param: Parameters) {
        WebServices.shared.requestToApi(type: PharmacyflowCartListEntity.self, with: PharmacyflowAPI.cartList, urlMethod: .get, showLoader: true, params: param, encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.pharmacyflowPresenter?.getCartListResponse(cartListEntity: response)
            }
        }
    }
    
    func postRemoveCart(param: Parameters) {
        WebServices.shared.requestToApi(type: PharmacyflowCartListEntity.self, with: PharmacyflowAPI.removeCart, urlMethod: .post, showLoader: true, params: param) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.pharmacyflowPresenter?.postRemoveCartResponse(cartListEntity: response)
            }
        }
    }
    
    func postOrderCheckout(param: Parameters) {
        
        WebServices.shared.requestToApi(type: PharmacyflowCheckoutEntity.self, with: PharmacyflowAPI.orderCheckout, urlMethod: .post, showLoader: true,  params: param) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.pharmacyflowPresenter?.postOrderCheckoutResponse(checkoutEntity: response)
            }
        }
    }
    
    func getPromoCodeList(param: Parameters){
        WebServices.shared.requestToApi(type: PromocodeEntity.self, with: PharmacyflowAPI.promocode, urlMethod: .get, showLoader: true, params: param, encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.pharmacyflowPresenter?.getPromoCodeResponse(getPromoCodeResponse: response)
            }
        }
    }
    func searchRestaurantList(id: Int,type: String,searchStr: String,param: Parameters) {
        let url =  PharmacyflowAPI.foodieSearch + "/" + id.toString()
        let urlString = "?q=" + searchStr + "&t=" + type
        let WeburlString = url + urlString
        WebServices.shared.requestToApi(type: SearchEntity.self, with: WeburlString, urlMethod: .get, showLoader: true, params: param, encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.pharmacyflowPresenter?.searchRestaturantResponse(getSearchRestuarantResponse: response)
            }
        }
    }
    func getCusineList(Id: Int) {
        WebServices.shared.requestToApi(type: CusineListEntity.self, with: "\(PharmacyflowAPI.cusineList)/\(Id)", urlMethod: .get, showLoader: true, encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.pharmacyflowPresenter?.cusineListResponse(getCusineListResponse: response)
            }
        }
    }
    
  
    func getFilterRestaurant(Id: Int, filter: String, qFilter: String,param: Parameters) {
        let url =  PharmacyflowAPI.storeList + "/" + Id.toString()
            let urlString =  "?filter=" + filter + "&qfilter=" + qFilter
        let WeburlString = url + urlString
        WebServices.shared.requestToApi(type: StoreListEntity.self, with:WeburlString, urlMethod: .get, showLoader: true, params: param, encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.pharmacyflowPresenter?.getFilterRestaurantResponse(getFilterRestaurantResponse: response)
            }
        }
    }
    func userRatingParam(param: Parameters) {
        WebServices.shared.requestToApi(type: SuccessEntity.self, with: PharmacyflowAPI.rating, urlMethod: .post, showLoader: true, params: param) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.pharmacyflowPresenter?.getUserRatingResponse(getUserRatingResponse: response)
            }
        }
    }
    
    func getSavedAddress() {
        WebServices.shared.requestToApi(type: SavedAddressEntity.self, with: AccountAPI.getAddress, urlMethod: .get, showLoader: true, encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.pharmacyflowPresenter?.getSavedAddressResponse(addressList: response)
            }
        }
    }
    func getOrderStatus(Id: Int) {
        WebServices.shared.requestToApi(type: PharmacyflowOrderDetailEntity.self, with: "\(PharmacyflowAPI.orderDetail)/\(Id)", urlMethod: .get, showLoader: false, encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.pharmacyflowPresenter?.pharmacyflowOrderStatusResponse(orderStatus: response)
            }
        }
    }
    

    
    func getPromoCodeCartList(promoCodeStr: String) {
        let promoCodeCartUrl = PharmacyflowAPI.cartList + "?&promocode_id=" + promoCodeStr + "&wallet=0"
        WebServices.shared.requestToApi(type: PharmacyflowCartListEntity.self, with: promoCodeCartUrl, urlMethod: .get, showLoader: true, encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.pharmacyflowPresenter?.postPromoCodeCartResponse(cartListEntity: response)
            }
        }
    }
    
    func getReasons(param: Parameters) {
        WebServices.shared.requestToApi(type: ReasonEntity.self, with: HomeAPI.reason, urlMethod: .get, showLoader: true, params: param, encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.pharmacyflowPresenter?.getReasons(reasonEntity: response)
            }
        }
    }
    
    func cancelRequest(param: Parameters) {
        WebServices.shared.requestToApi(type: SuccessEntity.self, with: PharmacyflowAPI.cancelRequest, urlMethod: .post, showLoader: true, params: param) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.pharmacyflowPresenter?.getCancelRequest(cancelEntity: response)
            }
            else if response?.error != nil {
                self.pharmacyflowPresenter?.getCancelError(response: response as Any)
            }
        }
    }
    func getCartList(){
        WebServices.shared.requestToApi(type: PharmacyflowCartListEntity.self, with: PharmacyflowAPI.cartList, urlMethod: .get, showLoader: true, params: nil, encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.pharmacyflowPresenter?.getCartListResponse(cartListEntity: response)
            }
        }
    }
    
    func uploadPrescription(params : Parameters,imagedata : [String : Data]){
        WebServices.shared.requestToImageUpload(type:  PharmacyflowCartListEntity.self, with: PharmacyflowAPI.addCart, imageData: imagedata, showLoader: true,params:params) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.pharmacyflowPresenter?.getCartListResponse(cartListEntity: response)
            }
        }
    }
    
}

