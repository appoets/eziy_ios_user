//
//  PharmacyflowRouter.swift
//  GoJekUser
//
//  Created by apple on 20/02/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit

class PharmacyflowRouter: PharmacyflowPresenterToPharmacyflowRouterProtocol {
    
    static func createPharmacyflowModule() -> UIViewController {
        let pharmacyflowDetailViewController  = pharmacyflowStoryboard.instantiateViewController(withIdentifier: PharmacyflowConstant.PharmacyflowHomeViewController) as! PharmacyflowHomeViewController
        let pharmacyflowPresenter: PharmacyflowViewToPharmacyflowPresenterProtocol & PharmacyflowInteractorToPharmacyflowPresenterProtocol = PharmacyflowPresenter()
        let pharmacyflowInteractor: PharmacyflowPresenterToPharmacyflowInteractorProtocol = PharmacyflowInteractor()
        let pharmacyflowRouter: PharmacyflowPresenterToPharmacyflowRouterProtocol = PharmacyflowRouter()
        pharmacyflowDetailViewController.pharmacyflowPresenter = pharmacyflowPresenter
        pharmacyflowPresenter.pharmacyflowView = pharmacyflowDetailViewController
        pharmacyflowPresenter.pharmacyflowRouter = pharmacyflowRouter
        pharmacyflowPresenter.pharmacyflowInteractor = pharmacyflowInteractor
        pharmacyflowInteractor.pharmacyflowPresenter = pharmacyflowPresenter
        return pharmacyflowDetailViewController
    }
    
    static func createPharmacyflowOrderStatusModule(isHome: Bool?, orderId: Int?,isChat: Bool?,isFromOrder: Bool) -> UIViewController {
           
           let pharmacyflowOrderStatusViewController  = pharmacyflowStoryboard.instantiateViewController(withIdentifier: PharmacyflowConstant.PharmacyflowOrderStatusViewController) as! PharmacyflowOrderStatusViewController
           let pharmacyflowPresenter: PharmacyflowViewToPharmacyflowPresenterProtocol & PharmacyflowInteractorToPharmacyflowPresenterProtocol = PharmacyflowPresenter()
           let pharmacyflowInteractor: PharmacyflowPresenterToPharmacyflowInteractorProtocol = PharmacyflowInteractor()
           let pharmacyflowRouter: PharmacyflowPresenterToPharmacyflowRouterProtocol = PharmacyflowRouter()
           pharmacyflowOrderStatusViewController.pharmacyflowPresenter = pharmacyflowPresenter
           pharmacyflowPresenter.pharmacyflowView = pharmacyflowOrderStatusViewController
           pharmacyflowPresenter.pharmacyflowRouter = pharmacyflowRouter
           pharmacyflowPresenter.pharmacyflowInteractor = pharmacyflowInteractor
           pharmacyflowInteractor.pharmacyflowPresenter = pharmacyflowPresenter
            pharmacyflowOrderStatusViewController.isFromOrderPage = isFromOrder
           pharmacyflowOrderStatusViewController.isHome = isHome ?? false
           orderRequestId = orderId ?? 0
           pharmacyflowOrderStatusViewController.OrderfromChatNotification = isChat ?? false
           return pharmacyflowOrderStatusViewController
       }
    
    static var pharmacyflowStoryboard: UIStoryboard {
        return UIStoryboard(name:"Pharmacyflow",bundle: Bundle.main)
    }
}



