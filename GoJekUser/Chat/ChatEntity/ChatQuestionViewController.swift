//
//  ChatQuestionViewController.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 31/01/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import ObjectMapper

@available(iOS 13.0, *)
class ChatQuestionViewController: UIViewController {
    
    
    @IBOutlet weak var suggestionList : UITableView!
    @IBOutlet weak var chatwithDotTitleLbl : UILabel!
    @IBOutlet weak var chooseSpecialityLbl : UILabel!
    @IBOutlet weak var symptomsLbl : UILabel!
    @IBOutlet weak var symptomsTxt : UITextView!
    @IBOutlet weak var submitBtn : UIButton!
    
    var category : [Category] = [Category]()
    var AllAategory : [Category] = [Category]()
    var selectedindex : Int  = 0
    private lazy var loader  : UIView = {
        return createActivityIndicator(self.view)
    }()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initailSetup()
    }
    
    
    func initailSetup(){
        self.symptomsTxt.delegate = self
        self.setupNavigation()
        self.setupAction()
        self.setupTableViewCell()
        self.setupFont()
//        self.getCatagoryList()
        self.localize()
    }
    
    func setupNavigation(){
        self.navigationController?.isNavigationBarHidden = false
        self.title = Constants.string.chat.localize()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "AppBlueColor")
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setupFont(){
    }
    
    private func localize(){
        self.chatwithDotTitleLbl.text = Constants.string.chatwithDoctor.localize()
        self.symptomsLbl.text = Constants.string.symptomsTitle.localize()
        self.chooseSpecialityLbl.text = Constants.string.chooseSpeciality.localize()
        self.submitBtn.setTitle(Constants.string.continueText.localize(), for: .normal)
    }
    
    
    func setupAction(){
        self.submitBtn.addTap {
            let vc = SummaryViewController.initVC(storyBoardName: .doctor, vc: SummaryViewController.self, viewConrollerID: Storyboard.Ids.SummaryViewController)
            vc.selectedCategory = self.category[self.selectedindex]
            vc.offerPrice = self.category[self.selectedindex].offer_fees ?? ""
            vc.price = "\(self.category[self.selectedindex].fees ?? 0)"
            vc.message = self.symptomsTxt.text ?? ""
            self.push(from: self, ToViewContorller: vc)
        }
    }
    
}

@available(iOS 13.0, *)
extension ChatQuestionViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  (self.category.count ) + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.SuggestedCell) as! SuggestedCell
        if indexPath.row ==  self.category.count {
            cell.allSplistView.isHidden = false
        }else{
            cell.allSplistView.isHidden = true
        }
        
        if selectedindex == indexPath.row{
            cell.suggetionView.borderColor = UIColor.AppBlueColor
            cell.titleLbl.textColor = UIColor.AppBlueColor
            cell.offerPriceLbl.textColor = UIColor.AppBlueColor
            cell.priceLbl.textColor = UIColor.AppBlueColor
            cell.offerPriceView.backgroundColor = UIColor.AppBlueColor
            cell.suggetionView.setCorneredElevation(shadow: 1, corner: 10, color: UIColor.AppBlueColor)
            cell.allSplistView.setCorneredElevation(shadow: 0, corner: 10, color: UIColor(named: "GreyTextcolor")!)

        }else{
            
            cell.suggetionView.setCorneredElevation(shadow: 1, corner: 10, color: UIColor(named: "GreyTextcolor")!)
            cell.allSplistView.setCorneredElevation(shadow: 0, corner: 10, color: UIColor(named: "GreyTextcolor")!)

            cell.suggetionView.borderColor = UIColor(named: "GreyTextcolor")! //UIColor.black
            cell.titleLbl.textColor = UIColor(named: "GreyTextcolor")!//UIColor.black
            cell.offerPriceLbl.textColor = UIColor(named: "GreyTextcolor")! //UIColor.black
            cell.priceLbl.textColor = UIColor(named: "GreyTextcolor")! //UIColor.black
            cell.offerPriceView.backgroundColor = UIColor(named: "GreyTextcolor")! //UIColor.black
        }
        if indexPath.row <= (self.category.count )-1{
            let data : Category = self.category[indexPath.row]
            cell.titleLbl.text = data.name ?? ""
            cell.offerPriceLbl.text = "$ \(data.fees ?? 0)"
            cell.priceLbl.text =  "$" + " " + (data.offer_fees ?? "")
        }
        
        return cell
            }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row ==  self.category.count {
            let vc = SelectedProblemAreaVC.initVC(storyBoardName: .doctor, vc: SelectedProblemAreaVC.self, viewConrollerID: Storyboard.Ids.SelectedProblemAreaVC)
            vc.category = self.AllAategory
            
            vc.selectedCategory = {(category,index) in
              
                if index >= 2{
                    self.category.remove(at: 2)
                    self.selectedindex = 2
                    self.category.append(category)
                }else{
//                    self.category.remove(at: index)
                    self.selectedindex = index
                    
                }
               
                
                self.suggestionList.reloadData()
            }
            
            self.present(vc, animated: true)
        }else{
            self.selectedindex = indexPath.row
            self.suggestionList.reloadData()
        }
    }
    
    func setupTableViewCell(){
        self.suggestionList.registerCell(withId: XIB.Names.SuggestedCell)
        
    }
    
}


//Api calls
@available(iOS 13.0, *)
extension ChatQuestionViewController : DoctorPresenterToDoctorViewProtocol{
    func showSuccess(api: String, dataArray: [Mappable]?, dataDict: Mappable?, modelClass: Any) {
        switch String(describing: modelClass) {
            case model.type.CategoryList:
                self.loader.isHideInMainThread(true)
                let data = dataDict as? CategoryList
                if (data?.category?.count ?? 0) > 3{
                    self.category.append((data?.category?[0]) ?? Category())
                    self.category.append((data?.category?[1]) ?? Category())
                    self.category.append((data?.category?[2]) ?? Category())
                }else{
                    self.category = data?.category ?? [Category]()
                }
                self.AllAategory = data?.category ?? [Category]()
                self.suggestionList.reloadInMainThread()
                break

            default: break

        }
    }

//    func showError(error: CustomError) {
//
//    }

//    func getCatagoryList(){
//        self.presenter?.HITAPI(api: DoctoryAPI.catagoryList.rawValue, params: nil, methodType: .GET, modelClass: CategoryList.self, token: true)
//        self.loader.isHidden = false
//
//    }
//
//    func cancelAppointment(id : String){
//        self.presenter?.HITAPI(api: DoctoryAPI.cancelAppointment.rawValue, params: ["id" : id], methodType: .POST, modelClass: CommonModel.self, token: true)
//        self.loader.isHidden = false
//    }
}




@available(iOS 13.0, *)
extension ChatQuestionViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
    }
    
}


struct demoSuggestion {
    var title : String
    var original : String
    var offer : String
    
}
