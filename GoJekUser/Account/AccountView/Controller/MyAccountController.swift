//
//  MyAccountController.swift
//  GoJekUser
//
//  Created by Ansar on 22/02/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit
import SDWebImage

class MyAccountController: UIViewController {
    
//    @IBOutlet weak var accountCollectionView: UICollectionView!
    @IBOutlet weak var editimage: UIImageView!
    @IBOutlet weak var MenuTableView: UITableView!
    @IBOutlet weak var profileimage: UIImageView!
    @IBOutlet weak var shopname: UILabel!
    @IBOutlet weak var gmaillabel: UILabel!
    let actionSheet = AppActionSheet.shared
    var isAppPresentTapOnPush:Bool = false
    var userData = UserProfileEntity()
    
    var accountImageArr = [
//        AccountConstant.icprofile
                           Constant.ic_location_pin,
                           Constant.payment,
                           Constant.walletSmall,
                           AccountConstant.icprivacyPolicy,
                           AccountConstant.icsupport,
                           AccountConstant.languageImage,
                           AccountConstant.logoutImage]
    
    var accountNameArr = [
//        AccountConstant.profile.localized,
                          AccountConstant.manageAddress.localized,
                          AccountConstant.payment.localized,
                          AccountConstant.wallet.localized,
                          AccountConstant.privacyPolicy.localized,
                          AccountConstant.support.localized,
                          AccountConstant.language.localized,
                          AccountConstant.logout.localized]
    
    
    var guestAccountImageArr = [
//        AccountConstant.icprofile,
                                AccountConstant.icprivacyPolicy,
                                AccountConstant.languageImage]
    
    var guestAccountNameArr = [
        AccountConstant.signin.localized,
                               AccountConstant.privacyPolicy.localized,
                               AccountConstant.language.localized]
    
    private var selectedLanguage: Language = .english {
        didSet {
            LocalizeManager.share.setLocalization(language: selectedLanguage)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialLoads()
//        setvalues()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(editbuttontap(tapGestureRecognizer:)))
            editimage.isUserInteractionEnabled = true
        editimage.addGestureRecognizer(tapGestureRecognizer)
        
        self.profileimage.sd_setImage(with: URL(string: AppManager.shared.getUserDetails()?.picture ?? ""), placeholderImage: UIImage(named: Constant.userPlaceholderImage),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
            // Perform operation.
            if (error != nil) {
                // Failed to load image
                self.profileimage.image = UIImage(named: Constant.userPlaceholderImage)
            } else {
                // Successful in loading image
                self.profileimage.image = image
            }
        })
        
        self.shopname.text = "\(AppManager.shared.getUserDetails()?.first_name ?? "") \(AppManager.shared.getUserDetails()?.last_name ?? "")"
        self.gmaillabel.text =  AppManager.shared.getUserDetails()?.email ?? ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showTabBar()
        self.navigationController?.navigationBar.topItem?.title = AccountConstant.account.localized
//        navigationController?.navigationBar.isTranslucent = false
            let titleLabel = UILabel(frame: CGRect(x: 0, y: 20, width: 350, height: 20))
            titleLabel.text = AccountConstant.account.localized
            titleLabel.textColor = UIColor.blackColor
            titleLabel.textAlignment = .left
            titleLabel.font = UIFont.systemFont(ofSize: 20)
            navigationItem.titleView = titleLabel
//        self.title = titleLabel
//            let lbNavTitle = UILabel (frame: CGRect(x: 0, y: 0, width: 320, height: 40))
//            lbNavTitle.backgroundColor = UIColor.red
//            lbNavTitle.textColor = UIColor.black
//            lbNavTitle.numberOfLines = 0
//            lbNavTitle.center = CGPoint(x: 0, y: 0)
//            lbNavTitle.textAlignment = .left
//            lbNavTitle.text = AccountConstant.account.localized
//        self.title = AccountConstant.account.localized
//        self.navigationController?.navigationBar.topItem?.title = lbNavTitle
//        editimage.addTarget(self, action: #selector(editbuttontap), for: .touchUpInside)
//        setupLanguage()
//        self.accountCollectionView.reloadData()
        self.MenuTableView.reloadData()
        // Chat Notification
      //  NotificationCenter.default.addObserver(self, selector: #selector(isTransportChatRedirection), name: Notification.Name(rawValue: pushNotificationType.chat_transport.rawValue), object: nil)
    }
    
    
    private func setDarkMode(){
        self.view.backgroundColor = .backgroundColor
//        self.accountCollectionView.backgroundColor = .backgroundColor
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    
    @objc func editbuttontap(tapGestureRecognizer: UITapGestureRecognizer)
    {
        self.push(id: AccountConstant.MyProfileController)

    }
//    func setvalues(){
//        self.gmaillabel.text =
//        self.profileimage.sd_setImage(with: URL(string: userData.picture ?? ""), placeholderImage: UIImage(named: Constant.userPlaceholderImage), completed: { (image, error, cacheType, imageURL) in
//            // Perform operation.
//            if (error != nil) {
//                // Failed to load image
//                self.profileimage.image = UIImage(named: Constant.userPlaceholderImage)
//            } else {
//                // Successful in loading image
//                self.profileimage.image = image
//            }
//        })
//    }
}

//MARK: - Methods

extension MyAccountController {
    
    private func initialLoads() {
        setNavigationTitle()
        self.view.backgroundColor = .veryLightGray
        setDarkMode()
//        self.accountCollectionView.register(nibName: AccountConstant.AccountCollectionViewCell)
        self.MenuTableView.register(nibName: AccountConstant.accounttableviewcell)
        let baseConfig = AppConfigurationManager.shared.baseConfigModel
        if !isGuestAccount {
            if (baseConfig?.responseData?.appsetting?.referral ?? 0) == 1 {
                accountImageArr.insert(AccountConstant.invitereff, at: 4)
                accountNameArr.insert("Coupons", at: 4)
            }
        }
    }
    
//    private func setupLanguage() {
//
//        if let languageStr = UserDefaults.standard.value(forKey: AccountConstant.language) as? String, let language = Language(rawValue: languageStr) {
//            LocalizeManager.share.setLocalization(language: language)
//            selectedLanguage = language
//        }else {
//            LocalizeManager.share.setLocalization(language: .english)
//        }
//
//        if !isGuestAccount {
//
//        if self.selectedLanguage == .arabic {
//            let rightBarButton = UIBarButtonItem(image: UIImage(named: AccountConstant.logoutImage)?.imageFlippedForRightToLeftLayoutDirection(), style: .plain, target: self, action: #selector(tapMore))
//            self.navigationItem.rightBarButtonItem  = rightBarButton
//        }else {
//            let rightBarButton = UIBarButtonItem(image: UIImage(named: AccountConstant.logoutImage), style: .plain, target: self, action: #selector(tapMore))
//            self.navigationItem.rightBarButtonItem  = rightBarButton
//        }
//        self.navigationItem.rightBarButtonItem?.tintColor = .blackColor
//        }
//        //self.accountCollectionView.reloadInMainThread()
//    }

    private func push(id: String)  {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: id)
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @objc private func tapMore() {
        actionSheet.showActionSheet(viewController: self,message: AccountConstant.logoutMsg.localized, buttonOne: AccountConstant.logout.localized)
        actionSheet.onTapAction = { [weak self] tag in
            guard let self = self else {
                return
            }
            if isGuestAccount {
                self.logOutProcess()
            }else {
                self.accountPresenter?.toLogout()
            }
        }
    }
    
    private func pushPrivacyPolicy() {
        let vc = WebViewController()
        if let privacyUrl = AppConfigurationManager.shared.baseConfigModel.responseData?.appsetting?.cmspage?.privacypolicy {
            vc.urlString = privacyUrl
        }
        vc.navTitle = AccountConstant.privacyPolicy.localized
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func logOutProcess() {
        AppConfigurationManager.shared.baseConfigModel = nil
         AppManager.shared.accessToken = ""
        PersistentManager.shared.delete(entityName: CoreDataEntity.loginData.rawValue)
        PersistentManager.shared.delete(entityName: CoreDataEntity.userData.rawValue)
        BackGroundRequestManager.share.stopBackGroundRequest()
        let walkThrough = LoginRouter.loginStoryboard.instantiateViewController(withIdentifier: LoginConstant.SplashViewController)
        CommonFunction.changeRootController(controller: walkThrough)
    }
}


//MARK: - Collectionview delegate

//extension MyAccountController:UICollectionViewDelegate {
//
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let baseConfig = AppConfigurationManager.shared.baseConfigModel
//        var isReferalEnable = false
//        if isGuestAccount {
//            isReferalEnable = false
//        }else{
//            isReferalEnable = (baseConfig?.responseData?.appsetting?.referral ?? 0) == 1
//        }
//        if isGuestAccount {
//            switch indexPath.row {
//            case 0:
//                logOutProcess()
//            case 1:
//                pushPrivacyPolicy()
//            case 2:
//                self.push(id: AccountConstant.LanguageController)
//            default:
//                break
//            }
//        }
//        switch indexPath.row {
//        case 0:
//            self.push(id: AccountConstant.MyProfileController)
//        case 1:
//            self.push(id: AccountConstant.ManageAddressController)
//        case 2:
//            self.push(id: AccountConstant.PaymentSelectViewController)
//        case 3:
//            self.push(id: AccountConstant.PaymentController)
//        case 4:
//            if isReferalEnable {
//                self.push(id: AccountConstant.InviteController)
//            }else {
//                pushPrivacyPolicy()
//            }
//        case 5:
//            if isReferalEnable {
//                pushPrivacyPolicy()
//            }else {
//                self.push(id: AccountConstant.SupportController)
//            }
//        case 6:
//            if isReferalEnable {
//                self.push(id: AccountConstant.SupportController)
//            }else {
//                self.push(id: AccountConstant.LanguageController)
//            }
//        case 7:
//            self.push(id: AccountConstant.LanguageController)
//        default:
//            break
//        }
//    }
//}

//MARK: - Collectionview datasource

//extension MyAccountController:UICollectionViewDataSource {
//
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 1
//    }
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        if isGuestAccount{
//            return guestAccountNameArr.count
//        }
//        return accountNameArr.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell:AccountCollectionViewCell = self.accountCollectionView.dequeueReusableCell(withReuseIdentifier: AccountConstant.AccountCollectionViewCell, for: indexPath) as! AccountCollectionViewCell
//        if isGuestAccount {
//            cell.setValues(name: guestAccountNameArr[indexPath.item].localized, imageString: guestAccountImageArr[indexPath.item])
//        }
//        else {
//            cell.setValues(name: accountNameArr[indexPath.item].localized, imageString: accountImageArr[indexPath.item])
//        }
//        cell.layoutIfNeeded()
//        return cell
//    }
//}

//extension MyAccountController: UICollectionViewDelegateFlowLayout {
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
//        let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
//        let width:CGFloat = (self.accountCollectionView.frame.size.width - space) / 2.0
//        let height:CGFloat = (self.accountCollectionView.frame.size.width - space) / 2.5
//
//        return CGSize(width: width, height: height)
//    }
//}

//MARK: API

extension MyAccountController: AccountPresenterToAccountViewProtocol {
    func getLogoutSuccess(logoutEntity: LogoutEntity) {
        self.logOutProcess()
//        self.gmaillabel.text = self.email
    }
    
}


/*
//MARK: CHAT NOTIFICATION
extension MyAccountController {
@objc func isTransportChatRedirection() {
           if isAppPresentTapOnPush == false {
               isAppPresentTapOnPush = true
               print("idPush")
               let taxiHomeViewController = TaxiRouter.createTaxiModuleChat(rideTypeId: 0)
               navigationController?.pushViewController(taxiHomeViewController, animated: true)
          }
}
}
*/
extension MyAccountController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return accountNameArr.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:accounttableviewcell = self.MenuTableView.dequeueReusableCell(withIdentifier: AccountConstant.accounttableviewcell, for: indexPath) as! accounttableviewcell
        cell.titleLabel.text = self.accountNameArr[indexPath.row]
        cell.categoryImageView.image = UIImage(named: accountImageArr[indexPath.row])
//               if isGuestAccount {
//                   cell.setValues(name: guestAccountNameArr[indexPath.item].localized, imageString: guestAccountImageArr[indexPath.item])
//               }
//               else {
//                   cell.setValues(name: accountNameArr[indexPath.item].localized, imageString: accountImageArr[indexPath.item])
//               }
//               cell.layoutIfNeeded()
               return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            self.push(id: AccountConstant.ManageAddressController)
        }
        else if indexPath.row == 1{
            self.push(id: AccountConstant.PaymentSelectViewController)
        }
        else if indexPath.row == 2{
            self.push(id: AccountConstant.PaymentController)
        }
        else if indexPath.row == 3{
            pushPrivacyPolicy()
        }
        else if indexPath.row == 4{
            self.push(id: AccountConstant.InviteController)
        }
        else if indexPath.row == 5{
            self.push(id: AccountConstant.SupportController)
        }
        else if indexPath.row == 6{
            self.push(id: AccountConstant.LanguageController)
        }
        else if indexPath.row == 7{
            logOutProcess()
        }
        
//        let baseConfig = AppConfigurationManager.shared.baseConfigModel
//                var isReferalEnable = false
//                if isGuestAccount {
//                    isReferalEnable = false
//                }else{
//                    isReferalEnable = (baseConfig?.responseData?.appsetting?.referral ?? 0) == 1
//                }
//                if isGuestAccount {
//                    switch indexPath.row {
//                    case 0:
//                        logOutProcess()
//                    case 1:
//                        pushPrivacyPolicy()
//                    case 2:
//                        self.push(id: AccountConstant.LanguageController)
//                    default:
//                        break
//                    }
//                }
//                switch indexPath.row {
////                case 0:
////                    self.push(id: AccountConstant.MyProfileController)
//                case 0:
//                    self.push(id: AccountConstant.ManageAddressController)
//                case 1:
//                    self.push(id: AccountConstant.PaymentSelectViewController)
//                case 2:
//                    self.push(id: AccountConstant.PaymentController)
//                case 3:
//                        self.push(id: AccountConstant.InviteController)
//                case 4:
//                        self.push(id: AccountConstant.SupportController)
//                case 5:
//                        self.push(id: AccountConstant.LanguageController)
//                case 6:
//                    self.push(id: AccountConstant.LanguageController)
//                case 7:
//                    logOutProcess()
//                default:
//                    break
//                }
            }
    
        
     
        }
