//
//  FoodieScheduleTimeView.swift
//  GoJekUser
//
//  Created by Thiru on 01/03/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit
        
class FoodieScheduleTimeView: UIView {

    //Outlets
             
    @IBOutlet weak var scheduleBackView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateValueLabel: UILabel!
    @IBOutlet weak var timeValueLabel: UILabel!
    @IBOutlet weak var scheduleButton: UIButton!
    @IBOutlet weak var calendarImageView: UIImageView!
    
    @IBOutlet weak var closeImg: UIImageView!
    @IBOutlet weak var timeImageViw: UIImageView!
    var onClickClose:(()->Void)?
    var onClickSchedule:((String,String)->Void)?
    var selectedDate:String = ""
    //LifeCycles
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initialLoad()
    }

}
extension FoodieScheduleTimeView {
    
    func initialLoad() {
        
        self.scheduleButton.addTarget(self, action: #selector(tapSchedule), for: .touchUpInside)
        self.closeImg.addTap {
            self.onClickClose!()
        }
        DispatchQueue.main.async {
//            self.scheduleBackView.roundCorners(corners: [.layerMinXMinYCorner,.layerMaxXMinYCorner], radius: 20)
//            self.scheduleButton.setCornerRadiuswithValue(value: 5)
            self.timeView.setCornerRadiuswithValue(value: 5)
            self.dateView.setCornerRadiuswithValue(value: 5)
            
            
        }
       setColors()
       localize()
        setFont()
        self.dateView.addTap {
            PickerManager.shared.showDatePicker(selectedDate: "", minDate: Date()) { [weak self] (selectedDate) in
                guard let self = self else {
                    return
                }
                self.dateValueLabel.text = selectedDate
                self.selectedDate = selectedDate
            }
        }
        
        self.timeView.addTap {
            let date = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let mystring = dateFormatter.string(from: date)
            var minTime:Date!
            if mystring == self.selectedDate {
                minTime = Date().adding(minutes: 20)
            }
            
            PickerManager.shared.showTimePicker(selectedDate: "",minDate: minTime) { [weak self] (selectedTime) in
                guard let self = self else {
                    return
                }
                self.timeValueLabel.text = selectedTime
            }
        }
    }
    private func setColors() {
    self.calendarImageView.image = UIImage(named: FoodieConstant.ic_calendar)?.imageTintColor(color1: .red)
    self.timeImageViw.image = UIImage(named: FoodieConstant.ic_clock)?.imageTintColor(color1: .red)
    }
    private func localize() {
        
        titleLabel.text = FoodieConstant.Tscheduledateandtime.localized
        dateLabel.text = FoodieConstant.TDate.localized
        timeLabel.text = FoodieConstant.TTime.localized
        scheduleButton.setTitle(FoodieConstant.TSchedule.localized, for: .normal)
        
        
    }
    
    private func setFont(){
        titleLabel.font = UIFont.setCustomFont(name: .bold, size: .x16)
        dateLabel.font = UIFont.setCustomFont(name: .medium, size: .x14)
        timeLabel.font = UIFont.setCustomFont(name: .medium, size: .x14)
        dateValueLabel.font = UIFont.setCustomFont(name: .bold, size: .x16)
        timeValueLabel.font = UIFont.setCustomFont(name: .bold, size: .x16)
        scheduleButton.titleLabel?.font = UIFont.setCustomFont(name: .bold, size: .x16)
        scheduleButton.setTitleColor(.white, for: .normal)
        scheduleButton.backgroundColor = .foodieColor
        scheduleButton.layer.cornerRadius = 20
        scheduleButton.addShadow(radius:  20, color: .darkGray)
        dateLabel.textColor = .foodieColor
        timeLabel.textColor = .foodieColor
        titleLabel.textColor = .foodieColor
    }
    
    @objc func tapSchedule() {
        if (self.dateValueLabel.text ?? "") != "Date" && (self.timeValueLabel.text ?? "") != "Time"{
            self.onClickSchedule!(self.dateValueLabel.text ?? "",self.timeValueLabel.text ?? "")
        }else{
            ToastManager.show(title:"Please Select Date and Time", state: .error)
        }
    }
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.onClickClose!()
//    }
}
