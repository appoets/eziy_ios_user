//
//  UXTableView.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 31/01/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import Foundation
import UIKit

extension UITableView{
   
    func registerCell(withId id : String){
        self.register(UINib(nibName: id, bundle: Bundle.main), forCellReuseIdentifier: id)
    }
}

extension UICollectionView{
   
    func registerCell(withId id : String){
        self.register(UINib(nibName: id, bundle: Bundle.main), forCellWithReuseIdentifier: id)
    }
}
