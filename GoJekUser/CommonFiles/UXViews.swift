//
//  UXViews.swift
//  Carvvo
//
//  Created by Shyamala's MacBook Pro on 05/05/21.
//

import Foundation


import Foundation
import UIKit



private var AssociatedObjectHandle: UInt8 = 25
private var ButtonAssociatedObjectHandle: UInt8 = 10
public enum closureActions : Int{
    case none = 0
    case tap = 1
    case swipe_left = 2
    case swipe_right = 3
    case swipe_down = 4
    case swipe_up = 5
}

public struct closure {
    typealias emptyCallback = ()->()
    static var actionDict = [Int:[closureActions : emptyCallback]]()
    static var btnActionDict = [Int:[String: emptyCallback]]()
}

extension UIView{
    
    
    func makeRoundedCorner(){
        
       // self.layer.masksToBounds = true
        self.layer.cornerRadius = self.bounds.width/2
        
    }
    
    
    var closureId:Int{
        get {
            let value = objc_getAssociatedObject(self, &AssociatedObjectHandle) as? Int ?? Int()
            return value
        }
        set {
            objc_setAssociatedObject(self, &AssociatedObjectHandle, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func addTap(Action action:@escaping ()->Void){
        self.actionHandleBlocks(.tap,action:action)
        let gesture = UITapGestureRecognizer()
        gesture.addTarget(self, action: #selector(triggerTapActionHandleBlocks))
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(gesture)
    }
    
    func actionHandleBlocks(_ type : closureActions = .none,action:(() -> Void)? = nil) {
        
        if type == .none{
            return
        }
        var actionDict : [closureActions : closure.emptyCallback]
        if self.closureId == Int(){
            self.closureId = closure.actionDict.count + 1
            closure.actionDict[self.closureId] = [:]
        }
        if action != nil {
            actionDict = closure.actionDict[self.closureId]!
            actionDict[type] = action
            closure.actionDict[self.closureId] = actionDict
        } else {
            let valueForId = closure.actionDict[self.closureId]
            if let exe = valueForId![type]{
                exe()
            }
        }
    }
    
    @objc func triggerTapActionHandleBlocks() {
        self.actionHandleBlocks(.tap)
    }
}


// MARK: - custom UI appearance

extension UIView{
    
    var identifiers : String{
          return "\(self)"
      }
    
    @objc func initView(view: UIView , vc : UIViewController) -> UIView{
          return self
      }
    
    @objc func deInitView(view: UIView , vc : UIViewController) -> UIView{
        removeFromSuperview()
        return self
    }
}


extension UIView{
    func setCorneredElevation(shadow With : Int = 2 , corner radius : Int = 20 , color : UIColor = UIColor.clear){
//        self.layer.masksToBounds = false
//        self.clipsToBounds  = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = 0.7
        self.layer.shadowOffset = .zero//CGSize(width: With, height: With)
        self.layer.shadowRadius = CGFloat(With)
        self.layer.cornerRadius = CGFloat(radius)
    }
    
    func blurViews(){
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurEffectView.alpha = 0.5
        self.addSubview(blurEffectView)
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func circleView(){
        self.layer.cornerRadius = self.layer.frame.width/2
        self.clipsToBounds = true
    }
    
    
    func setGradientLayer(colorsuperTop : UIColor ,colorTop : UIColor , colormiddle : UIColor , colorBottom : UIColor){
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect(x: -10, y: 0, width: self.frame.width + 10, height: self.frame.height + 70)
        gradientLayer.colors = [colorsuperTop.cgColor,colorTop.cgColor, colormiddle.cgColor, colorBottom.cgColor]
        self.layer.addSublayer(gradientLayer)
        self.backgroundColor = .clear
    }
    
    func setGradientLayer(colorTop : UIColor , colormiddle : UIColor , colorBottom : UIColor){
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect(x: -10, y: 0, width: self.frame.width + 10, height: self.frame.height + 70)
        gradientLayer.colors = [colorTop.cgColor, colormiddle.cgColor, colorBottom.cgColor]
        self.layer.addSublayer(gradientLayer)
        self.backgroundColor = .clear
    }
    
    
    
    
    func addDashedBorder(color : UIColor = .red) {
        let color = color.cgColor

       let shapeLayer:CAShapeLayer = CAShapeLayer()
       let frameSize = self.frame.size
       let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)

       shapeLayer.bounds = shapeRect
       shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
       shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
       shapeLayer.lineWidth = 1
       shapeLayer.lineJoin = CAShapeLayerLineJoin.round
       shapeLayer.lineDashPattern = [6,3]
       shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath

       self.layer.addSublayer(shapeLayer)
       }
    
    
    
    
}

extension UIView {
    func createDottedLine(width: CGFloat, color: CGColor) {
        let lineLayer = CAShapeLayer()
        lineLayer.strokeColor = color
        lineLayer.lineWidth = width
        lineLayer.lineDashPattern = [2,3]
        let path = CGMutablePath()
        path.addLines(between: [CGPoint(x: self.frame.width / 2, y: 0),
                                CGPoint(x: self.frame.width / 2, y: self.frame.height)])
        lineLayer.path = path
        self.layer.addSublayer(lineLayer)
    }
}


//Mark:- Animation
extension UIView{
    
    
    func showView(){
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
            self.alpha = 0.8
            var height = self.frame.height;
            self.frame = CGRect(x: Int(self.frame.minX), y: Int(self.superview?.frame.height ?? 0), width: Int(self.frame.maxX), height: Int(height))
        }) { _ in
            self.superview?.addSubview(self)
        }
    }

    func hiddeShowView(ishide : Bool){
        self.alpha = CGFloat(ishide ? 1 : 0)
        UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions.showHideTransitionViews, animations: { () -> Void in
            self.alpha = CGFloat(!ishide ? 1 : 0)
            self.isHidden = ishide
        }, completion: { (Bool) -> Void in  }
        )
    }
    
    
    func dottedBorder(){
        var yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = UIColor.black.cgColor
        yourViewBorder.lineDashPattern = [4, 4]
        yourViewBorder.frame = self.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: self.bounds).cgPath
        self.layer.addSublayer(yourViewBorder)
    }
}

extension UIView {
    
    //Set view corner radius with given value
    func setCornerRadiuswithValues(value: CGFloat) {
        self.maskToBounds = true
        self.layer.cornerRadius = value
    }
    
    
    //MARK:- Mask To Bounds
    
    @IBInspectable
    var maskToBoundss : Bool {
        get {
            return self.layer.masksToBounds
        }
        set(newValue) {
            
            self.layer.masksToBounds = newValue
            
        }
    }

}
extension UITextField{
    
    var getText: String{
        return self.text ?? ""
    }
    
    func setInputViewDatePicker(mode : UIDatePicker.Mode = .date ,target: Any, selector: Selector, minimumDate : Date) {
        // Create a UIDatePicker object and assign to inputView
        let screenWidth = UIScreen.main.bounds.width
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        datePicker.datePickerMode = mode //2
        datePicker.minimumDate = minimumDate
        self.inputView = datePicker //3
        
        // Create a toolbar and assign it to inputAccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) //4
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil) //5
        let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: nil, action: #selector(tapCancel)) // 6
        let barButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector) //7
        toolBar.setItems([cancel, flexible, barButton], animated: false) //8
        self.inputAccessoryView = toolBar //9
    }
    
    @objc func tapCancel() {
        self.resignFirstResponder()
    }
    
    
}
extension UILabel{
    var getText: String{
        return self.text ?? ""
    }
}


