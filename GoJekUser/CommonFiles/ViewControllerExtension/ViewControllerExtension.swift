//
//  ViewControllerExtension.swift
//  GoJekUser
//
//  Created by Ansar on 25/02/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import Foundation
import UIKit
import AVKit

private var imageCompletion : ((UIImage?)->())?

extension UIViewController {
    
    //MARK:- Show Image Selection Action Sheet
    
    func showImage(isRemoveNeed: String? = nil,with completion : @escaping ((UIImage?)->())){  //isRemoveNeed - used to remove photo in profile

        AppActionSheet.shared.showActionSheet(viewController: self,message: Constant.choosePicture.localized, buttonOne: Constant.SopenCamera.localized, buttonTwo: Constant.SopenGalley.localized,buttonThird: isRemoveNeed == nil ? nil : Constant.removePhoto.localized)
        imageCompletion = completion
        AppActionSheet.shared.onTapAction = { [weak self] tag in
            guard let self = self else {
                return
            }
            if tag == 0 {
                self.checkCameraPermission(source: .camera)
            }else if tag == 1 {
                self.checkCameraPermission(source: .photoLibrary)
            }else {
                let imageView = UIImageView()
                imageView.image = UIImage(named: Constant.userPlaceholderImage)
                imageCompletion?(imageView.image)
            }
        }
    }
    
    private func checkCameraPermission(source : UIImagePickerController.SourceType) {
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
        switch (cameraAuthorizationStatus){
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { granted in
                DispatchQueue.main.async {
                    if granted {
                        self.chooseImage(with: source)
                    }else {
                        ToastManager.show(title: Constant.cameraPermission.localized, state: .warning)
                    }
                }
            }
        case .restricted, .denied:
            ToastManager.show(title: Constant.cameraPermission.localized, state: .warning)
        case .authorized:
            self.chooseImage(with: source)
        default:
            ToastManager.show(title: Constant.cameraPermission.localized, state: .warning)
        }
    }
    
    func guestLogin ()-> Bool {
        if isGuestAccount{
            let walkthrough = LoginRouter.loginStoryboard.instantiateViewController(withIdentifier: LoginConstant.WalkThroughController)
            CommonFunction.changeRootController(controller: walkthrough)
            return true
        }
        return true
    }
    
    
    
    // MARK:- Show Image Picker
    
    private func chooseImage(with source : UIImagePickerController.SourceType){
        
        if UIImagePickerController.isSourceTypeAvailable(source) {
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = source
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    // MARK: - Hide KeyBoard
    func hideKeyboardWhenTappedAround()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    public func hideTabBar() {
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.layer.zPosition = -1

    }
    
    public func showTabBar() {
        
        if let languageStr = UserDefaults.standard.value(forKey: AccountConstant.language) as? String, let language = Language(rawValue: languageStr) {
            LocalizeManager.share.setLocalization(language: language)
        }
        else {
            LocalizeManager.share.setLocalization(language: .english)
        }
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.layer.zPosition = 0
        guard let items = self.tabBarController?.tabBar.items else { return }
        
        items[0].title = HomeConstant.THome.localized
        items[1].title = OrderConstant.history.localized
        items[2].title = NotificationConstant.TNotification.localized
        items[3].title = AccountConstant.account.localized
        
        if CommonFunction.checkisRTL() {
            
            self.tabBarController?.tabBar.semanticContentAttribute = .forceRightToLeft
            
        }else {
            self.tabBarController?.tabBar.semanticContentAttribute = .forceLeftToRight
        }
        
    }
    
    public func setNavigationTitle(colors : UIColor = .black) {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: colors, NSAttributedString.Key.font: UIFont.setCustomFont(name: .bold, size: .x20)]
    }
    
    //Left navigation button
    func setLeftBarButtonWith(color leftButtonImageColor: UIColor, leftButtonImage: String? = nil) {
        if CommonFunction.checkisRTL() {
            let leftBarButton = UIBarButtonItem.init(image: UIImage.init(named: Constant.ic_back)?.imageFlippedForRightToLeftLayoutDirection(), style: .plain, target: self, action: #selector(leftBarButtonAction))
            self.navigationController?.navigationBar.tintColor = leftButtonImageColor
            self.navigationItem.leftBarButtonItem = leftBarButton
        }else {
            let leftBarButton = UIBarButtonItem.init(image: UIImage.init(named: Constant.ic_back), style: .plain, target: self, action: #selector(leftBarButtonAction))
            self.navigationController?.navigationBar.tintColor = leftButtonImageColor
            self.navigationItem.leftBarButtonItem = leftBarButton
        }
    }
    
    //Left navigation bar button action
    @objc func leftBarButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
}


//MARK:- UIImagePickerControllerDelegate

extension UIViewController: UIImagePickerControllerDelegate {
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true) {
            if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
                imageCompletion?(image)
            }
        }
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}

//MARK:- UINavigationControllerDelegate

extension UIViewController: UINavigationControllerDelegate {
    
}

fileprivate var bottomConstraint : NSLayoutConstraint?
fileprivate var constraintValue : CGFloat = 0

extension UIViewController {
    
    
    
    
    //MARK:- Pop or dismiss View Controller
    
    func popOrDismiss(animation : Bool){
        
        DispatchQueue.main.async {
            
            if self.navigationController != nil {
                
                self.navigationController?.popViewController(animated: animation)
            } else {
                
                self.dismiss(animated: animation, completion: nil)
            }
            
        }
        
    }
    
    //MARK:- Present
    
    func present(id : String, animation : Bool){
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: id){
            vc.modalPresentationStyle = .custom
            self.present(vc, animated: animation, completion: nil)
            
        }
        
    }
    
    
    static func getStoryBoard(withName name : storyboardName) -> UIStoryboard{
        return UIStoryboard.init(name: name.rawValue, bundle: Bundle.main)
    }
    
    static func initVC<T : UIViewController>(storyBoardName name : storyboardName , vc : T.Type , viewConrollerID id : String) -> T{
        return getStoryBoard(withName: name).instantiateViewController(withIdentifier: id) as! T
    }
    
    
    //MARK:- Push
    
    func push(id : String, animation : Bool){
        
       if let vc = self.storyboard?.instantiateViewController(withIdentifier: id){
            self.navigationController?.pushViewController(vc, animated: animation)
        }
     
    }
    
    func push<T : UIViewController>(from vc : T ,ToViewContorller contoller : UIViewController ){
        vc.navigationController?.pushViewController(contoller, animated: true)
    }
    
    //MARK:- Push To Right
    
    func pushRight(toViewController viewController : UIViewController){
        
        self.makePush(transition: CATransitionSubtype.fromLeft.rawValue)
        navigationController?.pushViewController(viewController, animated: false)
        
    }
    
    private func makePush(transition type : String){
        
        let transition = CATransition()
        transition.duration = 0.45
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.default)
        transition.type = CATransitionType.push
  //      transition.subtype = type
        //transition.delegate = self
        navigationController?.view.layer.add(transition, forKey: nil)
        //navigationController?.isNavigationBarHidden = false
        
    }
  
    
   
    //MARK:- Keyboard will show
    
    @IBAction private func keyboardWillShow(info : NSNotification){
        
        guard let keyboard = (info.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else{
            return
        }
        bottomConstraint?.constant = -(keyboard.height)
        self.view.layoutIfNeeded()
    }
    
    
    //MARK:- Keyboard will hide
    
    @IBAction private func keyboardWillHide(info : NSNotification){
        
        bottomConstraint?.constant = constraintValue
        self.view.layoutIfNeeded()
        
    }
    
    
    //MARK:- Back Button Action
    
    @IBAction func backButtonClick() {
        
        self.popOrDismiss(animation: true)
        
    }
    
    
    
    //MARK:- Show Image Selection Action Sheet
    
    func showImage(with completion : @escaping ((UIImage?)->())){
        
        let alert = UIAlertController(title: Constants.string.selectSource, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: Constants.string.camera, style: .default, handler: { (Void) in
            self.chooseImage(with: .camera)
        }))
        alert.addAction(UIAlertAction(title: Constants.string.photoLibrary, style: .default, handler: { (Void) in
            self.chooseImage(with: .photoLibrary)
        }))
        alert.addAction(UIAlertAction(title: Constants.string.Cancel, style: .cancel, handler:nil))
        alert.view.tintColor = .doctorColor
        imageCompletion = completion
        self.present(alert, animated: true, completion: nil)
        
    }
    
    

    
    /*  //MARK:- Right Bar Button Action
     
     @IBAction private func rightBarButtonAction(){
     
     let alertRightBar = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
     
     alertRightBar.addAction(UIAlertAction(title: Constants.string.newGroup, style: .default, handler: { (Void) in
     
     }))
     
     alertRightBar.addAction(UIAlertAction(title: Constants.string.newBroadcast, style: .default, handler: { (Void) in
     
     }))
     
     alertRightBar.addAction(UIAlertAction(title: Constants.string.starredMessages, style: .default, handler: { (Void) in
     
     }))
     
     alertRightBar.addAction(UIAlertAction(title: Constants.string.settings, style: .default, handler: { (Void) in
     
     self.pushRight(toViewController: self.storyboard!.instantiateViewController(withIdentifier: Storyboard.Ids.SettingViewController))
     
     }))
     
     alertRightBar.addAction(UIAlertAction(title: Constants.string.Cancel, style: .cancel, handler: { (Void) in
     
     }))
     
     alertRightBar.view.tintColor = .primary
     
     self.present(alertRightBar, animated: true, completion: nil)
     
     }  */
    
    
    //MARK:- Show Search Bar with self delegation
    
    @IBAction private func showSearchBar(){
        
        let searchBar = UISearchController(searchResultsController: nil)
        searchBar.searchBar.delegate = self as? UISearchBarDelegate
        searchBar.hidesNavigationBarDuringPresentation = false
        searchBar.dimsBackgroundDuringPresentation = false
        searchBar.searchBar.tintColor = .doctorColor
        self.present(searchBar, animated: true, completion: nil)
        
    }
    
    
    
}
public enum storyboardName : String{
    case main = "Main"
    case user = "User"
    case doctor = "doctor"
    
}

// MARK:- Storyboard Id
struct Storyboard {
    
    static let Ids = Storyboard()
    let DrawerController = "DrawerController"
    let HomeViewController = "HomeViewController"
    let SigninInViewController = "SigninInViewController"
    let NameAndEmailViewController = "NameAndEmailViewController"
    let EmailViewController = "EmailViewController"
    let GenderConfirmationVC = "GenderConfirmationVC"
    let DateOfBirthViewController = "DateOfBirthViewController"
    let AppointmentViewController = "AppointmentViewController"
    let AppointmentListViewController = "AppointmentListViewController"
    let SideBarTableViewController = "SideBarTableViewController"
    let LaunchNavigationController = "LaunchNavigationController"
    let UpcomingDetailsController = "UpcomingDetailsController"
    let OnlineAvailabeDoctorsController = "OnlineAvailabeDoctorsController"
    let CategoryListController = "CategoryListController"
    let DoctorHomeVC = "DoctorHomeVC"
    let DoctorsListController = "DoctorsListController"
    let DoctorDetailsController = "DoctorDetailsController"
    let VisitedDoctorsViewController = "VisitedDoctorsViewController"
    let AppointmentDetailsViewController = "AppointmentDetailsViewController"
    let ThankYouViewController = "ThankYouViewController"
    let ChatTableViewController = "ChatTableViewController"
    let SearchViewController = "SearchViewController"
    let FavouriteDoctorsListController = "FavouriteDoctorsListController"
    let MedicalRecordsViewController = "MedicalRecordsViewController"
    let DetailMedicalRecordVc = "DetailMedicalRecordVc"
    let WalletViewController = "WalletViewController"
    let HealthFeedViewController = "HealthFeedViewController"
    let HealthFeedDetailsViewController = "HealthFeedDetailsViewController"
    let ReminderViewController = "ReminderViewController"
    let ReminderDetailViewController = "ReminderDetailViewController"
    let ServiceListViewController = "ServiceListViewController"
    let ChatQuestionViewController = "ChatQuestionViewController"
    let SelectedProblemAreaVC = "SelectedProblemAreaVC"
    let SummaryViewController = "SummaryViewController"
    let ChatViewController = "ChatViewController"
    let AlertVC = "AlertVC"
    let BookingViewController = "BookingViewController"
    let PatientDetailViewController = "PatientDetailViewController"
    let ProfileViewController = "ProfileViewController"
    let RelativeManagementViewController = "RelativeManagementViewController"
    let RelativeDetailViewController = "RelativeDetailViewController"
    let SettingsViewController = "SettingsViewController"
    let FAQViewController = "FAQViewController"
    let FilterViewController = "FilterViewController"
    let AddCardViewController = "AddCardViewController"
    let CardsListViewController = "CardsListViewController"
    let AddMedicalRecordViewController = "AddMedicalRecordViewController"
    let PatientRecordsViewController = "PatientRecordsViewController"
    let ShowRecordViewController = "ShowRecordViewController"
    let PaymentSelectViewController = "PaymentSelectViewController"
}


//MARK:- XIB Cell Names
    
struct XIB {
    
    static let Names = XIB()
    let DoctorsListCell = "DoctorsListCell"
    let LogoCell = "LogoCell"
    let SideBarCell = "SideBarCell"
    let UpcomingTableviewCell = "UpcomingTableviewCell"
    let OnlineDoctorCell = "OnlineDoctorCell"
    let ArticlesCell = "ArticlesCell"
    let ReminderCell = "ReminderCell"
    let CategoryCell = "CategoryCell"
    let DoctorCell = "DoctorCell"
    let VisitedDoctorsCell = "VisitedDoctorsCell"
    let SuggestedSpecialityCell = "SuggestedSpecialityCell"
    let ChatCommentCell = "ChatCommentCell"
    let ServiceSpecializationCell = "ServiceSpecializationCell"
    let PhotosCell = "PhotosCell"
    let ReviewCell = "ReviewCell"
    let SearchCell = "SearchCell"
    let FavDoctorTableViewCell = "FavDoctorTableViewCell"
    let HealthFeedTableViewCell = "HealthFeedTableViewCell"
    let SuggestedCell = "SuggestedCell"
    let ProblemCell = "ProblemCell"
    let ChatRightCell = "ChatRightCell"
    let ChatLeftCell = "ChatLeftCell"
    let RelativesTableViewCell = "RelativesTableViewCell"
    let AddAllergyTableViewCell = "AddAllergyTableViewCell"
    let CardsTableViewCell = "CardsTableViewCell"
    let InvoiceView = "InvoiceView"
    let PaymentModeTableViewCell = "PaymentModeTableViewCell"
    let ConsultedOnTableViewCell = "ConsultedOnTableViewCell"
    
    
}


//MARK:- Notification

extension Notification.Name {
   //public static let reachabilityChanged = Notification.Name("reachabilityChanged")
}



