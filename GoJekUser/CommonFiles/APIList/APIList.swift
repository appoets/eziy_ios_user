//
//  APIList.swift
//  GoJekUser
//
//  Created by Ansar on 21/03/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import Foundation
import UIKit

enum HttpType : String{
    
    case GET = "GET"
    case POST = "POST"
    case PATCH = "PATCH"
    case PUT = "PUT"
    case DELETE = "DELETE"
    
}

// Status Code

enum StatusCode : Int {
    
    case notreachable = 0
    case success = 200
    case multipleResponse = 300
    case unAuthorized = 401
    case notFound = 404
    case ServerError = 500
    
}

struct LoginAPI {
    static let signIn = "/user/login"
    static let countries = "/user/countries"
    static let signUp = "/user/signup"
    static let forgotPassword = "/user/forgot/otp"
    static let socialLogin = "/user/social/login"
    static let verify = "/user/verify"
    static let sendOtp = "/user/send-otp"
    static let verifyOtp = "/user/verify-otp"
}

struct TaxiAPI {
    static let serviceList = "/user/transport/services"
    static let estimateFare = "/user/transport/estimate"
    static let sendRequest = "/user/transport/send/request"
    static let checkRequest = "/user/transport/check/request"
    static let cancelRequest = "/user/transport/cancel/request"
    static let invoicePayment = "/user/transport/payment"
    static let rateProvider = "/user/transport/rate"
    static let updatePayment = "/user/transport/update/payment"
    static let extendTrip = "/user/transport/extend/trip"
}


struct CourierAPI
{
     static let serviceList = "/user/delivery/services"
     static let estimateFare = "/user/delivery/estimate"
     static let checkRequest = "/user/delivery/check/request"
     static let sendRequest = "/user/delivery/send/request"
     static let invoicePayment = "/user/delivery/payment"
     static let rateProvider = "/user/delivery/rate"
     static let packageList = "/user/delivery/package/types"
     static let deliveryTypeList = "/user/delivery/types/1"
    static let cancelRequest = "/user/delivery/cancel/request"

}

struct PaymentAPI {
    
    static let resetPassword = "/user/reset/otp"
    static let addCard = "/user/card"
}

struct PharmacyAPI{
    static let checkRequest = "/user/pharmacy/check/request"
}



struct DoctoryAPI {
    static let doctorServiceSubCategory = "/user/doctor_service_sub_category/"
    static let subcatorgylist = "/user/doctor/list"
    static let booking = "/user/doctor/book/appointment"
    static let countries = "/user/countries"
    static let signUp = "/user/signup"
    static let forgotPassword = "/user/forgot/otp"
    static let socialLogin = "/user/social/login"
    static let verify = "/user/verify"
    static let sendOtp = "/user/send-otp"
    static let verifyOtp = "/user/verify-otp"
    static let search = "/user/available/doctors"
    static let docDetail = "/user/available/doctors/byid"
    static let getvisitedDoctor = "/user/visited/doctors"
    static let articles = "/user/articles"
    static let getRelativesList = "/user/relatives"
    static let postRelativesList = "/user/relative/send/request"
    static let getMedicalRec = "/user/medical/records"
    static let getAppointmentList = "/user/video/appointments"
    static let cancelAppointmentss = "/user/appointment/cancel"
    static let slot = "/user/doctor/slot/check"
    static let cancelAppointment = "/user/cancel_appointment"
    static let visitorupdate = "/user/review/vistited/doctor"
    static let endCall = "/user/call/end/status"
    static let updateAppointment = "/user/appointments/update"
    static let reviews = "/user/review/vistited/doctor"
    

   
   
    
    
}
