//
//  AppointmentDetailsViewController.swift
//  GoJekUser
//
//  Created by Nivedha's MacBook Pro on 18/02/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire

class AppointmentDetailsViewController: UITableViewController {
    
    @IBOutlet weak var doctorImg: UIImageView!
    @IBOutlet weak var doctorName: UILabel!
    @IBOutlet weak var labelDesignation: UILabel!
    @IBOutlet weak var locationImg: UIImageView!
    @IBOutlet weak var labelHospitalName: UILabel!
    @IBOutlet weak var labelBookefor: UILabel!
    @IBOutlet weak var labelPatientName: UILabel!
    @IBOutlet weak var labelSchedule: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelCategory: UILabel!
    @IBOutlet weak var backbtn: UIButton!
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var labelStatusResponse: UILabel!
    @IBOutlet weak var labelShare: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var dislikeButton: UIButton!
    @IBOutlet weak var SubmitButton: UIButton!
    @IBOutlet weak var consultedText: UITextField!
    @IBOutlet weak var commentsText: UITextView!
    @IBOutlet weak var ratingView: FloatRatingView!
    

    var updatedVisitedDetail : VisitorupdateModel?
    var isFromVisited : Bool = false
    var likedStatus : String = ""
    var index : Int = 0
    var appoitmentID : Int = 0
    var firstname = ""
    var profileimage = ""
    var location = ""
    var experiences = ""
    var sheduledate = ""
    var requestid : Int?
    var doctorprofileid : Int?
    var providerid: Int?
   
   
    override func viewDidLoad() {
        super.viewDidLoad()
        initialLoads()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupData()
    }
    
}

//MARK: View Setups
extension AppointmentDetailsViewController {

    private func initialLoads() {
//        setupNavigationBar()
        setTextFonts()
        self.doctorImg.makeRoundedCorner()
        self.setupAction()
        self.ratingView.minRating = 1
        self.ratingView.maxRating = 5
        self.ratingView.rating = 3
        self.ratingView.tintColor = UIColor.systemYellow
        let tap = UITapGestureRecognizer(target: self, action: #selector(tappedMe))
        backbtn.addGestureRecognizer(tap)
        backbtn.isUserInteractionEnabled = true
        

    }
    @objc func tappedMe()
    {
        self.navigationController?.popViewController(animated: true)
    }


    private func setTextFonts() {

        Common.setFontWithType(to: labelBookefor, size: 16, type: .meduim)
        Common.setFontWithType(to: labelPatientName, size: 14, type: .meduim)
        Common.setFontWithType(to: labelSchedule, size: 16, type: .meduim)
        Common.setFontWithType(to: labelDate, size: 14, type: .meduim)
        Common.setFontWithType(to: labelCategory, size: 10, type: .light)
        Common.setFontWithType(to: labelStatus, size: 16, type: .meduim)
        Common.setFontWithType(to: labelStatusResponse, size: 14, type: .meduim)
        Common.setFontWithType(to: labelShare, size: 14, type: .regular)
        Common.setFontWithType(to: likeButton, size: 18, type: .regular)
        Common.setFontWithType(to: dislikeButton, size: 18, type: .regular)
        Common.setFontWithType(to: consultedText, size: 16, type: .regular)
        Common.setFontWithType(to: commentsText, size: 16, type: .regular)
        Common.setFontWithType(to: SubmitButton, size: 16, type: .meduim)

        self.likeButton.layer.cornerRadius = self.likeButton.frame.width / 2
        self.dislikeButton.layer.cornerRadius = self.dislikeButton.frame.width / 2
        self.likeButton.layer.borderColor = UIColor(named: "TextForegroundColor")?.cgColor
        self.dislikeButton.layer.borderColor = UIColor(named: "TextForegroundColor")?.cgColor
        self.likeButton.layer.borderWidth = 0.6
        self.dislikeButton.layer.borderWidth = 0.6
    }
    
    
    func setupData(){
        
        self.doctorName.text = self.firstname
        self.labelHospitalName.text = self.location
        let url = URL(string: self.profileimage)
                        let data = try? Data(contentsOf: url!)
        self.doctorImg.image = UIImage(data: data!)
        self.labelDesignation.text = experiences
        self.labelDate.text = self.sheduledate
        
                
        

    }
    func setupAction(){

        let likeImg = UIImage(named: "Like")
        let disLikeImg = UIImage(named: "dislike")
        let normalLikeImg = UIImage(named: "like")
        let normalDisLikeImg = UIImage(named: "dislike")

    
        self.likeButton.addTap {
            self.likeButton.setImage(likeImg, for: .normal)
            self.dislikeButton.setImage(normalDisLikeImg, for: .normal)
            self.likeButton.backgroundColor = UIColor.AppBlueColor
            self.dislikeButton.backgroundColor = UIColor.clear
            self.likedStatus = "LIKE"
            
        }

        self.dislikeButton.addTap {

            self.likeButton.setImage(normalLikeImg, for: .normal)
            self.dislikeButton.setImage(disLikeImg, for: .normal)
            self.likeButton.backgroundColor = UIColor.clear
            self.dislikeButton.backgroundColor = UIColor.AppBlueColor
            self.likedStatus = "DISLIKE"
        }
  
//
        self.SubmitButton.addTap {
            if self.validation(){
                let param:Parameters = [
                                        "description":self.commentsText.text ?? "",
                                        "like" : "\(self.ratingView.rating)"
                                        ]
                self.doctorPresenter?.visitorpost(param: param, id: self.requestid ?? 0, doctor_profile_id: self.doctorprofileid ?? 0, provider_id: self.providerid ?? 0)
            }
        }
    }

        func validation() -> Bool{
            if self.consultedText.text!.isEmpty{
                showToast(msg: "Enter consultant detail")
                return false
            }else if (self.commentsText.text ?? "").isEmpty{
                showToast(msg: "Please Enter Your Comments")
                return false
            }else if self.likedStatus.isEmpty{
                showToast(msg: "Please Select Like or DisLike option")
                return false
            }else{
                return true
            }
        }
    }

extension AppointmentDetailsViewController : DoctorPresenterToDoctorViewProtocol{
    func visitorpost(visitors: VisitorupdateModel) {
        self.updatedVisitedDetail = visitors
        ToastManager.show(title: "User Rated Successfully" , state: .success)
        self.navigationController?.popViewController(animated: true)
    }

}
